﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.IGTasks
{
    class RelatedAccountTask : IGTask
    {
        public override void DoTask()
        {
            RelatedAccount();
        }

        public List<string> ReturnTask()
        {
            return RelatedAccount();
        }

        IntPtr hwnd;
        string url;

        public RelatedAccountTask(IntPtr hwnd, string url)
        {
            this.hwnd = hwnd;
            this.url = url;
        }

        private List<string> RelatedAccount()
        {
            List<string> accounts = new List<string>();


            for (int retry = 0; retry < 3; retry++)
            {
                try
                {
                    PLAutoHelper.RPAFirefox.OpenUrlInCurrentTab(hwnd, url);
                    break;
                }
                catch { }
            }

            //check if account is removed
            if (PLAutoHelper.RPA.IsImageExisted(BMResource.RelatedAccount.RelatedAccount_NotFoundAccount, hwnd))
                return accounts;

            if (PLAutoHelper.RPA.IsImageExisted(BMResource.RelatedAccount.RelatedAccount_ShowRelateBtn, hwnd, 10))
            {
                PLAutoHelper.RPA.ClickByImageRandom(BMResource.RelatedAccount.RelatedAccount_ShowRelateBtn, hwnd);
                goto WaitRelatedACcount_Suggested;
            }
            else
            {
                if (PLAutoHelper.RPA.IsImageExisted(BMResource.RelatedAccount.RelatedAccount_ShowRelateBtn_White, hwnd, 2))
                {
                    PLAutoHelper.RPA.ClickByImageRandom(BMResource.RelatedAccount.RelatedAccount_ShowRelateBtn_White, hwnd);
                    goto WaitRelatedACcount_Suggested;
                }
            }

            return accounts;


        WaitRelatedACcount_Suggested:
            PLAutoHelper.RPA.WaitImage(BMResource.RelatedAccount.RelatedAccount_Suggested, hwnd);

            //click see all
            for (int i = 0; i <= 3; i++)
            {
                try
                {
                    PLAutoHelper.RPA.ClickByImageRandom(BMResource.RelatedAccount.RelatedAccount_SeeAll, hwnd);

                    PLAutoHelper.RPA.WaitImage(BMResource.RelatedAccount.RelatedAccount_SimilarAccountPopup, hwnd);
                }
                catch
                {
                    if (i == 3)
                        throw;
                }
            }


            int current_count = accounts.Count;

            while (true)
            {

                var html = PLAutoHelper.RPAFirefox.CopyHTML_ByHTMLTextWE(hwnd, "Similar Accounts");

                if (html == null)
                    break;

                var htmlDoc_load = new HtmlAgilityPack.HtmlDocument();
                htmlDoc_load.LoadHtml(html);

                var a_nodes = htmlDoc_load.DocumentNode.SelectNodes("//div[contains(@class,'_1XyCr')]//a");


                foreach (var item in a_nodes)
                {
                    accounts.Add("https://www.instagram.com" + item.Attributes["href"].Value);
                }

                accounts = accounts.Distinct().ToList();

                if (accounts.Count > current_count)
                {
                    current_count = accounts.Count;
                }
                else
                    break;

                //click scroll btn

                var point_follow_btn_tag = PLAutoHelper.RPA.FindOutPoint(BMResource.RelatedAccount.RelatedAccount_FollowBtn_Tag, hwnd, 5);

                KAutoHelper.AutoControl.SendClickOnPosition(hwnd, point_follow_btn_tag.Value.X, point_follow_btn_tag.Value.Y + BMResource.RelatedAccount.RelatedAccount_FollowBtn_Tag.Height + 10);

                PLAutoHelper.RPA.SendMouseScrollOnBackground(hwnd, "up", 4, new System.Drawing.Point(point_follow_btn_tag.Value.X - 10, point_follow_btn_tag.Value.Y));

                Thread.Sleep(500);

            }

            return accounts;

        }
    }
}
