﻿using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.IGTasks
{
    class QuickScrapeTask:IGTask
    {
        public override void DoTask()
        {
            //var driver = LaunchChrome("Profile1");

            //driver.Navigate().GoToUrl("https://en.wikipedia.org/wiki/Category:American_female_models");

            //var mw_category_groups = driver.FindElementsByXPath("(//div[@class='mw-content-ltr'])[1]//div[@class='mw-category-group']//a");

            //List<string> names = new List<string>();

            //foreach(var e in mw_category_groups)
            //{
            //    names.Add(e.Text);
            //}

            //var name_Str = "";

            //foreach(var n in names)
            //{
            //    name_Str += n + "\r\n";
            //}

            var names = File.ReadAllLines("names.txt",Encoding.UTF8).ToList();

            List<string> first_names = new List<string>();
            List<string> last_names = new List<string>();

            foreach(var n in names)
            {
                List<string> temps = n.Split(new char[] { ' ' }).ToList();

                if (temps.Count == 1)
                    continue;
                first_names.Add(temps[0]);

                last_names.Add(temps[temps.Count - 1]);
            }

            string choice_names = "";

            for(int i=0;i<1000;i++)
            {
                string f_name = first_names.OrderBy(x => Guid.NewGuid().GetHashCode()).ToList().First();

                string l_name = last_names.OrderBy(x => Guid.NewGuid().GetHashCode()).ToList().First();

                var fullname = f_name + " " + l_name;

                choice_names += fullname + "\r\n";
            }

            File.WriteAllText("fullnames.txt", choice_names);

        }

        private ChromeDriver LaunchChrome(string profile)
        {
            ChromeDriver driver = null;

            ChromeOptions options = new ChromeOptions();

            options.AddArgument(@"--user-data-dir=" + profile);
            options.AddArgument("--mute-audio");
            options.AddArgument("--start-minimized");

            //var proxy_str = API.DBHelpers.GetProxy();

            //var proxy = new Proxy();
            //proxy.Kind = ProxyKind.Manual;
            //proxy.IsAutoDetect = false;
            //proxy.HttpProxy =
            //proxy.SslProxy = proxy_str;
            //options.Proxy = proxy;
            //options.AddArgument("ignore-certificate-errors");

            ChromeDriverService chromeDriverService = ChromeDriverService.CreateDefaultService("driver\\" + profile.Replace("Profile", ""));
            chromeDriverService.HideCommandPromptWindow = true;

            driver = new ChromeDriver(chromeDriverService, options);


            return driver;
        }
    }
}
