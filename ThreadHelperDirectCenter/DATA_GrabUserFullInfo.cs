﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.ThreadHelperDirectCenter
{
    static class DATA_GrabUserFullInfo
    {
        static public dynamic RunByCenter(bool is_take_data)
        {
            switch (Settings.aio_settings.Project.ToString())
            {
                case "ww":
                    switch (Settings.aio_settings.GrabUserFullInfo.data.ToString())
                    {
                        case "default":
                            return DATA_GrabUserFullInfo_WW_Default(is_take_data);
                        default:
                            throw new System.ArgumentException("Not recognize this data type on " + Settings.aio_settings.Project + " project");
                    }
                default:
                    throw new System.ArgumentException("Not recognize this project");
            }
        }

        static private dynamic DATA_GrabUserFullInfo_WW_Default(bool is_take_data)
        {
            dynamic account = null;


            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                while (true)
                {
                    var command_query = @"SELECT top 1 [pk],[UserName],[detect_location]
                                    FROM scrapeusers
                                    WHERE ([state] is null or ([state] is not null and [state]=''))
                                      AND ([status] is null or [status] = '')
                                      AND 'https://www.instagram.com/' +[from_account]+'/' NOT IN
                                        (SELECT [ig_url]
                                         FROM target_accounts
                                         WHERE [state]='PAUSED')
                                      AND [detect_gender]='male'
									  AND 
											([detect_location] is not null and [detect_location]!='') AND 
								        'https://www.instagram.com/' +[from_account]+'/' IN
																	        (SELECT [ig_url]
																	         FROM target_accounts
																	         WHERE [state]='')
                                      --AND [detect_location] in      --tier1 country--
	                                      --('AU','AT','BE','CA','CO','HR','CZ','DK','FI','FR','GE','DE','IE','IL','IT','KR','LT','LU','NL','NZ','NO','PL','SK','SI','ES','SE','TW','GB','US')
                                    ORDER BY [first_added] ASC";

                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (var command = new SqlCommand(command_query, conn))
                        {

                            using (var dataReader = command.ExecuteReader())
                            {
                                if (dataReader.HasRows)
                                {
                                    dataReader.Read();
                                    var pk = dataReader["pk"].ToString();
                                    var username = dataReader["UserName"].ToString();
                                    var detect_location = dataReader["detect_location"].ToString();

                                    account = new
                                    {
                                        pk,
                                        username,
                                        detect_location
                                    };
                                }
                            }
                        }
                    }/*, "scrapeusers"*/);

                    if (account == null)
                        break;

                    //check if match condition about country

                    List<string> detect_locations = ((string)account.detect_location).Split(new char[] { ',' }).ToList();

                    List<string> accept_countries_code = new List<string>() { "AU", "AT", "BE", "CA", "CO", "HR", "CZ", "DK", "FI", "FR", "GE", "DE", "IE", "IL", "IT", "KR", "LT", "LU", "NL", "NZ", "NO", "PL", "SK", "SI", "ES", "SE", "TW", "GB", "US" };

                    if(detect_locations.Intersect(accept_countries_code).ToList().Count==0)
                    {
                        //update state to skip
                        PLAutoHelper.SQLHelper.Execute(() =>
                        {
                            using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                            {

                                command_query = "update scrapeusers set [state]='SKIP' where [UserName] = @user";

                                using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                {
                                    command.Parameters.Add("@user", System.Data.SqlDbType.VarChar).Value = account.username;
                                    var count = command.ExecuteNonQuery();

                                    sqlTransaction.Commit();
                                }
                            }
                        });

                        continue;
                    }



                    if (account != null && is_take_data)
                    {
                        PLAutoHelper.SQLHelper.Execute(() =>
                        {
                            using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                            {

                                command_query = "update scrapeusers set [status]='Claimed' where [UserName] = @user and ([status] is null or [status]='')";

                                using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                {
                                    command.Parameters.Add("@user", System.Data.SqlDbType.VarChar).Value = account.username;
                                    var count = command.ExecuteNonQuery();

                                    sqlTransaction.Commit();

                                    if (count != 1)
                                        account = null;
                                }
                            }
                        });

                        if (account != null)
                            break;
                    }
                    else
                        break;
                }
            }


            return account;
        }
    }
}
