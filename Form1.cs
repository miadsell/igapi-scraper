﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using InstagramApiSharp;
using InstagramApiSharp.API.Builder;
using InstagramApiSharp.Classes;
using InstagramApiSharp.Logger;
using OpenQA.Selenium.Chrome;

namespace IGAPI___Ramtinak
{
    public partial class form_igtoolkit : Form
    {
        public form_igtoolkit()
        {
            InitializeComponent();

            ReadAppSettings();

            CopyRequiredDLL();

            //this.Size = new Size(1100, 1000);

            API.DBHelpers.SettingUpWhenLoad();

            API.DBHelpers.AddSwitchPortable_FF();

            Settings.LoadSettings();

            IGThreads.ThreadHelper.ClearScrapeUserStatus();
        }


        string current_path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        private void ReadAppSettings()
        {
            var app_settings_json = File.ReadAllText(current_path + "\\app.ld");

            var dynamic = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(app_settings_json);

            string server_ip = dynamic["server_ip"];

            string igtoolkitdb_name = dynamic["igtoolkitdb_name"];
            string instagramdb_name = dynamic["instagramdb_name"];

            foreach (var item in dynamic["proxy_server_ips"])
            {
                Settings.proxy_server_ips.Add(item["ip"].ToString());
            }

            string project_name = dynamic["project"].ToString();

            //read aio settings from server

            string command_query =
                @"select *
                from ig_projects
                where
	                [project_name]=@project_name";

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.aio_ig_support))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@project_name", SqlDbType.VarChar).Value = project_name;
                    using (var dataReader = command.ExecuteReader())
                    {
                        dataReader.Read();
                        string aio_settings_str = dataReader["igtoolkit_aio_grab_setting"].ToString();
                        Settings.aio_settings = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(aio_settings_str);
                    }
                }
            }

            //Change setting

            API.DBHelpers.db_connection_igtoolkit = API.DBHelpers.db_connection_igtoolkit.Replace("@server_ip", server_ip).Replace("@igtoolkit", igtoolkitdb_name);
            API.DBHelpers.db_connection_instagram = API.DBHelpers.db_connection_instagram.Replace("@server_ip", server_ip).Replace("@instagramdb", instagramdb_name);

            API.DBHelpers.igtoolkit_dbname = igtoolkitdb_name;
            API.DBHelpers.instagram_dbname = instagramdb_name;

            //wait until sql server online

            while (true)
            {
                try
                {
                    using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                    {
                        conn.Open();
                    }
                    break;
                }
                catch
                {
                    Console.WriteLine("Sql server is disconnected! Please wait...");
                    Thread.Sleep(10000);
                }
            }
        }

        #region --Settings--

        private void CopyRequiredDLL()
        {
            string[] files = System.IO.Directory.GetFiles(current_path + "\\DLLSource");

            // Copy the files and overwrite destination files if they already exist.
            foreach (string s in files)
            {
                // Use static Path methods to extract only the file name from the path.
                var fileName = System.IO.Path.GetFileName(s);
                var destFile = System.IO.Path.Combine("", fileName);
                System.IO.File.Copy(s, destFile, true);
            }
        }

        #endregion


        #region --Datagridview target_accounts--

        public void ReloadManageDGV_TargetAccounts()
        {
            int current_index = dataGridView_target_accounts.FirstDisplayedScrollingRowIndex;

            if (current_index == -1)
                current_index = 0;

            List<dynamic> target_accounts = API.ManageDGVHelper.LoadAllTargetAccounts();

            //remove column 

            dataGridView_target_accounts.Columns.Clear();

            List<dynamic> filter_profiles = new List<dynamic>();

            if (!checkBox_target_accounts_not_checked.Checked && !checkBox_target_accounts_not_set_scrapetype.Checked && !checkBox_target_accounts_checked_OK.Checked && !checkBox_target_accounts_checked_not_OK.Checked)
            {
                filter_profiles.AddRange(target_accounts);
            }
            else
            {
                if (checkBox_target_accounts_not_checked.Checked)
                {
                    filter_profiles.AddRange(target_accounts.Where(s => s.ischecked != "Checked").ToList());
                }

                if (checkBox_target_accounts_not_set_scrapetype.Checked)
                {
                    filter_profiles.AddRange(target_accounts.Where(s => s.scrape_type == "" && s.ok == "OK").ToList());
                }

                if (checkBox_target_accounts_checked_OK.Checked)
                {
                    filter_profiles.AddRange(target_accounts.Where(s => s.ok == "OK").ToList());
                }

                if (checkBox_target_accounts_checked_not_OK.Checked)
                {
                    filter_profiles.AddRange(target_accounts.Where(s => s.ok.ToLower().Contains("not")).ToList());
                }
            }


            if (filter_profiles.Count == 0)
                return;

            //remove duplicate
            filter_profiles = filter_profiles.GroupBy(p => p.ig_url).Select(g => g.First()).ToList();

            //sort

            if (combobox_target_accounts_sort.Text != "")
            {
                switch (combobox_target_accounts_sort.Text)
                {
                    case "Added":
                        filter_profiles = filter_profiles.OrderByDescending(f =>f.added.ToString()).ToList();
                        break;
                    case "NoSort":
                        break;
                }
            }

            dataGridView_target_accounts.DataSource = filter_profiles;


            //setting for column
            dataGridView_target_accounts.Columns["ig_url"].Width = 280;

            dataGridView_target_accounts.Columns["follower"].Width = 60;

            dataGridView_target_accounts.Columns["following"].Width = 60;

            //add some additional column : OK|SKIP


            int index = 6;
            int columnIndex = 0;


            //combobox scrape type

            DataGridViewComboBoxColumn scrape_type_Combobox_Column = new DataGridViewComboBoxColumn();
            scrape_type_Combobox_Column.Name = "ScrapeType";
            scrape_type_Combobox_Column.HeaderText = "Scrape Type";
            scrape_type_Combobox_Column.Items.Add("Likers");
            scrape_type_Combobox_Column.Items.Add("Commenters");
            scrape_type_Combobox_Column.Items.Add("All");
            scrape_type_Combobox_Column.Items.Add("");

            columnIndex = index + 2;
            if (dataGridView_target_accounts.Columns["ScrapeType"] == null)
            {
                dataGridView_target_accounts.Columns.Insert(columnIndex, scrape_type_Combobox_Column);
            }

            foreach (DataGridViewRow row in dataGridView_target_accounts.Rows)
            {
                switch(row.Cells["scrape_type"].Value.ToString())
                {
                    case "Likers":
                        row.Cells["ScrapeType"].Value = "Likers";
                        break;
                    case "Commenters":
                        row.Cells["ScrapeType"].Value = "Commenters";
                        break;
                    case "All":
                        row.Cells["ScrapeType"].Value = "All";
                        break;
                    default:
                        row.Cells["ScrapeType"].Value = "";
                        break;
                }

                if (row.Cells["scrape_type"].Value.ToString() == "Checked")
                {
                    DataGridViewButtonCell add_cell = (DataGridViewButtonCell)row.Cells["Add"];
                    DataGridViewButtonCell skip_cell = (DataGridViewButtonCell)row.Cells["Skip"];

                    DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
                    dataGridViewCellStyle.Padding = new Padding(0, 0, 10000, 0);

                    add_cell.Style = dataGridViewCellStyle;
                    skip_cell.Style = dataGridViewCellStyle;
                }
            }

            //combobox state

            DataGridViewComboBoxColumn state_Combobox_Column = new DataGridViewComboBoxColumn();
            state_Combobox_Column.Name = "StateV";
            state_Combobox_Column.HeaderText = "StateV";
            state_Combobox_Column.Items.Add("");
            state_Combobox_Column.Items.Add("PAUSED");
            state_Combobox_Column.Items.Add("BAD");
            state_Combobox_Column.Items.Add("DISABLE");

            columnIndex = index + 1;
            if (dataGridView_target_accounts.Columns["StateV"] == null)
            {
                dataGridView_target_accounts.Columns.Insert(columnIndex, state_Combobox_Column);
            }

            foreach (DataGridViewRow row in dataGridView_target_accounts.Rows)
            {
                switch (row.Cells["state"].Value.ToString())
                {
                    case "":
                        row.Cells["StateV"].Value = "";
                        break;
                    case "PAUSED":
                        row.Cells["StateV"].Value = "PAUSED";
                        break;
                    case "BAD":
                        row.Cells["StateV"].Value = "BAD";
                        break;
                    default:
                        row.Cells["StateV"].Value = row.Cells["state"].Value.ToString();
                        break;
                }
            }

            //checkbox scrape comment

            DataGridViewCheckBoxColumn checkBoxColumn_scrape_follower = new DataGridViewCheckBoxColumn();
            checkBoxColumn_scrape_follower.Name = "ScrapeFollower";

            columnIndex = index + 3;
            if (dataGridView_target_accounts.Columns["ScrapeFollower"] == null)
            {
                dataGridView_target_accounts.Columns.Insert(columnIndex, checkBoxColumn_scrape_follower);
            }

            foreach (DataGridViewRow row in dataGridView_target_accounts.Rows)
            {
                switch (row.Cells["scrape_follower"].Value.ToString())
                {
                    case "yes":
                        row.Cells["ScrapeFollower"].Value = true;
                        break;
                    default:break;
                }
            }


            //addbutton

            DataGridViewButtonColumn addButtonColumn = new DataGridViewButtonColumn();
            addButtonColumn.Name = "Add";
            addButtonColumn.Text = "Add";
            addButtonColumn.UseColumnTextForButtonValue = true;

            columnIndex = index + 4;
            if (dataGridView_target_accounts.Columns["Add"] == null)
            {
                dataGridView_target_accounts.Columns.Insert(columnIndex, addButtonColumn);
            }

            DataGridViewButtonColumn skipButtonColumn = new DataGridViewButtonColumn();
            skipButtonColumn.Name = "Skip";
            skipButtonColumn.Text = "Skip";
            skipButtonColumn.UseColumnTextForButtonValue = true;

            columnIndex = index + 4;
            if (dataGridView_target_accounts.Columns["Skip"] == null)
            {
                dataGridView_target_accounts.Columns.Insert(columnIndex, skipButtonColumn);
            }



            //Hide some columns

            dataGridView_target_accounts.Columns["id"].Visible = false;
            dataGridView_target_accounts.Columns["scrape_related"].Visible = false;
            dataGridView_target_accounts.Columns["scrape_follower"].Visible = false;
            dataGridView_target_accounts.Columns["scrape_type"].Visible = false;
            dataGridView_target_accounts.Columns["state"].Visible = false;


            //setting for column
            dataGridView_target_accounts.Columns["ig_url"].Width = 200;
            dataGridView_target_accounts.Columns["ok"].Width = 50;
            dataGridView_target_accounts.Columns["ischecked"].Width = 55;
            dataGridView_target_accounts.Columns["Add"].Width = 60;
            dataGridView_target_accounts.Columns["Skip"].Width = 60;

            dataGridView_target_accounts.Invoke((MethodInvoker)delegate
            {
                dataGridView_target_accounts.FirstDisplayedScrollingRowIndex = current_index;
            });
        }


        private void checkBox_target_accounts_not_checked_CheckedChanged(object sender, EventArgs e)
        {
            ReloadManageDGV_TargetAccounts();
        }


        private void checkBox_target_accounts_not_set_scrapetype_CheckedChanged(object sender, EventArgs e)
        {
            ReloadManageDGV_TargetAccounts();
        }


        private void checkBox_target_accounts_checked_OK_CheckedChanged(object sender, EventArgs e)
        {
            ReloadManageDGV_TargetAccounts();
        }


        private void checkBox_target_accounts_checked_not_OK_CheckedChanged(object sender, EventArgs e)
        {
            ReloadManageDGV_TargetAccounts();
        }

        private void combobox_target_accounts_sort_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReloadManageDGV_TargetAccounts();
        }

        private void btn_import_target_accounts_Click(object sender, EventArgs e)
        {

            IGForms.Import import = new IGForms.Import("accounts");
            import.ShowDialog();
        }

        #endregion

        #region --Datagridview source_users--

        public void ReloadManageDGV_SourceUsers()
        {

            List<dynamic> source_users = API.ManageDGVHelper.LoadAllSourceUsers();


            //remove column 

            dataGridView_source_users.Columns.Clear();

            List<dynamic> filter_profiles = new List<dynamic>();


            if (checkBox_source_users_not_checked.Checked)
            {
                filter_profiles.AddRange(source_users.Where(s => s.ischecked != "Checked").ToList());
            }
            else
                filter_profiles.AddRange(source_users);


            if (filter_profiles.Count == 0)
                return;

            dataGridView_source_users.DataSource = filter_profiles;

            //setting for column
            dataGridView_source_users.Columns["ig_url"].Width = 280;

            dataGridView_source_users.Columns["follower"].Width = 60;

            dataGridView_source_users.Columns["following"].Width = 60;


            //add some additional column : OK|SKIP


            int index = 6;
            int columnIndex = 0;

            DataGridViewButtonColumn addButtonColumn = new DataGridViewButtonColumn();
            addButtonColumn.Name = "Add";
            addButtonColumn.Text = "Add";
            addButtonColumn.UseColumnTextForButtonValue = true;

            columnIndex = index + 1;
            if (dataGridView_source_users.Columns["Add"] == null)
            {
                dataGridView_source_users.Columns.Insert(columnIndex, addButtonColumn);
            }

            DataGridViewButtonColumn skipButtonColumn = new DataGridViewButtonColumn();
            skipButtonColumn.Name = "Skip";
            skipButtonColumn.Text = "Skip";
            skipButtonColumn.UseColumnTextForButtonValue = true;

            columnIndex = index + 2;
            if (dataGridView_source_users.Columns["Skip"] == null)
            {
                dataGridView_source_users.Columns.Insert(columnIndex, skipButtonColumn);
            }

            //Hide some columns

            dataGridView_source_users.Columns["id"].Visible = false;
            dataGridView_source_users.Columns["scrape_related"].Visible = false;

            //Set some ui based on balue

            //foreach (DataGridViewRow row in dataGridView_source_users.Rows)
            //{
            //    if (row.Cells["ischecked"].Value.ToString() == "Checked")
            //    {
            //        DataGridViewButtonCell add_cell = (DataGridViewButtonCell)row.Cells["Add"];
            //        DataGridViewButtonCell skip_cell = (DataGridViewButtonCell)row.Cells["Skip"];

            //        DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
            //        dataGridViewCellStyle.Padding = new Padding(0, 0, 10000, 0);

            //        add_cell.Style = dataGridViewCellStyle;
            //        skip_cell.Style = dataGridViewCellStyle;
            //    }
            //}

            //setting for column
            dataGridView_source_users.Columns["ig_url"].Width = 200;
            dataGridView_source_users.Columns["ok"].Width = 50;
            dataGridView_source_users.Columns["ischecked"].Width = 55;
            dataGridView_source_users.Columns["Add"].Width = 60;
            dataGridView_source_users.Columns["Skip"].Width = 60;
        }


        private void checkBox_source_users_not_checked_CheckedChanged(object sender, EventArgs e)
        {
            ReloadManageDGV_SourceUsers();
        }


        private void btn_import_source_users_Click(object sender, EventArgs e)
        {
            IGForms.Import import = new IGForms.Import("source_users");
            import.ShowDialog();
        }

        #endregion

        #region --datagridview event--



        private void dataGridView_target_accounts_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Add")
                {
                    //update Ok to db
                    API.DBHelpers.UpdateTargetAccounts_OK(senderGrid.Rows[e.RowIndex].Cells["ig_url"].Value.ToString(), "OK");

                    ReloadManageDGV_TargetAccounts();
                }

                if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Skip")
                {
                    //update Not OK to db
                    API.DBHelpers.UpdateTargetAccounts_OK(senderGrid.Rows[e.RowIndex].Cells["ig_url"].Value.ToString(), "NOT");

                    ReloadManageDGV_TargetAccounts();
                }
            }
            else
            {
                if (senderGrid.Columns[e.ColumnIndex].Name == "ig_url")
                {
                    if (hwnd_firefox == IntPtr.Zero)
                        return;
                    //load url
                    var url = senderGrid.Rows[e.RowIndex].Cells["ig_url"].Value.ToString();


                    PLAutoHelper.RPAFirefox.OpenUrlInCurrentTab(hwnd_firefox, url);
                }
            }
        }


        private void dataGridView_target_accounts_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn && e.RowIndex >= 0)
            {
                if (senderGrid.Columns[e.ColumnIndex].Name == "ScrapeFollower")
                {
                    string updated_value = "no";

                    if (bool.Parse(senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()) == true)
                    {
                        updated_value = "yes";
                    }

                    //API.DBHelpers.UpdateTargetAccounts_ScrapeFollower(senderGrid.Rows[e.RowIndex].Cells["ig_url"].Value.ToString(), updated_value);
                }
            }
        }


        private void dataGridView_target_accounts_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            var dataGridView = sender as DataGridView;

            DataGridViewColumn col = dataGridView_target_accounts.Columns[dataGridView_target_accounts.CurrentCell.ColumnIndex];
            if (col is DataGridViewComboBoxColumn)
            {

                if (col.Name == "ScrapeType")
                {

                    var combobox_val = dataGridView_target_accounts.CurrentCell.EditedFormattedValue.ToString();

                    //update to db
                    API.DBHelpers.UpdateTargetAccounts_ScrapeType(dataGridView_target_accounts.CurrentRow.Cells["ig_url"].Value.ToString(), combobox_val);
                }

                if(col.Name=="StateV")
                {
                    var combobox_val = dataGridView_target_accounts.CurrentCell.EditedFormattedValue.ToString();

                    //update to db
                    API.DBHelpers.UpdateTargetAccounts_State(dataGridView_target_accounts.CurrentRow.Cells["ig_url"].Value.ToString(), combobox_val);
                }
            }
        }


        private void dataGridView_source_users_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Add")
                {
                    //update Ok to db
                    API.DBHelpers.UpdateSourceUsers_OK(senderGrid.Rows[e.RowIndex].Cells["ig_url"].Value.ToString(), "OK");

                    ReloadManageDGV_SourceUsers();
                }

                if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Skip")
                {
                    //update Not OK to db
                    API.DBHelpers.UpdateSourceUsers_OK(senderGrid.Rows[e.RowIndex].Cells["ig_url"].Value.ToString(), "NOT");

                    ReloadManageDGV_SourceUsers();
                }
            }
            else
            {
                if (hwnd_firefox == IntPtr.Zero)
                    return;
                //load url
                var url = senderGrid.Rows[e.RowIndex].Cells["ig_url"].Value.ToString();

                PLAutoHelper.RPAFirefox.OpenUrlInCurrentTab(hwnd_firefox, url);
            }
        }


        #endregion


        ChromeDriver driver = null;
        private void btn_start_Click(object sender, EventArgs e)
        {
            //API.IGHelpers.ScrapeMediasFromUser("misthyyyy");

            //API.IGHelpers.ScrapeLikers("https://www.instagram.com/p/CAm4Am5hlTa/");

            //API.IGHelpers.ScrapeSuggestionUser("misthyyyy");


            //API.IGHelpers.ScrapeTopMediaHashTags("aosomi", 1);

            //API.IGHelpers.GetMediaFromUrlAsync("https://www.instagram.com/p/CAm_0EIprYi/");

            ChromeOptions options = new ChromeOptions();

            options.AddArgument(@"--user-data-dir=Profile1");
            options.AddArgument("--mute-audio");
            options.AddArgument("--start-minimized");

            ChromeDriverService chromeDriverService = ChromeDriverService.CreateDefaultService("driver\\1");
            chromeDriverService.HideCommandPromptWindow = true;

            driver = new ChromeDriver(chromeDriverService, options);


            driver.Navigate().GoToUrl("https://www.instagram.com/");

            var p = GetProcess();


            SetWindowPos(p.MainWindowHandle, new IntPtr(-1), 0, 0, 0, 0, 0x0040 | 0x0001);

            MessageBox.Show("Đã mở trình duyệt thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }










        #region --User32--

        [DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndParent);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, int uFlags);

        private Process GetProcess()
        {
            Process process = null;
            var processes = Process.GetProcessesByName("chrome");

            foreach (var item in processes)
            {
                if (item.MainWindowTitle.Contains("Instagram"))
                {
                    process = item;
                    break;
                }
            }

            return process;
        }



        #endregion



        #region --Filter--

        List<dynamic> filter_condition = new List<dynamic>();

        private string GenerateFilterStr()
        {
            string filter_str = "";
            for (int i = 0; i < filter_condition.Count; i++)
            {
                filter_str += filter_condition[i].name + "='" + filter_condition[i].value + "'";
                if (i < filter_condition.Count - 1)
                    filter_str += " AND ";
            }

            return filter_str;
        }

        #endregion



        private void form_igtoolkit_FormClosing(object sender, FormClosingEventArgs e)
        {


            try
            {
                driver.Close();
                driver.Quit();
            }
            catch
            { }

        }




        #region --Button Control Grab--

        private void btn_grabaccounts_Click(object sender, EventArgs e)
        {
            isstop = false;
            lb_status.Text = "Grabbing accounts....";
            lb_status.ForeColor = Color.Green;
            var thread = new IGThreads.GrabRelatedAccountsThread();
            thread.DoThread();
        }


        private void btn_target_accounts_details_Click(object sender, EventArgs e)
        {
            var thread = new IGThreads.GrabSourceUsersDetailThread("target_accounts");
            thread.DoThread();
        }


        private void btn_grab_source_users_Click(object sender, EventArgs e)
        {
            isstop = false;
            lb_status.Text = "Grabbing accounts....";
            lb_status.ForeColor = Color.Green;
            var thread = new IGThreads.GrabSourceUsersThread();
            thread.DoThread();
        }


        private void btn_source_users_details_Click(object sender, EventArgs e)
        {
            var thread = new IGThreads.GrabSourceUsersDetailThread("source_users");
            thread.DoThread();
        }


        private void btn_grabposts_Click(object sender, EventArgs e)
        {
            isstop = false;
            lb_status.Text = "Grabbing posts....";
            lb_status.ForeColor = Color.Green;
            var thread = new IGThreads.GrabPostsThread();
            thread.DoThread();
        }

        //private void TestUpdate()
        //{
        //    List<Thread> threads = new List<Thread>();
        //    for (int t = 0; t < 500; t++)
        //    {
        //        Thread thread = new Thread(() => TestUpdateIgToolkit());
        //        thread.SetApartmentState(ApartmentState.STA);
        //        threads.Add(thread);
        //    }

        //    Stopwatch watch = new Stopwatch();
        //    watch.Start();

        //    foreach (var t in threads)
        //    {
        //        t.Start();
        //    }

        //    foreach (var t in threads)
        //    {
        //        t.Join();
        //    }

        //    var finished = watch.Elapsed.TotalSeconds;

        //    Console.WriteLine(finished.ToString());
        //}

        //private void TestUpdateIgToolkit()
        //{
        //    using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection))
        //    {
        //        conn.Open();
        //        PLAutoHelper.SQLHelper.Execute(() =>
        //    {

        //        using (SqlTransaction sqlTransaction = conn.BeginTransaction())
        //        {
        //            var command_query = "update scrapeusers set [status]='' where [pk]=7577241070";


        //            using (var command = new SqlCommand(command_query, conn, sqlTransaction))
        //            {
        //                command.ExecuteNonQuery();
        //            }

        //            sqlTransaction.Commit();

        //        }

        //    });
        //        conn.Dispose();
        //    }
        //}
        

        public bool isstop = false;
        private void btn_stop_Click(object sender, EventArgs e)
        {


            //string data = PLAutoHelper.DetectPersonality.DetectGenderCountry_ByFullName_PLAuto("Doan Luan");

            //Thread.Sleep(2000);

            //PLAutoHelper.ProxyHelper.XProxy_Server(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "proxytb"), "192.168.1.8", true);

            //PLAutoHelper.DetectPersonality.DetectGenderCountry_ByFullName_PLAuto("vean a los que estoy siguiendo");

            //TestUpdate();
            //var apiusage = PLAutoHelper.DetectPersonality.Namsor_APIUsage("af993de101aa2783255739b2f0fc2de2", "192.168.1.8:4006");
            //            var maxdata = MaxNamSorPatch("87ffabc7088c8c283bd077c263502c1e", 50, 10, "");

            //PLAutoHelper.DetectPersonality.Namsor_APIUsage("b8009b5580c8cf7cc84216fe755f91c8");

            //var profiles = IGThreads.ThreadHelper.DetectGender_GetTask_Multi();

            //List<dynamic> dynamics = new List<dynamic>();

            //dynamics.Add(new
            //{
            //    id = 1,
            //    name = "luan"
            //});

            //dynamics.Add(new
            //{
            //    id = 2,
            //    name = "duyen"
            //});

            //var results = PLAutoHelper.DetectPersonality.DetectCountryPatch_Namsor(dynamics, "b8009b5580c8cf7cc84216fe755f91c8");

            //var temp = PLAutoHelper.DetectPersonality.DetectGender_Namsor("hacker", "01f4526eac12ab3c0c51554e4351251f", "192.168.1.8:4001");

            //PLAutoHelper.ProxyHelper.TinSoftServer(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tinsoft_tb"), true);

            //var json = PLAutoHelper.DetectPersonality.DetectCountry_Namsor("C.K.", "5896ca883255934a28e2a021bd8e650b", "192.168.1.8:4001");

            ////var json_dynamic = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(json);

            ////var gender = json_dynamic.country;

            ////LoginUseJson();
            ////var temp_proxy = PLAutoHelper.ProxyHelper.TinSoft_GetCurrentProxyByApiKey("TLJAhpzgC7i4NpbDftdJpe8Q3plNpR47mWmKq3");
            //IGAPIHelper.Scraper.APIObj dyn = IGAPIHelper.Scraper.GetAPIObj("192.168.1.8:4002", API.DBHelpers.db_connection);

            //InstagramApiSharp.API.IInstaApi _instaApi = dyn.instaApi;

            //var user_info = IGAPIHelper.Scraper.ScrapeCommenters("https://www.instagram.com/p/CI6JBX9FNhy/", _instaApi, 1);

            //var commenters = IGAPIHelper.Scraper.ScrapeFollowerByUsername("meichannnnn", _instaApi, 1);

            //var task_scrape_followers = Task.Run(() =>
            //{
            //    return _instaApi.GetStateDataAsStreamAsync();
            //});

            //var state = task_scrape_followers.GetAwaiter().GetResult();

            //var state_str=_instaApi.GetStateDataAsString();

            //string usernames = "";

            isstop = true;
            lb_status.Text = "Stopping";

            //IGAPIHelper.Scraper.APIObj dyn = IGAPIHelper.Scraper.GetAPIObj("192.168.100.6:4001", API.DBHelpers.db_connection);

            //InstagramApiSharp.API.IInstaApi _instaApi = dyn.instaApi;


            ////var likers = IGAPIHelper.Scraper.ScrapeLikers("https://www.instagram.com/p/CIgPiHUJPBm/", _instaApi);

            //var task_medias = Task.Run(() =>
            //{
            //    return _instaApi.UserProcessor.GetUserFollowersAsync("hanagianganh", PaginationParameters.MaxPagesToLoad(5));
            //});

            //var medias = task_medias.GetAwaiter().GetResult();

            //_instaApi.UserProcessor.GetUserFollowersAsync("hanagianganh", PaginationParameters.MaxPagesToLoad(1));

            //IGAPIHelper.Scraper.ScrapeMediasFromUser(_instaApi, "alinvlee");

            //API.IGHelpers.ScrapeLikers("https://www.instagram.com/p/CBeqRURpMnl/");
            //API.IGHelpers.ScrapeCommenters("https://www.instagram.com/p/CBiV7oCBFvx/");

            //API.DBHelpers.UpdateScrapeUserData(long.Parse("192215710"), "RecentPost", "");

            //API.IGHelpers.ScrapeFullUserInfo("4661920474");

            //API.ProxyHelpers.ProxyServerOthers(true);

            //API.IGHelpers.ScrapeSuggestionUser("t.o.p_2hand_");

            //API.TinSoftHelpers.CheckProxy("TLElwvlJjdIYOKWWVK03HeWhsML8ypIYM0Wqon");

            ////var proxy = API.TinSoftHelpers.GetProxy();

            //API.IGHelpers.ScrapeFullUserInfo("6576630");

            //var result = API.TinSoftHelpers.ChangeProxy("TLKQ59TzivEk0Kv1Q3hsZ0kor8h0cklsSi79nN");

            //API.IGHelpers.IsUsernameNotExisted(null, "19.summersofficialsss");

        }

        private void LoginUseJson()
        {
            string username = "isabelfoxworth";
            string password = "123456789kdL";

            InstagramApiSharp.API.IInstaApi _instaApi;

            var userSession = new UserSessionData
            {
                UserName = username,
                Password = password
            };

            var proxystr = "192.168.1.8:4001";
            if (proxystr != null)
            {
                var proxy = new WebProxy()
                {
                    Address = new Uri("http://" + proxystr), //i.e: http://1.2.3.4.5:8080
                    BypassProxyOnLocal = false,
                    UseDefaultCredentials = false,

                    //// *** These creds are given to the proxy server, not the web server ***
                    //Credentials = new NetworkCredential(
                    //userName: proxyinfo.user,
                    //password: proxyinfo.pass)
                };

                // Now create a client handler which uses that proxy
                var httpClientHandler = new HttpClientHandler()
                {
                    Proxy = proxy,
                };

                var httpClient = new HttpClient(handler: httpClientHandler, disposeHandler: true)
                {
                    // if you want to use httpClient, you should set this
                    BaseAddress = new Uri("https://i.instagram.com/")
                };

                // create new InstaApi instance using Builder
                _instaApi = InstaApiBuilder.CreateBuilder()
                    .SetUser(userSession)
                    .UseLogger(new DebugLogger(LogLevel.All)) // use logger for requests and debug messages
                    .UseHttpClient(httpClient)
                    .UseHttpClientHandler(httpClientHandler)
                    .Build();
            }
            else
            {
                _instaApi = InstaApiBuilder.CreateBuilder()
                    .SetUser(userSession)
                    .UseLogger(new DebugLogger(LogLevel.All)) // use logger for requests and debug messages
                    .Build();
            }

            _instaApi.LoadStateDataFromString(File.ReadAllText("temp_min_id.txt"));

            var commenters = IGAPIHelper.Scraper.ScrapeFollowerByUsername("meichannnnn", _instaApi, 1);


        }

        public void UpdateStatusLabel(string status)
        {
            lb_status.Invoke((MethodInvoker)delegate
            {
                lb_status.Text = status;
            });
        }

        public void FreezeForm()
        {
            btn_grabaccounts.Enabled = false;
            btn_grabposts.Enabled = false;
            btn_stop.Enabled = true;
        }

        public void UnfreezeForm()
        {
            btn_grabaccounts.Enabled = true;
            btn_grabposts.Enabled = true;
            btn_stop.Enabled = false;
        }

        public void ShowCompleted()
        {
            MessageBox.Show("Completed", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion



        private void btn_update_Click(object sender, EventArgs e)
        {
            var task = new IGTasks.QuickScrapeTask();
            task.DoTask();
        }


        private void btn_scrapeuser_info_Click(object sender, EventArgs e)
        {
            UpdateStatusLabel("Scraping User Info");
            var thread = new IGThreads.UpdateUserInfoThread();
            thread.DoThread();
        }

        private void btn_checkuser_Click(object sender, EventArgs e)
        {
            var thread = new IGThreads.CheckUsernameThreads();
            thread.DoThread();
        }

        private void form_igtoolkit_Shown(object sender, EventArgs e)
        {
            ReloadManageDGV_TargetAccounts();
            ReloadManageDGV_SourceUsers();
            ReloadCommentAccounts();

            dataGridView_target_accounts.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView_source_users.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView_commentaccount.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            LoadLocalSettings();

            InitTimer_AutoRun_Counter();

            //Load tutorial
            textBox_copycontent_tips.Text = BMResource.Tutorials.copycontent_tutorial;
            textBox_hashtags_tutorial.Text = BMResource.Tutorials.grabhashtags_tutorial;
        }

        int firefox_pid = 0;
        IntPtr hwnd_firefox = IntPtr.Zero;

        private void btn_launch_firefox_Click(object sender, EventArgs e)
        {
            firefox_pid = PLAutoHelper.RPAFirefox.LaunchFFPortable(Path.GetFullPath("FirefoxPortable"));

            hwnd_firefox = Process.GetProcessById(firefox_pid).MainWindowHandle;
        }

        private void btn_grab_followers_Click(object sender, EventArgs e)
        {
            UpdateStatusLabel("Scraping Followers");
            var thread = new IGThreads.GrabFollowerThread();
            thread.DoThread();
        }

        private void btn_grab_likers_Click(object sender, EventArgs e)
        {
            var thread = new IGThreads.GrabRecentLikersPost();
            thread.DoThread();

            //extract user from html
            var htmlDoc_load = new HtmlAgilityPack.HtmlDocument();
            htmlDoc_load.LoadHtml(File.ReadAllText("temp.txt"));

            var user_nodes = htmlDoc_load.DocumentNode.SelectNodes("//a[@class='FPmhX notranslate MBL3Z']");
        }

        #region --AIO Tab--

        public bool is_APITask_GrabPost;
        public bool is_APITask_GrabCommenters;
        public bool is_APITask_GrabLikers;
        public bool is_APITask_GrabFollower;
        public bool is_APITask_GrabUserFullInfo;
        public bool is_APITask_GrabUserFullInfo_RESCRAPE;
        public bool is_APITask_GrabPost_ForComment;
        public bool is_APITask_GrabPost_ForComment_FromScrapeUsers;
        public bool is_APITask_GrabComment_ForSlave;
        public bool is_APITask_GrabFollowerBack;
        public bool is_APITask_GrabFollower_ForMother;


        public void WriteLogToForm_AIOGrab(string message)
        {
            tb_aio_log.Invoke((MethodInvoker)delegate
            {
                tb_aio_log.AppendText(message + " at " + DateTime.Now.ToString() + "\r\n");
            });
        }

        private void btn_grab_aio_Click(object sender, EventArgs e)
        {

            is_APITask_GrabPost = checkBox_aio_grab_APITask_GrabPost.Checked;
            is_APITask_GrabCommenters = checkBox_aio_grab_APITask_GrabCommenters.Checked;
            is_APITask_GrabLikers = checkBox_aio_grab_APITask_GrabLikers.Checked;
            is_APITask_GrabFollower = checkBox_aio_grab_APITask_GrabFollower.Checked;
            is_APITask_GrabUserFullInfo = checkBox_aio_grab_APITask_GrabUserFullInfo.Checked;
            is_APITask_GrabUserFullInfo_RESCRAPE = checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape.Checked;
            is_APITask_GrabPost_ForComment = checkBox_aio_grab_APITask_GrabPost_ForComment.Checked;
            is_APITask_GrabFollowerBack = checkBox_aio_grab_APITask_GrabFollowerBack.Checked;
            is_APITask_GrabFollower_ForMother = checkBox_aio_grab_APITask_GrabFollower_ForMother.Checked;
            is_APITask_GrabPost_ForComment_FromScrapeUsers = checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers.Checked;
            is_APITask_GrabComment_ForSlave = checkBox_aio_grab_APITask_GrabComment_ForSlave.Checked;

            btn_grab_aio.Enabled = false;

            var thread = new IGThreads.AIOThread();
            thread.DoThread();
        }


        private void btn_aio_grab_settings_Click(object sender, EventArgs e)
        {
            IGForms.AIOGrab_Settings aio_settings = new IGForms.AIOGrab_Settings();
            aio_settings.ShowDialog();
        }

        #endregion

        #region --Detect Personality--

        public void WriteLogToForm_DetectPersonality(string message)
        {
            tb_detect_personality_log.Invoke((MethodInvoker)delegate
            {
                tb_detect_personality_log.AppendText(message + " at " + DateTime.Now.ToString() + "\r\n");
            });
        }

        private void btn_reg_namsor_Click(object sender, EventArgs e)
        {

            int target = int.Parse(tb_namsor_quantity.Text);

            var thread = new IGThreads.NamSorRegisterThread(target);
            thread.DoThread();
        }

        public bool is_DetectNameImage;
        public bool is_DetectLanguage;
        public bool is_DetectGender;
        public bool is_DetectCountry;
        public bool is_Detect_run_proxy_server;


        private void btn_detect_aio_Click(object sender, EventArgs e)
        {
            is_DetectNameImage = checkBox_aio_detect_DetectNameImage.Checked;
            is_DetectLanguage = checkBox_aio_detect_DetectLanguage.Checked;
            is_DetectGender = checkBox_aio_detect_DetectGender.Checked;
            is_DetectCountry = checkBox_aio_detect_DetectCountry.Checked;
            is_Detect_run_proxy_server = checkBox_aio_detect_proxy_server.Checked;

            btn_detect_aio.Enabled = false;

            var thread = new IGThreads.DetectAIOThread(tb_aio_detect_proxy_apikey.Text);
            thread.DoThread();
        }


        #endregion

        #region --Hashtag--



        #endregion


        #region --CommentTAB--

        private void ReloadCommentAccounts()
        {
            int current_index = dataGridView_commentaccount.FirstDisplayedScrollingRowIndex;

            if (current_index == -1)
                current_index = 0;

            List<dynamic> comment_accounts = API.ManageDGVHelper.LoadCommentAccounts();

            //remove column 

            dataGridView_commentaccount.Columns.Clear();

            List<dynamic> filter_profiles = new List<dynamic>();

            if (checkBox_comment_account_notcheck.Checked)
            {
                filter_profiles.AddRange(comment_accounts.Where(a => a.ischecked != "Checked"));
            }
            else
                filter_profiles.AddRange(comment_accounts);

            filter_profiles = filter_profiles.Distinct().ToList();

            dataGridView_commentaccount.DataSource = filter_profiles;

            if (filter_profiles.Count == 0)
                return;

            int index = 4;
            int columnIndex = 0;



            //addbutton

            DataGridViewButtonColumn addButtonColumn = new DataGridViewButtonColumn();
            addButtonColumn.Name = "Add";
            addButtonColumn.Text = "Add";
            addButtonColumn.UseColumnTextForButtonValue = true;

            columnIndex = index;
            if (dataGridView_commentaccount.Columns["Add"] == null)
            {
                dataGridView_commentaccount.Columns.Insert(columnIndex, addButtonColumn);
            }

            DataGridViewButtonColumn skipButtonColumn = new DataGridViewButtonColumn();
            skipButtonColumn.Name = "Skip";
            skipButtonColumn.Text = "Skip";
            skipButtonColumn.UseColumnTextForButtonValue = true;

            columnIndex = index + 1;
            if (dataGridView_commentaccount.Columns["Skip"] == null)
            {
                dataGridView_commentaccount.Columns.Insert(columnIndex, skipButtonColumn);
            }


            dataGridView_commentaccount.Invoke((MethodInvoker)delegate
            {
                dataGridView_commentaccount.FirstDisplayedScrollingRowIndex = current_index;
            });
        }


        private void dataGridView_commentaccount_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Add")
                {
                    //update Ok to db
                    API.DBHelpers.UpdateCommentAccount_OK(senderGrid.Rows[e.RowIndex].Cells["ig_url"].Value.ToString(), "OK");

                    ReloadCommentAccounts();
                }

                if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Skip")
                {
                    //update Not OK to db
                    API.DBHelpers.UpdateCommentAccount_OK(senderGrid.Rows[e.RowIndex].Cells["ig_url"].Value.ToString(), "NOT");

                    ReloadCommentAccounts();
                }
            }
            else
            {
                if (senderGrid.Columns[e.ColumnIndex].Name == "ig_url")
                {
                    if (hwnd_firefox == IntPtr.Zero)
                        return;
                    //load url
                    var url = senderGrid.Rows[e.RowIndex].Cells["ig_url"].Value.ToString();


                    PLAutoHelper.RPAFirefox.OpenUrlInCurrentTab(hwnd_firefox, url);
                }
            }
        }


        private void checkBox_comment_account_notcheck_CheckedChanged(object sender, EventArgs e)
        {
            ReloadCommentAccounts();
        }


        private void button_commentaccount_grab_related_account_Click(object sender, EventArgs e)
        {
            var dothread = new IGThreads.GrabRelatedCommentAccountThread();
            dothread.DoThread();
        }

        #endregion

        private void button_tool_settings_Click(object sender, EventArgs e)
        {
            var tool_Settings_form = new IGForms.ToolSetting();
            tool_Settings_form.Show();
        }


        #region --auto launch, auto run--


        private void LoadLocalSettings()
        {
            if(Settings.aio_settings["aio_grab_option"]==null)
            {
                ////generate default file

                //dynamic default_local_settings = new ExpandoObject();

                //default_local_settings.checkBox_aio_grab_APITask_GrabPost = checkBox_aio_grab_APITask_GrabPost.Checked;
                //default_local_settings.checkBox_aio_grab_APITask_GrabCommenters = checkBox_aio_grab_APITask_GrabCommenters.Checked;
                //default_local_settings.checkBox_aio_grab_APITask_GrabFollower_ForMother = checkBox_aio_grab_APITask_GrabFollower_ForMother.Checked;
                //default_local_settings.checkBox_aio_grab_APITask_GrabLikers = checkBox_aio_grab_APITask_GrabLikers.Checked;
                //default_local_settings.checkBox_aio_grab_APITask_GrabFollower = checkBox_aio_grab_APITask_GrabFollower.Checked;
                //default_local_settings.checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers = checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers.Checked;
                //default_local_settings.checkBox_aio_grab_APITask_GrabUserFullInfo = checkBox_aio_grab_APITask_GrabUserFullInfo.Checked;
                //default_local_settings.checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape = checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape.Checked;
                //default_local_settings.checkBox_aio_grab_APITask_GrabComment_ForSlave = checkBox_aio_grab_APITask_GrabComment_ForSlave.Checked;
                //default_local_settings.checkBox_aio_grab_APITask_GrabPost_ForComment = checkBox_aio_grab_APITask_GrabPost_ForComment.Checked;
                //default_local_settings.checkBox_aio_grab_APITask_GrabFollowerBack = checkBox_aio_grab_APITask_GrabFollowerBack.Checked;


                //var settings_json_str = Newtonsoft.Json.JsonConvert.SerializeObject(default_local_settings);

                //File.WriteAllText(current_path+ "\\local_settings.txt", settings_json_str);
                UpdateLocalSettings();
            }

            var settings_json = Settings.aio_settings.aio_grab_option;

            checkBox_aio_grab_APITask_GrabPost.Checked = settings_json.checkBox_aio_grab_APITask_GrabPost;
            checkBox_aio_grab_APITask_GrabCommenters.Checked = settings_json.checkBox_aio_grab_APITask_GrabCommenters;
            checkBox_aio_grab_APITask_GrabFollower_ForMother.Checked = settings_json.checkBox_aio_grab_APITask_GrabFollower_ForMother;
            checkBox_aio_grab_APITask_GrabLikers.Checked = settings_json.checkBox_aio_grab_APITask_GrabLikers;
            checkBox_aio_grab_APITask_GrabFollower.Checked = settings_json.checkBox_aio_grab_APITask_GrabFollower;
            checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers.Checked = settings_json.checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers;
            checkBox_aio_grab_APITask_GrabUserFullInfo.Checked = settings_json.checkBox_aio_grab_APITask_GrabUserFullInfo;
            checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape.Checked = settings_json.checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape;
            checkBox_aio_grab_APITask_GrabComment_ForSlave.Checked = settings_json.checkBox_aio_grab_APITask_GrabComment_ForSlave;
            checkBox_aio_grab_APITask_GrabPost_ForComment.Checked = settings_json.checkBox_aio_grab_APITask_GrabPost_ForComment;
            checkBox_aio_grab_APITask_GrabFollowerBack.Checked = settings_json.checkBox_aio_grab_APITask_GrabFollowerBack;

            checkBox_aio_detect_DetectNameImage.Checked = settings_json.checkBox_aio_detect_DetectNameImage;
            checkBox_aio_detect_DetectCountry.Checked = settings_json.checkBox_aio_detect_DetectCountry;
            checkBox_aio_detect_DetectGender.Checked = settings_json.checkBox_aio_detect_DetectGender;


        }

        private System.Windows.Forms.Timer timer_autorun;

        int counter_auto_run = 20;

        protected void InitTimer_AutoRun_Counter()
        {
            timer_autorun = new System.Windows.Forms.Timer();
            timer_autorun.Tick += new EventHandler(timer_autorun_Tick);
            timer_autorun.Interval = 1000;

            this.Invoke(new MethodInvoker(delegate ()
            {
                timer_autorun.Start();
            }));

        }

        private void timer_autorun_Tick(object sender, EventArgs e)
        {
            counter_auto_run--;
            if (counter_auto_run == 0)
            {
                timer_autorun.Stop();
                AutoRun();
            }
            label_autorun_counter.Text = counter_auto_run.ToString();

            //Thread worker = new Thread(new ThreadStart(Program.form.UpdateRunningStatus));
            //worker.Name = "Monitor Devices Thread";
            //worker.IsBackground = true;
            //worker.Start();
        }

        private void AutoRun()
        {

            //run tasking
            btn_grab_aio.PerformClick();

            //run detectaio

            is_DetectNameImage = checkBox_aio_detect_DetectNameImage.Checked;
            is_DetectLanguage = checkBox_aio_detect_DetectLanguage.Checked;
            is_DetectGender = checkBox_aio_detect_DetectGender.Checked;
            is_DetectCountry = checkBox_aio_detect_DetectCountry.Checked;
            is_Detect_run_proxy_server = checkBox_aio_detect_proxy_server.Checked;

            btn_detect_aio.Enabled = false;

            var thread = new IGThreads.DetectAIOThread(tb_aio_detect_proxy_apikey.Text);
            thread.DoThread();
        }

        private void button_stop_autorun_Click(object sender, EventArgs e)
        {
            timer_autorun.Stop();
        }

        private void UpdateLocalSettings()
        {
            dynamic default_local_settings = new ExpandoObject();

            default_local_settings.checkBox_aio_grab_APITask_GrabPost = checkBox_aio_grab_APITask_GrabPost.Checked;
            default_local_settings.checkBox_aio_grab_APITask_GrabCommenters = checkBox_aio_grab_APITask_GrabCommenters.Checked;
            default_local_settings.checkBox_aio_grab_APITask_GrabFollower_ForMother = checkBox_aio_grab_APITask_GrabFollower_ForMother.Checked;
            default_local_settings.checkBox_aio_grab_APITask_GrabLikers = checkBox_aio_grab_APITask_GrabLikers.Checked;
            default_local_settings.checkBox_aio_grab_APITask_GrabFollower = checkBox_aio_grab_APITask_GrabFollower.Checked;
            default_local_settings.checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers = checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers.Checked;
            default_local_settings.checkBox_aio_grab_APITask_GrabUserFullInfo = checkBox_aio_grab_APITask_GrabUserFullInfo.Checked;
            default_local_settings.checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape = checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape.Checked;
            default_local_settings.checkBox_aio_grab_APITask_GrabComment_ForSlave = checkBox_aio_grab_APITask_GrabComment_ForSlave.Checked;
            default_local_settings.checkBox_aio_grab_APITask_GrabPost_ForComment = checkBox_aio_grab_APITask_GrabPost_ForComment.Checked;
            default_local_settings.checkBox_aio_grab_APITask_GrabFollowerBack = checkBox_aio_grab_APITask_GrabFollowerBack.Checked;

            default_local_settings.checkBox_aio_detect_DetectNameImage = checkBox_aio_detect_DetectNameImage.Checked;
            default_local_settings.checkBox_aio_detect_DetectCountry = checkBox_aio_detect_DetectCountry.Checked;
            default_local_settings.checkBox_aio_detect_DetectGender = checkBox_aio_detect_DetectGender.Checked;


            var settings_json_str = Newtonsoft.Json.JsonConvert.SerializeObject(default_local_settings);

            Settings.aio_settings["aio_grab_option"] = Newtonsoft.Json.Linq.JToken.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(default_local_settings));

            //update to db

            string command_query =
                @"update ig_projects set [igtoolkit_aio_grab_setting]=@aio_settings
                where
	                [project_name]=@project_name";

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.aio_ig_support))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@project_name", SqlDbType.VarChar).Value = Settings.aio_settings["Project"].ToString();
                    command.Parameters.Add("@aio_settings", SqlDbType.VarChar).Value = Newtonsoft.Json.JsonConvert.SerializeObject(Settings.aio_settings);

                    command.ExecuteNonQuery();
                }
            }

            //File.WriteAllText(current_path + "\\local_settings.txt", settings_json_str);
        }

        private void ReadLocalSettings()
        {

        }

        #endregion

        private void checkBox_aio_grab_APITask_GrabPost_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLocalSettings();
        }

        private void checkBox_aio_grab_APITask_GrabCommenters_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLocalSettings();
        }

        private void checkBox_aio_grab_APITask_GrabFollower_ForMother_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLocalSettings();
        }

        private void checkBox_aio_grab_APITask_GrabLikers_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLocalSettings();
        }

        private void checkBox_aio_grab_APITask_GrabFollower_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLocalSettings();
        }

        private void checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLocalSettings();
        }

        private void checkBox_aio_grab_APITask_GrabUserFullInfo_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLocalSettings();
        }

        private void checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLocalSettings();
        }

        private void checkBox_aio_grab_APITask_GrabComment_ForSlave_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLocalSettings();
        }

        private void checkBox_aio_grab_APITask_GrabPost_ForComment_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLocalSettings();
        }

        private void checkBox_aio_grab_APITask_GrabFollowerBack_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLocalSettings();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        #region --Leech Resources--

        #region --Leech Images--

        private void button_leech_resources_images_browse_des_Click(object sender, EventArgs e)
        {
            var FD = new System.Windows.Forms.FolderBrowserDialog();
            if (FD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox_leech_resources_images_des_folder.Text = FD.SelectedPath;
            }
        }

        private void button_leech_resources_images_download_Click(object sender, EventArgs e)
        {
            if (textBox_leech_resources_images_des_folder.Text == "" || textBox_leech_resources_images_listposts.Text == "")
            {
                MessageBox.Show("Please fill all required information");
                return;
            }

            List<string> posts = new List<string>();

            posts = textBox_leech_resources_images_listposts.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();

            string des_folder = PLAutoHelper.NormalFuncHelper.OptimizePathEndBackSlash(textBox_leech_resources_images_des_folder.Text);

            PLAutoHelper.ProxyHelper.XProxyServer_Ticker(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), Settings.proxy_server_ips);

            //download image to des
            Thread thread = new Thread(() => LeechImages(posts, des_folder));
            thread.Name = "leech images";
            thread.SetApartmentState(ApartmentState.STA);
            thread.IsBackground = true;
            thread.Start();
        }

        private void LeechImages(List<string> posts, string des_folder)
        {
            int total_posts = posts.Count;
            int finished_count = 0;

            while (posts.Count > 0)
            {

                var continous_error = 0;

            GetAPIOBJ:

                string proxy = "";


                while (proxy == "" || proxy == null)
                {
                    //proxy = PLAutoHelper.ProxyHelper.TMProxy_DB_GetProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                    proxy = PLAutoHelper.ProxyHelper.XProxy_DB_GetProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"));
                    Thread.Sleep(5000);
                }

                IGAPIHelper.Scraper.APIObj dyn = IGAPIHelper.Scraper.GetAPIObj(proxy, API.DBHelpers.db_connection_igtoolkit);

                if (dyn == null)
                {
                    //PLAutoHelper.ProxyHelper.TMProxy_DB_ReleaseProxy(proxy, new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                    PLAutoHelper.ProxyHelper.XProxy_DB_ReleaseProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), proxy);
                    goto GetAPIOBJ;
                }

                InstagramApiSharp.API.IInstaApi _instaApi = dyn.instaApi;

                Stopwatch watch_scrape = new Stopwatch();
                watch_scrape.Start();


                try
                {

                    var time_session = int.Parse(API.DBHelpers.GetValueFromAccountTb(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(dyn.account, "username"), "time_per_session"));

                    API.DBHelpers.AddAPICallLog(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(dyn.account, "username"), "new_session");


                    while (true)
                    {

                        if (watch_scrape.Elapsed.TotalMinutes > time_session)
                            break;

                        if (continous_error > 5)
                        {
                            IGThreads.ThreadHelper.HandleException("Error Over 5 Times", "");
                            break;
                        }

                        if (posts.Count == 0)
                            break;

                        string target_link = posts[0];


                        var media = IGAPIHelper.Scraper.GetMediaFromUrlAsync(target_link, _instaApi);

                        string image_link = null;

                        //take link
                        if (media.Images.Count > 0)
                            image_link = media.Images[0].Uri;
                        else
                            if (media.Carousel.Count > 0)
                        {
                            foreach (var c in media.Carousel)
                            {
                                if (c.MediaType.ToString() == "Image")
                                {
                                    image_link = c.Images[0].Uri;
                                }
                            }
                        }

                        //download link to destination folder
                        var code = target_link.Replace("https://www.instagram.com/p/", "").Replace("/", "");

                        var download_path = des_folder + code + ".jpg";

                        using (var m = new MemoryStream(new WebClient().DownloadData(image_link)))
                        {
                            using (var img = Bitmap.FromStream(m))
                            {

                                img.Save(download_path, System.Drawing.Imaging.ImageFormat.Jpeg);
                            }
                        }

                        posts.Remove(target_link);

                        finished_count++;

                        label_leech_resources_images_status.Invoke((MethodInvoker)delegate
                        {
                            label_leech_resources_images_status.Text = "Finished " + finished_count.ToString() + "/" + total_posts.ToString();
                        });
                    }
                }
                finally
                {
                    IGAPIHelper.Scraper.ReleaseScrapeAccount(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(dyn.account, "username"), API.DBHelpers.db_connection_igtoolkit);
                    Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish APITask - Release Scrape Account");

                    //PLAutoHelper.ProxyHelper.TMProxy_DB_ReleaseProxy(proxy, new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                    PLAutoHelper.ProxyHelper.XProxy_DB_ReleaseProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), proxy);
                    Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish APITask - Release Proxy");

                    //Write session time
                    Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Finished round in " + watch_scrape.Elapsed.TotalMinutes.ToString());
                }
            }

            label_leech_resources_images_status.Invoke((MethodInvoker)delegate
            {
                label_leech_resources_images_status.Text = "Leech Images: Completed";
            });
        }

        #endregion

        #endregion


        #region --CopyContent--

        private void button_copycontent_copy_now_Click(object sender, EventArgs e)
        {
            string active_copycontent_type = tabControl_copycontent_type.SelectedTab.Text;

            switch(active_copycontent_type)
            {
                case "Copy From Source Accounts":
                    CopyContent_CopyFromSourceAccounts();
                    break;
                case "Copy From Posts":
                    CopyContent_CopyFromPosts();
                    break;
                default:
                    break;
            }
        }

        private void CopyContent_CopyFromSourceAccounts()
        {
            var niche_content = textBox_copycontent_niche_content.Text;

            List<string> source_accounts = textBox_copycontent_copy_from_accounts_list_accounts.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();

            int max_pages = int.Parse(textBox_copycontent_copy_from_accounts_max_pages.Text == "" ? "0" : textBox_copycontent_copy_from_accounts_max_pages.Text);

            if (niche_content == "" || source_accounts.Count == 0 || max_pages == 0)
            {
                MessageBox.Show("Please fill all required value");
                return;
            }

            var thread = new IGThreads.CopyPostFromSourceThread(niche_content, source_accounts, max_pages);
            thread.DoThread();
        }

        private void CopyContent_CopyFromPosts()
        {
            var niche_content = textBox_copycontent_niche_content.Text;

            List<string> posts = textBox_copycontent_copy_from_posts_listposts.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();

            if (niche_content == "" || posts.Count == 0)
            {
                MessageBox.Show("Please fill all required value");
                return;
            }

            var thread = new IGThreads.CopyPostFromPostsThread(posts, niche_content);
            thread.DoThread();
        }

        private void button_copycontent_uploadnow_Click(object sender, EventArgs e)
        {

            Thread thread = new Thread(() => IGThreads.ThreadHelper.UploadContentsToOwnServer());
            thread.Name = "upload images to server";
            thread.SetApartmentState(ApartmentState.STA);
            thread.IsBackground = true;
            thread.Start();

        }

        public void UpdateCopyContentStatus(string status)
        {
            label_copy_content_status.Invoke((MethodInvoker)delegate
            {
                label_copy_content_status.Text = status;
            });
        }

        private void button__copycontent_updatepost_updatenow_Click(object sender, EventArgs e)
        {
            List<string> postIds = textBox_copycontent_updatepost_list_post_ids.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();

            if (postIds.Count == 0)
            {
                MessageBox.Show("Please fill all required value");
                return;
            }

            var thread = new IGThreads.CopyContent_UpdatePostThread(postIds);
            thread.DoThread();
        }

        private void button_upload_gdrive_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => IGThreads.ThreadHelper.UploadImagesToGdrive());
            thread.Name = "upload images to server";
            thread.SetApartmentState(ApartmentState.STA);
            thread.IsBackground = true;
            thread.Start();
        }

        #endregion

        #region --Hashtags tab--

        private void button_hashtags_grab_hashtags_Click(object sender, EventArgs e)
        {

            isstop = false;
            lb_status.Text = "Grabbing hashtags....";
            lb_status.ForeColor = Color.Green;
            var thread = new IGThreads.GrabHashTagsThreads();
            thread.DoThread();
        }

        private void button_hashtags_import_Click(object sender, EventArgs e)
        {
            IGForms.Import import = new IGForms.Import("hashtags");
            import.ShowDialog();
        }

        private void button_hashtags_grab_details_Click(object sender, EventArgs e)
        {
            isstop = false;
            lb_status.Text = "Grabbing hashtags....";
            lb_status.ForeColor = Color.Green;
            var thread = new IGThreads.GrabHashtagDetailThread();
            thread.DoThread();
        }


        #endregion

        private void checkBox_aio_detect_DetectNameImage_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLocalSettings();
        }

        private void checkBox_aio_detect_DetectCountry_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLocalSettings();
        }

        private void checkBox_aio_detect_DetectGender_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLocalSettings();
        }

        #region  --Addon AccountDetails--

        private void btn_addon_account_details_grab_Click(object sender, EventArgs e)
        {
            List<string> usernames = textBox_addon_account_details_list.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();

            var thread = new IGThreads.Addon_Thread(usernames);
            thread.DoThread();
        }

        public void UpdateAddon_AccountDetails_Status(string status)
        {
            label_copy_content_status.Invoke((MethodInvoker)delegate
            {
                label_copy_content_status.Text = status;
            });
        }

        #endregion


        #region --Login To New Account--

        private void button_login_to_newaccount_Click(object sender, EventArgs e)
        {

            var dr = MessageBox.Show("Make sure set max thread proxy to 1", "Alert", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

            if (dr == DialogResult.OK)
            {
                var thread = new IGThreads.LoginToNewAccount_Thread();
                thread.DoThread();
            }

        }

        #endregion
    }
}


