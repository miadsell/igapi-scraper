﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.API
{
    class ManageDGVHelper
    {
        static public List<dynamic> LoadAllTargetAccounts()
        {
            List<dynamic> accounts = new List<dynamic>();

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("select * from target_accounts", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var id = dataReader["id"].ToString();
                            var ig_url = dataReader["ig_url"].ToString();
                            var follower = dataReader["follower"].ToString() == "" ? 0 : int.Parse(dataReader["follower"].ToString());
                            var following = dataReader["following"].ToString() == "" ? 0 : int.Parse(dataReader["following"].ToString());
                            var ok = dataReader["ok"].ToString();
                            var ischecked = dataReader["checked"].ToString();
                            var added = dataReader["added"].ToString();
                            var scrape_related = dataReader["scrape_related"].ToString();
                            var scrape_type = dataReader["scrape_type"] == null ? "" : dataReader["scrape_type"].ToString();
                            var scrape_follower = dataReader["scrape_follower"] == null ? "" : dataReader["scrape_follower"].ToString();
                            var state = dataReader["state"] == null ? "" : dataReader["state"].ToString();

                            accounts.Add(new
                            {
                                id,
                                ig_url,
                                follower,
                                following,
                                ok,
                                ischecked,
                                added,
                                scrape_related,
                                scrape_type,
                                scrape_follower,
                                state
                            });
                        }
                    }
                }
                return accounts;
            }
        }

        static public List<dynamic> LoadAllSourceUsers()
        {
            List<dynamic> accounts = new List<dynamic>();

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("select * from source_users", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var id = dataReader["id"].ToString();
                            var ig_url = dataReader["ig_url"].ToString();
                            var follower = dataReader["follower"].ToString() == "" ? 0 : int.Parse(dataReader["follower"].ToString());
                            var following = dataReader["following"].ToString() == "" ? 0 : int.Parse(dataReader["following"].ToString());
                            var ok = dataReader["ok"].ToString();
                            var ischecked = dataReader["checked"].ToString();
                            var added = dataReader["last_scrape"].ToString();
                            var scrape_related = dataReader["scrape_related"].ToString();

                            accounts.Add(new
                            {
                                id,
                                ig_url,
                                follower,
                                following,
                                ok,
                                ischecked,
                                added,
                                scrape_related
                            });
                        }
                    }
                }
                return accounts;
            }
        }

        static public List<dynamic> LoadCommentAccounts()
        {
            List<dynamic> accounts = new List<dynamic>();

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                string command_query =
                    @"select *
                    from comment_accounts";

                using (var command = new SqlCommand(command_query, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var ig_url = dataReader["ig_url"].ToString();
                            var ok = dataReader["ok"].ToString();
                            var ischecked = dataReader["checked"].ToString();
                            var scrape_related = dataReader["scrape_related"].ToString();

                            accounts.Add(new
                            {
                                ig_url,
                                ok,
                                ischecked,
                                scrape_related
                            });
                        }
                    }
                }
                return accounts;
            }
        }
    }
}

