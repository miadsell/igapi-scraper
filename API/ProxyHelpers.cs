﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.API
{
    static class ProxyHelpers
    {
        static public void ProxyServerOthers(bool firsttime = false)
        {
            if (firsttime)
            {

                using (SqlConnection conn = new SqlConnection(DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    var sql_command = "update proxytb set [status]='Updating'";

                    using (var command = new SqlCommand(sql_command, conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
            else
            {
                using (SqlConnection conn = new SqlConnection(DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    var sql_command = "update proxytb set [status]='Updating' where used = 0 and ([status] = 'Used' or [status] is null)";

                    using (var command = new SqlCommand(sql_command, conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }

            // reset each proxy that needed

            List<string> resetproxies = new List<string>();
            List<Thread> threads = new List<Thread>();

            using (SqlConnection conn = new SqlConnection(DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("select * from proxytb where [status]='Updating'", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            resetproxies.Add(dataReader["proxy_str"].ToString());
                        }
                    }
                }


                foreach (var item in resetproxies)
                {
                    using (var command = new SqlCommand("update proxytb set [status]='Reseting' where proxy_str='" + item + "' and [status]='Updating'", conn))
                    {
                        command.ExecuteNonQuery();

                        //Update proxy multi thread

                        Thread thread = new Thread(() => ResetEachProxy(item));
                        threads.Add(thread);
                    }
                }
            }

            foreach (var t in threads)
            {
                t.Start();
            }
        }

        static public bool ResetEachProxy(string proxy_str, string type = "engage")
        {
            RestClient client = new RestClient("http://192.168.100.6:10000/");

            var request = new RestRequest("reset?proxy=" + proxy_str, Method.GET);

        ResetProxy:
            var query = client.Execute(request);

            //Check condition if proxy is ready

            Thread.Sleep(5000);

            Stopwatch watch_Reset = new Stopwatch();
            watch_Reset.Start();

            while (true)
            {
                if (watch_Reset.Elapsed.TotalMinutes > 3)
                {
                    Thread.Sleep(30000);
                    watch_Reset.Restart();
                }

                var request_proxy_data = new RestRequest("proxy_list", Method.GET);

                var query_proxy_data = client.Execute(request_proxy_data);

                if (query_proxy_data.StatusCode.ToString() != "OK")
                    continue;

                var json_proxies = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(query_proxy_data.Content);

                var port = proxy_str.Split(':')[1];

                bool is_changed = false;

                foreach (var proxy in json_proxies)
                {
                    if (proxy["proxy_port"].Value.ToString() == port)
                    {
                        if (proxy["public_ip"].Value != null)
                        {
                            if (proxy["public_ip"].Value.ToString().Contains("1") && proxy["public_ip"].Value.ToString().Contains("."))
                            {
                                is_changed = true;
                            }
                        }

                    }
                }

                if (is_changed)
                    break;

                Thread.Sleep(2000);
            }

            //check if work with fb and ig

            if (!IsWorkFB(proxy_str))
                goto ResetProxy;

            //update ready status to db

            using (SqlConnection conn = new SqlConnection(DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("update proxytb set [status]='Ready', used=0 where [proxy_str]='" + proxy_str + "'", conn))
                {
                    command.ExecuteNonQuery();
                }
            }

            return true;
        }

        static public bool IsWorkFB(string proxy_str)
        {
            RestClient client = new RestClient(new Uri("https://www.facebook.com/"));

            client.Timeout = 5000;
            client.ReadWriteTimeout = 10000;

            //client.Timeout = 30000;

            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;

            client.Proxy = new WebProxy(proxy_str.Split(':')[0], int.Parse(proxy_str.Split(':')[1]));

            CookieContainer _cookieJar = new CookieContainer();
            client.CookieContainer = _cookieJar;

            var request = new RestRequest("", Method.GET);

            request.AddHeader("Host", "www.facebook.com");
            request.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0");
            request.AddHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            request.AddHeader("Accept-Language", "en-US,en;q=0.5");
            request.AddHeader("Accept-Encoding", "gzip, deflate, br");
            request.AddHeader("Connection", "keep-alive");
            request.AddHeader("Upgrade-Insecure-Requests", "1");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            var task = Task.Run(() =>
            {
                return client.Execute(request);
            });
            if (!task.Wait(TimeSpan.FromSeconds(10)))
                return false;

            var query_load = task.Result;

            if (query_load.StatusCode.ToString() != "OK")
                return false;
            return true;

        }
    }
}
