﻿//using InstagramApiSharp;
//using InstagramApiSharp.API.Builder;
//using InstagramApiSharp.Classes;
//using InstagramApiSharp.Logger;
//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace IGAPI___Ramtinak.API
//{
//    static class IGHelpers
//    {
//        const string state_Location = @"E:\Google Drive\LDPlayerData\scraper-cookie\";
//        static public InstagramApiSharp.API.IInstaApi Login(string username, string password, dynamic proxyinfo)
//        {
//            InstagramApiSharp.API.IInstaApi _instaApi;

//            var userSession = new UserSessionData
//            {
//                UserName = username,
//                Password = password
//            };

//            if (proxyinfo != null)
//            {
//                var proxy = new WebProxy()
//                {
//                    Address = new Uri("http://" + proxyinfo.proxystr), //i.e: http://1.2.3.4.5:8080
//                    BypassProxyOnLocal = false,
//                    UseDefaultCredentials = false,

//                    //// *** These creds are given to the proxy server, not the web server ***
//                    //Credentials = new NetworkCredential(
//                    //userName: proxyinfo.user,
//                    //password: proxyinfo.pass)
//                };

//                // Now create a client handler which uses that proxy
//                var httpClientHandler = new HttpClientHandler()
//                {
//                    Proxy = proxy,
//                };

//                var httpClient = new HttpClient(handler: httpClientHandler, disposeHandler: true)
//                {
//                    // if you want to use httpClient, you should set this
//                    BaseAddress = new Uri("https://i.instagram.com/")
//                };

//                // create new InstaApi instance using Builder
//                _instaApi = InstaApiBuilder.CreateBuilder()
//                    .SetUser(userSession)
//                    .UseLogger(new DebugLogger(LogLevel.All)) // use logger for requests and debug messages
//                    .UseHttpClient(httpClient)
//                    .UseHttpClientHandler(httpClientHandler)
//                    .Build();
//            }
//            else
//            {
//                _instaApi = InstaApiBuilder.CreateBuilder()
//                    .SetUser(userSession)
//                    .UseLogger(new DebugLogger(LogLevel.All)) // use logger for requests and debug messages
//                    .Build();
//            }


//            string stateFile = state_Location + username + "_state.bin";
//            try
//            {
//                // load session file if exists
//                if (File.Exists(stateFile))
//                {
//                    Console.WriteLine("Loading state from file");
//                    using (var fs = File.OpenRead(stateFile))
//                    {
//                        _instaApi.LoadStateDataFromStream(fs);
//                        // in .net core or uwp apps don't use LoadStateDataFromStream
//                        // use this one:
//                        // _instaApi.LoadStateDataFromString(new StreamReader(fs).ReadToEnd());
//                        // you should pass json string as parameter to this function.
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                Console.WriteLine(ex);
//            }

//            if (!_instaApi.IsUserAuthenticated)
//            {
//                // login
//                Console.WriteLine($"Logging in as {userSession.UserName}");
//                var task = Task.Run(() =>
//                {
//                    return _instaApi.LoginAsync();
//                });
//                task.Wait();
//                var logInResult = task.Result;
//                if (!logInResult.Succeeded)
//                {
//                    Console.WriteLine($"Unable to login: {logInResult.Info.Message}");
//                    return null;
//                }
//                else
//                {
//                    if (logInResult.Value == InstaLoginResult.ChallengeRequired)
//                    {
//                        var task_challenge = Task.Run(() =>
//                        {
//                            return _instaApi.GetChallengeRequireVerifyMethodAsync();
//                        });
//                        task_challenge.Wait();
//                        var challenge = task_challenge.Result;
//                        if (challenge.Succeeded)
//                        {

//                            if (challenge.Value.SubmitPhoneRequired)
//                            {
//                            }
//                            else
//                            {
//                                if (challenge.Value.StepData != null)
//                                {
//                                    if (!string.IsNullOrEmpty(challenge.Value.StepData.Email))
//                                    {
//                                        // send verification code to email
//                                        var task_email = Task.Run(() =>
//                                        {
//                                            return _instaApi.RequestVerifyCodeToEmailForChallengeRequireAsync();
//                                        });
//                                        var email = task_email.GetAwaiter().GetResult();
//                                        if (email.Succeeded)
//                                        {
//                                            Thread.Sleep(20000);
//                                            throw new NotImplementedException();
//                                            string verify_code = "097568";
//                                            try
//                                            {
//                                                // Note: calling VerifyCodeForChallengeRequireAsync function, 
//                                                // if user has two factor enabled, will wait 15 seconds and it will try to
//                                                // call LoginAsync.

//                                                var task_verifyLogin = Task.Run(() =>
//                                                {
//                                                    return _instaApi.VerifyCodeForChallengeRequireAsync(verify_code);
//                                                });
//                                                var verifyLogin = task_verifyLogin.GetAwaiter().GetResult();

//                                                if (verifyLogin.Succeeded)
//                                                {
//                                                    // Save session
//                                                    //SaveSession();
//                                                }

//                                            }
//                                            catch (Exception ex) { }
//                                        }
//                                    }
//                                }

//                            }
//                        }
//                    }
//                }
//                // save session in file
//                var state = _instaApi.GetStateDataAsStream();
//                // in .net core or uwp apps don't use GetStateDataAsStream.
//                // use this one:
//                // var state = _instaApi.GetStateDataAsString();
//                // this returns you session as json string.
//                using (var fileStream = File.Create(stateFile))
//                {
//                    state.Seek(0, SeekOrigin.Begin);
//                    state.CopyTo(fileStream);
//                }
//            }

//            return _instaApi;
//        }

//        static public dynamic GetAccount()
//        {
//        GetAccount:
//            var account = DBHelpers.GetAccountToScrape();

//            if (account == null)
//                return null;

//            var proxy_str = TinSoftHelpers.GetProxy();

//            var proxyinfo = new
//            {
//                proxystr = proxy_str
//            };


//            var _instaApi = Login(account.username, account.password, proxyinfo);

//            if (_instaApi == null)
//            {
//                goto GetAccount;
//            }

//            var dyn = new
//            {
//                _instaApi,
//                account,
//                proxyinfo
//            };


//            return dyn;
//        }

//        #region --Scrape Posts--

//        static public string ScrapeComment(string post_url, InstagramApiSharp.API.IInstaApi _instaApi = null)
//        {
//            if (_instaApi == null)
//            {
//                _instaApi = (GetAccount())._instaApi;
//            }

//            var currentUser = Task.Run(_instaApi.GetCurrentUserAsync).GetAwaiter().GetResult();

//            var task_media_id = Task.Run(() =>
//            {
//                return _instaApi.MediaProcessor.GetMediaIdFromUrlAsync(new Uri(post_url));
//            });

//            var media_id = task_media_id.GetAwaiter().GetResult().Value.ToString();

//            var task_commenters = Task.Run(() =>
//            {
//                return _instaApi.CommentProcessor.GetMediaCommentsAsync(media_id, PaginationParameters.MaxPagesToLoad(5));
//            });

//            var commenters = task_commenters.GetAwaiter().GetResult().Value.Comments;

//            var items = commenters.Where(x => !x.Text.Contains("\\")).OrderBy(s => s.LikesCount).Skip(10).Take(20).ToList<InstagramApiSharp.Classes.Models.InstaComment>();

//            //Choose random comment content

//            var choiced = (new Random()).Next(items.Count);

//            var comment_content = items[choiced].Text;

//            return comment_content;
//        }

//        static public List<InstagramApiSharp.Classes.Models.InstaUserShort> ScrapeLikers(string post_url, InstagramApiSharp.API.IInstaApi _instaApi = null)
//        {
//            if (_instaApi == null)
//                _instaApi = (GetAccount())._instaApi;


//            var task_media_id = Task.Run(() =>
//            {
//                return _instaApi.MediaProcessor.GetMediaIdFromUrlAsync(new Uri(post_url));
//            });

//            var media_id = task_media_id.GetAwaiter().GetResult().Value.ToString();


//            var task_likers = Task.Run(() =>
//            {
//                return _instaApi.MediaProcessor.GetMediaLikersAsync(media_id);
//            });

//            var likers = task_likers.GetAwaiter().GetResult().Value;

//            return likers;
//        }


//        static public InstagramApiSharp.Classes.Models.InstaCommentList ScrapeCommenters(string post_url, InstagramApiSharp.API.IInstaApi _instaApi = null)
//        {
//            if (_instaApi == null)
//                _instaApi = (GetAccount())._instaApi;

//            var task_media_id = Task.Run(() =>
//            {
//                return _instaApi.MediaProcessor.GetMediaIdFromUrlAsync(new Uri(post_url));
//            });

//            var media_id = task_media_id.GetAwaiter().GetResult().Value.ToString();

//            var task_commenters = Task.Run(() =>
//            {
//                return _instaApi.CommentProcessor.GetMediaCommentsAsync(media_id, PaginationParameters.MaxPagesToLoad(5));
//            });

//            var commenters = task_commenters.GetAwaiter().GetResult().Value;

//            return commenters;
//        }

//        static public List<InstagramApiSharp.Classes.Models.InstaMedia> ScrapeMediasFromUser(InstagramApiSharp.API.IInstaApi _instaApi, string username, int maxpages = 1)
//        {
//            //var _instaApi = GetAccount();

//            var task_medias = Task.Run(() =>
//            {
//                return _instaApi.UserProcessor.GetUserMediaAsync(username, PaginationParameters.MaxPagesToLoad(maxpages));
//            });

//            var medias = task_medias.GetAwaiter().GetResult().Value;

//            return medias;
//        }

//        static public InstagramApiSharp.Classes.Models.InstaSectionMedia ScrapeTopMediaHashTags(InstagramApiSharp.API.IInstaApi _instaApi, string hashtag, int maxpages = 1)
//        {
//            //var _instaApi = GetAccount();

//            var task_medias = Task.Run(() =>
//            {
//                return _instaApi.HashtagProcessor.GetTopHashtagMediaListAsync(hashtag, PaginationParameters.MaxPagesToLoad(maxpages));
//            });

//            var medias = task_medias.GetAwaiter().GetResult().Value;

//            return medias;
//        }

//        static public void GetMediaFromUrlAsync(string post_url)
//        {
//            var _instaApi = GetAccount();


//            var task_mediaId = Task.Run(() =>
//            {
//                return _instaApi.MediaProcessor.GetMediaIdFromUrlAsync(new Uri(post_url));
//            });

//            var mediaId = task_mediaId.GetAwaiter().GetResult().Value;

//            var task_media = Task.Run(() =>
//            {
//                return _instaApi.MediaProcessor.GetMediaByIdAsync(mediaId);
//            });

//            var media = task_media.GetAwaiter().GetResult().Value;

//        }

//        #endregion

//        #region --Scrape Follower, Following--

//        static public void ScrapeFollowerByUsername(string username)
//        {
//            var _instaApi = GetAccount();

//            var task_scrape_followers = Task.Run(() =>
//            {
//                return _instaApi.UserProcessor.GetUserFollowersAsync(username, PaginationParameters.MaxPagesToLoad(1));
//            });

//            var users = task_scrape_followers.GetAwaiter().GetResult().Value;

//        }


//        #endregion

//        #region --User--

//        static public void ScrapeSuggestionUser(string username)
//        {
//            var _instaApi = GetAccount();

//            var task_user_detail = Task.Run(() =>
//            {
//                return _instaApi.UserProcessor.GetUserAsync(username);
//            });

//            var user_detail = task_user_detail.GetAwaiter().GetResult().Value;

//            long userId = user_detail.Pk;

//            var task_usersuggest = Task.Run(() =>
//            {
//                return _instaApi.UserProcessor.GetSuggestionDetailsAsync(new long[] { userId });
//            });

//            var users = task_usersuggest.GetAwaiter().GetResult().Value;

//        }


//        static public string ScrapeFullUserInfo(string userId, InstagramApiSharp.API.IInstaApi _instaApi = null)
//        {
//            if (_instaApi == null)
//                _instaApi = (GetAccount())._instaApi;
//            int retry = 0;
//        GetFullUserInfo:

//            var task_user_fullinfo = Task.Run(() =>
//            {
//                return _instaApi.UserProcessor.GetFullUserInfoAsync(long.Parse(userId));
//            });

//            var user_full = task_user_fullinfo.GetAwaiter().GetResult().Value;

//            if (user_full == null)
//            {
//                //Thread.Sleep(10000);
//                //retry++;
//                //if (retry > 5)
//                return "user_full is null";
//                //goto GetFullUserInfo;
//            }

//            var Biography = user_full.UserDetail.Biography;
//            var FollowerCount = user_full.UserDetail.FollowerCount;
//            var FollowingCount = user_full.UserDetail.FollowingCount;
//            var MediaCount = user_full.UserDetail.MediaCount;
//            var FeedCount = user_full.Feed.Items.Count;
//            double AVGComment = -1, AVGLike = -1;
//            string RecentPost = "";
//            if (user_full.Feed.Items.Count > 0)
//            {
//                AVGComment = System.Math.Round(user_full.Feed.Items.Average(c => int.Parse(c.CommentsCount)), 3);
//                AVGLike = System.Math.Round(user_full.Feed.Items.Average(c => c.LikesCount), 3);
//                RecentPost = user_full.Feed.Items[0].TakenAt.ToString();
//            }

//            var CurrentStories = user_full.UserStory.Reel.Items.Count;

//            API.DBHelpers.UpdateScrapeUserData(long.Parse(userId), "Biography", Biography);
//            API.DBHelpers.UpdateScrapeUserData(long.Parse(userId), "FollowerCount", FollowerCount.ToString());
//            API.DBHelpers.UpdateScrapeUserData(long.Parse(userId), "FollowingCount", FollowingCount.ToString());
//            API.DBHelpers.UpdateScrapeUserData(long.Parse(userId), "MediaCount", MediaCount.ToString());
//            API.DBHelpers.UpdateScrapeUserData(long.Parse(userId), "FeedCount", FeedCount.ToString());
//            API.DBHelpers.UpdateScrapeUserData(long.Parse(userId), "AVGComment", AVGComment.ToString());
//            API.DBHelpers.UpdateScrapeUserData(long.Parse(userId), "AVGLike", AVGLike.ToString());
//            var engage_rate = AVGLike == -1 ? 0 : (AVGLike / FollowerCount * 100);
//            API.DBHelpers.UpdateScrapeUserData(long.Parse(userId), "engage_rate", engage_rate.ToString());

//            if (RecentPost != "")
//                API.DBHelpers.UpdateScrapeUserData(long.Parse(userId), "RecentPost", RecentPost);
//            API.DBHelpers.UpdateScrapeUserData(long.Parse(userId), "CurrentStories", CurrentStories.ToString());

//            return "Success";
//        }

//        static public void ScrapeFullUserInfoByUrl(string username, InstagramApiSharp.API.IInstaApi _instaApi = null)
//        {
//            if (_instaApi == null)
//                _instaApi = (GetAccount())._instaApi;

//            var task_user_detail = Task.Run(() =>
//            {
//                return _instaApi.UserProcessor.GetUserAsync(username);
//            });

//            var user_detail = task_user_detail.GetAwaiter().GetResult().Value;

//            long userId = user_detail.Pk;

//            var task_user_fullinfo = Task.Run(() =>
//            {
//                return _instaApi.UserProcessor.GetFullUserInfoAsync(userId);
//            });

//            var user_full = task_user_fullinfo.GetAwaiter().GetResult().Value;
//        }

//        #endregion

//        #region --CheckUserName--

//        static public bool IsUsernameNotExisted(InstagramApiSharp.API.IInstaApi _instaApi, string username)
//        {


//            //if (_instaApi == null)
//            //    _instaApi = (GetAccount())._instaApi;


//            var task_checkuser = Task.Run(() =>
//            {
//                return _instaApi.CheckUsernameAsync(username);
//            });

//            var checked_user = task_checkuser.GetAwaiter().GetResult().Value.Available;

//            return checked_user;
//        }


//        #endregion

//        #region --Hashtag--

//        static public void ScrapeHashtagInfo(string hashtag, InstagramApiSharp.API.IInstaApi _instaApi = null)
//        {
//            if (_instaApi == null)
//            {
//                _instaApi = (GetAccount())._instaApi;
//            }

//            var task_hashtag_info = Task.Run(() =>
//            {
//                return _instaApi.HashtagProcessor.GetHashtagInfoAsync(hashtag);
//            });

//            var hashtag_value = task_hashtag_info.GetAwaiter().GetResult().Value;

//            //var hashtag_info = hashtag_value.Value;

//        }

//        #endregion

//        static public void Delay(int ms_from = 200, int ms_to = 500)
//        {
//            Thread.Sleep((new Random()).Next(ms_from, ms_to));
//        }
//    }
//}
