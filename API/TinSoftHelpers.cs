﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.API
{
    static class TinSoftHelpers
    {
        static public void StartDriver()
        {
            ProcessStartInfo startInfo = new ProcessStartInfo("Tinsoft Proxy Driver.exe");
            startInfo.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
            //startInfo.Arguments = "hidden";
            Process.Start(startInfo);
        }

        //     command

        //     get_status   	// Kiểm tra mode của driver là Auto hay manual

        //         start_auto      // Bật chế độ auto
        //         stop_auto		//Tắt auto chuyển qua Manual

        //         add_key[key]|[location_id]  // Thêm key vào driver, nếu ko truyền theo location_id thì mặc định random, Ví dụ: add_key TLXXX|1
        //     set_max_use[number]         // Dùng cho chế độ auto, giới hạn tối đa số luồng trên 1 IP. VD: set_max_use 3
        //        set_min_timeout[number]		// Dùng cho chế độ auto, giới hạn timeout tối thiểu cho driver (giây) khi cấp IP. VD: set_min_timeout 120

        //         get_proxy hoặc get_proxy random 	// Dùng cho chế độ auto, lấy ra 1 proxy từ driver

        //         set_thread_stop[proxy]          // Dùng cho chế độ auto, Khi một luồng hoàn thành, hãy báo cho driver biết để thư viện tự tính toán kiểm soát. 
        //         Ví dụ: set_thread_stop 192.168.9.5:5655
        ////Lưu ý: Một Thread sẽ bắt đầu bằng get_proxy và kêt thúc bằng set_thread_stop

        //change_proxy_by_key[key]        // Dùng chính cho chế độ manual, auto cũng có thể dùng được nhưng tốt nhất hãy để driver tự tính toán và đổi.
        //         change_all						// Dùng chính cho chế độ manual, auto cũng có thể dùng được nhưng tốt nhất hãy để driver tự tính toán và đổi.

        //         get_list_proxies                //	Dùng chính cho chế độ manual, trả về danh sách proxy hiện tại.
        //         hide_form						// Ẩn Form chính.

        //         show_form                       // Hiện Form chính.
        //         clear_keys						// Xóa tất cả các keys được add vào driver

        static public string ExecuteCommand(string command)
        {
            System.Net.Sockets.TcpClient clientSocket = new System.Net.Sockets.TcpClient();
            clientSocket.Connect("127.0.0.1", 888);
            NetworkStream serverStream = clientSocket.GetStream();

            byte[] outStream = System.Text.Encoding.ASCII.GetBytes(command);
            serverStream.Write(outStream, 0, outStream.Length);
            serverStream.Flush();

            byte[] inStream = new byte[1024];
            serverStream.Read(inStream, 0, inStream.Length);
            string returndata = System.Text.Encoding.ASCII.GetString(inStream);
            returndata = returndata.Replace("\0", ""); // Giá trị trả về

            return returndata;
        }



        static public void SettingUp()
        {
            //start driver

            API.TinSoftHelpers.StartDriver();

            //add key

            var api_keys = API.DBHelpers.ListAPIKey();

            foreach(var item in api_keys)
            {
                var result = API.TinSoftHelpers.ExecuteCommand("add_key " + item + "|3");

                if (!result.Contains("success\":true"))
                    throw new Exception();
            }


            //change all

            var query = API.TinSoftHelpers.ExecuteCommand("change_all");

            //set timeout

            API.TinSoftHelpers.ExecuteCommand("set_min_timeout 125");

            //set max used

            API.TinSoftHelpers.ExecuteCommand("set_max_use 3");

            //set auto

            API.TinSoftHelpers.ExecuteCommand("start_auto");


            //change by apikey

            query = API.TinSoftHelpers.ExecuteCommand("change_proxy_by_key TLElwvlJjdIYOKWWVK03HeWhsML8ypIYM0Wqon");

            //get_proxy

            query = API.TinSoftHelpers.ExecuteCommand("get_proxy");

        }


        static public string ChangeProxy(string apiKey, string location = "3")
        {
            //Check if meet requirement about next_change

            if (!API.DBHelpers.IsCanChange(apiKey))
                return "";

            RestClient client = new RestClient("http://proxy.tinsoftsv.com/api/");

            var request = new RestRequest("changeProxy.php?key=" + apiKey + "&location=" + location, Method.GET);

            var query = client.Execute(request);

            //Check if success

            string proxy_str = "";

            var json = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(query.Content);

            if (json.success == "true")
            {
                proxy_str = json.proxy;
            }

            if(proxy_str!="")
            {
                API.DBHelpers.UpdateTinSoft(apiKey, "proxy_str", "'" + proxy_str + "'");
                API.DBHelpers.UpdateTinSoft(apiKey, "used", "0");
                API.DBHelpers.UpdateTinSoft(apiKey, "current_used", "0");
                API.DBHelpers.UpdateTinSoft(apiKey, "status", "'Ready'");
            }

            //update last_request to apiKey
            API.DBHelpers.UpdateTinSoft(apiKey, "last_request", "getdate()");

            return query.Content;
        }

        static public string CheckProxy(string apiKey)
        {
            RestClient client = new RestClient("http://proxy.tinsoftsv.com/api/");

            var request = new RestRequest("getProxy.php?key=" + apiKey, Method.GET);

            var query = client.Execute(request);

            return query.Content;
        }

        static public void TinSoftServer(bool first = false)
        {
            var api_keys = new List<string>();
            if (first)
            {
                api_keys = API.DBHelpers.ListAPIKey();
            }
            //get proxy need reset
            else
                api_keys = API.DBHelpers.ProxiesNeedChange();


            foreach (var item in api_keys)
            {
                Thread thread = new Thread(() => ChangeProxy(item));
                thread.Name = "tinsoft_" + item;
                thread.SetApartmentState(ApartmentState.STA);
                thread.IsBackground = true;
                thread.Start();

                Thread.Sleep(1000);
            }

        }

        static public string GetProxy()
        {
            var proxy_str = "";
            Mutex m = new Mutex(false, "get_tinsoft");
            m.WaitOne();

            Thread.Sleep(5000);

            while (true)
            {
                proxy_str = API.DBHelpers.TinSoft_GetProxy();

                if (proxy_str == "")
                {
                    Thread.Sleep(10000);
                }
                else break;
            }


            m.ReleaseMutex();

            return proxy_str;
        }


        static public void ReleaseProxy(string proxy)
        {
            Mutex m = new Mutex(false, "release_tinsoft");
            m.WaitOne();



            API.DBHelpers.TinSoft_ReleaseProxy(proxy);


            m.ReleaseMutex();
        }
    }
}
