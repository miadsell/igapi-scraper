﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.API
{
    static class DBHelpers
    {
        static public string db_connection_igtoolkit = @"Data Source=@server_ip;Initial Catalog=@igtoolkit;User ID=sa;Password=123456789kdL;Pooling=false;Max Pool Size=150;Min Pool Size=50;Connect Timeout=30;Connection Lifetime=300;ConnectRetryCount=3";
        static public string db_connection_instagram = @"Data Source=@server_ip;Initial Catalog=@instagramdb;User ID=sa;Password=123456789kdL;Pooling=false;Max Pool Size=150;Min Pool Size=50;Connect Timeout=30;Connection Lifetime=300;ConnectRetryCount=3";
        static public string aio_ig_support = @"Data Source=192.168.1.9;Initial Catalog=aio_support_ig;User ID=sa;Password=123456789kdL;Pooling=true;Max Pool Size=150;Min Pool Size=50;Connect Timeout=30;Connection Lifetime=300;ConnectRetryCount=3";

        static public string igtoolkit_dbname = "";
        static public string instagram_dbname = "";

        static public int threads = 5;

        static public void SettingUpWhenLoad()
        {
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                //clear claimed status in scrape accounts

                using (var command = new SqlCommand("update accounts set [status]=''", conn))
                {
                    command.ExecuteNonQuery();
                }

                //clear hashtag claimed status
                using (var command = new SqlCommand("update hashtags set [status]=''", conn))
                {
                    command.ExecuteNonQuery();
                }
            }

            //set threads automatically base on proxy max_thread
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                //clear claimed status in scrape accounts

                using (var command = new SqlCommand("select sum([max_thread]) from tinsoft_tb", conn))
                {
                    var value = command.ExecuteScalar().ToString();
                    threads = int.Parse(value == "" ? "0" : value.ToString());
                }
            }

        }

        #region --Post Table--

        static public void AddPostToDB(List<dynamic> posturl, string scrape_type, string from_account, bool ischecked = false, bool ishashtag = false)
        {
            var sql_command = "insert into posts (posturl, creation_date) values (@url, @date)";

            if (ischecked)
            {
                sql_command = "insert into posts (posturl, creation_date, ok, checked, scrape_type, from_account) values (@url, @date, 'OK', 'Checked', '" + scrape_type + "', '" + from_account + "')";
            }

            if (!ischecked && ishashtag)
            {
                sql_command = "insert into posts (posturl, creation_date, fromhashtag, scrape_type, from_account) values (@url, @date,'yes','" + scrape_type + "', '" + from_account + "')";
            }

            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                foreach (var item in posturl)
                {
                    try
                    {
                        using (SqlCommand command = new SqlCommand(sql_command, conn))
                        {
                            command.Parameters.Add("@url", System.Data.SqlDbType.VarChar).Value = item.url;
                            command.Parameters.Add("@date", System.Data.SqlDbType.VarChar).Value = item.date;
                            command.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }

        #endregion

        #region --Scrape Users Table

        static public void AddScrapeUserToDB(InstagramApiSharp.Classes.Models.InstaUserShort instaUserShort, string source, string from_account)
        {
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("igtoolkit_AddScrapeUsers", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    command.Parameters.Add("@username", System.Data.SqlDbType.VarChar).Value = instaUserShort.UserName;
                    command.Parameters.Add("@source", System.Data.SqlDbType.VarChar).Value = source;
                    command.Parameters.Add("@from_account", System.Data.SqlDbType.VarChar).Value = from_account;

                    command.ExecuteNonQuery();
                }
            }

            //Update value to db for this pk

            //UpdateScrapeUserData(instaUserShort.Pk, "IsVerified", instaUserShort.IsVerified.ToString());
            //UpdateScrapeUserData(instaUserShort.Pk, "IsPrivate", instaUserShort.IsPrivate.ToString());
            //UpdateScrapeUserData(instaUserShort.Pk, "ProfilePicture", instaUserShort.ProfilePicUrl.ToString());
            //UpdateScrapeUserData(instaUserShort.Pk, "UserName", instaUserShort.UserName.ToString());
            //UpdateScrapeUserData(instaUserShort.Pk, "FullName", instaUserShort.FullName.ToString());

            UpdateScrapeUserShortByUser(instaUserShort.UserName, "IsVerified", instaUserShort.IsVerified.ToString());
            UpdateScrapeUserShortByUser(instaUserShort.UserName, "IsPrivate", instaUserShort.IsPrivate.ToString());
            UpdateScrapeUserShortByUser(instaUserShort.UserName, "ProfilePicture", instaUserShort.ProfilePicUrl.ToString());
            UpdateScrapeUserShortByUser(instaUserShort.UserName, "pk", instaUserShort.Pk.ToString());
            UpdateScrapeUserShortByUser(instaUserShort.UserName, "FullName", instaUserShort.FullName.ToString());
        }

        static public void AddScrapeUserToDB_ByList(List<InstagramApiSharp.Classes.Models.InstaUserShort> instaUserShorts, string source, string from_account)
        {
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();


                PLAutoHelper.SQLHelper.Execute(() =>
                {
                    using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                    {
                        foreach (var instaUser in instaUserShorts)
                        {

                            ////lock table
                            //var command_query = "select count(*) from scrapeusers with (tablockx)";

                            //using (SqlCommand command = new SqlCommand(command_query, conn, sqlTransaction))
                            //{
                            //    command.ExecuteScalar();
                            //}
                            //check if user is existed in db
                            int count = 0;

                            var command_query =
                                @"select count([UserName]) from scrapeusers
                            where [UserName]='" + instaUser.UserName + "'";

                            using (SqlCommand command = new SqlCommand(command_query, conn, sqlTransaction))
                            {
                                count = int.Parse(command.ExecuteScalar().ToString());
                            }

                            if (count == 1)
                            {
                                //increase freq
                                command_query =
                                    @"update scrapeusers set [frequency]=[frequency]+1, latest_added=getdate() where [UserName]='" + instaUser.UserName + "'";

                                using (SqlCommand command = new SqlCommand(command_query, conn, sqlTransaction))
                                {
                                    command.ExecuteNonQuery();
                                }

                                //check if [source] contains source

                                command_query = "select [source] from scrapeusers where [UserName]='" + instaUser.UserName + "'";

                                string source_value = "";

                                using (SqlCommand command = new SqlCommand(command_query, conn, sqlTransaction))
                                {
                                    source_value = command.ExecuteScalar().ToString();
                                }

                                if (!source_value.Contains(source))
                                {
                                    //append source to [source]
                                    command_query =
                                        @"update scrapeusers set [source]=[source]+@source+'|'
		                            where [UserName]='" + instaUser.UserName + "'";

                                    using (SqlCommand command = new SqlCommand(command_query, conn, sqlTransaction))
                                    {
                                        command.Parameters.Add("@source", System.Data.SqlDbType.VarChar).Value = source;
                                        command.ExecuteNonQuery();
                                    }
                                }
                            }
                            else
                            {
                                //insert to db
                                command_query = @"insert into scrapeusers ([UserName],[source], [from_account]) values (@username, @source + '|', @from_account)";

                                using (SqlCommand command = new SqlCommand(command_query, conn, sqlTransaction))
                                {
                                    command.Parameters.Add("@username", System.Data.SqlDbType.VarChar).Value = instaUser.UserName;
                                    command.Parameters.Add("@source", System.Data.SqlDbType.VarChar).Value = source;
                                    command.Parameters.Add("@from_account", System.Data.SqlDbType.VarChar).Value = from_account;

                                    command.ExecuteNonQuery();
                                }

                                //update available property

                                command_query =
                                    @"update scrapeusers
                                    set
	                                    [IsVerified]=@IsVerified,
	                                    [IsPrivate]=@IsPrivate,
	                                    [ProfilePicture]=@ProfilePicture,
	                                    [pk]=@pk,
	                                    [FullName]=@FullName
                                    where [UserName]=@UserName";

                                using (SqlCommand command = new SqlCommand(command_query, conn, sqlTransaction))
                                {
                                    command.Parameters.Add("@IsVerified", System.Data.SqlDbType.VarChar).Value = instaUser.IsVerified;
                                    command.Parameters.Add("@IsPrivate", System.Data.SqlDbType.VarChar).Value = instaUser.IsPrivate;
                                    command.Parameters.Add("@ProfilePicture", System.Data.SqlDbType.VarChar).Value = instaUser.ProfilePicUrl;
                                    command.Parameters.Add("@pk", System.Data.SqlDbType.Float).Value = instaUser.Pk;
                                    command.Parameters.Add("@FullName", System.Data.SqlDbType.NVarChar).Value = instaUser.FullName;
                                    command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = instaUser.UserName;

                                    command.ExecuteNonQuery();
                                }
                            }

                        }


                        sqlTransaction.Commit();
                    }
                });
            }
        }
    

        static public void AddScrapeUsersToDB_ByUsername(string username, string source, string from_account)
        {
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("igtoolkit_AddScrapeUsers", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    command.Parameters.Add("@username", System.Data.SqlDbType.VarChar).Value = username;
                    command.Parameters.Add("@source", System.Data.SqlDbType.VarChar).Value = source;
                    command.Parameters.Add("@from_account", System.Data.SqlDbType.VarChar).Value = from_account;

                    command.ExecuteNonQuery();
                }
            }
        }

        static public void UpdateScrapeUserData(long pk, string col, string colvalue)
        {
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                int retry = 0;

                while (true)
                {
                    using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                    {

                        try
                        {
                            using (var command = new SqlCommand("update scrapeusers set [" + col + "]=@colvalue where [pk]=@pk", conn, sqlTransaction))
                            {

                                command.Parameters.Add("@pk", System.Data.SqlDbType.Float).Value = pk;

                                if (col == "FullName" || col == "Biography")
                                    command.Parameters.Add("@colvalue", System.Data.SqlDbType.NText).Value = colvalue;
                                else
                                if (col == "RecentPost")
                                {
                                    if (colvalue == "")
                                        command.Parameters.Add("@colvalue", System.Data.SqlDbType.DateTime).Value = DBNull.Value;
                                    else
                                        command.Parameters.Add("@colvalue", System.Data.SqlDbType.DateTime).Value = colvalue;
                                }
                                else
                                    command.Parameters.Add("@colvalue", System.Data.SqlDbType.VarChar).Value = colvalue;

                                command.ExecuteNonQuery();
                            }

                            sqlTransaction.Commit();
                        }
                        catch
                        {
                            sqlTransaction.Rollback();
                        }

                        retry++;
                        if (retry > 10)
                            throw new System.ArgumentException("Deadlock Error");
                    }
                }
            }
        }

        static public void UpdateScrapeUserShortByUser(string username, string col, string value)
        {
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();
                int retry = 0;

                while (true)
                {
                    using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                    {

                        try
                        {
                            var command_query = "update scrapeusers set [" + col + "]=@value where [UserName]=@username";

                            using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                            {
                                if (col == "FullName" || col == "Biography")
                                    command.Parameters.Add("@value", System.Data.SqlDbType.NText).Value = value;
                                else
                                if (col == "RecentPost")
                                {
                                    if (value == "")
                                        command.Parameters.Add("@value", System.Data.SqlDbType.DateTime).Value = DBNull.Value;
                                    else
                                        command.Parameters.Add("@value", System.Data.SqlDbType.DateTime).Value = value;
                                }
                                else
                                if (col == "pk")
                                {
                                    command.Parameters.Add("@value", System.Data.SqlDbType.Float).Value = value;
                                }
                                else
                                    command.Parameters.Add("@value", System.Data.SqlDbType.VarChar).Value = value;

                                command.Parameters.Add("@username", System.Data.SqlDbType.VarChar).Value = username;
                                command.ExecuteNonQuery();
                            }

                            sqlTransaction.Commit();

                            break;
                        }
                        catch (Exception ex)
                        {
                            sqlTransaction.Rollback();
                        }

                        retry++;

                        if(retry>10)
                            throw new System.ArgumentException("Deadlock Error");
                    }
                }
            }
        }

        #endregion


        #region --Accounts Table--

        static public void AddTargetAccountToDB(List<string> account, bool ischecked = false)
        {
            var sql_command = "insert into target_accounts (ig_url) values (@account)";

            if (ischecked)
            {
                sql_command = "insert into target_accounts (ig_url, ok, checked) values (@account,'OK','Checked')";
            }

            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                foreach (var item in account)
                {
                    try
                    {
                        using (SqlCommand command = new SqlCommand(sql_command, conn))
                        {
                            command.Parameters.Add("@account", System.Data.SqlDbType.VarChar).Value = item;
                            command.ExecuteNonQuery();
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        static public void UpdateTargetAccounts_OK(string ig_url, string status)
        {


            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand("update target_accounts set [ok]='" + status + "',[checked]='Checked' where [ig_url]='" + ig_url + "'", conn))
                {
                    command.ExecuteNonQuery();
                }

            }
        }

        static public void UpdateTargetAccounts_ScrapeType(string ig_url, string scrape_type)
        {
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand("update target_accounts set [scrape_type]=@scrape_type where [ig_url]='" + ig_url + "'", conn))
                {
                    if (scrape_type == "")
                        command.Parameters.Add("@scrape_type", System.Data.SqlDbType.VarChar).Value = DBNull.Value;
                    else command.Parameters.Add("@scrape_type", System.Data.SqlDbType.VarChar).Value = scrape_type;
                    command.ExecuteNonQuery();
                }

            }
        }

        static public void UpdateTargetAccounts_State(string ig_url, string state)
        {
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand("update target_accounts set [state]=@state where [ig_url]='" + ig_url + "'", conn))
                {
                    if (state == "")
                        command.Parameters.Add("@state", System.Data.SqlDbType.VarChar).Value = DBNull.Value;
                    else command.Parameters.Add("@state", System.Data.SqlDbType.VarChar).Value = state;
                    command.ExecuteNonQuery();
                }

            }
        }

        static public void UpdateTargetAccounts_ScrapeFollower(string ig_url, string value)
        {
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand("update target_accounts set [scrape_follower]='" + value + "' where [ig_url]='" + ig_url + "'", conn))
                {
                    command.ExecuteNonQuery();
                }

            }
        }

        static public void AddSourceUsersToDB(List<string> account, bool ischecked = false)
        {
            var sql_command = "insert into source_users (ig_url) values (@account)";

            if (ischecked)
            {
                sql_command = "insert into source_users (ig_url, ok, checked) values (@account,'OK','Checked')";
            }

            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                foreach (var item in account)
                {
                    try
                    {
                        using (SqlCommand command = new SqlCommand(sql_command, conn))
                        {
                            command.Parameters.Add("@account", System.Data.SqlDbType.VarChar).Value = item;
                            command.ExecuteNonQuery();
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        static public void UpdateSourceUsers_OK(string ig_url, string status)
        {


            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand("update source_users set [ok]='" + status + "',[checked]='Checked' where [ig_url]='" + ig_url + "'", conn))
                {
                    command.ExecuteNonQuery();
                }

            }
        }

        #endregion


        #region --Hashtag--

        static public void AddHashtagToDB(List<string> hashtag, bool ischecked = false)
        {

            var sql_command = "insert into hashtags (hashtag) values (@hashtag)";

            if (ischecked)
            {
                sql_command = "insert into hashtags (hashtag, ok, checked) values (@hashtag,'OK','Checked')";
            }

            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                foreach (var item in hashtag)
                {
                    try
                    {
                        using (SqlCommand command = new SqlCommand(sql_command, conn))
                        {
                            command.Parameters.Add("@hashtag", System.Data.SqlDbType.NVarChar).Value = item;
                            command.ExecuteNonQuery();
                        }
                    }
                    catch
                    {
                        //tuc la hashtag da ton tai ==> increase repeat count
                        var command_query =
                            @"update hashtags set repeat_count=repeat_count+1 where [hashtag]=@hashtag";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@hashtag", System.Data.SqlDbType.NVarChar).Value = item;
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
        }


        #endregion


        static public dynamic GetAccountToScrape()
        {
            Mutex m = new Mutex(false, "igtoolkit_getaccount");
            m.WaitOne();
            dynamic account = null;
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("igtoolkit_GetAccount", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();

                            var username = dataReader["username"].ToString();
                            var password = dataReader["password"].ToString();
                            var profile = dataReader["profile"].ToString();
                            var time_per_session = int.Parse(dataReader["time_per_session"].ToString());

                            account = new
                            {
                                username,
                                password,
                                profile,
                                time_per_session
                            };
                        }
                    }
                }
            }

            m.ReleaseMutex();


            return account;
        }

        static public string GetValueFromAccountTb(string username, string col)
        {
            string value = null;
            var sql = "select " + col + " from accounts where [username]='" + username + "'";
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand(sql, conn))
                {
                    value = command.ExecuteScalar().ToString();
                }
            }

            return value;
        }

        static public void ReleaseScrapeAccount(dynamic dyn, bool setpause=false)
        {

            var username = dyn.account.username;
            var proxy = dyn.proxyinfo.proxystr;

            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("update accounts set [status]='' where [username]='" + username + "'", conn))
                {
                    command.ExecuteNonQuery();
                }

                if (setpause)
                {
                    using (var command = new SqlCommand("update accounts set [pause_until]='" + DateTime.Now.AddHours(3).ToString() + "' where [username]='" + username + "'", conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }

                using (var command = new SqlCommand("update proxytb set [used]=[used]-1 where [proxy_str]='" + proxy + "'", conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        static public dynamic GetTargetAccount()
        {
            dynamic dyn_account = null;
            Mutex m = new Mutex(false, "igtoolkit_targetaccount");
            m.WaitOne();
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("igtoolkit_TargetAccount", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();
                            var ig_url = dataReader["ig_url"].ToString();
                            var username = ig_url.Replace("https://www.instagram.com/", "").Replace("/", "");
                            var scrape_type = dataReader["scrape_type"].ToString();

                            dyn_account = new { username, scrape_type };
                        }
                    }
                }
            }

            m.ReleaseMutex();

            return dyn_account;
        }

        static public string GetHashtags()
        {
            string hashtag = null;
            Mutex m = new Mutex(false, "igtoolkit_targetaccount");
            m.WaitOne();
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("igtoolkit_Hashtags", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();
                            hashtag = dataReader["hashtag"].ToString();
                        }
                    }
                }
            }

            m.ReleaseMutex();


            return hashtag;
        }

        static public dynamic GetPostToScrape(string recent_days)
        {
            dynamic post = null;
            Mutex m = new Mutex(false, "igtoolkit_GetPostToScrape");
            m.WaitOne();
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("igtoolkit_GetTargetPost", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    command.Parameters.Add("@recent_days", System.Data.SqlDbType.Int).Value = recent_days;

                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();
                            var posturl = dataReader["posturl"].ToString();
                            var scrape_type = dataReader["scrape_type"].ToString();
                            var from_account = dataReader["from_account"].ToString();

                            post = new
                            {
                                posturl,
                                scrape_type,
                                from_account
                            };
                        }
                    }
                }
            }

            m.ReleaseMutex();

            return post;
        }

        static public string GetUserToScrapeInfo()
        {
            string pk = null;
            Mutex m = new Mutex(false, "igtoolkit_GetAccountToScrapeInfo");
            m.WaitOne();
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("igtoolkit_GetAccountToScrapeInfo", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();

                            pk = dataReader[0].ToString();
                        }
                    }
                }
            }

            m.ReleaseMutex();

            return pk;
        }

        static private object GetObjectValue(object ob, string name)
        {
            return ob.GetType().GetProperty(name).GetValue(ob, null);
        }

        static public void UpdateUserDetails_ByPK(string pk, dynamic user_details)
        {
            var Pk = GetObjectValue(user_details, "Pk");
            var IsVerified = GetObjectValue(user_details, "IsVerified");
            var IsPrivate = GetObjectValue(user_details, "IsPrivate");
            var ProfilePicture = GetObjectValue(user_details, "ProfilePicture");
            var FullName = GetObjectValue(user_details, "FullName");
            var Biography = GetObjectValue(user_details, "Biography");
            var FollowerCount = GetObjectValue(user_details, "FollowerCount");
            var FollowingCount = GetObjectValue(user_details, "FollowingCount");
            var MediaCount = GetObjectValue(user_details, "MediaCount");
            var FeedCount = GetObjectValue(user_details, "FeedCount");
            var AVGComment = GetObjectValue(user_details, "AVGComment");
            var AVGLike = GetObjectValue(user_details, "AVGLike");
            var RecentPost = GetObjectValue(user_details, "RecentPost");

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("update scrapeusers set [Biography]=@Biography,[pk]=@Pk,[IsVerified]=@IsVerified,[IsPrivate]=@IsPrivate,[ProfilePicture]=@ProfilePicture,[FullName]=@FullName,[FollowerCount]='" + FollowerCount + "',[FollowingCount]='" + FollowingCount + "',[MediaCount]='" + MediaCount + "',[FeedCount]='" + FeedCount + "',AVGComment='" + AVGComment + "',AVGLike='" + AVGLike + "',RecentPost='" + RecentPost + "' where [pk]=" + pk, conn))
                {
                    command.Parameters.Add("@Biography", System.Data.SqlDbType.NVarChar).Value = Biography;
                    command.Parameters.Add("@Pk", System.Data.SqlDbType.VarChar).Value = Biography;
                    command.Parameters.Add("@IsVerified", System.Data.SqlDbType.VarChar).Value = IsVerified;
                    command.Parameters.Add("@IsPrivate", System.Data.SqlDbType.VarChar).Value = IsPrivate;
                    command.Parameters.Add("@ProfilePicture", System.Data.SqlDbType.VarChar).Value = ProfilePicture;
                    command.Parameters.Add("@FullName", System.Data.SqlDbType.NVarChar).Value = FullName;
                    command.ExecuteNonQuery();
                }
            }

        }

        static public void UpdateUserDetails(string username, dynamic user_details)
        {
            var Pk = GetObjectValue(user_details, "Pk");
            var IsVerified = GetObjectValue(user_details, "IsVerified");
            var IsPrivate = GetObjectValue(user_details, "IsPrivate");
            var ProfilePicture = GetObjectValue(user_details, "ProfilePicture");
            var FullName = GetObjectValue(user_details, "FullName");
            var Biography = GetObjectValue(user_details, "Biography");
            var FollowerCount = GetObjectValue(user_details, "FollowerCount");
            var FollowingCount = GetObjectValue(user_details, "FollowingCount");
            var MediaCount = GetObjectValue(user_details, "MediaCount");
            var FeedCount = GetObjectValue(user_details, "FeedCount");
            var AVGComment = GetObjectValue(user_details, "AVGComment");
            var AVGLike = GetObjectValue(user_details, "AVGLike");
            var RecentPost = GetObjectValue(user_details, "RecentPost");

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("update scrapeusers set [Biography]=@Biography,[pk]=@Pk,[IsVerified]=@IsVerified,[IsPrivate]=@IsPrivate,[ProfilePicture]=@ProfilePicture,[FullName]=@FullName,[FollowerCount]='" + FollowerCount + "',[FollowingCount]='" + FollowingCount + "',[MediaCount]='" + MediaCount + "',[FeedCount]='" + FeedCount + "',AVGComment='" + AVGComment + "',AVGLike='" + AVGLike + "',RecentPost='" + RecentPost + "' where [UserName]=@username", conn))
                {
                    command.Parameters.Add("@Biography", System.Data.SqlDbType.NVarChar).Value = Biography;
                    command.Parameters.Add("@Pk", System.Data.SqlDbType.Float).Value = Pk;
                    command.Parameters.Add("@IsVerified", System.Data.SqlDbType.VarChar).Value = IsVerified;
                    command.Parameters.Add("@IsPrivate", System.Data.SqlDbType.VarChar).Value = IsPrivate;
                    command.Parameters.Add("@ProfilePicture", System.Data.SqlDbType.VarChar).Value = ProfilePicture;
                    command.Parameters.Add("@FullName", System.Data.SqlDbType.NVarChar).Value = FullName;
                    command.Parameters.Add("@username", System.Data.SqlDbType.VarChar).Value = username;
                    command.ExecuteNonQuery();
                }

            }

        }

        static public string GetProxy()
        {
            string proxy = null;
        GetProxy:
            Mutex m = new Mutex(false, "igtoolkit_proxy");
            m.WaitOne();
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("igtoolkit_GetProxy", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();
                            proxy = dataReader["proxy_str"].ToString();
                        }
                    }
                }
            }

            m.ReleaseMutex();

            if (proxy == null)
            {
                Thread.Sleep(10000);
                goto GetProxy;
            }


            return proxy;
        }

        static public void AddAPICallLog(string accountUser, string log)
        {
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("insert into apicall_log (accountUser,[log]) values ('" + accountUser + "','" + log + "')", conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        #region --AIOThread-TaskChecking--

        

        static public void IsAvailableBrowserTask()
        {

        }

        #endregion

        #region --TinSoft--

        static public List<string> ListAPIKey()
        {

            List<string> api_keys = new List<string>();

            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("select [api_key] from tinsoft_tb", conn))
                {
                    using(var dataReader=command.ExecuteReader())
                    {
                        while(dataReader.Read())
                        {
                            api_keys.Add(dataReader["api_key"].ToString());
                        }
                    }

                    
                }
            }

            return api_keys;
        }


        static public void UpdateTinSoft(string apiKey, string col, string value)
        {
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("update tinsoft_tb set [" + col + "]=" + value + " where api_key='" + apiKey + "'", conn))
                {
                    command.ExecuteNonQuery();

                }
            }
        }

        static public bool IsCanChange(string apiKey)
        {
            int min_next_change = 0;
            string last_request_str = "";
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("select [last_request], [min_next_change] from tinsoft_tb where api_key='"+apiKey+"'", conn))
                {
                    using(var dataReader=command.ExecuteReader())
                    {
                        dataReader.Read();
                        var last_request = dataReader["last_request"].ToString();

                        if (last_request != null)
                        {
                            last_request_str = last_request.ToString();
                        }

                        min_next_change = int.Parse(dataReader["min_next_change"].ToString());
                    }
                    

                }
            }

            if (last_request_str == "")
                return true;

            var subtract_time_seconds = DateTime.Now.Subtract(DateTime.Parse(last_request_str)).TotalSeconds;

            if (subtract_time_seconds > min_next_change)
                return true;

            return false;
        }


        static public List<string> ProxiesNeedChange()
        {
            List<string> api_keys = new List<string>();

            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("select [api_key] from tinsoft_tb where ([used]=[max_thread] and [current_used]=0 and (DATEDIFF(second,[last_request],getdate())>min_next_change or last_request is null))", conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            api_keys.Add(dataReader["api_key"].ToString());
                        }
                    }


                }
            }

            return api_keys;
        }

        static public string TinSoft_GetProxy()
        {
            string proxy_str = "";
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("update tinsoft_tb set [used]=[used]+1, current_used=[current_used]+1 output inserted.proxy_str where [proxy_str]=(select top 1 [proxy_str] from tinsoft_tb where [used]<max_thread)", conn))
                {
                    var proxy_str_obj = command.ExecuteScalar();

                    if (proxy_str_obj != null)
                    {
                        proxy_str = proxy_str_obj.ToString() == "0" ? "" : proxy_str_obj.ToString();
                    }


                }
            }

            return proxy_str;
        }

        static public void TinSoft_ReleaseProxy(string proxy_str)
        {
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("update tinsoft_tb set [current_used]=[current_used]-1 where [proxy_str]='" + proxy_str + "'", conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region --Source Users Details--

        static public string SourceUsers_GetAccountNeedScrapeInfo()
        {
            string profile_url = null;

            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = "select top 1 * from source_users where [follower] is null";

                using(var dataReader=command.ExecuteReader())
                {
                    if(dataReader.HasRows)
                    {
                        dataReader.Read();

                        profile_url = dataReader["ig_url"].ToString();
                    }
                }
            }

            return profile_url;
        }

        #endregion


        #region --COMMENTACCOUNT--

        static public void UpdateCommentAccount_OK(string ig_url, string status)
        {


            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand("update comment_accounts set [ok]='" + status + "',[checked]='Checked' where [ig_url]='" + ig_url + "'", conn))
                {
                    command.ExecuteNonQuery();
                }

            }
        }

        static public void AddCommentAccountToDB(List<string> account, bool ischecked = false)
        {
            var sql_command = "insert into comment_accounts (ig_url) values (@account)";

            if (ischecked)
            {
                sql_command = "insert into comment_accounts (ig_url, ok, checked) values (@account,'OK','Checked')";
            }

            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                foreach (var item in account)
                {
                    try
                    {
                        using (SqlCommand command = new SqlCommand(sql_command, conn))
                        {
                            command.Parameters.Add("@account", System.Data.SqlDbType.VarChar).Value = item;
                            command.ExecuteNonQuery();
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        static public void AddCommentPostToDB(List<dynamic> posturl)
        {
            var sql_command = "insert into comment_posts (posturl, creation_date) values (@url, @date)";

            
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                foreach (var item in posturl)
                {
                    try
                    {
                        using (SqlCommand command = new SqlCommand(sql_command, conn))
                        {
                            command.Parameters.Add("@url", System.Data.SqlDbType.VarChar).Value = item.url;
                            command.Parameters.Add("@date", System.Data.SqlDbType.VarChar).Value = item.date;
                            command.ExecuteNonQuery();
                        }
                    }
                    catch { }
                }
            }
        }

        #endregion


        #region --ScrapeFollower--

        public static string GetTargetAccount_ScrapeFollower()
        {
            lock ("igtoolkit_TargetAccount_ScrapeFollower")
            {
                using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
                {
                    conn.Open();

                    using (var command = new SqlCommand("igtoolkit_TargetAccount_ScrapeFollower", conn))
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                dataReader.Read();
                                var ig_url = dataReader["ig_url"].ToString();
                                var username = ig_url.Replace("https://www.instagram.com/", "").Replace("/", "");

                                return username;
                            }
                        }
                    }
                }
            }

            return null;
        }

        #endregion

        #region --Firefox--

        static public void AddSwitchPortable_FF()
        {
            var switch_portables = Directory.GetDirectories(Settings.switch_portables_locate);

            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand("delete ffportables_switch", conn))
                {
                    command.ExecuteNonQuery();
                }

                foreach (var s in switch_portables)
                {
                    using (var command = new SqlCommand("insert into ffportables_switch (portable_path,[status]) values ('" + s + "',''" + ")", conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        static private string switch_portable = "switch_portable";

        static public string GetFreeSwitchPortable_FF()
        {
            string switch_portable_path = null;

            lock (switch_portable)
            {
                using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
                {
                    conn.Open();

                    var command_text = "update top (1) ffportables_switch set [status]='Claimed' output inserted.portable_path where ([status] is null or [status]='')";

                    using (var command = new SqlCommand(command_text, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            dataReader.Read();

                            switch_portable_path = dataReader["portable_path"].ToString();
                        }
                    }
                }
            }

            //Check if this switch is open, if yes, close it

            var current_open_hwnd = PLAutoHelper.RPAFirefox.GetFFHandleByPath(Path.GetFullPath(switch_portable_path) + @"\App\firefox64\firefox.exe");

            if (current_open_hwnd != IntPtr.Zero)
            {
                PLAutoHelper.RPAFirefox.CloseFF(current_open_hwnd);
            }

            return switch_portable_path;
        }

        static public void SetSwitchPortableStatus_FF(string switch_portable_path, string status)
        {
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                var command_text = "update ffportables_switch set [status]='" + status + "' where [portable_path]='" + switch_portable_path + "'";

                using (var command = new SqlCommand(command_text, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        static public void Accounts_UpdateProperty(string username, string property_name, string property_value)
        {
            using (SqlConnection conn = new SqlConnection(db_connection_igtoolkit))
            {
                conn.Open();

                var command_text = "update accounts set [" + property_name + "]='" + property_value + "' where [username]='" + username + "'";

                using (var command = new SqlCommand(command_text, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion
    }
}
