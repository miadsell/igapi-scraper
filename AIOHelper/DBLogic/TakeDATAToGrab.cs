﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.AIOHelper.DBLogic
{
    static class TakeDATAToGrab
    {
        #region --DATA_GrabPost--

        //"DATA_GrabPost":{
        //    "type":""
        //  }

        static public dynamic DATA_GrabPost(bool is_take_data = true)
        {
            var aio_settings = Settings.aio_settings;

            string project = aio_settings["Project"].ToString();
            string type = aio_settings.DATA_GrabPost.type.ToString();

            if (type == "")
            {
                return DATA_GrabPost_Default(is_take_data);
            }

            return DBLogicDirectCenter.TakeDATAToGrab_DATA_GrabPost.Run(is_take_data);
        }

        static private dynamic DATA_GrabPost_Default(bool is_take_data)
        {
            dynamic dyn_account = null;
            string id = null;

            lock ("DATA_GrabPost")
            {
                var command_query = @"SELECT top 1 [id],[ig_url],[scrape_type]
                                    FROM target_accounts
                                    WHERE CHECKED= 'Checked'
                                      AND [ok] = 'OK'
                                      AND (DATEDIFF(DAY,[last_scrape], getdate()) > 1
                                           OR last_scrape IS NULL)
                                      AND ([state] is null or [state]='')";

                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    using (var command = new SqlCommand(command_query, conn))
                    {

                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                dataReader.Read();
                                id = dataReader["id"].ToString();
                                var ig_url = dataReader["ig_url"].ToString();
                                var username = ig_url.Replace("https://www.instagram.com/", "").Replace("/", "");
                                var scrape_type = dataReader["scrape_type"].ToString();

                                dyn_account = new { username, scrape_type };
                            }
                        }
                    }

                    if (id != null && is_take_data)
                    {
                        command_query = "update target_accounts set last_scrape=GETDATE() where[id] = @id";
                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }

            return dyn_account;
        }

        #endregion

        #region --DATA_GrabCommenters--

        //"DATA_GrabCommenters":{
        //    "type":""
        //  }

        static public dynamic DATA_GrabCommenters(int recent_days = 1, bool is_take_data = true)
        {

            //TAM THOI PAUSE TASK NAY DO DO QUERY DA LAU K CHAY, K OPTIMIZE DE LAY DATA NHANH NHAT
            var aio_settings = Settings.aio_settings;

            string project = aio_settings["Project"].ToString();
            string type = aio_settings.DATA_GrabCommenters.type.ToString();

            if (type == "")
            {
                return DATA_GrabCommenters_Default(recent_days, is_take_data);
            }

            return DBLogicDirectCenter.TakeDATAToGrab_DATA_GrabCommenters.Run(recent_days, is_take_data);
        }

        static private dynamic DATA_GrabCommenters_Default(int recent_days, bool is_take_data)
        {
            dynamic post = null;
            string choiced_postId = null;

            lock ("DATA_GrabCommenters")
            {

                var command_query = @"
                                    SELECT top 1 [posturl],[id],[scrape_type],[from_account]
                                    FROM posts p OUTER apply
                                      (SELECT top 1 *
                                       FROM actionlog
                                       WHERE [log_account]=p.posturl  and [log_name]='scrape_commenter' --get lan scrape gan nhat doi voi url nay

                                       ORDER BY id DESC) AS al
                                    WHERE [checked] = 'Checked'
                                      AND [ok] = 'OK'
                                      AND ([scrape_type] = 'All' or[scrape_type] = 'Commenters')
                                      AND [posturl] not in (select [log_account]
                                      FROM actionlog WHERE [log_name] = 'scrape_commenter'
                                      AND DATEDIFF(MINUTE, do_at, getdate()) < 5) --dieu kien de tranh 2 thread lay cung 1 data

                                      AND (@recent_days = 0
                                           OR DATEDIFF(DAY, [creation_date], getdate()) < @recent_days)
                                      AND 'https://www.instagram.com/' +[from_account]+'/' NOT IN --skip data tu target_account da paused

                                        (SELECT [ig_url]
                                         FROM target_accounts
                                         WHERE [state]='PAUSED')
                                    ORDER BY do_at ASC";

                List<dynamic> temp_posturls = new List<dynamic>();

                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@recent_days", System.Data.SqlDbType.Int).Value = recent_days;

                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                dataReader.Read();

                                var posturl = dataReader["posturl"].ToString();
                                var postId = dataReader["id"].ToString();
                                var scrape_type = dataReader["scrape_type"].ToString();
                                var from_account = dataReader["from_account"].ToString();

                                temp_posturls.Add(new
                                {
                                    postId,
                                    posturl,
                                    scrape_type,
                                    from_account
                                });
                            }
                        }
                    }


                    foreach (var item in temp_posturls)
                    {
                        //check if posturl has nextMinID="" or not

                        //check if has scrape_commenter_nextMinID log, if not this is first time scrape commenters for this url

                        var command_query_count_nextMinId =
                                @"select count(*)
                            from actionlog
                            where

                                [log_name] = 'scrape_commenter_nextMinID' and
                                  [log_account] = @postUrl";

                        using (var command = new SqlCommand(command_query_count_nextMinId, conn))
                        {
                            command.Parameters.Add("@postUrl", System.Data.SqlDbType.VarChar).Value = item.posturl;

                            var count = int.Parse(command.ExecuteScalar().ToString());
                            if (count == 0)
                            {
                                choiced_postId = item.postId;
                                post = item;
                                break;
                            }
                        }

                        //check xem co ton tai nextMinId="" khong

                        command_query_count_nextMinId =
                                    @"select count(*)
                                    from actionlog
                                    where

                                        [log_name] = 'scrape_commenter_nextMinID' and
                                          [log_account] = @postUrl and
                                          [value]=''";

                        using (var command = new SqlCommand(command_query_count_nextMinId, conn))
                        {
                            command.Parameters.Add("@postUrl", System.Data.SqlDbType.VarChar).Value = item.posturl;

                            var count = int.Parse(command.ExecuteScalar().ToString());
                            if (count >= 1)
                            {
                                continue;
                            }
                            else
                            {
                                choiced_postId = item.postId;
                                post = item;
                                break;
                            }
                        }
                    }

                    if (choiced_postId != null && is_take_data)
                    {
                        command_query = "insert into actionlog (log_name,log_account) values ('scrape_commenter',@ig_url)";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@ig_url", System.Data.SqlDbType.VarChar).Value = post.posturl;
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }

            return post;
        }

        #endregion

        #region --DATA_GrabFollower--

        static public string DATA_GrabFollower(bool is_take_data = true)
        {
            var aio_settings = Settings.aio_settings;

            string project = aio_settings["Project"].ToString();
            string type = aio_settings.DATA_GrabFollower.type.ToString();

            if (type == "")
            {
                return DATA_GrabFollower_Default(is_take_data);
            }

            return DBLogicDirectCenter.TakeDATAToGrab_DATA_GrabFollower.Run(is_take_data);
        }

        static private List<string> DATA_GrabFollower_Default_accounts = new List<string>();

        static private string DATA_GrabFollower_Default(bool is_take_data)
        {
            string username = null;
            string choiced_ig_url = null;

            string command_query = null;

            lock ("DATA_GrabFollower")
            {
          //      var command_query = @"SELECT top 1 [ig_url]
          //                          FROM target_accounts
          //                          WHERE [scrape_follower] = 'yes' and
										//([state] is null or [state]='')
          //                            AND 
          //                              (SELECT count([log_account])
          //                               FROM actionlog
          //                               WHERE [log_name] = 'scrape_follower'
          //                                 AND DATEDIFF(MINUTE, do_at, getdate()) < @mins
										//   AND [log_account]=target_accounts.ig_url)=0
          //                            AND ([state] is null or [state]='')";

                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    if (DATA_GrabFollower_Default_accounts.Count > 0)
                    {
                        choiced_ig_url = DATA_GrabFollower_Default_accounts.First();
                    }
                    else
                    {

                        //get list target_accounts has scrape_follower and proper state

                        List<string> ig_urls = new List<string>();

                        command_query =
                            @"	SELECT [ig_url]
                            FROM target_accounts
                            WHERE [scrape_follower] = 'yes' and
								([state] is null or [state]='') and
								[checked] = 'Checked' and
                                [ok] = 'OK'";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            using (var dataReader = command.ExecuteReader())
                            {
                                while (dataReader.Read())
                                {
                                    ig_urls.Add(dataReader["ig_url"].ToString());
                                }
                            }
                        }

                        //shuffle

                        ig_urls = ig_urls.OrderBy(a => Guid.NewGuid()).ToList();

                        //get list ig_url scraped in last @mins

                        //check each ig_url to see if match account latest scrape time

                        List<string> scraped_last_x_mins_ig_urls = new List<string>();

                        command_query =
                            @"SELECT [log_account]
                              FROM actionlog
                              WHERE 
                                    [log_name] = 'scrape_follower'
                                    AND DATEDIFF(MINUTE, do_at, getdate()) < @mins";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@mins", System.Data.SqlDbType.Int).Value = Settings.settings_grab_followers_check_x_mins;

                            using (var dataReader = command.ExecuteReader())
                            {
                                while (dataReader.Read())
                                {
                                    scraped_last_x_mins_ig_urls.Add(dataReader["log_account"].ToString());
                                }
                            }
                        }

                        //except

                        DATA_GrabFollower_Default_accounts = ig_urls.Except(scraped_last_x_mins_ig_urls).ToList();

                        if(DATA_GrabFollower_Default_accounts.Count>0)
                        {
                            choiced_ig_url = DATA_GrabFollower_Default_accounts.First();
                        }

                    }

                    if (choiced_ig_url == null)
                        goto ReturnValue;


                    if (!is_take_data)
                    {
                        goto ReturnValue;
                    }
                    else
                    {
                        command_query = "insert into actionlog (log_name,log_account) values ('scrape_follower',@ig_url)";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@ig_url", System.Data.SqlDbType.VarChar).Value = choiced_ig_url;

                            command.ExecuteNonQuery();
                        }

                        DATA_GrabFollower_Default_accounts.Remove(choiced_ig_url);
                    }

         //           foreach (var item in ig_urls)
         //           {
         //               command_query =
         //                   @"SELECT count([log_account])
         //                     FROM actionlog
         //                     WHERE 
         //                           [log_name] = 'scrape_follower'
         //                           AND DATEDIFF(MINUTE, do_at, getdate()) < @mins
									//AND [log_account]=@ig_url";

         //               using (var command = new SqlCommand(command_query, conn))
         //               {
         //                   command.Parameters.Add("@mins", System.Data.SqlDbType.Int).Value = Settings.settings_grab_followers_check_x_mins;
         //                   command.Parameters.Add("@ig_url", System.Data.SqlDbType.VarChar).Value = item;
         //                   var count = int.Parse(command.ExecuteScalar().ToString());

         //                   if (count == 0)
         //                   {
         //                       //choose it
         //                       choiced_ig_url = item;
         //                       break;
         //                   }
         //               }
         //           }

         //           if (choiced_ig_url != null && is_take_data)
         //           {
         //               command_query = "insert into actionlog (log_name,log_account) values ('scrape_follower',@ig_url)";

         //               using (var command = new SqlCommand(command_query, conn))
         //               {
         //                   command.Parameters.Add("@ig_url", System.Data.SqlDbType.VarChar).Value = choiced_ig_url;

         //                   command.ExecuteNonQuery();
         //               }
         //           }

                }
            }

            ReturnValue:

            if (choiced_ig_url != null)
                username = choiced_ig_url.Replace("https://www.instagram.com/", "").Replace("/", "");

            return username;
        }

        #endregion

        #region --DATA_GrabLiker--
        static public dynamic DATA_GrabLiker(int recent_days = 1, bool is_take_data = true)
        {
            var aio_settings = Settings.aio_settings;

            string project = aio_settings["Project"].ToString();
            string type = aio_settings.DATA_GrabLiker.type.ToString();

            if (type == "")
            {
                return DATA_GrabLiker_Default(recent_days, is_take_data);
            }

            return DBLogicDirectCenter.TakeDATAToGrab_DATA_GrabLiker.Run(recent_days, is_take_data);
        }

        static private dynamic DATA_GrabLiker_Default(int recent_days, bool is_take_data)
        {
            dynamic post = null;
            string postId = null;

            lock ("DATA_GrabLiker")
            {

                //var command_query = @"SELECT top 1 p.[id],[posturl],[scrape_type],[from_account]
                //                    FROM posts p OUTER apply
                //                      (SELECT top 1 *
                //                       FROM actionlog
                //                       WHERE [log_account]=p.posturl and [log_name]='scrape_liker'
                //                       ORDER BY id DESC) AS al
                //                    WHERE [checked] = 'Checked'
                //                      AND [ok] = 'OK'
                //                      AND ([scrape_type] = 'All' or[scrape_type] = 'Likers')
                //                      AND [posturl] not in (select [log_account]
                //                      FROM actionlog WHERE [log_name] = 'scrape_liker'
                //                      AND DATEDIFF(MINUTE, do_at, getdate()) < 10)
                //                      AND (@recent_days = 0
                //                           OR DATEDIFF(DAY, [creation_date], getdate()) < @recent_days)
                //                      AND 'https://www.instagram.com/' +[from_account]+'/' NOT IN
                //                        (SELECT [ig_url]
                //                         FROM target_accounts
                //                         WHERE [state]='PAUSED')
                //                    ORDER BY do_at ASC";

                var command_query =
                    @"SELECT  p.[id],[posturl],[scrape_type],[from_account]--, [do_at]
                                    FROM posts p OUTER apply
                                      (SELECT top 1 *
                                       FROM actionlog
                                       WHERE [log_account]=p.posturl and [log_name]='scrape_liker'
                                       ORDER BY id DESC) AS al
                                    WHERE [checked] = 'Checked'
                                      AND [ok] = 'OK'
                                      AND ([scrape_type] = 'All' or[scrape_type] = 'Likers')
                                      --AND [posturl] not in (select [log_account]
                                      --FROM actionlog WHERE [log_name] = 'scrape_liker'
                                      --AND DATEDIFF(MINUTE, do_at, getdate()) < 10)
                                      AND (@recent_days = 0
                                           OR DATEDIFF(DAY, [creation_date], getdate()) <= @recent_days)
                                      AND 'https://www.instagram.com/' +[from_account]+'/' IN
                                        (SELECT [ig_url]
                                         FROM target_accounts
                                         WHERE [state]='' or [state] is null)
										order by creation_date asc"; //get list account that can be scrape liker

                List<dynamic> post_lists = new List<dynamic>();

                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@recent_days", System.Data.SqlDbType.Int).Value = recent_days;
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {

                                    var temp_postId = dataReader["id"].ToString();
                                    var posturl = dataReader["posturl"].ToString();
                                    var scrape_type = dataReader["scrape_type"].ToString();
                                    var from_account = dataReader["from_account"].ToString();

                                    post_lists.Add(new
                                    {
                                        postId = temp_postId,
                                        posturl,
                                        scrape_type,
                                        from_account
                                    });
                                }
                            }
                        }
                    }

                    foreach (var p in post_lists)
                    {
                        //check each post

                        //check if this post from from_account that was paused
                        command_query =
                            @"select [state]
                            from target_accounts
                            where
	                            [ig_url] like '%/" + p.from_account + "/'";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            var state_obj = command.ExecuteScalar();

                            if (state_obj.ToString() != "")
                            {
                                continue;

                            }
                        }

                        //check to verify not do action in last x minutes
                        command_query =
                            @"select count([do_at])
                            from actionlog
                            where
	                            [log_name] = 'scrape_liker' and
	                            [log_account]=@ig_url and
	                            DATEDIFF(MINUTE, do_at, getdate()) < @minutes";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@ig_url", System.Data.SqlDbType.VarChar).Value = p.posturl;
                            command.Parameters.Add("@minutes", System.Data.SqlDbType.Int).Value = Settings.settings_likers_grab_per_x_mins;

                            if (int.Parse(command.ExecuteScalar().ToString()) > 0)
                                continue;
                        }

                        //toi buoc nay tuc data thoa dieu kien


                        postId = p.postId;
                        post = p;
                        break;

                    }

                    if (postId != null && is_take_data)
                    {
                        //add log

                        command_query = "insert into actionlog (log_name,log_account) values ('scrape_liker',@ig_url)";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@ig_url", System.Data.SqlDbType.VarChar).Value = post.posturl;

                            command.ExecuteNonQuery();
                        }
                    }
                }
            }

            return post;
        }

        #endregion

        #region --DATA_GrabUserFullInfo--

        static public dynamic DATA_GrabUserFullInfo(bool is_take_data = true)
        {
            var aio_settings = Settings.aio_settings;

            string project = aio_settings["Project"].ToString();
            string type = aio_settings.DATA_GrabUserFullInfo.type.ToString();

            if (type == "")
            {
                return DATA_GrabUserFullInfo_Default(is_take_data);
            }

            return DBLogicDirectCenter.TakeDATAToGrab_DATA_GrabUserFullInfo.Run(is_take_data);
        }


        static private List<dynamic> DATA_GrabUserFullInfo_Default_accounts = new List<dynamic>();

        static private dynamic DATA_GrabUserFullInfo_Default(bool is_take_data)
        {
            dynamic account = null;

            string command_query = null;

            lock ("DATA_GrabUserFullInfo_weightloss_Default")
            {

                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    while (true)
                    {

                        if (DATA_GrabUserFullInfo_Default_accounts.Count > 0)
                        {
                            account = DATA_GrabUserFullInfo_Default_accounts.First();
                        }
                        else
                        {
                            command_query = @"SELECT top 500 [pk],[UserName],[detect_location]
                                    FROM scrapeusers
                                    WHERE ([state] is null or ([state] is not null and [state]=''))
                                      AND ([status] is null or [status] = '')
                                      AND 'https://www.instagram.com/' +[from_account]+'/' NOT IN
                                        (SELECT [ig_url]
                                         FROM target_accounts
                                         WHERE [state]='PAUSED')
                                    ORDER BY [first_added] DESC";

                            using (var command = new SqlCommand(command_query, conn))
                            {
                                using (var dataReader = command.ExecuteReader())
                                {
                                    if (dataReader.HasRows)
                                    {
                                        while (dataReader.Read())
                                        {
                                            var pk = dataReader["pk"].ToString();
                                            var username = dataReader["UserName"].ToString();
                                            var detect_location = dataReader["detect_location"].ToString();

                                            DATA_GrabUserFullInfo_Default_accounts.Add(new
                                            {
                                                pk,
                                                username,
                                                detect_location
                                            });
                                        }
                                    }
                                }
                            }

                            if (DATA_GrabUserFullInfo_Default_accounts.Count > 0)
                            {
                                DATA_GrabUserFullInfo_Default_accounts = DATA_GrabUserFullInfo_Default_accounts.OrderBy(a => Guid.NewGuid()).ToList();
                                account = DATA_GrabUserFullInfo_Default_accounts.First();
                            }
                        }

                        if (account != null && is_take_data)
                        {
                            command_query = "update scrapeusers set [status]='Claimed' where [UserName] = @user and ([status] is null or [status]='')";

                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@user", System.Data.SqlDbType.VarChar).Value = account.username;
                                var count = command.ExecuteNonQuery();

                                //remove from list event it's claimed by this pc_devices or another devices

                                DATA_GrabUserFullInfo_Default_accounts.RemoveAll(x => x.username == account.username);

                                if (count != 1)
                                    account = null;
                            }
                        }
                        else
                            break;



                        if (account != null)
                            break;

                    }
                }
            }

            return account;
        }


        #endregion

        #region --DATA_GrabUserFullInfo-RESCRAPE--

        static public dynamic DATA_GrabUserFullInfo_ReScrape(bool is_take_data = true)
        {
            var aio_settings = Settings.aio_settings;

            string project = aio_settings["Project"].ToString();
            string type = aio_settings.DATA_GrabUserFullInfo.type.ToString();

            if (type == "")
            {
                return DATA_GrabUserFullInfo_Rescrape_Default(is_take_data);
            }

            return DBLogicDirectCenter.TakeDATAToGrab_DATA_GrabUserFullInfo.Run(is_take_data);
        }

        static private dynamic DATA_GrabUserFullInfo_Rescrape_Default(bool is_take_data)
        {
            dynamic account = null;


            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                while (true)
                {
                    var command_query = @"select top 1 *
                                        from scrapeusers
                                        where
                                            ([status] is null or [status] = '') and
	                                        [following]='Done' and
	                                        [IsPrivate]='False' and
	                                        DATEDIFF(day,[update_at],getdate())>14 and
	                                        (MediaCount>0) and
	                                        AVGLike>0 and
	                                        [state]='READY' and
	                                        DATEDIFF(month,latest_added,getdate())<=3 --care account < 3 month since latest_added --tuc la account van quan tam den vay trong 3 thang gan nhat
                                        order by update_at asc";

                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (var command = new SqlCommand(command_query, conn))
                        {

                            using (var dataReader = command.ExecuteReader())
                            {
                                if (dataReader.HasRows)
                                {
                                    dataReader.Read();
                                    var pk = dataReader["pk"].ToString();
                                    var username = dataReader["UserName"].ToString();
                                    var detect_location = dataReader["detect_location"].ToString();

                                    account = new
                                    {
                                        pk,
                                        username,
                                        detect_location
                                    };
                                }
                            }
                        }
                    }/*, "scrapeusers"*/);

                    if (account == null)
                        break;

                    if (account != null && is_take_data)
                    {
                        PLAutoHelper.SQLHelper.Execute(() =>
                        {
                            using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                            {

                                command_query = "update scrapeusers set [status]='Claimed' where [UserName] = @user and ([status] is null or [status]='')";

                                using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                {
                                    command.Parameters.Add("@user", System.Data.SqlDbType.VarChar).Value = account.username;
                                    var count = command.ExecuteNonQuery();

                                    sqlTransaction.Commit();

                                    if (count != 1)
                                        account = null;
                                }
                            }
                        });

                        if (account != null)
                            break;
                    }
                    else
                        break;
                }
            }


            return account;
        }


        #endregion

        #region --DATA_GrabPost_ForComment--


        static public dynamic DATA_GrabPost_ForComment(bool is_take_data = true)
        {
            var aio_settings = Settings.aio_settings;

            string project = aio_settings["Project"].ToString();
            string type = aio_settings.DATA_GrabPost_ForComment.type.ToString();

            if (type == "")
            {
                return DATA_GrabPost_ForComment_Default(is_take_data);
            }

            throw new System.ArgumentException("Not implemented");
        }

        static private dynamic DATA_GrabPost_ForComment_Default(bool is_take_data)
        {
            dynamic dyn_account = null;
            string id = null;

            lock ("DATA_GrabPost_ForComment")
            {
                var command_query = @"SELECT top 1 [id],[ig_url]
                                    FROM comment_accounts
                                    WHERE CHECKED= 'Checked'
                                      AND [ok] = 'OK'
                                      AND (DATEDIFF(minute,[last_scrape], getdate()) > 120
                                           OR last_scrape IS NULL)
                                      AND ([state] is null or [state]='')";

                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    using (var command = new SqlCommand(command_query, conn))
                    {

                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                dataReader.Read();
                                id = dataReader["id"].ToString();
                                var ig_url = dataReader["ig_url"].ToString();
                                var username = ig_url.Replace("https://www.instagram.com/", "").Replace("/", "");

                                dyn_account = new { username };
                            }
                        }
                    }

                    if (id != null && is_take_data)
                    {
                        command_query = "update comment_accounts set last_scrape=GETDATE() where [id] = @id";
                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }

            return dyn_account;
        }

        #endregion

        #region --DATA_GrabFollowerBack--

        static public dynamic DATA_GrabFollowerBack(bool is_take_data = true)
        {
            var aio_settings = Settings.aio_settings;

            string project = aio_settings["Project"].ToString();
            string type = aio_settings.DATA_GrabFollowerBack.type.ToString();
            lock ("DATA_GrabFollowerBack")
            {
                if (type == "")
                {
                    return DATA_GrabFollowerBack_Default(is_take_data);
                }

                throw new System.ArgumentException("Not implemented");
            }
        }

        static private dynamic DATA_GrabFollowerBack_Default(bool is_take_data)
        {
            dynamic account = null;


            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_instagram))
            {
                conn.Open();

                //get list account turn on followinguser

                List<dynamic> following_accounts = new List<dynamic>();

                string command_query =
                    @"select *
                    from ig_account
                    where
						[state]='ACTIVE' and
	                    [id] in
	                    (select [accountId] --turn on followinguser
	                    from specifictasks
	                    where [followinguser]='Do')";

                using (var command = new SqlCommand(command_query, conn))
                {

                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            following_accounts.Add(new
                            {
                                id = dataReader["id"].ToString(),
                                user = dataReader["user"].ToString()
                            });
                        }
                    }
                }

                //get list account do grab_followback in recent X hours

                List<string> recent_grab_followback = new List<string>();

                command_query =
                    @"select *
                    from actionlog
                    where
	                    [action]='grab_followback' and
	                    DATEDIFF(minute,do_at,getdate())<@mins";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@mins", System.Data.SqlDbType.Int).Value = Settings.settings_grab_followback_check_x_mins;
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            recent_grab_followback.Add(dataReader["accountid"].ToString());
                        }
                    }
                }

                //exclude account that recent grab followback from following_accounts

                following_accounts = following_accounts.Where(f => !recent_grab_followback.Contains(f.id)).ToList();

                if (following_accounts.Count > 0)
                    account = following_accounts[0];

                if (account != null && is_take_data)
                {

                    command_query = "insert into actionlog ([accountid],[action]) values (@accountId,'grab_followback')";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = account.id;
                        command.ExecuteNonQuery();
                    }
                }
            }

            return account;

        }

        #endregion


        #region --DATA_GrabFollowBack_Main--

        static public dynamic DATA_GrabFollower_ForMother(bool is_take_data = true)
        {
            var aio_settings = Settings.aio_settings;

            string project = aio_settings["Project"].ToString();
            string type = aio_settings.DATA_GrabFollower_ForMother.type.ToString();
            lock ("DATA_GrabFollower_ForMother")
            {
                if (type == "")
                {
                    return DATA_GrabFollower_ForMother_Default(is_take_data);
                }

                throw new System.ArgumentException("Not implemented");
            }
        }
        static private dynamic DATA_GrabFollower_ForMother_Default(bool is_take_data)
        {
            dynamic account = null;

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                //get list mother accounts

                List<string> mother_accounts = new List<string>();

                string command_query =
                    @"select *
                    from mother_accounts";

                using (var command = new SqlCommand(command_query, conn))
                {

                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            mother_accounts.Add(dataReader["user"].ToString());
                        }
                    }
                }

                //get list account do grab_follower in recent X hours

                List<string> recent_grab_follower = new List<string>();

                command_query =
                    @"select [log_account]
                    from actionlog
                    where
	                    [log_name]='grab_follower_formother' and
	                    DATEDIFF(minute,do_at,getdate())<@mins";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@mins", System.Data.SqlDbType.Int).Value = 12 * 60;
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            recent_grab_follower.Add(dataReader["log_account"].ToString());
                        }
                    }
                }

                //exclude account that recent grab followback from following_accounts

                mother_accounts = mother_accounts.Where(f => !recent_grab_follower.Contains(f)).ToList();

                if (mother_accounts.Count > 0)
                    account = mother_accounts[0];

                if (account != null && is_take_data)
                {

                    command_query = "insert into actionlog ([log_name],[log_account]) values ('grab_follower_formother',@account)";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@account", System.Data.SqlDbType.VarChar).Value = account;
                        command.ExecuteNonQuery();
                    }
                }
            }

            return account;
        }

        #endregion

        #region --DATA_GrabPost_ForComment_FromScrapeUsers--

        static private List<string> DATA_GrabPost_ForComment_FromScrapeUsers_accounts = new List<string>();

        static public string DATA_GrabPost_ForComment_FromScrapeUsers(bool is_take_data)
        {
            string command_query = "";

            dynamic data = new ExpandoObject();

            string choiced_user = null;

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_instagram))
            {
                conn.Open();

                //get list account already done following_task

                List<string> target_accounts = new List<string>();

                command_query =
                    @"select [UserName]
                        from " + API.DBHelpers.igtoolkit_dbname + @".dbo.scrapeusers
                        where
	                        [state]='READY' and
							[following]='Done' and
							[IsPrivate]='False' and
							[MediaCount]>0 and
							([status] is null or [status]='')
                        order by first_added asc";

                using (var command = new SqlCommand(command_query, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {

                        while (dataReader.Read())
                        {
                            target_accounts.Add(dataReader["UserName"].ToString());
                        }
                    }
                }

                List<string> followback_target_accounts = new List<string>();
                //get list account followback
                command_query =
                    @"select [target_account]
                    from actionlog
                    where
	                    [action]='followback'";

                using (var command = new SqlCommand(command_query, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {

                        while (dataReader.Read())
                        {
                            followback_target_accounts.Add(dataReader["target_account"].ToString());
                        }
                    }
                }
                target_accounts = target_accounts.Except(followback_target_accounts).ToList();

                //get list account done commentpost, [value]='zalo_group
                List<string> excluded_target_accounts = new List<string>();

                command_query =
                    @"select [target_account]
                        from actionlog
                        where
	                        ([action]='commentpost_task' and
	                        [value]='zalo_group') and
	                        ([target_account] is not null and [target_account]!='')";

                using (var command = new SqlCommand(command_query, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {

                        while (dataReader.Read())
                        {
                            excluded_target_accounts.Add(dataReader["target_account"].ToString());
                        }
                    }
                }

                List<string> already_scrape_accounts = new List<string>();

                //get list account has scrape latest post

                command_query =
                    @"select [from_account]
                    from " + API.DBHelpers.igtoolkit_dbname + @".dbo.scrapeuser_comment_posts";

                using (var command = new SqlCommand(command_query, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            already_scrape_accounts.Add(dataReader["from_account"].ToString());
                        }
                    }
                }

                //except list and take 100 oldest account
                target_accounts = target_accounts.Except(excluded_target_accounts).Except(already_scrape_accounts).Take(100).ToList();

                //shuffel

                var final_accounts = target_accounts.OrderBy(a => Guid.NewGuid()).ToList();

                if (final_accounts.Count > 0 && is_take_data)
                {

                    foreach (var f in final_accounts)
                    {

                        command_query =
                                @"update igtoolkit.dbo.scrapeusers set [status]='Claimed' where [UserName]=@UserName and ([status] is null or [status]='')";


                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = f;
                            var affected_row = command.ExecuteNonQuery();

                            if (affected_row == 1)
                            {
                                choiced_user = f;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    choiced_user = final_accounts[0];
                }
            }

            return choiced_user;
        }

        #endregion


        #region --DATA_GrabPost_ForComment_FromScrapeUsers--

        static public string DATA_APITask_GrabComment_ForSlave(bool is_take_data)
        {
            string choiced_account = null;

            lock ("DATA_APITask_GrabComment_ForSlave")
            {

                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_instagram))
                {
                    conn.Open();

                    List<string> accounts = new List<string>();

                    //get list account need to scrape comment
                    string command_query =
                    @"select [user]
                from ig_account
                where
	                [niche]='ldshop' and
	                [type]='slave' and
                    [state]='ACTIVE'";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                accounts.Add(dataReader["user"].ToString());
                            }
                        }
                    }

                    //get list account scraped in last 8 hours  ==> actionlog: GrabComment_ForSlave

                    command_query =
                            @"select [log_account]
                            from igtoolkit.dbo.actionlog
                            where
	                            [log_name]='GrabComment_ForSlave' and
	                            DATEDIFF(hour,do_at,getdate())<=8";


                    List<string> last_x_hours_accounts = new List<string>();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                last_x_hours_accounts.Add(dataReader["log_account"].ToString());
                            }
                        }
                    }

                    //except
                    accounts = accounts.Except(last_x_hours_accounts).ToList();

                    if(accounts.Count>0)
                    {
                        //take first and insert into log
                        choiced_account = accounts[0];

                        if (is_take_data)
                        {

                            command_query = "insert into igtoolkit.dbo.actionlog (log_name,log_account) values ('GrabComment_ForSlave',@log_account)";

                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@log_account", System.Data.SqlDbType.VarChar).Value = choiced_account;
                                command.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }

            return choiced_account;
        }

        #endregion

        #region --DATA_Hashtag--

        static string lock_hashtag = "lock_hashtag";

        static public string DATA_HASHTAG_GrabRelatedHashtag()
        {
            string hashtag = null;

            lock (lock_hashtag)
            {

                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    while (hashtag == null)
                    {

                        string command_query =
                    @"select top 1 *
                    from hashtags
                    where 
	                    (scrape_related is null or scrape_related != 'Used') and 
	                    [ok]='OK' and
	                    ([status] is null or [status]='')";


                        using (var command = new SqlCommand(command_query, conn))
                        {
                            using (var dataReader = command.ExecuteReader())
                            {
                                if (dataReader.HasRows)
                                {
                                    dataReader.Read();
                                    hashtag = dataReader["hashtag"].ToString();
                                }
                            }
                        }

                        if (hashtag != null)
                        {
                            command_query =
                                @"update hashtags set [status]='Claimed'
                            where
	                            [hashtag]=@hashtag and
	                            ([status] is null or [status]='')";

                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@hashtag", System.Data.SqlDbType.NVarChar).Value = hashtag;
                                int affect_row = command.ExecuteNonQuery();

                                if (affect_row == 1)
                                    break;
                                else
                                {
                                    hashtag = null;
                                }
                            }
                        }
                        else
                            break;
                    }
                }
            }

            return hashtag;
        }


        static public string DATA_HASHTAG_GrabMedia_CountOfHashtag()
        {
            string hashtag = null;

            lock(lock_hashtag)
            {
                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    while (hashtag == null)
                    {

                        string command_query =
                            @"select top 1 *
                                from hashtags
                                where
	                                [media_count] is null and
	                                ([ok]='OK' and [checked]='Checked') and
	                                ([status] is null or [status]='')";


                        using (var command = new SqlCommand(command_query, conn))
                        {
                            using (var dataReader = command.ExecuteReader())
                            {
                                if (dataReader.HasRows)
                                {
                                    dataReader.Read();
                                    hashtag = dataReader["hashtag"].ToString();
                                }
                            }
                        }

                        if (hashtag != null)
                        {
                            command_query =
                                @"update hashtags set [status]='Claimed'
                            where
	                            [hashtag]=@hashtag and
	                            ([status] is null or [status]='')";

                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@hashtag", System.Data.SqlDbType.NVarChar).Value = hashtag;
                                int affect_row = command.ExecuteNonQuery();

                                if (affect_row == 1)
                                    break;
                                else
                                {
                                    hashtag = null;
                                }
                            }
                        }
                        else
                            break;
                    }
                }
            }

            return hashtag;
        }

        #endregion
    }
}
