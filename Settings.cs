﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static IGFirefoxHelper.FFPCSwitch;

namespace IGAPI___Ramtinak
{
    static class Settings
    {

        static public string switch_portables_locate = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\SwitchPortableFF";
        static public string firefox_download_locate = @"F:\DOWNLOAD";

        static public List<string> proxy_server_ips = new List<string>();

        static public dynamic aio_settings;// = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(File.ReadAllText(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\settings.txt"));

        //Example
        //{
        //  "Project": "sunstore95",
        //  "AIOGrab": {
        //    "settings_commenters_x_days": 5,
        //    "settings_commenters_max_page_loads": 2,
        //    "settings_grab_followers_max_scrape": 500,
        //    "settings_grab_followers_check_x_mins": 240,
        //    "settings_likers_x_days": 5,
        //    "settings_likers_grab_per_x_mins":720
        //  },
        //  "DATA_GrabPost": {
        //    "type": ""
        //  },
        //  "DATA_GrabCommenters": {
        //    "type": ""
        //  },
        //  "DATA_GrabFollower": {
        //    "type": ""
        //  },
        //  "DATA_GrabLiker": {
        //    "type": ""
        //  },
        //  "DATA_GrabUserFullInfo": {
        //    "type": ""
        //  }
        //}

        #region --AIO-Grab Settings--

        static public int aio_threads_quantity;
        static public int settings_commenters_x_days;
        static public int settings_commenters_max_page_loads;
        static public int settings_grab_followers_max_scrape;
        static public int settings_grab_followers_check_x_mins;
        static public int settings_likers_x_days;
        static public int settings_likers_grab_per_x_mins;
        static public int settings_grab_followback_loadspage;
        static public int settings_grab_followback_check_x_mins;

        #endregion



        static public void LoadSettings()
        {
            aio_threads_quantity = aio_settings.AIOGrab.threads;
            settings_commenters_x_days = aio_settings.AIOGrab.settings_commenters_x_days;
            settings_commenters_max_page_loads = aio_settings.AIOGrab.settings_commenters_max_page_loads;
            settings_grab_followers_max_scrape = aio_settings.AIOGrab.settings_grab_followers_max_scrape;
            settings_grab_followers_check_x_mins = aio_settings.AIOGrab.settings_grab_followers_check_x_mins;
            settings_likers_x_days = aio_settings.AIOGrab.settings_likers_x_days;
            settings_likers_grab_per_x_mins = aio_settings.AIOGrab.settings_likers_grab_per_x_mins;
            settings_grab_followback_loadspage = aio_settings.AIOGrab.settings_grab_followback_loadspage;
            settings_grab_followback_check_x_mins = aio_settings.AIOGrab.settings_grab_followback_check_x_mins;
        }

        #region --FFPC--
        static public string ffprofile_locate = "FFProfiles";
        static public string originalprofile_local = "IG-Original-Portable-Scrape-Profile.zip";
        static public string original_profile_zip = ""; //local file, in execute folder

        //static public FFPC fFPC = new FFPC(ffprofile_locate, ftp_backup_path, ftp_original_firefox, originalprofile_local);

        #endregion

        #region --Proxy

        static public PLAutoHelper.ProxyHelper.ProxyDB proxyDB_Tinsoft = new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "tinsoft_tb");
        static public PLAutoHelper.ProxyHelper.ProxyDB proxyDB_XProxy = new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxy_tb");

        #endregion
    }
}
