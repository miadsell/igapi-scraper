﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.IGClass
{
    class IGFFPro
    {
        public dynamic ig_account;
        public IntPtr firefox_hwnd;
        public string ig_status;
        string proxy_type = "";
        bool already_hasprofile;
        string switch_portable;
        public IGFirefoxHelper.FFPCSwitch.FFProxy fFProxy;

        ///ig_account: username, id, already_hasprofile, password

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ig_account"></param>
        /// <param name="proxy_type"></param>

        public IGFFPro(dynamic ig_account, string proxy_type = "MOBILE")
        {
            this.ig_account = ig_account;
            this.proxy_type = proxy_type;
            LaunchIG_FF();
        }

        /// <summary>
        /// use for quick launch test
        /// </summary>
        /// <param name="accountId"></param>
        //public IGFFPro(string accountId, string proxy_type = "MOBILE")
        //{
        //    this.ig_account = DBHelper.IGAccount_FullProperty(accountId);
        //    this.proxy_type = proxy_type;

        //    LaunchIG_FF();
        //}


        private void LaunchIG_FF()
        {

            string already_hasprofile_val = LocalHelper.GetDynamicProperty(ig_account, "already_hasprofile");

            already_hasprofile = already_hasprofile_val == "yes" ? true : false;

            //get switch firefox portable profile
            switch_portable = API.DBHelpers.GetFreeSwitchPortable_FF();

            string data_profile_zip_path = null;

            switch (already_hasprofile)
            {
                case true:

                    var backup_folder = "BackupFF\\" + LocalHelper.GetDynamicProperty(ig_account, "id");

                    //extract backup to profile folder
                    data_profile_zip_path = IGFirefoxHelper.FFPCSwitch.FFPC_LatestBackup(backup_folder);

                    break;
                case false:
                    //extract original profile to firefox
                    data_profile_zip_path = Settings.originalprofile_local;

                    break;
            }

            if (data_profile_zip_path == null)
                throw new System.ArgumentException("Not found backup");

            PLAutoHelper.RPAFirefox.RestoreFFPortable(switch_portable, data_profile_zip_path);


            //get proxy
            GetProxy();

            var pid = PLAutoHelper.RPAFirefox.LaunchFFPortable(switch_portable);

            firefox_hwnd = Process.GetProcessById(pid).MainWindowHandle;

            PLAutoHelper.RPAFirefox.MaximizeBrowser(firefox_hwnd);

            switch (already_hasprofile)
            {
                case true:
                    break;
                case false:
                    PLAutoHelper.RPAFirefox.SettingBrowser(firefox_hwnd, null, null);

                    //Backup now

                    PLAutoHelper.RPAFirefox.CloseFF(firefox_hwnd);

                    Process.GetProcessById(pid).Kill();

                    var daily_backup_folder = "BackupFF\\" + LocalHelper.GetDynamicProperty(ig_account, "id");

                    IGFirefoxHelper.FFPCSwitch.BackupFFProfile(switch_portable, Path.GetFullPath(daily_backup_folder) + "\\igprofile_" + LocalHelper.GetDynamicProperty(ig_account, "id") + "_" + LocalHelper.RandomGUID(3) + ".zip");


                    API.DBHelpers.Accounts_UpdateProperty(LocalHelper.GetDynamicProperty(ig_account, "username"), "already_hasprofile", "yes");

                    //relaunch firefox

                    pid = PLAutoHelper.RPAFirefox.LaunchFFPortable(switch_portable);

                    firefox_hwnd = Process.GetProcessById(pid).MainWindowHandle;

                    break;
            }

            string proxy_protocal_type = null;

            switch (fFProxy.type)
            {
                case "MOBILE":
                    proxy_protocal_type = "SOCKS5";
                    break;
                case "RESIDENT":
                    proxy_protocal_type = "HTTP";
                    break;
                default:
                    throw new System.ArgumentException("Not recognize this type of proxy");
            }

            //Setproxy
            PLAutoHelper.RPAFirefox.SetCustomProxy(fFProxy.proxy_str, proxy_protocal_type, firefox_hwnd, null);

            ig_status = IGFirefoxHelper.ActionHelper.Login(firefox_hwnd, LocalHelper.GetDynamicProperty(ig_account, "username"), LocalHelper.GetDynamicProperty(ig_account, "password"), "nanosim.vn");



            if (ig_status == "LoginSuccess")
            {
                //Add last use log
                //DBHelper.InstagramDB_AddToLog(LocalHelper.GetDynamicProperty(ig_account, "id"), "last_use_on_desktop");
            }
            else
                if (ig_status.ToLower() == "disable")
            {

                API.DBHelpers.Accounts_UpdateProperty(LocalHelper.GetDynamicProperty(ig_account, "username"), "state", "DISABLE");
            }
            else
            if (ig_status == "NotReceiveSms")
            {
                //no action
            }
            else throw new System.ArgumentException("Not login");

        }

        public void Dispose()
        {
            //Add last use log
            //DBHelper.InstagramDB_AddToLog(LocalHelper.GetDynamicProperty(ig_account, "id"), "last_use_on_desktop");

            //Close FF
            PLAutoHelper.RPAFirefox.CloseFF(firefox_hwnd);

            //Backup ff
            var daily_backup_folder = "BackupFF\\" + LocalHelper.GetDynamicProperty(ig_account, "id");

            IGFirefoxHelper.FFPCSwitch.BackupFFProfile(switch_portable, Path.GetFullPath(daily_backup_folder) + "\\igprofile_" + LocalHelper.GetDynamicProperty(ig_account, "id") + "_" + LocalHelper.RandomGUID(3) + ".zip");

            //Clear switch profile

            var profile_folder = Path.GetFullPath(switch_portable) + "\\Data\\profile";

            Directory.Delete(profile_folder, true); Thread.Sleep(1000);

            Directory.CreateDirectory(profile_folder);

            //Release proxy
            ReleaseProxy();

            //Free status
            API.DBHelpers.Accounts_UpdateProperty(LocalHelper.GetDynamicProperty(ig_account, "username"), "status", "");

            //Free portable firefox
            API.DBHelpers.SetSwitchPortableStatus_FF(switch_portable, "");
        }



        #region --Helper--

        private void GetProxy()
        {
            switch (proxy_type)
            {
                case "MOBILE":
                    while (true)
                    {
                        var proxy_str_mobile = PLAutoHelper.ProxyHelper.TinSoft_DB_GetProxy(Settings.proxyDB_XProxy);
                        if (proxy_str_mobile != "" && proxy_str_mobile != null)
                        {
                            fFProxy = new IGFirefoxHelper.FFPCSwitch.FFProxy(proxy_str_mobile, proxy_type);
                            break;
                        }

                        Thread.Sleep(2000);
                    }
                    break;
                case "RESIDENT":
                    while (true)
                    {
                        var proxy_str_resident = PLAutoHelper.ProxyHelper.TinSoft_DB_GetProxy(Settings.proxyDB_Tinsoft);
                        if (proxy_str_resident != "" && proxy_str_resident != null)
                        {
                            fFProxy = new IGFirefoxHelper.FFPCSwitch.FFProxy(proxy_str_resident, proxy_type);
                            break;
                        }
                        Thread.Sleep(2000);
                    }
                    break;
                default:
                    throw new System.ArgumentException("Not recognize this proxy type");
            }
        }

        private void ReleaseProxy()
        {
            switch (proxy_type)
            {
                case "MOBILE":
                    PLAutoHelper.ProxyHelper.TinSoft_DB_ReleaseProxy(fFProxy.proxy_str, Settings.proxyDB_XProxy);
                    break;
                case "RESIDENT":
                    PLAutoHelper.ProxyHelper.TinSoft_DB_ReleaseProxy(fFProxy.proxy_str, Settings.proxyDB_Tinsoft);
                    break;
                default:
                    throw new System.ArgumentException("Not recognize this proxy type");
            }
        }

        #endregion
    }
}
