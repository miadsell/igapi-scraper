﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak
{
    static class LocalHelper
    {
        static public object GetDynamicProperty(object obj, string name)
        {
            return obj.GetType().GetProperty(name).GetValue(obj, null);
        }

        static public string RandomGUID(int count)
        {
            var guid = Guid.NewGuid().ToString().Replace("-", "");

            var optimize_guid = guid.Substring(guid.Length - count);

            return optimize_guid;
        }

        static public string ReplaceFirst(string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }
    }
}
