﻿namespace IGAPI___Ramtinak.IGForms
{
    partial class AIOGrab_Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_settings_commenters_x_days = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_settings_commenters_max_page_loads = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_settings_grab_followers_max_scrape = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_settings_grab_followers_check_x_mins = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tb_settings_likers_x_days = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btn_settings_update = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Grab Commenters";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(65, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Last X Days";
            // 
            // tb_settings_commenters_x_days
            // 
            this.tb_settings_commenters_x_days.Location = new System.Drawing.Point(135, 53);
            this.tb_settings_commenters_x_days.Name = "tb_settings_commenters_x_days";
            this.tb_settings_commenters_x_days.Size = new System.Drawing.Size(100, 20);
            this.tb_settings_commenters_x_days.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Max Page Loads";
            // 
            // tb_settings_commenters_max_page_loads
            // 
            this.tb_settings_commenters_max_page_loads.Location = new System.Drawing.Point(135, 81);
            this.tb_settings_commenters_max_page_loads.Name = "tb_settings_commenters_max_page_loads";
            this.tb_settings_commenters_max_page_loads.Size = new System.Drawing.Size(100, 20);
            this.tb_settings_commenters_max_page_loads.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(254, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(202, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "(grab from post was posted in last X days)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(254, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(353, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "(max page loads per APITask_GrabCommenters - 1 page = 20 comments)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 114);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Grab Followers";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(65, 144);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Max Scrape";
            // 
            // tb_settings_grab_followers_max_scrape
            // 
            this.tb_settings_grab_followers_max_scrape.Location = new System.Drawing.Point(135, 141);
            this.tb_settings_grab_followers_max_scrape.Name = "tb_settings_grab_followers_max_scrape";
            this.tb_settings_grab_followers_max_scrape.Size = new System.Drawing.Size(100, 20);
            this.tb_settings_grab_followers_max_scrape.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(254, 144);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(235, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "max scrape followers per APITask_GrabFollower";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(37, 173);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Check Per X Mins";
            // 
            // tb_settings_grab_followers_check_x_mins
            // 
            this.tb_settings_grab_followers_check_x_mins.Location = new System.Drawing.Point(135, 170);
            this.tb_settings_grab_followers_check_x_mins.Name = "tb_settings_grab_followers_check_x_mins";
            this.tb_settings_grab_followers_check_x_mins.Size = new System.Drawing.Size(100, 20);
            this.tb_settings_grab_followers_check_x_mins.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(254, 173);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(148, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "min time to re-scrape followers";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 200);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Grab Likers";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(254, 226);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(202, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "(grab from post was posted in last X days)";
            // 
            // tb_settings_likers_x_days
            // 
            this.tb_settings_likers_x_days.Location = new System.Drawing.Point(135, 223);
            this.tb_settings_likers_x_days.Name = "tb_settings_likers_x_days";
            this.tb_settings_likers_x_days.Size = new System.Drawing.Size(100, 20);
            this.tb_settings_likers_x_days.TabIndex = 16;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(65, 226);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 13);
            this.label13.TabIndex = 15;
            this.label13.Text = "Last X Days";
            // 
            // btn_settings_update
            // 
            this.btn_settings_update.Location = new System.Drawing.Point(135, 269);
            this.btn_settings_update.Name = "btn_settings_update";
            this.btn_settings_update.Size = new System.Drawing.Size(100, 23);
            this.btn_settings_update.TabIndex = 18;
            this.btn_settings_update.Text = "Update";
            this.btn_settings_update.UseVisualStyleBackColor = true;
            // 
            // AIOGrab_Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_settings_update);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tb_settings_likers_x_days);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tb_settings_grab_followers_check_x_mins);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tb_settings_grab_followers_max_scrape);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_settings_commenters_max_page_loads);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_settings_commenters_x_days);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AIOGrab_Settings";
            this.Text = "AIOGrab_Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_settings_commenters_x_days;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_settings_commenters_max_page_loads;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_settings_grab_followers_max_scrape;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb_settings_grab_followers_check_x_mins;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tb_settings_likers_x_days;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btn_settings_update;
    }
}