﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IGAPI___Ramtinak.IGForms
{
    public partial class ToolSetting : Form
    {
        public ToolSetting()
        {
            InitializeComponent();
        }

        string aio_ig_support = @"Data Source=192.168.1.9;Initial Catalog=aio_support_ig;User ID=sa;Password=123456789kdL;Pooling=true;Max Pool Size=150;Min Pool Size=50;Connect Timeout=30;Connection Lifetime=300;ConnectRetryCount=3";
        dynamic current_setting = null;
        string project_name = null;
        string current_path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        private void button_load_project_Click(object sender, EventArgs e)
        {
            //check if project_name exists
            var projects = ProjectList();

            if(!projects.Contains(comboBox_projects.Text))
            {
                MessageBox.Show("Not recognize this project");
                return;
            }
            project_name = comboBox_projects.Text;

            //get setting from server
            dynamic setting = LoadSettingFromServer(project_name);

            //write to textbox
            textBox_tool_setting.Text = setting.igtoolkit_app_config;
            textBox_aio_grab_settings.Text = setting.igtoolkit_aio_grab_setting;

            ////write to local file
            //File.WriteAllText(current_path + "\\app.ld", setting.igtoolkit_app_config);
            //File.WriteAllText(current_path + "\\settings.txt", setting.igtoolkit_aio_grab_setting);


            //if setting if empty or null, ask to use default setting
            if (setting.igtoolkit_app_config=="" || setting.igtoolkit_aio_grab_setting=="")
            {
                var dr = MessageBox.Show("Settings is empty! Do you want to load default setting and edit manually?", "Annoucement", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (dr == DialogResult.Yes)
                {
                    //if yes, save to local file, reload textbox
                    //write to textbox

                    dynamic default_setting = GenerateDefaultSetting_By_ProjectName(project_name);

                    textBox_tool_setting.Text = default_setting.default_app_config;
                    textBox_aio_grab_settings.Text = default_setting.default_aio_grab_Setting;
                }
            }



            //ask close and reopen to load current project
            MessageBox.Show("Settings are loaded! You must close and reopen software to use new settings", "Annoucement", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }



        private void ToolSetting_Shown(object sender, EventArgs e)
        {
            var projects = ProjectList();

            comboBox_projects.Items.Clear();
            comboBox_projects.Items.AddRange(projects.ToArray());

            //read settings from file
            current_setting = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(File.ReadAllText(current_path + "\\app.ld"));

            project_name = current_setting.project;

            if (!projects.Contains(project_name))
                throw new System.ArgumentException("Not recognize this project");

            comboBox_projects.SelectedItem = project_name;

            textBox_tool_setting.Text = Newtonsoft.Json.JsonConvert.SerializeObject(current_setting, Newtonsoft.Json.Formatting.Indented);

            //load aio_grab_settings

            dynamic setting = LoadSettingFromServer(project_name);
            dynamic aio_grab_settings = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(setting.igtoolkit_aio_grab_setting);
            textBox_aio_grab_settings.Text = Newtonsoft.Json.JsonConvert.SerializeObject(aio_grab_settings, Newtonsoft.Json.Formatting.Indented);
        }


        private void button_sync_setting_to_server_Click(object sender, EventArgs e)
        {
            var project_setting = textBox_tool_setting.Text;
            var aio_grab_settings = textBox_aio_grab_settings.Text;

            //write to local file
            //File.WriteAllText(current_path + "\\app.ld", project_setting);
            //File.WriteAllText(current_path + "\\settings.txt", aio_grab_settings);


            //sync to server
            SynceSettingToServer(project_name, project_setting, aio_grab_settings);


            MessageBox.Show("Synced Complete!!");
        }

        #region --DBHelper--

        public dynamic GenerateDefaultSetting_By_ProjectName(string project_name)
        {
            dynamic project_data = LoadSettingFromServer(project_name);

            dynamic default_app_config_data = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(BMResource.settings.default_app_config.Replace("@project_name", project_name).Replace("@server_ip", project_data.server_ip).Replace("@instagramdb_name", project_data.instagram_db_name).Replace("@igtoolkitdb_name", project_data.igtoolkit_db_name));
            dynamic default_aio_grab_setting_data = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(BMResource.settings.default_aio_grab_setting.Replace("@project_name", project_name));

            dynamic default_setting = new
            {
                default_app_config = Newtonsoft.Json.JsonConvert.SerializeObject(default_app_config_data, Newtonsoft.Json.Formatting.Indented),
                default_aio_grab_Setting = Newtonsoft.Json.JsonConvert.SerializeObject(default_aio_grab_setting_data, Newtonsoft.Json.Formatting.Indented)
            };

            return default_setting;
        }

        public List<string> ProjectList()
        {
            List<string> projects = new List<string>();

            string command_query =
                @"select *
                from ig_projects";

            using (SqlConnection conn = new SqlConnection(aio_ig_support))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                            projects.Add(dataReader["project_name"].ToString());
                    }
                }
            }

            return projects;
        }

        private void SynceSettingToServer(string project_name, string igtoolkit_app_config, string igtoolkit_aio_grab_setting)
        {
            string command_query =
                @"update ig_projects
                set
	                [igtoolkit_app_config]=@igtoolkit_app_config,
	                [igtoolkit_aio_grab_setting]=@igtoolkit_aio_grab_setting
                where
	                [project_name]=@project_name";

            using (SqlConnection conn = new SqlConnection(aio_ig_support))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@igtoolkit_app_config", SqlDbType.NText).Value = igtoolkit_app_config;
                    command.Parameters.Add("@igtoolkit_aio_grab_setting", SqlDbType.NText).Value = igtoolkit_aio_grab_setting;
                    command.Parameters.Add("@project_name", SqlDbType.VarChar).Value = project_name;

                    command.ExecuteNonQuery();
                }
            }
        }

        private dynamic LoadSettingFromServer(string project_name)
        {
            dynamic setting = null;

            string command_query =
                @"select *
                from ig_projects
                where
	                [project_name]=@project_name";

            using (SqlConnection conn = new SqlConnection(aio_ig_support))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@project_name", SqlDbType.VarChar).Value = project_name;

                    using(var dataReader=command.ExecuteReader())
                    {
                        dataReader.Read();

                        setting = new
                        {
                            server_ip=dataReader["server_ip"].ToString(),
                            instagram_db_name=dataReader["instagram_db_name"].ToString(),
                            igtoolkit_db_name=dataReader["igtoolkit_db_name"].ToString(),
                            igtoolkit_app_config = dataReader["igtoolkit_app_config"].ToString(),
                            igtoolkit_aio_grab_setting = dataReader["igtoolkit_aio_grab_setting"].ToString()
                        };
                    }
                }
            }

            return setting;
        }

        #endregion

    }
}

