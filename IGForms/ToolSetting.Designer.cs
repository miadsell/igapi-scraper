﻿namespace IGAPI___Ramtinak.IGForms
{
    partial class ToolSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox_projects = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button_load_project = new System.Windows.Forms.Button();
            this.textBox_tool_setting = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_aio_grab_settings = new System.Windows.Forms.TextBox();
            this.button_sync_setting_to_server = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBox_projects
            // 
            this.comboBox_projects.FormattingEnabled = true;
            this.comboBox_projects.Location = new System.Drawing.Point(95, 8);
            this.comboBox_projects.Name = "comboBox_projects";
            this.comboBox_projects.Size = new System.Drawing.Size(121, 21);
            this.comboBox_projects.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Current Project";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(263, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(403, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "when you change project, it\'ll upload current settings in file to server by proje" +
    "ct name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(263, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(281, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "and synce setting of selected project to file and reload tool";
            // 
            // button_load_project
            // 
            this.button_load_project.Location = new System.Drawing.Point(95, 46);
            this.button_load_project.Name = "button_load_project";
            this.button_load_project.Size = new System.Drawing.Size(121, 23);
            this.button_load_project.TabIndex = 4;
            this.button_load_project.Text = "Load Project";
            this.button_load_project.UseVisualStyleBackColor = true;
            this.button_load_project.Click += new System.EventHandler(this.button_load_project_Click);
            // 
            // textBox_tool_setting
            // 
            this.textBox_tool_setting.Location = new System.Drawing.Point(15, 106);
            this.textBox_tool_setting.Multiline = true;
            this.textBox_tool_setting.Name = "textBox_tool_setting";
            this.textBox_tool_setting.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_tool_setting.Size = new System.Drawing.Size(333, 332);
            this.textBox_tool_setting.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Current Project Settings";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(353, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "AIO Grab Settings";
            // 
            // textBox_aio_grab_settings
            // 
            this.textBox_aio_grab_settings.Location = new System.Drawing.Point(356, 106);
            this.textBox_aio_grab_settings.Multiline = true;
            this.textBox_aio_grab_settings.Name = "textBox_aio_grab_settings";
            this.textBox_aio_grab_settings.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_aio_grab_settings.Size = new System.Drawing.Size(432, 332);
            this.textBox_aio_grab_settings.TabIndex = 8;
            // 
            // button_sync_setting_to_server
            // 
            this.button_sync_setting_to_server.Location = new System.Drawing.Point(266, 46);
            this.button_sync_setting_to_server.Name = "button_sync_setting_to_server";
            this.button_sync_setting_to_server.Size = new System.Drawing.Size(149, 23);
            this.button_sync_setting_to_server.TabIndex = 9;
            this.button_sync_setting_to_server.Text = "Sync Settings To Server";
            this.button_sync_setting_to_server.UseVisualStyleBackColor = true;
            this.button_sync_setting_to_server.Click += new System.EventHandler(this.button_sync_setting_to_server_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(421, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(291, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "save current setting in textbox to local file and sync to server";
            // 
            // ToolSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button_sync_setting_to_server);
            this.Controls.Add(this.textBox_aio_grab_settings);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox_tool_setting);
            this.Controls.Add(this.button_load_project);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox_projects);
            this.Name = "ToolSetting";
            this.Text = "ToolSetting";
            this.Shown += new System.EventHandler(this.ToolSetting_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox_projects;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_load_project;
        private System.Windows.Forms.TextBox textBox_tool_setting;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_aio_grab_settings;
        private System.Windows.Forms.Button button_sync_setting_to_server;
        private System.Windows.Forms.Label label6;
    }
}