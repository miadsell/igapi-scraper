﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IGAPI___Ramtinak.IGForms
{
    public partial class Import : Form
    {
        string table;
        public Import(string table)
        {
            InitializeComponent();
            this.table = table;
        }

        private void btn_import_Click(object sender, EventArgs e)
        {
            List<string> data = new List<string>();

            var reader = new StringReader(tb_data.Text);
            string line;
            while (null != (line = reader.ReadLine()))
            {
                //validate data

                if (!line.Contains("https://www.instagram.com/") && (table == "accounts" || table == "source_users"))
                {
                    MessageBox.Show("Một số dữ liệu sai format", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                //remove last character if contains /

                var item = line;

                if (item[item.Length - 1] != '/' && table == "accounts")
                {
                    item = item + "/";
                }
                else
                {
                    if (table == "hashtags")
                        item = item.Replace("#", "");
                }

                data.Add(item);
            }

            switch(table)
            {
                case "accounts":
                    API.DBHelpers.AddTargetAccountToDB(data, cb_ischeck.Checked);
                    break;
                case "hashtags":
                    API.DBHelpers.AddHashtagToDB(data, cb_ischeck.Checked);
                    break;
                case "source_users":
                    API.DBHelpers.AddSourceUsersToDB(data, cb_ischeck.Checked);
                    break;
            }

            MessageBox.Show("Đã thêm dữ liệu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

            tb_data.Clear();

        }
    }
}
