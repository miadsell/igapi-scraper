﻿namespace IGAPI___Ramtinak
{
    partial class form_igtoolkit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.igurlDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ok = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ig_url = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_grabaccounts = new System.Windows.Forms.Button();
            this.btn_grabposts = new System.Windows.Forms.Button();
            this.btn_stop = new System.Windows.Forms.Button();
            this.lb_status = new System.Windows.Forms.Label();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posturlDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.creationdateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cb_posts_isOkay = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posturl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.creation_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.used = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_update = new System.Windows.Forms.Button();
            this.btn_scrapeuser_info = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.checkBox_aio_grab_APITask_GrabComment_ForSlave = new System.Windows.Forms.CheckBox();
            this.checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers = new System.Windows.Forms.CheckBox();
            this.checkBox_aio_grab_APITask_GrabFollower_ForMother = new System.Windows.Forms.CheckBox();
            this.checkBox_aio_grab_APITask_GrabFollowerBack = new System.Windows.Forms.CheckBox();
            this.checkBox_aio_grab_APITask_GrabPost_ForComment = new System.Windows.Forms.CheckBox();
            this.checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape = new System.Windows.Forms.CheckBox();
            this.checkBox_aio_grab_APITask_GrabUserFullInfo = new System.Windows.Forms.CheckBox();
            this.checkBox_aio_grab_APITask_GrabFollower = new System.Windows.Forms.CheckBox();
            this.checkBox_aio_grab_APITask_GrabLikers = new System.Windows.Forms.CheckBox();
            this.checkBox_aio_grab_APITask_GrabCommenters = new System.Windows.Forms.CheckBox();
            this.checkBox_aio_grab_APITask_GrabPost = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_aio_log = new System.Windows.Forms.TextBox();
            this.btn_grab_aio = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.checkBox_aio_detect_proxy_server = new System.Windows.Forms.CheckBox();
            this.checkBox_aio_detect_DetectNameImage = new System.Windows.Forms.CheckBox();
            this.tb_aio_detect_proxy_apikey = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBox_aio_detect_DetectCountry = new System.Windows.Forms.CheckBox();
            this.checkBox_aio_detect_DetectGender = new System.Windows.Forms.CheckBox();
            this.checkBox_aio_detect_DetectLanguage = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_namsor_quantity = new System.Windows.Forms.TextBox();
            this.btn_reg_namsor = new System.Windows.Forms.Button();
            this.tb_detect_personality_log = new System.Windows.Forms.TextBox();
            this.btn_detect_aio = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btn_target_accounts_details = new System.Windows.Forms.Button();
            this.btn_import_target_accounts = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBox_target_accounts_checked_not_OK = new System.Windows.Forms.CheckBox();
            this.combobox_target_accounts_sort = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBox_target_accounts_checked_OK = new System.Windows.Forms.CheckBox();
            this.checkBox_target_accounts_not_set_scrapetype = new System.Windows.Forms.CheckBox();
            this.checkBox_target_accounts_not_checked = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridView_target_accounts = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btn_source_users_details = new System.Windows.Forms.Button();
            this.btn_import_source_users = new System.Windows.Forms.Button();
            this.btn_grab_source_users = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.checkBox_source_users_not_checked = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dataGridView_source_users = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.button_hashtags_grab_details = new System.Windows.Forms.Button();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.dataGridView_hashtags = new System.Windows.Forms.DataGridView();
            this.tabPage18 = new System.Windows.Forms.TabPage();
            this.textBox_hashtags_tutorial = new System.Windows.Forms.TextBox();
            this.button_hashtags_grab_hashtags = new System.Windows.Forms.Button();
            this.button_hashtags_import = new System.Windows.Forms.Button();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.checkBox_comment_account_notcheck = new System.Windows.Forms.CheckBox();
            this.button_commentaccount_grab_related_account = new System.Windows.Forms.Button();
            this.button_commentaccount_import = new System.Windows.Forms.Button();
            this.dataGridView_commentaccount = new System.Windows.Forms.DataGridView();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.label_copy_content_status = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.button_copycontent_copy_now = new System.Windows.Forms.Button();
            this.textBox_copycontent_niche_content = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tabControl_copycontent_type = new System.Windows.Forms.TabControl();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.textBox_copycontent_copy_from_accounts_max_pages = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox_copycontent_copy_from_accounts_list_accounts = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.textBox_copycontent_copy_from_posts_listposts = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.button_upload_gdrive = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.button_copycontent_uploadnow = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.tabPage19 = new System.Windows.Forms.TabPage();
            this.label26 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.button__copycontent_updatepost_updatenow = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox_copycontent_updatepost_list_post_ids = new System.Windows.Forms.TextBox();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.textBox_copycontent_tips = new System.Windows.Forms.TextBox();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.label17 = new System.Windows.Forms.Label();
            this.button_leech_resources_images_download = new System.Windows.Forms.Button();
            this.label_leech_resources_images_status = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.button_leech_resources_images_browse_des = new System.Windows.Forms.Button();
            this.textBox_leech_resources_images_des_folder = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox_leech_resources_images_listposts = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.tabPage20 = new System.Windows.Forms.TabPage();
            this.tabControl5 = new System.Windows.Forms.TabControl();
            this.tabPage21 = new System.Windows.Forms.TabPage();
            this.btn_addon_account_details_grab = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox_addon_account_details_list = new System.Windows.Forms.TextBox();
            this.tabPage22 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_launch_firefox = new System.Windows.Forms.Button();
            this.button_tool_settings = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label_autorun_counter = new System.Windows.Forms.Label();
            this.button_stop_autorun = new System.Windows.Forms.Button();
            this.button_login_to_newaccount = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_target_accounts)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_source_users)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tabPage17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_hashtags)).BeginInit();
            this.tabPage18.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_commentaccount)).BeginInit();
            this.tabPage8.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabControl_copycontent_type.SuspendLayout();
            this.tabPage13.SuspendLayout();
            this.tabPage15.SuspendLayout();
            this.tabPage16.SuspendLayout();
            this.tabPage19.SuspendLayout();
            this.tabPage14.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.tabPage20.SuspendLayout();
            this.tabControl5.SuspendLayout();
            this.tabPage21.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // igurlDataGridViewTextBoxColumn
            // 
            this.igurlDataGridViewTextBoxColumn.DataPropertyName = "ig_url";
            this.igurlDataGridViewTextBoxColumn.HeaderText = "ig_url";
            this.igurlDataGridViewTextBoxColumn.Name = "igurlDataGridViewTextBoxColumn";
            // 
            // ok
            // 
            this.ok.DataPropertyName = "ok";
            this.ok.HeaderText = "ok";
            this.ok.Name = "ok";
            this.ok.ReadOnly = true;
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "id";
            this.id.Name = "id";
            // 
            // ig_url
            // 
            this.ig_url.DataPropertyName = "ig_url";
            this.ig_url.HeaderText = "ig_url";
            this.ig_url.Name = "ig_url";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ok";
            this.dataGridViewTextBoxColumn1.HeaderText = "ok";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn2.HeaderText = "id";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ig_url";
            this.dataGridViewTextBoxColumn3.HeaderText = "ig_url";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ok";
            this.dataGridViewTextBoxColumn4.HeaderText = "ok";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn5.HeaderText = "id";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // btn_grabaccounts
            // 
            this.btn_grabaccounts.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_grabaccounts.Location = new System.Drawing.Point(710, 320);
            this.btn_grabaccounts.Name = "btn_grabaccounts";
            this.btn_grabaccounts.Size = new System.Drawing.Size(138, 23);
            this.btn_grabaccounts.TabIndex = 7;
            this.btn_grabaccounts.Text = "Grab Target Accounts";
            this.btn_grabaccounts.UseVisualStyleBackColor = true;
            this.btn_grabaccounts.Click += new System.EventHandler(this.btn_grabaccounts_Click);
            // 
            // btn_grabposts
            // 
            this.btn_grabposts.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_grabposts.Location = new System.Drawing.Point(3, 4);
            this.btn_grabposts.Name = "btn_grabposts";
            this.btn_grabposts.Size = new System.Drawing.Size(107, 23);
            this.btn_grabposts.TabIndex = 9;
            this.btn_grabposts.Text = "Grab Posts";
            this.btn_grabposts.UseVisualStyleBackColor = true;
            this.btn_grabposts.Click += new System.EventHandler(this.btn_grabposts_Click);
            // 
            // btn_stop
            // 
            this.btn_stop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_stop.Location = new System.Drawing.Point(234, 5);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(99, 52);
            this.btn_stop.TabIndex = 10;
            this.btn_stop.Text = "Stop";
            this.btn_stop.UseVisualStyleBackColor = true;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // lb_status
            // 
            this.lb_status.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_status.AutoSize = true;
            this.lb_status.Location = new System.Drawing.Point(349, 25);
            this.lb_status.Name = "lb_status";
            this.lb_status.Size = new System.Drawing.Size(47, 13);
            this.lb_status.TabIndex = 11;
            this.lb_status.Text = "Stopped";
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // posturlDataGridViewTextBoxColumn
            // 
            this.posturlDataGridViewTextBoxColumn.DataPropertyName = "posturl";
            this.posturlDataGridViewTextBoxColumn.HeaderText = "posturl";
            this.posturlDataGridViewTextBoxColumn.Name = "posturlDataGridViewTextBoxColumn";
            // 
            // creationdateDataGridViewTextBoxColumn
            // 
            this.creationdateDataGridViewTextBoxColumn.DataPropertyName = "creation_date";
            this.creationdateDataGridViewTextBoxColumn.HeaderText = "creation_date";
            this.creationdateDataGridViewTextBoxColumn.Name = "creationdateDataGridViewTextBoxColumn";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "ok";
            this.dataGridViewTextBoxColumn6.HeaderText = "ok";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // cb_posts_isOkay
            // 
            this.cb_posts_isOkay.HeaderText = "IsOkay?";
            this.cb_posts_isOkay.Name = "cb_posts_isOkay";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn7.HeaderText = "id";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // posturl
            // 
            this.posturl.DataPropertyName = "posturl";
            this.posturl.HeaderText = "posturl";
            this.posturl.Name = "posturl";
            // 
            // creation_date
            // 
            this.creation_date.DataPropertyName = "creation_date";
            this.creation_date.HeaderText = "creation_date";
            this.creation_date.Name = "creation_date";
            // 
            // used
            // 
            this.used.DataPropertyName = "used";
            this.used.HeaderText = "used";
            this.used.Name = "used";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "ok";
            this.dataGridViewTextBoxColumn8.HeaderText = "ok";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // btn_update
            // 
            this.btn_update.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_update.BackColor = System.Drawing.Color.SlateGray;
            this.btn_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_update.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_update.Location = new System.Drawing.Point(13, 420);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(136, 46);
            this.btn_update.TabIndex = 12;
            this.btn_update.Text = "Update";
            this.btn_update.UseVisualStyleBackColor = false;
            this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
            // 
            // btn_scrapeuser_info
            // 
            this.btn_scrapeuser_info.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_scrapeuser_info.Location = new System.Drawing.Point(3, 34);
            this.btn_scrapeuser_info.Name = "btn_scrapeuser_info";
            this.btn_scrapeuser_info.Size = new System.Drawing.Size(213, 23);
            this.btn_scrapeuser_info.TabIndex = 16;
            this.btn_scrapeuser_info.Text = "Scrape Full Info";
            this.btn_scrapeuser_info.UseVisualStyleBackColor = true;
            this.btn_scrapeuser_info.Click += new System.EventHandler(this.btn_scrapeuser_info_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage20);
            this.tabControl1.Location = new System.Drawing.Point(11, 43);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(863, 375);
            this.tabControl1.TabIndex = 18;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.checkBox_aio_grab_APITask_GrabComment_ForSlave);
            this.tabPage3.Controls.Add(this.checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers);
            this.tabPage3.Controls.Add(this.checkBox_aio_grab_APITask_GrabFollower_ForMother);
            this.tabPage3.Controls.Add(this.checkBox_aio_grab_APITask_GrabFollowerBack);
            this.tabPage3.Controls.Add(this.checkBox_aio_grab_APITask_GrabPost_ForComment);
            this.tabPage3.Controls.Add(this.checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape);
            this.tabPage3.Controls.Add(this.checkBox_aio_grab_APITask_GrabUserFullInfo);
            this.tabPage3.Controls.Add(this.checkBox_aio_grab_APITask_GrabFollower);
            this.tabPage3.Controls.Add(this.checkBox_aio_grab_APITask_GrabLikers);
            this.tabPage3.Controls.Add(this.checkBox_aio_grab_APITask_GrabCommenters);
            this.tabPage3.Controls.Add(this.checkBox_aio_grab_APITask_GrabPost);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.tb_aio_log);
            this.tabPage3.Controls.Add(this.btn_grab_aio);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(855, 349);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "AIO Grab";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // checkBox_aio_grab_APITask_GrabComment_ForSlave
            // 
            this.checkBox_aio_grab_APITask_GrabComment_ForSlave.AutoSize = true;
            this.checkBox_aio_grab_APITask_GrabComment_ForSlave.Location = new System.Drawing.Point(466, 65);
            this.checkBox_aio_grab_APITask_GrabComment_ForSlave.Name = "checkBox_aio_grab_APITask_GrabComment_ForSlave";
            this.checkBox_aio_grab_APITask_GrabComment_ForSlave.Size = new System.Drawing.Size(188, 17);
            this.checkBox_aio_grab_APITask_GrabComment_ForSlave.TabIndex = 38;
            this.checkBox_aio_grab_APITask_GrabComment_ForSlave.Text = "APITask_GrabComment_ForSlave";
            this.checkBox_aio_grab_APITask_GrabComment_ForSlave.UseVisualStyleBackColor = true;
            this.checkBox_aio_grab_APITask_GrabComment_ForSlave.CheckedChanged += new System.EventHandler(this.checkBox_aio_grab_APITask_GrabComment_ForSlave_CheckedChanged);
            // 
            // checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers
            // 
            this.checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers.AutoSize = true;
            this.checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers.Location = new System.Drawing.Point(211, 65);
            this.checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers.Name = "checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers";
            this.checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers.Size = new System.Drawing.Size(249, 17);
            this.checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers.TabIndex = 37;
            this.checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers.Text = "APITask_GrabPost_ForComment_ScrapeUsers";
            this.checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers.UseVisualStyleBackColor = true;
            this.checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers.CheckedChanged += new System.EventHandler(this.checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers_CheckedChanged);
            // 
            // checkBox_aio_grab_APITask_GrabFollower_ForMother
            // 
            this.checkBox_aio_grab_APITask_GrabFollower_ForMother.AutoSize = true;
            this.checkBox_aio_grab_APITask_GrabFollower_ForMother.Checked = true;
            this.checkBox_aio_grab_APITask_GrabFollower_ForMother.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_aio_grab_APITask_GrabFollower_ForMother.Location = new System.Drawing.Point(20, 65);
            this.checkBox_aio_grab_APITask_GrabFollower_ForMother.Name = "checkBox_aio_grab_APITask_GrabFollower_ForMother";
            this.checkBox_aio_grab_APITask_GrabFollower_ForMother.Size = new System.Drawing.Size(189, 17);
            this.checkBox_aio_grab_APITask_GrabFollower_ForMother.TabIndex = 36;
            this.checkBox_aio_grab_APITask_GrabFollower_ForMother.Text = "APITask_GrabFollower_ForMother";
            this.checkBox_aio_grab_APITask_GrabFollower_ForMother.UseVisualStyleBackColor = true;
            this.checkBox_aio_grab_APITask_GrabFollower_ForMother.CheckedChanged += new System.EventHandler(this.checkBox_aio_grab_APITask_GrabFollower_ForMother_CheckedChanged);
            // 
            // checkBox_aio_grab_APITask_GrabFollowerBack
            // 
            this.checkBox_aio_grab_APITask_GrabFollowerBack.AutoSize = true;
            this.checkBox_aio_grab_APITask_GrabFollowerBack.Location = new System.Drawing.Point(581, 42);
            this.checkBox_aio_grab_APITask_GrabFollowerBack.Name = "checkBox_aio_grab_APITask_GrabFollowerBack";
            this.checkBox_aio_grab_APITask_GrabFollowerBack.Size = new System.Drawing.Size(160, 17);
            this.checkBox_aio_grab_APITask_GrabFollowerBack.TabIndex = 35;
            this.checkBox_aio_grab_APITask_GrabFollowerBack.Text = "APITask_GrabFollowerBack";
            this.checkBox_aio_grab_APITask_GrabFollowerBack.UseVisualStyleBackColor = true;
            this.checkBox_aio_grab_APITask_GrabFollowerBack.CheckedChanged += new System.EventHandler(this.checkBox_aio_grab_APITask_GrabFollowerBack_CheckedChanged);
            // 
            // checkBox_aio_grab_APITask_GrabPost_ForComment
            // 
            this.checkBox_aio_grab_APITask_GrabPost_ForComment.AutoSize = true;
            this.checkBox_aio_grab_APITask_GrabPost_ForComment.Location = new System.Drawing.Point(581, 19);
            this.checkBox_aio_grab_APITask_GrabPost_ForComment.Name = "checkBox_aio_grab_APITask_GrabPost_ForComment";
            this.checkBox_aio_grab_APITask_GrabPost_ForComment.Size = new System.Drawing.Size(182, 17);
            this.checkBox_aio_grab_APITask_GrabPost_ForComment.TabIndex = 34;
            this.checkBox_aio_grab_APITask_GrabPost_ForComment.Text = "APITask_GrabPost_ForComment";
            this.checkBox_aio_grab_APITask_GrabPost_ForComment.UseVisualStyleBackColor = true;
            this.checkBox_aio_grab_APITask_GrabPost_ForComment.CheckedChanged += new System.EventHandler(this.checkBox_aio_grab_APITask_GrabPost_ForComment_CheckedChanged);
            // 
            // checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape
            // 
            this.checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape.AutoSize = true;
            this.checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape.Location = new System.Drawing.Point(368, 42);
            this.checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape.Name = "checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape";
            this.checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape.Size = new System.Drawing.Size(204, 17);
            this.checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape.TabIndex = 33;
            this.checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape.Text = "APITask_GrabUserFullInfo_Rescrape";
            this.checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape.UseVisualStyleBackColor = true;
            this.checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape.CheckedChanged += new System.EventHandler(this.checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape_CheckedChanged);
            // 
            // checkBox_aio_grab_APITask_GrabUserFullInfo
            // 
            this.checkBox_aio_grab_APITask_GrabUserFullInfo.AutoSize = true;
            this.checkBox_aio_grab_APITask_GrabUserFullInfo.Location = new System.Drawing.Point(368, 19);
            this.checkBox_aio_grab_APITask_GrabUserFullInfo.Name = "checkBox_aio_grab_APITask_GrabUserFullInfo";
            this.checkBox_aio_grab_APITask_GrabUserFullInfo.Size = new System.Drawing.Size(152, 17);
            this.checkBox_aio_grab_APITask_GrabUserFullInfo.TabIndex = 32;
            this.checkBox_aio_grab_APITask_GrabUserFullInfo.Text = "APITask_GrabUserFullInfo";
            this.checkBox_aio_grab_APITask_GrabUserFullInfo.UseVisualStyleBackColor = true;
            this.checkBox_aio_grab_APITask_GrabUserFullInfo.CheckedChanged += new System.EventHandler(this.checkBox_aio_grab_APITask_GrabUserFullInfo_CheckedChanged);
            // 
            // checkBox_aio_grab_APITask_GrabFollower
            // 
            this.checkBox_aio_grab_APITask_GrabFollower.AutoSize = true;
            this.checkBox_aio_grab_APITask_GrabFollower.Checked = true;
            this.checkBox_aio_grab_APITask_GrabFollower.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_aio_grab_APITask_GrabFollower.Location = new System.Drawing.Point(211, 42);
            this.checkBox_aio_grab_APITask_GrabFollower.Name = "checkBox_aio_grab_APITask_GrabFollower";
            this.checkBox_aio_grab_APITask_GrabFollower.Size = new System.Drawing.Size(135, 17);
            this.checkBox_aio_grab_APITask_GrabFollower.TabIndex = 31;
            this.checkBox_aio_grab_APITask_GrabFollower.Text = "APITask_GrabFollower";
            this.checkBox_aio_grab_APITask_GrabFollower.UseVisualStyleBackColor = true;
            this.checkBox_aio_grab_APITask_GrabFollower.CheckedChanged += new System.EventHandler(this.checkBox_aio_grab_APITask_GrabFollower_CheckedChanged);
            // 
            // checkBox_aio_grab_APITask_GrabLikers
            // 
            this.checkBox_aio_grab_APITask_GrabLikers.AutoSize = true;
            this.checkBox_aio_grab_APITask_GrabLikers.Checked = true;
            this.checkBox_aio_grab_APITask_GrabLikers.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_aio_grab_APITask_GrabLikers.Location = new System.Drawing.Point(211, 19);
            this.checkBox_aio_grab_APITask_GrabLikers.Name = "checkBox_aio_grab_APITask_GrabLikers";
            this.checkBox_aio_grab_APITask_GrabLikers.Size = new System.Drawing.Size(124, 17);
            this.checkBox_aio_grab_APITask_GrabLikers.TabIndex = 30;
            this.checkBox_aio_grab_APITask_GrabLikers.Text = "APITask_GrabLikers";
            this.checkBox_aio_grab_APITask_GrabLikers.UseVisualStyleBackColor = true;
            this.checkBox_aio_grab_APITask_GrabLikers.CheckedChanged += new System.EventHandler(this.checkBox_aio_grab_APITask_GrabLikers_CheckedChanged);
            // 
            // checkBox_aio_grab_APITask_GrabCommenters
            // 
            this.checkBox_aio_grab_APITask_GrabCommenters.AutoSize = true;
            this.checkBox_aio_grab_APITask_GrabCommenters.Checked = true;
            this.checkBox_aio_grab_APITask_GrabCommenters.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_aio_grab_APITask_GrabCommenters.Location = new System.Drawing.Point(20, 42);
            this.checkBox_aio_grab_APITask_GrabCommenters.Name = "checkBox_aio_grab_APITask_GrabCommenters";
            this.checkBox_aio_grab_APITask_GrabCommenters.Size = new System.Drawing.Size(154, 17);
            this.checkBox_aio_grab_APITask_GrabCommenters.TabIndex = 29;
            this.checkBox_aio_grab_APITask_GrabCommenters.Text = "APITask_GrabCommenters";
            this.checkBox_aio_grab_APITask_GrabCommenters.UseVisualStyleBackColor = true;
            this.checkBox_aio_grab_APITask_GrabCommenters.CheckedChanged += new System.EventHandler(this.checkBox_aio_grab_APITask_GrabCommenters_CheckedChanged);
            // 
            // checkBox_aio_grab_APITask_GrabPost
            // 
            this.checkBox_aio_grab_APITask_GrabPost.AutoSize = true;
            this.checkBox_aio_grab_APITask_GrabPost.Checked = true;
            this.checkBox_aio_grab_APITask_GrabPost.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_aio_grab_APITask_GrabPost.Location = new System.Drawing.Point(20, 19);
            this.checkBox_aio_grab_APITask_GrabPost.Name = "checkBox_aio_grab_APITask_GrabPost";
            this.checkBox_aio_grab_APITask_GrabPost.Size = new System.Drawing.Size(117, 17);
            this.checkBox_aio_grab_APITask_GrabPost.TabIndex = 28;
            this.checkBox_aio_grab_APITask_GrabPost.Text = "APITask_GrabPost";
            this.checkBox_aio_grab_APITask_GrabPost.UseVisualStyleBackColor = true;
            this.checkBox_aio_grab_APITask_GrabPost.CheckedChanged += new System.EventHandler(this.checkBox_aio_grab_APITask_GrabPost_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Grab Options";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(141, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(301, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "note: please refer to settings.txt in exe folder to update settings";
            // 
            // tb_aio_log
            // 
            this.tb_aio_log.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_aio_log.Location = new System.Drawing.Point(6, 129);
            this.tb_aio_log.Multiline = true;
            this.tb_aio_log.Name = "tb_aio_log";
            this.tb_aio_log.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_aio_log.Size = new System.Drawing.Size(842, 214);
            this.tb_aio_log.TabIndex = 25;
            // 
            // btn_grab_aio
            // 
            this.btn_grab_aio.Location = new System.Drawing.Point(6, 100);
            this.btn_grab_aio.Name = "btn_grab_aio";
            this.btn_grab_aio.Size = new System.Drawing.Size(71, 23);
            this.btn_grab_aio.TabIndex = 24;
            this.btn_grab_aio.Text = "Grab AIO";
            this.btn_grab_aio.UseVisualStyleBackColor = true;
            this.btn_grab_aio.Click += new System.EventHandler(this.btn_grab_aio_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.checkBox_aio_detect_proxy_server);
            this.tabPage4.Controls.Add(this.checkBox_aio_detect_DetectNameImage);
            this.tabPage4.Controls.Add(this.tb_aio_detect_proxy_apikey);
            this.tabPage4.Controls.Add(this.label8);
            this.tabPage4.Controls.Add(this.checkBox_aio_detect_DetectCountry);
            this.tabPage4.Controls.Add(this.checkBox_aio_detect_DetectGender);
            this.tabPage4.Controls.Add(this.checkBox_aio_detect_DetectLanguage);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.tb_namsor_quantity);
            this.tabPage4.Controls.Add(this.btn_reg_namsor);
            this.tabPage4.Controls.Add(this.tb_detect_personality_log);
            this.tabPage4.Controls.Add(this.btn_detect_aio);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(855, 349);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Detect Personality";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // checkBox_aio_detect_proxy_server
            // 
            this.checkBox_aio_detect_proxy_server.Location = new System.Drawing.Point(276, 30);
            this.checkBox_aio_detect_proxy_server.Name = "checkBox_aio_detect_proxy_server";
            this.checkBox_aio_detect_proxy_server.Size = new System.Drawing.Size(338, 40);
            this.checkBox_aio_detect_proxy_server.TabIndex = 11;
            this.checkBox_aio_detect_proxy_server.Text = "Run Proxy Server (just use if another thread not run proxy server (tinsoft or tmp" +
    "roxy)";
            this.checkBox_aio_detect_proxy_server.UseVisualStyleBackColor = true;
            // 
            // checkBox_aio_detect_DetectNameImage
            // 
            this.checkBox_aio_detect_DetectNameImage.AutoSize = true;
            this.checkBox_aio_detect_DetectNameImage.Location = new System.Drawing.Point(6, 32);
            this.checkBox_aio_detect_DetectNameImage.Name = "checkBox_aio_detect_DetectNameImage";
            this.checkBox_aio_detect_DetectNameImage.Size = new System.Drawing.Size(127, 17);
            this.checkBox_aio_detect_DetectNameImage.TabIndex = 10;
            this.checkBox_aio_detect_DetectNameImage.Text = "Detect Name - Image";
            this.checkBox_aio_detect_DetectNameImage.UseVisualStyleBackColor = true;
            this.checkBox_aio_detect_DetectNameImage.CheckedChanged += new System.EventHandler(this.checkBox_aio_detect_DetectNameImage_CheckedChanged);
            // 
            // tb_aio_detect_proxy_apikey
            // 
            this.tb_aio_detect_proxy_apikey.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_aio_detect_proxy_apikey.Location = new System.Drawing.Point(296, 4);
            this.tb_aio_detect_proxy_apikey.Name = "tb_aio_detect_proxy_apikey";
            this.tb_aio_detect_proxy_apikey.Size = new System.Drawing.Size(318, 20);
            this.tb_aio_detect_proxy_apikey.TabIndex = 9;
            this.tb_aio_detect_proxy_apikey.Text = "60953b0d29f762efe9c6092d3b79e85b";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(205, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Proxy ApiKey";
            // 
            // checkBox_aio_detect_DetectCountry
            // 
            this.checkBox_aio_detect_DetectCountry.AutoSize = true;
            this.checkBox_aio_detect_DetectCountry.Checked = true;
            this.checkBox_aio_detect_DetectCountry.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_aio_detect_DetectCountry.Location = new System.Drawing.Point(144, 32);
            this.checkBox_aio_detect_DetectCountry.Name = "checkBox_aio_detect_DetectCountry";
            this.checkBox_aio_detect_DetectCountry.Size = new System.Drawing.Size(97, 17);
            this.checkBox_aio_detect_DetectCountry.TabIndex = 7;
            this.checkBox_aio_detect_DetectCountry.Text = "Detect Country";
            this.checkBox_aio_detect_DetectCountry.UseVisualStyleBackColor = true;
            this.checkBox_aio_detect_DetectCountry.CheckedChanged += new System.EventHandler(this.checkBox_aio_detect_DetectCountry_CheckedChanged);
            // 
            // checkBox_aio_detect_DetectGender
            // 
            this.checkBox_aio_detect_DetectGender.AutoSize = true;
            this.checkBox_aio_detect_DetectGender.Location = new System.Drawing.Point(6, 55);
            this.checkBox_aio_detect_DetectGender.Name = "checkBox_aio_detect_DetectGender";
            this.checkBox_aio_detect_DetectGender.Size = new System.Drawing.Size(96, 17);
            this.checkBox_aio_detect_DetectGender.TabIndex = 6;
            this.checkBox_aio_detect_DetectGender.Text = "Detect Gender";
            this.checkBox_aio_detect_DetectGender.UseVisualStyleBackColor = true;
            this.checkBox_aio_detect_DetectGender.CheckedChanged += new System.EventHandler(this.checkBox_aio_detect_DetectGender_CheckedChanged);
            // 
            // checkBox_aio_detect_DetectLanguage
            // 
            this.checkBox_aio_detect_DetectLanguage.AutoSize = true;
            this.checkBox_aio_detect_DetectLanguage.Enabled = false;
            this.checkBox_aio_detect_DetectLanguage.Location = new System.Drawing.Point(144, 55);
            this.checkBox_aio_detect_DetectLanguage.Name = "checkBox_aio_detect_DetectLanguage";
            this.checkBox_aio_detect_DetectLanguage.Size = new System.Drawing.Size(109, 17);
            this.checkBox_aio_detect_DetectLanguage.TabIndex = 5;
            this.checkBox_aio_detect_DetectLanguage.Text = "Detect Language";
            this.checkBox_aio_detect_DetectLanguage.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Detect Options";
            // 
            // tb_namsor_quantity
            // 
            this.tb_namsor_quantity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_namsor_quantity.Location = new System.Drawing.Point(541, 93);
            this.tb_namsor_quantity.Name = "tb_namsor_quantity";
            this.tb_namsor_quantity.Size = new System.Drawing.Size(73, 20);
            this.tb_namsor_quantity.TabIndex = 3;
            // 
            // btn_reg_namsor
            // 
            this.btn_reg_namsor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_reg_namsor.Location = new System.Drawing.Point(418, 91);
            this.btn_reg_namsor.Name = "btn_reg_namsor";
            this.btn_reg_namsor.Size = new System.Drawing.Size(121, 23);
            this.btn_reg_namsor.TabIndex = 2;
            this.btn_reg_namsor.Text = "Namsor Register";
            this.btn_reg_namsor.UseVisualStyleBackColor = true;
            this.btn_reg_namsor.Click += new System.EventHandler(this.btn_reg_namsor_Click);
            // 
            // tb_detect_personality_log
            // 
            this.tb_detect_personality_log.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_detect_personality_log.Location = new System.Drawing.Point(6, 120);
            this.tb_detect_personality_log.Multiline = true;
            this.tb_detect_personality_log.Name = "tb_detect_personality_log";
            this.tb_detect_personality_log.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_detect_personality_log.Size = new System.Drawing.Size(608, 176);
            this.tb_detect_personality_log.TabIndex = 1;
            // 
            // btn_detect_aio
            // 
            this.btn_detect_aio.Location = new System.Drawing.Point(6, 91);
            this.btn_detect_aio.Name = "btn_detect_aio";
            this.btn_detect_aio.Size = new System.Drawing.Size(75, 23);
            this.btn_detect_aio.TabIndex = 0;
            this.btn_detect_aio.Text = "Detect AIO";
            this.btn_detect_aio.UseVisualStyleBackColor = true;
            this.btn_detect_aio.Click += new System.EventHandler(this.btn_detect_aio_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btn_target_accounts_details);
            this.tabPage1.Controls.Add(this.btn_import_target_accounts);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.dataGridView_target_accounts);
            this.tabPage1.Controls.Add(this.btn_grabaccounts);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(855, 349);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Target Accounts";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btn_target_accounts_details
            // 
            this.btn_target_accounts_details.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_target_accounts_details.Location = new System.Drawing.Point(594, 320);
            this.btn_target_accounts_details.Name = "btn_target_accounts_details";
            this.btn_target_accounts_details.Size = new System.Drawing.Size(101, 23);
            this.btn_target_accounts_details.TabIndex = 9;
            this.btn_target_accounts_details.Text = "Grab Details";
            this.btn_target_accounts_details.UseVisualStyleBackColor = true;
            this.btn_target_accounts_details.Click += new System.EventHandler(this.btn_target_accounts_details_Click);
            // 
            // btn_import_target_accounts
            // 
            this.btn_import_target_accounts.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_import_target_accounts.Location = new System.Drawing.Point(503, 320);
            this.btn_import_target_accounts.Name = "btn_import_target_accounts";
            this.btn_import_target_accounts.Size = new System.Drawing.Size(75, 23);
            this.btn_import_target_accounts.TabIndex = 8;
            this.btn_import_target_accounts.Text = "Import";
            this.btn_import_target_accounts.UseVisualStyleBackColor = true;
            this.btn_import_target_accounts.Click += new System.EventHandler(this.btn_import_target_accounts_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.checkBox_target_accounts_checked_not_OK);
            this.panel1.Controls.Add(this.combobox_target_accounts_sort);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.checkBox_target_accounts_checked_OK);
            this.panel1.Controls.Add(this.checkBox_target_accounts_not_set_scrapetype);
            this.panel1.Controls.Add(this.checkBox_target_accounts_not_checked);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(6, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(842, 65);
            this.panel1.TabIndex = 1;
            // 
            // checkBox_target_accounts_checked_not_OK
            // 
            this.checkBox_target_accounts_checked_not_OK.AutoSize = true;
            this.checkBox_target_accounts_checked_not_OK.Location = new System.Drawing.Point(161, 28);
            this.checkBox_target_accounts_checked_not_OK.Name = "checkBox_target_accounts_checked_not_OK";
            this.checkBox_target_accounts_checked_not_OK.Size = new System.Drawing.Size(61, 17);
            this.checkBox_target_accounts_checked_not_OK.TabIndex = 6;
            this.checkBox_target_accounts_checked_not_OK.Text = "Not OK";
            this.checkBox_target_accounts_checked_not_OK.UseVisualStyleBackColor = true;
            this.checkBox_target_accounts_checked_not_OK.CheckedChanged += new System.EventHandler(this.checkBox_target_accounts_checked_not_OK_CheckedChanged);
            // 
            // combobox_target_accounts_sort
            // 
            this.combobox_target_accounts_sort.FormattingEnabled = true;
            this.combobox_target_accounts_sort.Items.AddRange(new object[] {
            "Added",
            "NoSort"});
            this.combobox_target_accounts_sort.Location = new System.Drawing.Point(341, 3);
            this.combobox_target_accounts_sort.Name = "combobox_target_accounts_sort";
            this.combobox_target_accounts_sort.Size = new System.Drawing.Size(107, 21);
            this.combobox_target_accounts_sort.TabIndex = 5;
            this.combobox_target_accounts_sort.SelectedIndexChanged += new System.EventHandler(this.combobox_target_accounts_sort_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(297, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "SortBy";
            // 
            // checkBox_target_accounts_checked_OK
            // 
            this.checkBox_target_accounts_checked_OK.AutoSize = true;
            this.checkBox_target_accounts_checked_OK.Location = new System.Drawing.Point(52, 28);
            this.checkBox_target_accounts_checked_OK.Name = "checkBox_target_accounts_checked_OK";
            this.checkBox_target_accounts_checked_OK.Size = new System.Drawing.Size(41, 17);
            this.checkBox_target_accounts_checked_OK.TabIndex = 3;
            this.checkBox_target_accounts_checked_OK.Text = "OK";
            this.checkBox_target_accounts_checked_OK.UseVisualStyleBackColor = true;
            this.checkBox_target_accounts_checked_OK.CheckedChanged += new System.EventHandler(this.checkBox_target_accounts_checked_OK_CheckedChanged);
            // 
            // checkBox_target_accounts_not_set_scrapetype
            // 
            this.checkBox_target_accounts_not_set_scrapetype.AutoSize = true;
            this.checkBox_target_accounts_not_set_scrapetype.Location = new System.Drawing.Point(161, 5);
            this.checkBox_target_accounts_not_set_scrapetype.Name = "checkBox_target_accounts_not_set_scrapetype";
            this.checkBox_target_accounts_not_set_scrapetype.Size = new System.Drawing.Size(126, 17);
            this.checkBox_target_accounts_not_set_scrapetype.TabIndex = 2;
            this.checkBox_target_accounts_not_set_scrapetype.Text = "Not Set Scrape Type";
            this.checkBox_target_accounts_not_set_scrapetype.UseVisualStyleBackColor = true;
            this.checkBox_target_accounts_not_set_scrapetype.CheckedChanged += new System.EventHandler(this.checkBox_target_accounts_not_set_scrapetype_CheckedChanged);
            // 
            // checkBox_target_accounts_not_checked
            // 
            this.checkBox_target_accounts_not_checked.AutoSize = true;
            this.checkBox_target_accounts_not_checked.Location = new System.Drawing.Point(52, 5);
            this.checkBox_target_accounts_not_checked.Name = "checkBox_target_accounts_not_checked";
            this.checkBox_target_accounts_not_checked.Size = new System.Drawing.Size(89, 17);
            this.checkBox_target_accounts_not_checked.TabIndex = 1;
            this.checkBox_target_accounts_not_checked.Text = "Not Checked";
            this.checkBox_target_accounts_not_checked.UseVisualStyleBackColor = true;
            this.checkBox_target_accounts_not_checked.CheckedChanged += new System.EventHandler(this.checkBox_target_accounts_not_checked_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Filter";
            // 
            // dataGridView_target_accounts
            // 
            this.dataGridView_target_accounts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_target_accounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_target_accounts.Location = new System.Drawing.Point(6, 78);
            this.dataGridView_target_accounts.Name = "dataGridView_target_accounts";
            this.dataGridView_target_accounts.Size = new System.Drawing.Size(842, 236);
            this.dataGridView_target_accounts.TabIndex = 0;
            this.dataGridView_target_accounts.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_target_accounts_CellContentClick);
            this.dataGridView_target_accounts.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_target_accounts_CellValueChanged);
            this.dataGridView_target_accounts.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridView_target_accounts_CurrentCellDirtyStateChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btn_source_users_details);
            this.tabPage2.Controls.Add(this.btn_import_source_users);
            this.tabPage2.Controls.Add(this.btn_grab_source_users);
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Controls.Add(this.dataGridView_source_users);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(855, 349);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Source Users";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btn_source_users_details
            // 
            this.btn_source_users_details.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_source_users_details.Location = new System.Drawing.Point(568, 320);
            this.btn_source_users_details.Name = "btn_source_users_details";
            this.btn_source_users_details.Size = new System.Drawing.Size(114, 23);
            this.btn_source_users_details.TabIndex = 9;
            this.btn_source_users_details.Text = "Grab Details";
            this.btn_source_users_details.UseVisualStyleBackColor = true;
            this.btn_source_users_details.Click += new System.EventHandler(this.btn_source_users_details_Click);
            // 
            // btn_import_source_users
            // 
            this.btn_import_source_users.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_import_source_users.Location = new System.Drawing.Point(466, 320);
            this.btn_import_source_users.Name = "btn_import_source_users";
            this.btn_import_source_users.Size = new System.Drawing.Size(75, 23);
            this.btn_import_source_users.TabIndex = 8;
            this.btn_import_source_users.Text = "Import";
            this.btn_import_source_users.UseVisualStyleBackColor = true;
            this.btn_import_source_users.Click += new System.EventHandler(this.btn_import_source_users_Click);
            // 
            // btn_grab_source_users
            // 
            this.btn_grab_source_users.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_grab_source_users.Location = new System.Drawing.Point(710, 320);
            this.btn_grab_source_users.Name = "btn_grab_source_users";
            this.btn_grab_source_users.Size = new System.Drawing.Size(138, 23);
            this.btn_grab_source_users.TabIndex = 8;
            this.btn_grab_source_users.Text = "Grab Source Users";
            this.btn_grab_source_users.UseVisualStyleBackColor = true;
            this.btn_grab_source_users.Click += new System.EventHandler(this.btn_grab_source_users_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.checkBox_source_users_not_checked);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(6, 13);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(842, 41);
            this.panel2.TabIndex = 2;
            // 
            // checkBox_source_users_not_checked
            // 
            this.checkBox_source_users_not_checked.AutoSize = true;
            this.checkBox_source_users_not_checked.Location = new System.Drawing.Point(52, 5);
            this.checkBox_source_users_not_checked.Name = "checkBox_source_users_not_checked";
            this.checkBox_source_users_not_checked.Size = new System.Drawing.Size(89, 17);
            this.checkBox_source_users_not_checked.TabIndex = 1;
            this.checkBox_source_users_not_checked.Text = "Not Checked";
            this.checkBox_source_users_not_checked.UseVisualStyleBackColor = true;
            this.checkBox_source_users_not_checked.CheckedChanged += new System.EventHandler(this.checkBox_source_users_not_checked_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Filter";
            // 
            // dataGridView_source_users
            // 
            this.dataGridView_source_users.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_source_users.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_source_users.Location = new System.Drawing.Point(6, 60);
            this.dataGridView_source_users.Name = "dataGridView_source_users";
            this.dataGridView_source_users.Size = new System.Drawing.Size(842, 254);
            this.dataGridView_source_users.TabIndex = 0;
            this.dataGridView_source_users.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_source_users_CellContentClick);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.button_hashtags_grab_details);
            this.tabPage5.Controls.Add(this.tabControl4);
            this.tabPage5.Controls.Add(this.button_hashtags_grab_hashtags);
            this.tabPage5.Controls.Add(this.button_hashtags_import);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(855, 349);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Hashtags";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // button_hashtags_grab_details
            // 
            this.button_hashtags_grab_details.Location = new System.Drawing.Point(625, 320);
            this.button_hashtags_grab_details.Name = "button_hashtags_grab_details";
            this.button_hashtags_grab_details.Size = new System.Drawing.Size(123, 23);
            this.button_hashtags_grab_details.TabIndex = 4;
            this.button_hashtags_grab_details.Text = "Grab Details";
            this.button_hashtags_grab_details.UseVisualStyleBackColor = true;
            this.button_hashtags_grab_details.Click += new System.EventHandler(this.button_hashtags_grab_details_Click);
            // 
            // tabControl4
            // 
            this.tabControl4.Controls.Add(this.tabPage17);
            this.tabControl4.Controls.Add(this.tabPage18);
            this.tabControl4.Location = new System.Drawing.Point(6, 6);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(842, 308);
            this.tabControl4.TabIndex = 3;
            // 
            // tabPage17
            // 
            this.tabPage17.Controls.Add(this.dataGridView_hashtags);
            this.tabPage17.Location = new System.Drawing.Point(4, 22);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage17.Size = new System.Drawing.Size(834, 282);
            this.tabPage17.TabIndex = 0;
            this.tabPage17.Text = "Grab";
            this.tabPage17.UseVisualStyleBackColor = true;
            // 
            // dataGridView_hashtags
            // 
            this.dataGridView_hashtags.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_hashtags.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_hashtags.Location = new System.Drawing.Point(6, 6);
            this.dataGridView_hashtags.Name = "dataGridView_hashtags";
            this.dataGridView_hashtags.Size = new System.Drawing.Size(822, 270);
            this.dataGridView_hashtags.TabIndex = 0;
            // 
            // tabPage18
            // 
            this.tabPage18.Controls.Add(this.textBox_hashtags_tutorial);
            this.tabPage18.Location = new System.Drawing.Point(4, 22);
            this.tabPage18.Name = "tabPage18";
            this.tabPage18.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage18.Size = new System.Drawing.Size(834, 282);
            this.tabPage18.TabIndex = 1;
            this.tabPage18.Text = "Tutorial";
            this.tabPage18.UseVisualStyleBackColor = true;
            // 
            // textBox_hashtags_tutorial
            // 
            this.textBox_hashtags_tutorial.Location = new System.Drawing.Point(6, 6);
            this.textBox_hashtags_tutorial.Multiline = true;
            this.textBox_hashtags_tutorial.Name = "textBox_hashtags_tutorial";
            this.textBox_hashtags_tutorial.Size = new System.Drawing.Size(822, 270);
            this.textBox_hashtags_tutorial.TabIndex = 0;
            // 
            // button_hashtags_grab_hashtags
            // 
            this.button_hashtags_grab_hashtags.Location = new System.Drawing.Point(754, 320);
            this.button_hashtags_grab_hashtags.Name = "button_hashtags_grab_hashtags";
            this.button_hashtags_grab_hashtags.Size = new System.Drawing.Size(94, 23);
            this.button_hashtags_grab_hashtags.TabIndex = 2;
            this.button_hashtags_grab_hashtags.Text = "Grab Hashtags";
            this.button_hashtags_grab_hashtags.UseVisualStyleBackColor = true;
            this.button_hashtags_grab_hashtags.Click += new System.EventHandler(this.button_hashtags_grab_hashtags_Click);
            // 
            // button_hashtags_import
            // 
            this.button_hashtags_import.Location = new System.Drawing.Point(544, 320);
            this.button_hashtags_import.Name = "button_hashtags_import";
            this.button_hashtags_import.Size = new System.Drawing.Size(75, 23);
            this.button_hashtags_import.TabIndex = 1;
            this.button_hashtags_import.Text = "Import";
            this.button_hashtags_import.UseVisualStyleBackColor = true;
            this.button_hashtags_import.Click += new System.EventHandler(this.button_hashtags_import_Click);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.tabControl2);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(855, 349);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Comment";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage7);
            this.tabControl2.Controls.Add(this.tabPage8);
            this.tabControl2.Location = new System.Drawing.Point(6, 6);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(842, 337);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.checkBox_comment_account_notcheck);
            this.tabPage7.Controls.Add(this.button_commentaccount_grab_related_account);
            this.tabPage7.Controls.Add(this.button_commentaccount_import);
            this.tabPage7.Controls.Add(this.dataGridView_commentaccount);
            this.tabPage7.Controls.Add(this.label9);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(834, 311);
            this.tabPage7.TabIndex = 0;
            this.tabPage7.Text = "CommentAccount";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // checkBox_comment_account_notcheck
            // 
            this.checkBox_comment_account_notcheck.AutoSize = true;
            this.checkBox_comment_account_notcheck.Location = new System.Drawing.Point(9, 30);
            this.checkBox_comment_account_notcheck.Name = "checkBox_comment_account_notcheck";
            this.checkBox_comment_account_notcheck.Size = new System.Drawing.Size(77, 17);
            this.checkBox_comment_account_notcheck.TabIndex = 4;
            this.checkBox_comment_account_notcheck.Text = "Not Check";
            this.checkBox_comment_account_notcheck.UseVisualStyleBackColor = true;
            this.checkBox_comment_account_notcheck.CheckedChanged += new System.EventHandler(this.checkBox_comment_account_notcheck_CheckedChanged);
            // 
            // button_commentaccount_grab_related_account
            // 
            this.button_commentaccount_grab_related_account.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_commentaccount_grab_related_account.Location = new System.Drawing.Point(678, 276);
            this.button_commentaccount_grab_related_account.Name = "button_commentaccount_grab_related_account";
            this.button_commentaccount_grab_related_account.Size = new System.Drawing.Size(150, 23);
            this.button_commentaccount_grab_related_account.TabIndex = 3;
            this.button_commentaccount_grab_related_account.Text = "Grab Related Account";
            this.button_commentaccount_grab_related_account.UseVisualStyleBackColor = true;
            this.button_commentaccount_grab_related_account.Click += new System.EventHandler(this.button_commentaccount_grab_related_account_Click);
            // 
            // button_commentaccount_import
            // 
            this.button_commentaccount_import.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_commentaccount_import.Location = new System.Drawing.Point(597, 276);
            this.button_commentaccount_import.Name = "button_commentaccount_import";
            this.button_commentaccount_import.Size = new System.Drawing.Size(75, 23);
            this.button_commentaccount_import.TabIndex = 2;
            this.button_commentaccount_import.Text = "Import";
            this.button_commentaccount_import.UseVisualStyleBackColor = true;
            // 
            // dataGridView_commentaccount
            // 
            this.dataGridView_commentaccount.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_commentaccount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_commentaccount.Location = new System.Drawing.Point(9, 53);
            this.dataGridView_commentaccount.Name = "dataGridView_commentaccount";
            this.dataGridView_commentaccount.Size = new System.Drawing.Size(819, 217);
            this.dataGridView_commentaccount.TabIndex = 1;
            this.dataGridView_commentaccount.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_commentaccount_CellContentClick);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(381, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "***list account that will be scrape post to use to do comment and market @main";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.label10);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(834, 311);
            this.tabPage8.TabIndex = 1;
            this.tabPage8.Text = "Post_Comment";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(347, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "***posts are scraped from CommentAccount - do comment on these post";
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.label_copy_content_status);
            this.tabPage9.Controls.Add(this.label22);
            this.tabPage9.Controls.Add(this.button_copycontent_copy_now);
            this.tabPage9.Controls.Add(this.textBox_copycontent_niche_content);
            this.tabPage9.Controls.Add(this.label16);
            this.tabPage9.Controls.Add(this.tabControl_copycontent_type);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(855, 349);
            this.tabPage9.TabIndex = 6;
            this.tabPage9.Text = "Copy Content";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // label_copy_content_status
            // 
            this.label_copy_content_status.AutoSize = true;
            this.label_copy_content_status.ForeColor = System.Drawing.Color.ForestGreen;
            this.label_copy_content_status.Location = new System.Drawing.Point(509, 14);
            this.label_copy_content_status.Name = "label_copy_content_status";
            this.label_copy_content_status.Size = new System.Drawing.Size(42, 13);
            this.label_copy_content_status.TabIndex = 5;
            this.label_copy_content_status.Text = "nothing";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(463, 14);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(40, 13);
            this.label22.TabIndex = 4;
            this.label22.Text = "Status:";
            // 
            // button_copycontent_copy_now
            // 
            this.button_copycontent_copy_now.Location = new System.Drawing.Point(232, 9);
            this.button_copycontent_copy_now.Name = "button_copycontent_copy_now";
            this.button_copycontent_copy_now.Size = new System.Drawing.Size(75, 23);
            this.button_copycontent_copy_now.TabIndex = 3;
            this.button_copycontent_copy_now.Text = "Copy NOW";
            this.button_copycontent_copy_now.UseVisualStyleBackColor = true;
            this.button_copycontent_copy_now.Click += new System.EventHandler(this.button_copycontent_copy_now_Click);
            // 
            // textBox_copycontent_niche_content
            // 
            this.textBox_copycontent_niche_content.Location = new System.Drawing.Point(88, 11);
            this.textBox_copycontent_niche_content.Name = "textBox_copycontent_niche_content";
            this.textBox_copycontent_niche_content.Size = new System.Drawing.Size(104, 20);
            this.textBox_copycontent_niche_content.TabIndex = 2;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 14);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Niche Content";
            // 
            // tabControl_copycontent_type
            // 
            this.tabControl_copycontent_type.Controls.Add(this.tabPage13);
            this.tabControl_copycontent_type.Controls.Add(this.tabPage15);
            this.tabControl_copycontent_type.Controls.Add(this.tabPage16);
            this.tabControl_copycontent_type.Controls.Add(this.tabPage19);
            this.tabControl_copycontent_type.Controls.Add(this.tabPage14);
            this.tabControl_copycontent_type.Location = new System.Drawing.Point(6, 41);
            this.tabControl_copycontent_type.Name = "tabControl_copycontent_type";
            this.tabControl_copycontent_type.SelectedIndex = 0;
            this.tabControl_copycontent_type.Size = new System.Drawing.Size(842, 302);
            this.tabControl_copycontent_type.TabIndex = 0;
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.textBox_copycontent_copy_from_accounts_max_pages);
            this.tabPage13.Controls.Add(this.label19);
            this.tabPage13.Controls.Add(this.textBox_copycontent_copy_from_accounts_list_accounts);
            this.tabPage13.Controls.Add(this.label18);
            this.tabPage13.Location = new System.Drawing.Point(4, 22);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage13.Size = new System.Drawing.Size(834, 276);
            this.tabPage13.TabIndex = 0;
            this.tabPage13.Text = "Copy From Source Accounts";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // textBox_copycontent_copy_from_accounts_max_pages
            // 
            this.textBox_copycontent_copy_from_accounts_max_pages.Location = new System.Drawing.Point(323, 29);
            this.textBox_copycontent_copy_from_accounts_max_pages.Name = "textBox_copycontent_copy_from_accounts_max_pages";
            this.textBox_copycontent_copy_from_accounts_max_pages.Size = new System.Drawing.Size(100, 20);
            this.textBox_copycontent_copy_from_accounts_max_pages.TabIndex = 3;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(320, 13);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(103, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "Max Pages To Copy";
            // 
            // textBox_copycontent_copy_from_accounts_list_accounts
            // 
            this.textBox_copycontent_copy_from_accounts_list_accounts.Location = new System.Drawing.Point(9, 29);
            this.textBox_copycontent_copy_from_accounts_list_accounts.Multiline = true;
            this.textBox_copycontent_copy_from_accounts_list_accounts.Name = "textBox_copycontent_copy_from_accounts_list_accounts";
            this.textBox_copycontent_copy_from_accounts_list_accounts.Size = new System.Drawing.Size(288, 241);
            this.textBox_copycontent_copy_from_accounts_list_accounts.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 13);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(242, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Source Accounts (list source users url, line by line)";
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.textBox_copycontent_copy_from_posts_listposts);
            this.tabPage15.Controls.Add(this.label20);
            this.tabPage15.Location = new System.Drawing.Point(4, 22);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Size = new System.Drawing.Size(834, 276);
            this.tabPage15.TabIndex = 2;
            this.tabPage15.Text = "Copy From Posts";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // textBox_copycontent_copy_from_posts_listposts
            // 
            this.textBox_copycontent_copy_from_posts_listposts.Location = new System.Drawing.Point(6, 30);
            this.textBox_copycontent_copy_from_posts_listposts.Multiline = true;
            this.textBox_copycontent_copy_from_posts_listposts.Name = "textBox_copycontent_copy_from_posts_listposts";
            this.textBox_copycontent_copy_from_posts_listposts.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_copycontent_copy_from_posts_listposts.Size = new System.Drawing.Size(291, 243);
            this.textBox_copycontent_copy_from_posts_listposts.TabIndex = 1;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(3, 14);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(109, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "List posts (line by line)";
            // 
            // tabPage16
            // 
            this.tabPage16.Controls.Add(this.button_upload_gdrive);
            this.tabPage16.Controls.Add(this.label25);
            this.tabPage16.Controls.Add(this.button_copycontent_uploadnow);
            this.tabPage16.Controls.Add(this.label21);
            this.tabPage16.Location = new System.Drawing.Point(4, 22);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Size = new System.Drawing.Size(834, 276);
            this.tabPage16.TabIndex = 3;
            this.tabPage16.Text = "Upload Content To Own Server";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // button_upload_gdrive
            // 
            this.button_upload_gdrive.Location = new System.Drawing.Point(6, 117);
            this.button_upload_gdrive.Name = "button_upload_gdrive";
            this.button_upload_gdrive.Size = new System.Drawing.Size(118, 23);
            this.button_upload_gdrive.TabIndex = 5;
            this.button_upload_gdrive.Text = "Upload GDRIVE";
            this.button_upload_gdrive.UseVisualStyleBackColor = true;
            this.button_upload_gdrive.Click += new System.EventHandler(this.button_upload_gdrive_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(3, 91);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(127, 13);
            this.label25.TabIndex = 4;
            this.label25.Text = "*** upload to google drive";
            // 
            // button_copycontent_uploadnow
            // 
            this.button_copycontent_uploadnow.Location = new System.Drawing.Point(6, 41);
            this.button_copycontent_uploadnow.Name = "button_copycontent_uploadnow";
            this.button_copycontent_uploadnow.Size = new System.Drawing.Size(118, 23);
            this.button_copycontent_uploadnow.TabIndex = 3;
            this.button_copycontent_uploadnow.Text = "Upload NOW";
            this.button_copycontent_uploadnow.UseVisualStyleBackColor = true;
            this.button_copycontent_uploadnow.Click += new System.EventHandler(this.button_copycontent_uploadnow_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 12);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(241, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "*** upload images to own server (imodstyle server)";
            // 
            // tabPage19
            // 
            this.tabPage19.Controls.Add(this.label26);
            this.tabPage19.Controls.Add(this.label24);
            this.tabPage19.Controls.Add(this.button__copycontent_updatepost_updatenow);
            this.tabPage19.Controls.Add(this.label23);
            this.tabPage19.Controls.Add(this.textBox_copycontent_updatepost_list_post_ids);
            this.tabPage19.Location = new System.Drawing.Point(4, 22);
            this.tabPage19.Name = "tabPage19";
            this.tabPage19.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage19.Size = new System.Drawing.Size(834, 276);
            this.tabPage19.TabIndex = 4;
            this.tabPage19.Text = "UpdatePost";
            this.tabPage19.UseVisualStyleBackColor = true;
            // 
            // label26
            // 
            this.label26.Location = new System.Drawing.Point(330, 119);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(712, 13);
            this.label26.TabIndex = 4;
            this.label26.Text = "dùng tính năng này để update post theo post id";
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(322, 104);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(712, 13);
            this.label24.TabIndex = 3;
            this.label24.Text = "**Sử dụng trong trường hợp copy content nhưng chưa upload image to own server dẫn" +
    " tới url expired, ";
            // 
            // button__copycontent_updatepost_updatenow
            // 
            this.button__copycontent_updatepost_updatenow.Location = new System.Drawing.Point(325, 37);
            this.button__copycontent_updatepost_updatenow.Name = "button__copycontent_updatepost_updatenow";
            this.button__copycontent_updatepost_updatenow.Size = new System.Drawing.Size(106, 23);
            this.button__copycontent_updatepost_updatenow.TabIndex = 2;
            this.button__copycontent_updatepost_updatenow.Text = "Update NOW";
            this.button__copycontent_updatepost_updatenow.UseVisualStyleBackColor = true;
            this.button__copycontent_updatepost_updatenow.Click += new System.EventHandler(this.button__copycontent_updatepost_updatenow_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 12);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(120, 13);
            this.label23.TabIndex = 1;
            this.label23.Text = "List post ids (line by line)";
            // 
            // textBox_copycontent_updatepost_list_post_ids
            // 
            this.textBox_copycontent_updatepost_list_post_ids.Location = new System.Drawing.Point(6, 37);
            this.textBox_copycontent_updatepost_list_post_ids.Multiline = true;
            this.textBox_copycontent_updatepost_list_post_ids.Name = "textBox_copycontent_updatepost_list_post_ids";
            this.textBox_copycontent_updatepost_list_post_ids.Size = new System.Drawing.Size(291, 233);
            this.textBox_copycontent_updatepost_list_post_ids.TabIndex = 0;
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.textBox_copycontent_tips);
            this.tabPage14.Location = new System.Drawing.Point(4, 22);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage14.Size = new System.Drawing.Size(834, 276);
            this.tabPage14.TabIndex = 1;
            this.tabPage14.Text = "Tips";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // textBox_copycontent_tips
            // 
            this.textBox_copycontent_tips.Location = new System.Drawing.Point(6, 6);
            this.textBox_copycontent_tips.Multiline = true;
            this.textBox_copycontent_tips.Name = "textBox_copycontent_tips";
            this.textBox_copycontent_tips.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_copycontent_tips.Size = new System.Drawing.Size(822, 299);
            this.textBox_copycontent_tips.TabIndex = 0;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.tabControl3);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(855, 349);
            this.tabPage10.TabIndex = 7;
            this.tabPage10.Text = "Leech Resources";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage11);
            this.tabControl3.Controls.Add(this.tabPage12);
            this.tabControl3.Location = new System.Drawing.Point(6, 6);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(842, 337);
            this.tabControl3.TabIndex = 0;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.label17);
            this.tabPage11.Controls.Add(this.button_leech_resources_images_download);
            this.tabPage11.Controls.Add(this.label_leech_resources_images_status);
            this.tabPage11.Controls.Add(this.label15);
            this.tabPage11.Controls.Add(this.button_leech_resources_images_browse_des);
            this.tabPage11.Controls.Add(this.textBox_leech_resources_images_des_folder);
            this.tabPage11.Controls.Add(this.label14);
            this.tabPage11.Controls.Add(this.label13);
            this.tabPage11.Controls.Add(this.textBox_leech_resources_images_listposts);
            this.tabPage11.Controls.Add(this.label12);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(834, 311);
            this.tabPage11.TabIndex = 0;
            this.tabPage11.Text = "Images";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(560, 181);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(121, 13);
            this.label17.TabIndex = 9;
            this.label17.Text = "(overwrite if same name)";
            // 
            // button_leech_resources_images_download
            // 
            this.button_leech_resources_images_download.Location = new System.Drawing.Point(411, 176);
            this.button_leech_resources_images_download.Name = "button_leech_resources_images_download";
            this.button_leech_resources_images_download.Size = new System.Drawing.Size(115, 23);
            this.button_leech_resources_images_download.TabIndex = 8;
            this.button_leech_resources_images_download.Text = "Download NOW";
            this.button_leech_resources_images_download.UseVisualStyleBackColor = true;
            this.button_leech_resources_images_download.Click += new System.EventHandler(this.button_leech_resources_images_download_Click);
            // 
            // label_leech_resources_images_status
            // 
            this.label_leech_resources_images_status.AutoSize = true;
            this.label_leech_resources_images_status.Location = new System.Drawing.Point(459, 130);
            this.label_leech_resources_images_status.Name = "label_leech_resources_images_status";
            this.label_leech_resources_images_status.Size = new System.Drawing.Size(50, 13);
            this.label_leech_resources_images_status.TabIndex = 7;
            this.label_leech_resources_images_status.Text = "no status";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(408, 130);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "Status:";
            // 
            // button_leech_resources_images_browse_des
            // 
            this.button_leech_resources_images_browse_des.Location = new System.Drawing.Point(734, 54);
            this.button_leech_resources_images_browse_des.Name = "button_leech_resources_images_browse_des";
            this.button_leech_resources_images_browse_des.Size = new System.Drawing.Size(94, 23);
            this.button_leech_resources_images_browse_des.TabIndex = 5;
            this.button_leech_resources_images_browse_des.Text = "Browse";
            this.button_leech_resources_images_browse_des.UseVisualStyleBackColor = true;
            this.button_leech_resources_images_browse_des.Click += new System.EventHandler(this.button_leech_resources_images_browse_des_Click);
            // 
            // textBox_leech_resources_images_des_folder
            // 
            this.textBox_leech_resources_images_des_folder.Location = new System.Drawing.Point(411, 56);
            this.textBox_leech_resources_images_des_folder.Name = "textBox_leech_resources_images_des_folder";
            this.textBox_leech_resources_images_des_folder.Size = new System.Drawing.Size(307, 20);
            this.textBox_leech_resources_images_des_folder.TabIndex = 4;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(408, 40);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Destination (folder)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 40);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(202, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "List posts (instagram post link, line by line)";
            // 
            // textBox_leech_resources_images_listposts
            // 
            this.textBox_leech_resources_images_listposts.Location = new System.Drawing.Point(9, 56);
            this.textBox_leech_resources_images_listposts.Multiline = true;
            this.textBox_leech_resources_images_listposts.Name = "textBox_leech_resources_images_listposts";
            this.textBox_leech_resources_images_listposts.Size = new System.Drawing.Size(375, 249);
            this.textBox_leech_resources_images_listposts.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 13);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(393, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "** leech image from post (just first image) (normally use for profile image in ac" +
    "count)";
            // 
            // tabPage12
            // 
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(834, 311);
            this.tabPage12.TabIndex = 1;
            this.tabPage12.Text = "tabPage12";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // tabPage20
            // 
            this.tabPage20.Controls.Add(this.tabControl5);
            this.tabPage20.Location = new System.Drawing.Point(4, 22);
            this.tabPage20.Name = "tabPage20";
            this.tabPage20.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage20.Size = new System.Drawing.Size(855, 349);
            this.tabPage20.TabIndex = 8;
            this.tabPage20.Text = "Addon";
            this.tabPage20.UseVisualStyleBackColor = true;
            // 
            // tabControl5
            // 
            this.tabControl5.Controls.Add(this.tabPage21);
            this.tabControl5.Controls.Add(this.tabPage22);
            this.tabControl5.Location = new System.Drawing.Point(6, 6);
            this.tabControl5.Name = "tabControl5";
            this.tabControl5.SelectedIndex = 0;
            this.tabControl5.Size = new System.Drawing.Size(842, 337);
            this.tabControl5.TabIndex = 0;
            // 
            // tabPage21
            // 
            this.tabPage21.Controls.Add(this.btn_addon_account_details_grab);
            this.tabPage21.Controls.Add(this.label28);
            this.tabPage21.Controls.Add(this.label27);
            this.tabPage21.Controls.Add(this.textBox_addon_account_details_list);
            this.tabPage21.Location = new System.Drawing.Point(4, 22);
            this.tabPage21.Name = "tabPage21";
            this.tabPage21.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage21.Size = new System.Drawing.Size(834, 311);
            this.tabPage21.TabIndex = 0;
            this.tabPage21.Text = "Account Details";
            this.tabPage21.UseVisualStyleBackColor = true;
            // 
            // btn_addon_account_details_grab
            // 
            this.btn_addon_account_details_grab.Location = new System.Drawing.Point(362, 282);
            this.btn_addon_account_details_grab.Name = "btn_addon_account_details_grab";
            this.btn_addon_account_details_grab.Size = new System.Drawing.Size(75, 23);
            this.btn_addon_account_details_grab.TabIndex = 3;
            this.btn_addon_account_details_grab.Text = "Grab NOW";
            this.btn_addon_account_details_grab.UseVisualStyleBackColor = true;
            this.btn_addon_account_details_grab.Click += new System.EventHandler(this.btn_addon_account_details_grab_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(359, 15);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(267, 13);
            this.label28.TabIndex = 2;
            this.label28.Text = "**grab account details and save to actionlog of igtoolkit";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 15);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(71, 13);
            this.label27.TabIndex = 1;
            this.label27.Text = "List Accounts";
            // 
            // textBox_addon_account_details_list
            // 
            this.textBox_addon_account_details_list.Location = new System.Drawing.Point(6, 31);
            this.textBox_addon_account_details_list.Multiline = true;
            this.textBox_addon_account_details_list.Name = "textBox_addon_account_details_list";
            this.textBox_addon_account_details_list.Size = new System.Drawing.Size(315, 274);
            this.textBox_addon_account_details_list.TabIndex = 0;
            // 
            // tabPage22
            // 
            this.tabPage22.Location = new System.Drawing.Point(4, 22);
            this.tabPage22.Name = "tabPage22";
            this.tabPage22.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage22.Size = new System.Drawing.Size(834, 311);
            this.tabPage22.TabIndex = 1;
            this.tabPage22.Text = "tabPage22";
            this.tabPage22.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel3.Controls.Add(this.btn_grabposts);
            this.panel3.Controls.Add(this.btn_scrapeuser_info);
            this.panel3.Controls.Add(this.btn_stop);
            this.panel3.Controls.Add(this.lb_status);
            this.panel3.Location = new System.Drawing.Point(13, 472);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(854, 61);
            this.panel3.TabIndex = 19;
            // 
            // btn_launch_firefox
            // 
            this.btn_launch_firefox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_launch_firefox.Location = new System.Drawing.Point(175, 420);
            this.btn_launch_firefox.Name = "btn_launch_firefox";
            this.btn_launch_firefox.Size = new System.Drawing.Size(158, 46);
            this.btn_launch_firefox.TabIndex = 20;
            this.btn_launch_firefox.Text = "Launch Firefox";
            this.btn_launch_firefox.UseVisualStyleBackColor = true;
            this.btn_launch_firefox.Click += new System.EventHandler(this.btn_launch_firefox_Click);
            // 
            // button_tool_settings
            // 
            this.button_tool_settings.Location = new System.Drawing.Point(11, 12);
            this.button_tool_settings.Name = "button_tool_settings";
            this.button_tool_settings.Size = new System.Drawing.Size(105, 23);
            this.button_tool_settings.TabIndex = 36;
            this.button_tool_settings.Text = "Tool Settings";
            this.button_tool_settings.UseVisualStyleBackColor = true;
            this.button_tool_settings.Click += new System.EventHandler(this.button_tool_settings_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(345, 440);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 38;
            this.label2.Text = "Auto run within ";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(478, 437);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 13);
            this.label11.TabIndex = 39;
            this.label11.Text = "seconds";
            // 
            // label_autorun_counter
            // 
            this.label_autorun_counter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_autorun_counter.AutoSize = true;
            this.label_autorun_counter.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_autorun_counter.ForeColor = System.Drawing.Color.Red;
            this.label_autorun_counter.Location = new System.Drawing.Point(431, 432);
            this.label_autorun_counter.Name = "label_autorun_counter";
            this.label_autorun_counter.Size = new System.Drawing.Size(34, 25);
            this.label_autorun_counter.TabIndex = 40;
            this.label_autorun_counter.Text = "00";
            // 
            // button_stop_autorun
            // 
            this.button_stop_autorun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_stop_autorun.Location = new System.Drawing.Point(542, 432);
            this.button_stop_autorun.Name = "button_stop_autorun";
            this.button_stop_autorun.Size = new System.Drawing.Size(109, 23);
            this.button_stop_autorun.TabIndex = 41;
            this.button_stop_autorun.Text = "Stop AutoRun";
            this.button_stop_autorun.UseVisualStyleBackColor = true;
            this.button_stop_autorun.Click += new System.EventHandler(this.button_stop_autorun_Click);
            // 
            // button_login_to_newaccount
            // 
            this.button_login_to_newaccount.Location = new System.Drawing.Point(737, 432);
            this.button_login_to_newaccount.Name = "button_login_to_newaccount";
            this.button_login_to_newaccount.Size = new System.Drawing.Size(133, 23);
            this.button_login_to_newaccount.TabIndex = 42;
            this.button_login_to_newaccount.Text = "Login To New Account";
            this.button_login_to_newaccount.UseVisualStyleBackColor = true;
            this.button_login_to_newaccount.Click += new System.EventHandler(this.button_login_to_newaccount_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.Color.Red;
            this.label29.Location = new System.Drawing.Point(49, 48);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(686, 13);
            this.label29.TabIndex = 7;
            this.label29.Text = "Warning: if you want to update through this form, please uncomment line 581 (form" +
    "1.cs), func dataGridView_target_accounts_CellValueChanged";
            // 
            // form_igtoolkit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(875, 540);
            this.Controls.Add(this.button_login_to_newaccount);
            this.Controls.Add(this.button_stop_autorun);
            this.Controls.Add(this.label_autorun_counter);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button_tool_settings);
            this.Controls.Add(this.btn_launch_firefox);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btn_update);
            this.Name = "form_igtoolkit";
            this.Text = "IG Toolkit";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.form_igtoolkit_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.form_igtoolkit_Shown);
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_target_accounts)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_source_users)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabControl4.ResumeLayout(false);
            this.tabPage17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_hashtags)).EndInit();
            this.tabPage18.ResumeLayout(false);
            this.tabPage18.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_commentaccount)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            this.tabControl_copycontent_type.ResumeLayout(false);
            this.tabPage13.ResumeLayout(false);
            this.tabPage13.PerformLayout();
            this.tabPage15.ResumeLayout(false);
            this.tabPage15.PerformLayout();
            this.tabPage16.ResumeLayout(false);
            this.tabPage16.PerformLayout();
            this.tabPage19.ResumeLayout(false);
            this.tabPage19.PerformLayout();
            this.tabPage14.ResumeLayout(false);
            this.tabPage14.PerformLayout();
            this.tabPage10.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.tabPage11.ResumeLayout(false);
            this.tabPage11.PerformLayout();
            this.tabPage20.ResumeLayout(false);
            this.tabControl5.ResumeLayout(false);
            this.tabPage21.ResumeLayout(false);
            this.tabPage21.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridViewTextBoxColumn usedDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn igurlDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ok;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn ig_url;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.Button btn_grabaccounts;
        private System.Windows.Forms.Button btn_grabposts;
        private System.Windows.Forms.Button btn_stop;
        private System.Windows.Forms.Label lb_status;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn posturlDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn creationdateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cb_posts_isOkay;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn posturl;
        private System.Windows.Forms.DataGridViewTextBoxColumn creation_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn used;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.Button btn_update;
        private System.Windows.Forms.Button btn_scrapeuser_info;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dataGridView_target_accounts;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridView_source_users;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBox_target_accounts_not_checked;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox checkBox_source_users_not_checked;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_grab_source_users;
        private System.Windows.Forms.Button btn_import_source_users;
        private System.Windows.Forms.Button btn_import_target_accounts;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btn_launch_firefox;
        private System.Windows.Forms.Button btn_source_users_details;
        private System.Windows.Forms.Button btn_target_accounts_details;
        private System.Windows.Forms.CheckBox checkBox_target_accounts_not_set_scrapetype;
        private System.Windows.Forms.Button btn_grab_aio;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox tb_aio_log;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox tb_detect_personality_log;
        private System.Windows.Forms.Button btn_detect_aio;
        private System.Windows.Forms.Button btn_reg_namsor;
        private System.Windows.Forms.TextBox tb_namsor_quantity;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox_target_accounts_checked_OK;
        private System.Windows.Forms.ComboBox combobox_target_accounts_sort;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBox_aio_grab_APITask_GrabCommenters;
        private System.Windows.Forms.CheckBox checkBox_aio_grab_APITask_GrabPost;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox checkBox_aio_grab_APITask_GrabFollower;
        private System.Windows.Forms.CheckBox checkBox_aio_grab_APITask_GrabLikers;
        private System.Windows.Forms.CheckBox checkBox_aio_grab_APITask_GrabUserFullInfo;
        private System.Windows.Forms.CheckBox checkBox_aio_detect_DetectLanguage;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkBox_aio_detect_DetectCountry;
        private System.Windows.Forms.CheckBox checkBox_aio_detect_DetectGender;
        private System.Windows.Forms.TextBox tb_aio_detect_proxy_apikey;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkBox_aio_detect_DetectNameImage;
        private System.Windows.Forms.CheckBox checkBox_aio_detect_proxy_server;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView dataGridView_hashtags;
        private System.Windows.Forms.CheckBox checkBox_aio_grab_APITask_GrabUserFullInfo_Rescrape;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Button button_commentaccount_grab_related_account;
        private System.Windows.Forms.Button button_commentaccount_import;
        private System.Windows.Forms.DataGridView dataGridView_commentaccount;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox checkBox_comment_account_notcheck;
        private System.Windows.Forms.CheckBox checkBox_aio_grab_APITask_GrabPost_ForComment;
        private System.Windows.Forms.CheckBox checkBox_aio_grab_APITask_GrabFollowerBack;
        private System.Windows.Forms.Button button_tool_settings;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.CheckBox checkBox_aio_grab_APITask_GrabFollower_ForMother;
        private System.Windows.Forms.CheckBox checkBox_aio_grab_APITask_GrabPost_ForComment_ScrapeUsers;
        private System.Windows.Forms.CheckBox checkBox_aio_grab_APITask_GrabComment_ForSlave;
        private System.Windows.Forms.CheckBox checkBox_target_accounts_checked_not_OK;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label_autorun_counter;
        private System.Windows.Forms.Button button_stop_autorun;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button_leech_resources_images_download;
        private System.Windows.Forms.Label label_leech_resources_images_status;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button button_leech_resources_images_browse_des;
        private System.Windows.Forms.TextBox textBox_leech_resources_images_des_folder;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox_leech_resources_images_listposts;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.TabControl tabControl_copycontent_type;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.TextBox textBox_copycontent_tips;
        private System.Windows.Forms.Button button_copycontent_copy_now;
        private System.Windows.Forms.TextBox textBox_copycontent_niche_content;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox_copycontent_copy_from_accounts_max_pages;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox_copycontent_copy_from_accounts_list_accounts;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.TextBox textBox_copycontent_copy_from_posts_listposts;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TabPage tabPage16;
        private System.Windows.Forms.Button button_copycontent_uploadnow;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label_copy_content_status;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button button_hashtags_grab_hashtags;
        private System.Windows.Forms.Button button_hashtags_import;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPage17;
        private System.Windows.Forms.TabPage tabPage18;
        private System.Windows.Forms.TextBox textBox_hashtags_tutorial;
        private System.Windows.Forms.Button button_hashtags_grab_details;
        private System.Windows.Forms.TabPage tabPage19;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button button__copycontent_updatepost_updatenow;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox_copycontent_updatepost_list_post_ids;
        private System.Windows.Forms.Button button_upload_gdrive;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TabPage tabPage20;
        private System.Windows.Forms.TabControl tabControl5;
        private System.Windows.Forms.TabPage tabPage21;
        private System.Windows.Forms.Button btn_addon_account_details_grab;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBox_addon_account_details_list;
        private System.Windows.Forms.TabPage tabPage22;
        private System.Windows.Forms.Button button_login_to_newaccount;
        private System.Windows.Forms.Label label29;
    }
}

