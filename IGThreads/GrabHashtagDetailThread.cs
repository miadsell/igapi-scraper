﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.IGThreads
{
    class GrabHashtagDetailThread:IGThread
    {
        
        //JUST WORK FOR 1 THREAD --> IF MULTI THREAD, PLEASE CHANGE WAY TO TAKE AND CLAIM DATA

        public override void DoThread()
        {

            threads.Clear();

            for (int t = 0; t < 1; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => GrabHashtagDetail());
                thread.Name = "scrape" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }
            //Load chrome to tab

            //IGHelpers.Helpers.LaunchDriver();
            PLAutoHelper.ProxyHelper.XProxyServer_Ticker(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), Settings.proxy_server_ips);

            foreach (var t in threads)
            {
                t.Start();
            }


            InitTimer_ManageThread();
        }

        private void GrabHashtagDetail()
        {
            while (true)
            {

                var continous_error = 0;

                if (Program.form.isstop)
                    break;

                GetAPIOBJ:

                string proxy = "";


                while (proxy == "" || proxy == null)
                {
                    //proxy = PLAutoHelper.ProxyHelper.TMProxy_DB_GetProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                    proxy = PLAutoHelper.ProxyHelper.XProxy_DB_GetProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"));
                    Thread.Sleep(5000);
                }

                IGAPIHelper.Scraper.APIObj dyn = IGAPIHelper.Scraper.GetAPIObj(proxy, API.DBHelpers.db_connection_igtoolkit);

                if (dyn == null)
                {
                    //PLAutoHelper.ProxyHelper.TMProxy_DB_ReleaseProxy(proxy, new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                    PLAutoHelper.ProxyHelper.XProxy_DB_ReleaseProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), proxy);
                    goto GetAPIOBJ;
                }

                InstagramApiSharp.API.IInstaApi _instaApi = dyn.instaApi;

                Stopwatch watch_scrape = new Stopwatch();
                watch_scrape.Start();

                try
                {
                    var time_session = int.Parse(API.DBHelpers.GetValueFromAccountTb(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(dyn.account, "username"), "time_per_session"));

                    API.DBHelpers.AddAPICallLog(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(dyn.account, "username"), "new_session");

                    while (true)
                    {
                        if (watch_scrape.Elapsed.TotalMinutes > time_session)
                            break;

                        if (continous_error > 5)
                        {
                            IGThreads.ThreadHelper.HandleException("Error Over 5 Times", "");
                            break;
                        }


                        //Get hashtag that was checked
                        string hashtag = null;

                        hashtag = AIOHelper.DBLogic.TakeDATAToGrab.DATA_HASHTAG_GrabMedia_CountOfHashtag();

                        if (hashtag == null)
                            return;

                        //Scrape related hashtags

                        var task_media_count = Task.Run(() =>
                        {
                            return _instaApi.HashtagProcessor.GetHashtagInfoAsync(hashtag);
                        });

                        var media_count = task_media_count.GetAwaiter().GetResult().Value.MediaCount;

                        //update media_count

                        using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                        {
                            conn.Open();

                            string command_query =
                                @"update hashtags set [media_count]=@media_count, [status]='' where [hashtag]=@hashtag";


                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@media_count", System.Data.SqlDbType.Int).Value = media_count;
                                command.Parameters.Add("@hashtag", System.Data.SqlDbType.NVarChar).Value = hashtag;
                                command.ExecuteNonQuery();
                            }
                        }

                        Thread.Sleep((new Random()).Next(2500, 5000));
                    }
                }
                finally
                {
                    IGAPIHelper.Scraper.ReleaseScrapeAccount(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(dyn.account, "username"), API.DBHelpers.db_connection_igtoolkit);
                    Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish APITask - Release Scrape Account");

                    //PLAutoHelper.ProxyHelper.TMProxy_DB_ReleaseProxy(proxy, new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                    PLAutoHelper.ProxyHelper.XProxy_DB_ReleaseProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), proxy);
                    Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish APITask - Release Proxy");

                    //Write session time
                    Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Finished round in " + watch_scrape.Elapsed.TotalMinutes.ToString());
                }
            }
        }
    }
}
