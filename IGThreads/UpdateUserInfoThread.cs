﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static IGAPIHelper.Scraper;

namespace IGAPI___Ramtinak.IGThreads
{
    class UpdateUserInfoThread : IGThread
    {
        public override void DoThread()
        {
            threads.Clear();

            for (int t = 0; t < API.DBHelpers.threads; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => ScrapeInfo());
                thread.Name = "scrape" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }


            //API.ProxyHelpers.ProxyServerOthers(true);

            API.TinSoftHelpers.TinSoftServer(true);

            foreach (var t in threads)
            {
                t.Start();
            }


            InitTimer_ManageThread();
            //InitTimer_UpdateProxyStatus();
            InitTimer_UpdateProxyTinSoft();
        }

        private void ScrapeInfo()
        {
            while (true)
            {
                if (Program.form.isstop)
                    break;

                //get account

                GetAPIOBJ:

                string proxy = "";

                lock ("TinSoft_DB_GetProxy")
                {
                    while (proxy == "" || proxy == null)
                    {
                        proxy = PLAutoHelper.ProxyHelper.TinSoft_DB_GetProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "tinsoft_tb"));
                        Thread.Sleep(5000);
                    }
                }



                APIObj dyn = IGAPIHelper.Scraper.GetAPIObj(proxy, API.DBHelpers.db_connection_igtoolkit);

                if (dyn == null)
                    goto GetAPIOBJ;

                InstagramApiSharp.API.IInstaApi _instaApi = dyn.instaApi;


                if (_instaApi == null)
                    return;

                try
                {

                    var time_session = int.Parse(API.DBHelpers.GetValueFromAccountTb(GetObjectValue(dyn.account, "username"), "time_per_session"));

                    if (_instaApi == null)
                        return;

                    API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "new_session");

                    Stopwatch watch_scrape = new Stopwatch();
                    watch_scrape.Start();

                    int continue_error = 0; //so lan bi loi lien tuc

                    while (true)
                    {
                        if (continue_error > 3)
                            throw new System.ArgumentException("Maybe Account Error");

                        if (watch_scrape.Elapsed.TotalMinutes > time_session)
                            break;

                        //Get user to scrape
                        try
                        {
                            var account_pk = API.DBHelpers.GetUserToScrapeInfo();

                            if (account_pk == null)
                                return;

                            string status = null;

                            var user_info = IGAPIHelper.Scraper.ScrapeFullUserInfo(account_pk, _instaApi);

                            if (GetObjectValue(user_info, "status") == "Success")
                                status = "";
                            else
                            {
                                status = "Error";
                                goto UpdateStatus;
                            }

                            var user_details = GetObjectValue(user_info, "user");

                            API.DBHelpers.UpdateUserDetails_ByPK(account_pk, user_details);

                        //Update update_at

                        UpdateStatus:
                            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                            {
                                conn.Open();

                                using (var command = new SqlCommand("update scrapeusers set [update_at]=getdate(), [status]='" + status + "' where [pk]=" + account_pk, conn))
                                {
                                    command.ExecuteNonQuery();
                                }
                            }

                            API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "scrape_full_info");

                            continue_error = 0;
                        }
                        catch
                        {
                            continue_error++;

                            API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "scrape_error_full_info");
                        }

                        IGAPIHelper.Scraper.Delay(1000, 3000);
                    }

                    //Release account and proxy

                    IGAPIHelper.Scraper.ReleaseScrapeAccount(GetObjectValue(dyn.account, "username"), API.DBHelpers.db_connection_igtoolkit);
                }
                catch (Exception ex)
                {
                    if (ex.Message == "Maybe Account Error")
                    {
                        IGAPIHelper.Scraper.ReleaseScrapeAccount(GetObjectValue(dyn.account, "username"), API.DBHelpers.db_connection_igtoolkit, true);

                        API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "account_maybe_error");
                    }
                }

                PLAutoHelper.ProxyHelper.TinSoft_DB_ReleaseProxy(proxy, new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "tinsoft_tb"));
            }
        }
    }
}
