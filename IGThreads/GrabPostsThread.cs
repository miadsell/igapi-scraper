﻿using InstagramApiSharp.API;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static IGAPIHelper.Scraper;

namespace IGAPI___Ramtinak.IGThreads
{
    class GrabPostsThread : IGThread
    {


        public override void DoThread()
        {

            threads.Clear();

            for (int t = 0; t < API.DBHelpers.threads; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => GrabPosts());
                thread.Name = "scrape" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }
            //Load chrome to tab

            //IGHelpers.Helpers.LaunchDriver();

            //API.ProxyHelpers.ProxyServerOthers(true);

            //API.TinSoftHelpers.TinSoftServer(true);
            PLAutoHelper.ProxyHelper.TinSoftServer(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "tinsoft_tb"), true);

            foreach (var t in threads)
            {
                t.Start();
            }


            InitTimer_ManageThread();
            //InitTimer_UpdateProxyStatus();
            //InitTimer_UpdateProxyTinSoft();
            PLAutoHelper.ProxyHelper.TinsoftServer_Ticker(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "tinsoft_tb"));
        }


        private void GrabPosts()
        {
            

            while (true)
            {
                if (Program.form.isstop)
                    break;

                //get account
                //get account
                //var dyn = IGAPIHelper.Scraper.GetAccountToScrape(API.DBHelpers.db_connection);

                string proxy = "";

                lock ("TinSoft_DB_GetProxy")
                {
                    while (proxy == "" || proxy == null)
                    {
                        proxy = PLAutoHelper.ProxyHelper.TinSoft_DB_GetProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "tinsoft_tb"));
                        Thread.Sleep(5000);
                    }
                }

                APIObj dyn = IGAPIHelper.Scraper.GetAPIObj(proxy, API.DBHelpers.db_connection_igtoolkit);

                InstagramApiSharp.API.IInstaApi _instaApi = dyn.instaApi;

                if (_instaApi == null)
                    throw new Exception();

                var time_session = int.Parse(API.DBHelpers.GetValueFromAccountTb(GetObjectValue(dyn.account, "username"), "time_per_session"));

                if (_instaApi == null)
                    return;

                API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "new_session");

                Stopwatch watch_scrape = new Stopwatch();
                watch_scrape.Start();


                while (true)
                {
                    //Get target account
                    var account = API.DBHelpers.GetTargetAccount();

                    if (watch_scrape.Elapsed.TotalMinutes > time_session)
                        break;

                    if (account != null)
                    {
                        var dyn_medias = GrabFromAccount(_instaApi, GetObjectValue(account, "username"));

                        //Add to db without manual checking
                        API.DBHelpers.AddPostToDB(dyn_medias, GetObjectValue(account, "scrape_type"), GetObjectValue(account, "username"), true);
                    }
                    else
                    {
                        //If target account is null, get hashtag

                        var hashtag = API.DBHelpers.GetHashtags();

                        if (hashtag == null)
                            return;

                        var dyn_medias = GrabFromHashtag(_instaApi, hashtag);

                        //Add to db with manual checking required
                        API.DBHelpers.AddPostToDB(dyn_medias, null, null, false, true);
                    }

                    API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "grab_posts");

                    Thread.Sleep((new Random()).Next(2000, 5000));
                }

                PLAutoHelper.ProxyHelper.TinSoft_DB_ReleaseProxy(proxy, new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "tinsoft_tb"));

                IGAPIHelper.Scraper.ReleaseScrapeAccount(GetObjectValue(dyn.account, "username"), API.DBHelpers.db_connection_igtoolkit);
            }
        }


        private List<dynamic> GrabFromAccount(IInstaApi _instaApi, string account_user)
        {
            var medias_result = IGAPIHelper.Scraper.ScrapeMediasFromUser(_instaApi, account_user, 1);

            List<dynamic> dyn_medias = new List<dynamic>();

            if (medias_result.Succeeded == false)
                return dyn_medias;

            var medias = medias_result.Value;

            foreach (var item in medias)
            {
                dyn_medias.Add(new
                {
                    url = "https://www.instagram.com/p/" + item.Code + "/",
                    date = item.TakenAt.ToString()
                });
            }

            return dyn_medias;
        }


        private List<dynamic> GrabFromHashtag(IInstaApi _instaApi, string hashtag)
        {

            List<dynamic> dyn_medias = new List<dynamic>();

            try
            {
                var medias = IGAPIHelper.Scraper.ScrapeTopMediaHashTags(_instaApi, hashtag).Medias;


                foreach (var item in medias)
                {
                    dyn_medias.Add(new
                    {
                        url = "https://www.instagram.com/p/" + item.Code + "/",
                        date = item.TakenAt.ToString()
                    });
                }
            }
            catch { }

            return dyn_medias;
        }
    }
}
