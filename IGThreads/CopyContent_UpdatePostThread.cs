﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.IGThreads
{
    class CopyContent_UpdatePostThread : IGThread
    {

        List<string> postIds;

        public CopyContent_UpdatePostThread(List<string> postIds)
        {
            this.postIds = postIds;
        }

        public override void DoThread()
        {
            threads.Clear();

            for (int t = 0; t < 1; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => CopyContent_UpdatePost());
                thread.Name = "scrape" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }

            //API.ProxyHelpers.ProxyServerOthers(true);

            PLAutoHelper.ProxyHelper.XProxyServer_Ticker(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), Settings.proxy_server_ips);

            foreach (var t in threads)
            {
                t.Start();
            }


            InitTimer_ManageThread();
        }

        private void CopyContent_UpdatePost()
        {
            int totalposts = postIds.Count;
            int finished = 0;

            while (postIds.Count > 0)
            {

                var continous_error = 0;

            GetAPIOBJ:

                string proxy = "";


                while (proxy == "" || proxy == null)
                {
                    //proxy = PLAutoHelper.ProxyHelper.TMProxy_DB_GetProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                    proxy = PLAutoHelper.ProxyHelper.XProxy_DB_GetProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"));
                    Thread.Sleep(5000);
                }

                IGAPIHelper.Scraper.APIObj dyn = IGAPIHelper.Scraper.GetAPIObj(proxy, API.DBHelpers.db_connection_igtoolkit);

                if (dyn == null)
                {
                    //PLAutoHelper.ProxyHelper.TMProxy_DB_ReleaseProxy(proxy, new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                    PLAutoHelper.ProxyHelper.XProxy_DB_ReleaseProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), proxy);
                    goto GetAPIOBJ;
                }

                InstagramApiSharp.API.IInstaApi _instaApi = dyn.instaApi;

                Stopwatch watch_scrape = new Stopwatch();
                watch_scrape.Start();


                try
                {

                    var time_session = int.Parse(API.DBHelpers.GetValueFromAccountTb(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(dyn.account, "username"), "time_per_session"));

                    API.DBHelpers.AddAPICallLog(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(dyn.account, "username"), "new_session");


                    while (true)
                    {

                        if (watch_scrape.Elapsed.TotalMinutes > time_session)
                            break;

                        if (continous_error > 5)
                        {
                            IGThreads.ThreadHelper.HandleException("Error Over 5 Times", "");
                            break;
                        }

                        if (postIds.Count == 0)
                            break;

                        string target_postId = postIds[0];

                        string target_link = null;

                        //check if niche_content with this link is available, if yes skip it

                        string command_query =
                            @"select *
                            from copy_posts
                            where
	                            [id]=@postId";

                        using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                        {
                            conn.Open();

                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@postId", System.Data.SqlDbType.VarChar).Value = target_postId;

                                using (var dataReader = command.ExecuteReader())
                                {
                                    dataReader.Read();
                                    target_link = "https://www.instagram.com/p/" + dataReader["ig_code"].ToString() + "/";
                                }

                            }
                        }

                        var media = IGAPIHelper.Scraper.GetMediaFromUrlAsync(target_link, _instaApi);

                        bool hasaudio = media.HasAudio;

                        string producttype = media.ProductType;

                        string post_type = "image";

                        if (hasaudio || producttype == "clips")
                        {
                            post_type = "video";//SKIP POST_TYPE VIDEO
                            continue;
                        };

                        //--
                        var ig_code = media.Code;

                        //---

                        var taken_at = media.TakenAt.ToString();

                        //--

                        List<string> images = new List<string>();

                        if (media.Carousel != null)
                        {
                            foreach (var c in media.Carousel)
                            {
                                if (c.MediaType.ToString() == "Image")
                                {
                                    images.Add(c.Images[0].Uri);
                                }
                            }

                            //foreach (var img in media.Carousel[0].Images)
                            //    images.Add(img.Uri);
                        }
                        else
                        {
                            images.Add(media.Images[0].Uri);
                        }

                        string caption = "";

                        if (media.Caption != null)
                        {
                            caption = media.Caption.Text;
                        }

                        //update value for just [contents]

                        using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                        {
                            conn.Open();

                            //insert data to database
                            command_query = @"update copy_posts set [contents]=@contents where [id]=@postId";

                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@contents", System.Data.SqlDbType.VarChar).Value = Newtonsoft.Json.JsonConvert.SerializeObject(images);
                                command.Parameters.Add("@postId", System.Data.SqlDbType.Int).Value = target_postId;

                                command.ExecuteNonQuery();
                            }
                        }

                        postIds.Remove(target_postId);

                        finished++;

                        Program.form.UpdateCopyContentStatus("Finished " + finished.ToString() + "/" + totalposts.ToString());

                    }
                }
                finally
                {
                    IGAPIHelper.Scraper.ReleaseScrapeAccount(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(dyn.account, "username"), API.DBHelpers.db_connection_igtoolkit);
                    Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish APITask - Release Scrape Account");

                    //PLAutoHelper.ProxyHelper.TMProxy_DB_ReleaseProxy(proxy, new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                    PLAutoHelper.ProxyHelper.XProxy_DB_ReleaseProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), proxy);
                    Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish APITask - Release Proxy");

                    //Write session time
                    Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Finished round in " + watch_scrape.Elapsed.TotalMinutes.ToString());
                }
            }
        }
    }
}
