﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static IGAPIHelper.Scraper;

namespace IGAPI___Ramtinak.IGThreads
{
    //grab recently followers of account
    class GrabFollowerThread : IGThread
    {
        public override void DoThread()
        {
            threads.Clear();

            for (int t = 0; t < API.DBHelpers.threads; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => GrabFollower());
                thread.Name = "scrape" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }
            //Load chrome to tab

            //IGHelpers.Helpers.LaunchDriver();

            //API.ProxyHelpers.ProxyServerOthers(true);

            //API.TinSoftHelpers.TinSoftServer(true);
            PLAutoHelper.ProxyHelper.TinSoftServer(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "tinsoft_tb"), true);

            foreach (var t in threads)
            {
                t.Start();
            }


            InitTimer_ManageThread();
            //InitTimer_UpdateProxyStatus();
            //InitTimer_UpdateProxyTinSoft();
            PLAutoHelper.ProxyHelper.TinsoftServer_Ticker(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "tinsoft_tb"));
        }


        private void GrabFollower()
        {
            while (true)
            {
                if (Program.form.isstop)
                    break;

                //get apiobj
                string proxy = "";

                lock ("TinSoft_DB_GetProxy")
                {
                    while (proxy == "" || proxy == null)
                    {
                        proxy = PLAutoHelper.ProxyHelper.TinSoft_DB_GetProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "tinsoft_tb"));
                        Thread.Sleep(5000);
                    }
                }

                APIObj dyn = IGAPIHelper.Scraper.GetAPIObj(proxy, API.DBHelpers.db_connection_igtoolkit);

                InstagramApiSharp.API.IInstaApi _instaApi = dyn.instaApi;

                API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "new_session");

                var time_session = int.Parse(API.DBHelpers.GetValueFromAccountTb(GetObjectValue(dyn.account, "username"), "time_per_session"));
                if (_instaApi == null)
                    throw new Exception();

                Stopwatch watch_scrape = new Stopwatch();
                watch_scrape.Start();

                while (true)
                {
                    if (watch_scrape.Elapsed.TotalMinutes > time_session)
                        break;
                    //get account to get follower

                    string username = API.DBHelpers.GetTargetAccount_ScrapeFollower();

                    if (username == null)
                    {
                        Thread.Sleep(TimeSpan.FromMinutes(5));
                    }

                    //get follower
                    var followers = IGAPIHelper.Scraper.ScrapeFollowerByUsername(username, _instaApi, 2);

                    //add account to db
                    foreach (var item in followers)
                    {
                        API.DBHelpers.AddScrapeUserToDB(item, "follower", username);
                    }

                    API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "grab_follower");
                }


                PLAutoHelper.ProxyHelper.TinSoft_DB_ReleaseProxy(proxy, new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "tinsoft_tb"));

                IGAPIHelper.Scraper.ReleaseScrapeAccount(GetObjectValue(dyn.account, "username"), API.DBHelpers.db_connection_igtoolkit);
            }

        }
    }
}
