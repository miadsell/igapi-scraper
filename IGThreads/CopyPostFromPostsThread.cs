﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.IGThreads
{
    class CopyPostFromPostsThread : IGThread
    {

        List<string> posts;
        string niche_content;

        public CopyPostFromPostsThread(List<string> posts, string niche_content)
        {
            this.posts = posts;
            this.niche_content = niche_content;
        }

        public override void DoThread()
        {
            threads.Clear();

            for (int t = 0; t < 1; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => CopyPostFromPosts());
                thread.Name = "scrape" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }

            //API.ProxyHelpers.ProxyServerOthers(true);

            PLAutoHelper.ProxyHelper.XProxyServer_Ticker(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), Settings.proxy_server_ips);

            foreach (var t in threads)
            {
                t.Start();
            }


            InitTimer_ManageThread();
        }

        private void CopyPostFromPosts()
        {
            int totalposts = posts.Count;
            int finished = 0;

            while (posts.Count > 0)
            {

                var continous_error = 0;

            GetAPIOBJ:

                string proxy = "";


                while (proxy == "" || proxy == null)
                {
                    //proxy = PLAutoHelper.ProxyHelper.TMProxy_DB_GetProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                    proxy = PLAutoHelper.ProxyHelper.XProxy_DB_GetProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"));
                    Thread.Sleep(5000);
                }

                IGAPIHelper.Scraper.APIObj dyn = IGAPIHelper.Scraper.GetAPIObj(proxy, API.DBHelpers.db_connection_igtoolkit);

                if (dyn == null)
                {
                    //PLAutoHelper.ProxyHelper.TMProxy_DB_ReleaseProxy(proxy, new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                    PLAutoHelper.ProxyHelper.XProxy_DB_ReleaseProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), proxy);
                    goto GetAPIOBJ;
                }

                InstagramApiSharp.API.IInstaApi _instaApi = dyn.instaApi;

                Stopwatch watch_scrape = new Stopwatch();
                watch_scrape.Start();


                try
                {

                    var time_session = int.Parse(API.DBHelpers.GetValueFromAccountTb(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(dyn.account, "username"), "time_per_session"));

                    API.DBHelpers.AddAPICallLog(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(dyn.account, "username"), "new_session");


                    while (true)
                    {

                        if (watch_scrape.Elapsed.TotalMinutes > time_session)
                            break;

                        if (continous_error > 5)
                        {
                            IGThreads.ThreadHelper.HandleException("Error Over 5 Times", "");
                            break;
                        }

                        if (posts.Count == 0)
                            break;

                        string target_link = posts[0];

                        //check if niche_content with this link is available, if yes skip it

                        string command_query =
                            @"select *
                            from copy_posts
                            where
	                            [niche_content]=@niche_content and [ig_code]=@ig_code";

                        using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                        {
                            conn.Open();

                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@niche_content", System.Data.SqlDbType.VarChar).Value = niche_content;
                                command.Parameters.Add("@ig_code", System.Data.SqlDbType.VarChar).Value = target_link.Replace("https://www.instagram.com/p/", "").Replace("/", "");

                                using (var dataReader = command.ExecuteReader())
                                {
                                    if (dataReader.HasRows)
                                    {
                                        goto Finished;
                                    }
                                }

                            }
                        }

                        var media = IGAPIHelper.Scraper.GetMediaFromUrlAsync(target_link, _instaApi);

                        bool hasaudio = media.HasAudio;

                        string producttype = media.ProductType;

                        string post_type = "image";

                        if (hasaudio || producttype == "clips")
                        {
                            post_type = "video";//SKIP POST_TYPE VIDEO
                            continue;
                        };

                        //--
                        var ig_code = media.Code;

                        //---

                        var taken_at = media.TakenAt.ToString();

                        //--

                        List<string> images = new List<string>();

                        if (media.Carousel != null)
                        {
                            foreach (var c in media.Carousel)
                            {
                                if (c.MediaType.ToString() == "Image")
                                {
                                    images.Add(c.Images[0].Uri);
                                }
                            }

                            //foreach (var img in media.Carousel[0].Images)
                            //    images.Add(img.Uri);
                        }
                        else
                        {
                            images.Add(media.Images[0].Uri);
                        }

                        string caption = "";

                        if (media.Caption != null)
                        {
                            caption = media.Caption.Text;
                        }

                        //check if exists in database (niche_content + code)

                        command_query =
                            @"select count(*)
                        from copy_posts
                        where
	                        [niche_content]=@niche_content and [ig_code]=@ig_code";

                        using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                        {
                            conn.Open();

                            using (var command = new SqlCommand(command_query, conn))
                            {

                                command.Parameters.Add("@niche_content", System.Data.SqlDbType.VarChar).Value = niche_content;
                                command.Parameters.Add("@ig_code", System.Data.SqlDbType.VarChar).Value = ig_code;
                                var count = int.Parse(command.ExecuteScalar().ToString());
                                if (count > 0)
                                    continue;
                            }

                            //insert data to database
                            command_query = @"insert into copy_posts (niche_content,ig_code,posted_date,caption,contents,post_type, source_account) values (@niche_content,@ig_code,@posted_date,@caption,@contents,@post_type,@source_account)";

                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@niche_content", System.Data.SqlDbType.VarChar).Value = niche_content;
                                command.Parameters.Add("@ig_code", System.Data.SqlDbType.VarChar).Value = ig_code;
                                command.Parameters.Add("@posted_date", System.Data.SqlDbType.DateTime).Value = taken_at;
                                command.Parameters.Add("@caption", System.Data.SqlDbType.NText).Value = caption;
                                command.Parameters.Add("@post_type", System.Data.SqlDbType.VarChar).Value = post_type;
                                command.Parameters.Add("@contents", System.Data.SqlDbType.VarChar).Value = Newtonsoft.Json.JsonConvert.SerializeObject(images);
                                command.Parameters.Add("@source_account", System.Data.SqlDbType.NText).Value = media.User.UserName;

                                command.ExecuteNonQuery();
                            }
                        }

                        Finished:

                        posts.Remove(target_link);

                        finished++;

                        Program.form.UpdateCopyContentStatus("Finished " + finished.ToString() + "/" + totalposts.ToString());

                    }
                }
                finally
                {
                    IGAPIHelper.Scraper.ReleaseScrapeAccount(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(dyn.account, "username"), API.DBHelpers.db_connection_igtoolkit);
                    Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish APITask - Release Scrape Account");

                    //PLAutoHelper.ProxyHelper.TMProxy_DB_ReleaseProxy(proxy, new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                    PLAutoHelper.ProxyHelper.XProxy_DB_ReleaseProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), proxy);
                    Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish APITask - Release Proxy");

                    //Write session time
                    Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Finished round in " + watch_scrape.Elapsed.TotalMinutes.ToString());
                }
            }

        }

    }
}
