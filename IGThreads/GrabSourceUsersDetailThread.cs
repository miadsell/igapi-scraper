﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static IGAPIHelper.Scraper;

namespace IGAPI___Ramtinak.IGThreads
{
    class GrabSourceUsersDetailThread : IGThread
    {

        /// <summary>
        /// target_accounts or source_users
        /// </summary>
        string user_table;

        public GrabSourceUsersDetailThread(string user_table)
        {
            this.user_table = user_table;
        }

        public override void DoThread()
        {
            //JUST 1 THREAD - NOT OPTIMIZE DATABASE FOR MULTI THREAD
            threads.Clear();

            for (int t = 0; t < 1; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => GrabDetails());
                thread.Name = "scrape" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }
            //Load chrome to tab

            //IGHelpers.Helpers.LaunchDriver();

            foreach (var t in threads)
            {
                t.Start();
            }

        }

        private void GrabDetails()
        {
            while (true)
            {
                if (Program.form.isstop)
                    break;
                //get account
                APIObj dyn = IGAPIHelper.Scraper.GetAPIObj("192.168.100.6:4007", API.DBHelpers.db_connection_igtoolkit);

                InstagramApiSharp.API.IInstaApi _instaApi = dyn.instaApi;

                if (_instaApi == null)
                    throw new Exception();

                var account = dyn.account;
                var username = GetObjectValue(account, "username"); //account.GetType().GetProperty("username").GetValue(account, null);

                try
                {
                    var time_session = int.Parse(API.DBHelpers.GetValueFromAccountTb(username, "time_per_session"));

                    if (_instaApi == null)
                        return;

                    API.DBHelpers.AddAPICallLog(username, "new_session");

                    Stopwatch watch_scrape = new Stopwatch();
                    watch_scrape.Start();

                    int continue_error = 0; //so lan bi loi lien tuc

                    while (true)
                    {
                        if (continue_error > 3)
                            throw new System.ArgumentException("Maybe Account Error");

                        if (watch_scrape.Elapsed.TotalMinutes > time_session)
                            break;

                        //Get user to scrape
                        try
                        {
                            string user_url = null;

                            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                            {
                                conn.Open();

                                SqlCommand command = conn.CreateCommand();

                                command.CommandText = "select top 1 * from " + user_table + " where [follower] is null or [follower]=0";

                                using (var dataReader = command.ExecuteReader())
                                {
                                    if (dataReader.HasRows)
                                    {
                                        dataReader.Read();

                                        user_url = dataReader["ig_url"].ToString();
                                    }
                                }
                            }

                            if (user_url == null)
                                break;

                            int follower_count = -1;
                            int following_count = -1;

                            var status = "";

                            var account_pk = IGAPIHelper.Scraper.UserPKByURL(user_url, _instaApi);

                            if (account_pk == null)
                                goto UpdateQuantity;

                            var user_info = IGAPIHelper.Scraper.ScrapeFullUserInfo(account_pk, _instaApi);

                            if (user_info.GetType().GetProperty("status").GetValue(user_info, null) == "Success")
                                status = "";

                            var user = GetObjectValue(user_info, "user");

                            var follower = GetObjectValue(user, "FollowerCount");

                            follower_count = GetObjectValue(user, "FollowerCount");
                            following_count = GetObjectValue(user, "FollowingCount");

                        UpdateQuantity:

                            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                            {
                                conn.Open();
                                //Update information to db

                                using (var command = new SqlCommand("update " + user_table + " set [follower]=" + follower_count + ", [following]=" + following_count + " where [ig_url]='" + user_url + "'", conn))
                                {
                                    command.ExecuteNonQuery();
                                }
                            }

                            API.DBHelpers.AddAPICallLog(username, "scrape_full_info");

                            continue_error = 0;
                        }
                        catch (Exception ex)
                        {
                            continue_error++;

                            API.DBHelpers.AddAPICallLog(username, "scrape_error_full_info");
                        }

                        IGAPIHelper.Scraper.Delay(1000, 3000);
                    }

                    //Release account and proxy

                    IGAPIHelper.Scraper.ReleaseScrapeAccount(username, API.DBHelpers.db_connection_igtoolkit);
                }
                catch (Exception ex)
                {
                    if (ex.Message == "Maybe Account Error")
                    {
                        IGAPIHelper.Scraper.ReleaseScrapeAccount(username, API.DBHelpers.db_connection_igtoolkit, true);

                        API.DBHelpers.AddAPICallLog(username, "account_maybe_error");
                    }
                }
            }

            Program.form.ShowCompleted();
        }


        
    }
}
