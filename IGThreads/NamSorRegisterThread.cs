﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.IGThreads
{
    class NamSorRegisterThread : IGThread
    {
        int successful_target;
        int current_successful = 0;

        public NamSorRegisterThread(int successful_target)
        {
            this.successful_target = successful_target;
        }

        public override void DoThread()
        {
            threads.Clear();

            for (int t = 0; t < 11; t++)
            {
                var pos = t;
                string proxy = "192.168.1.8:400" + (pos + 1).ToString();
                Thread thread = new Thread(() => NamSorRegister(proxy));
                thread.Name = "namsor_reg" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }

            foreach (var t in threads)
            {
                t.Start();
            }


            InitTimer_ManageThread();
        }

        private void NamSorRegister(string proxy = null)
        {

            while (true)
            {
                PLAutoHelper.ProxyHelper.XProxy_NO_DB_ResetProxy(proxy, "192.168.1.8");

                ////get temporary email at https://www.minuteinbox.com/

                //RestClient client = new RestClient("https://www.minuteinbox.com/");

                //CookieContainer _cookieJar = new CookieContainer();
                //client.CookieContainer = _cookieJar;

                //if (proxy != null)
                //    client.Proxy = new WebProxy(proxy.Split(':')[0], int.Parse(proxy.Split(':')[1]));

                //client.UserAgent = " Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko/20100101 Firefox/84.0";

                //var request_load = new RestRequest("", Method.GET);

                //request_load.AddHeader("Host", "www.minuteinbox.com");
                //request_load.AddHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
                //request_load.AddHeader("Accept-Language", "en-US,en;q=0.5");
                //request_load.AddHeader("Accept-Encoding", "gzip, deflate, br");
                //request_load.AddHeader("Connection", "keep-alive");
                //request_load.AddHeader("Upgrade-Insecure-Requests", "1");
                //request_load.AddHeader("DNT", "1");

                //var query_load = client.Execute(request_load);

                ////-------------

                //var request_index = new RestRequest("index/index", Method.GET);

                //request_index.AddHeader("Host", "www.minuteinbox.com");
                //request_index.AddHeader("Accept", "application/json, text/javascript, */*; q=0.01");
                //request_index.AddHeader("Accept-Language", "en-US,en;q=0.5");
                //request_index.AddHeader("Accept-Encoding", "gzip, deflate, br");
                //request_index.AddHeader("X-Requested-With", "XMLHttpRequest");
                //request_index.AddHeader("Connection", "keep-alive");
                //request_index.AddHeader("DNT", "1");

                //var query_index = client.Execute(request_index);

                //string email = null;

                //if (query_index.StatusCode.ToString() == "OK")
                //{
                //    email = query_index.Content.Replace("{\"email\":\"", "").Replace("\"}", "");
                //}

                //if (email == null)
                //{
                //    throw new System.ArgumentException("Can't get email");
                //}


                string fullname = GenerateRandomName();

                string email = fullname.Replace(" ", "").ToLower() + (new Random(Guid.NewGuid().GetHashCode())).Next(999).ToString() + "@gmail.com";

                var apiKey = PLAutoHelper.DetectPersonality.Register_Namsor(email, "123456789kdL", fullname, proxy);

                if (apiKey == null || apiKey == "")
                    throw new System.ArgumentException("apiKey is empty");

                //insert api key to db

                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    //clear claimed status in scrape accounts

                    using (var command = new SqlCommand("insert into namsor_api ([apiKey]) values ('" + apiKey + "')", conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }

                lock ("increase_success")
                {
                    current_successful++;

                    if (current_successful >= successful_target)
                        break;
                }

                Program.form.WriteLogToForm_DetectPersonality("Current successful api: " + current_successful.ToString());
            }

            //RefreshMinuteinbox(client);
        }

        private string GenerateRandomName()
        {
            RestClient client = new RestClient("http://random-name-generator.info/");

            CookieContainer _cookieJar = new CookieContainer();
            client.CookieContainer = _cookieJar;

            client.UserAgent = " Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko/20100101 Firefox/84.0";

            var request_load = new RestRequest("", Method.GET);

            request_load.AddHeader("Host", "random-name-generator.info");
            client.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko/20100101 Firefox/84.0";
            request_load.AddHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            request_load.AddHeader("Accept-Language", "en-US,en;q=0.5");
            request_load.AddHeader("Accept-Encoding", "gzip, deflate");
            request_load.AddHeader("Connection", "keep-alive");
            request_load.AddHeader("Upgrade-Insecure-Requests", "1");
            request_load.AddHeader("DNT", "1");

            var query_load = client.Execute(request_load);

            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(query_load.Content);

            var name_node = doc.DocumentNode.SelectSingleNode("//ol[@class='nameList']//li");

            var name = name_node.InnerText.Replace("\t", "").Replace("\n", "");

            return name;
        }

        private void RefreshMinuteinbox(RestClient client)
        {
            //refresh to check email

            var request_refresh = new RestRequest("index/refresh", Method.GET);

            request_refresh.AddHeader("Host", "www.minuteinbox.com");
            request_refresh.AddHeader("Accept", "application/json, text/javascript, */*; q=0.01");
            request_refresh.AddHeader("Accept-Language", "en-US,en;q=0.5");
            request_refresh.AddHeader("Accept-Encoding", "gzip, deflate, br");
            request_refresh.AddHeader("X-Requested-With", "XMLHttpRequest");
            request_refresh.AddHeader("Connection", "keep-alive");
            request_refresh.AddHeader("DNT", "1");

            var query_refresh = client.Execute(request_refresh);

            var json = query_refresh.Content.Remove(0, 1);

            var json_dynamic = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(json);

            foreach(var item in json_dynamic)
            {

            }
        }
    }
}
