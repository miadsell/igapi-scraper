﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.IGThreads
{
    class GrabRecentLikersPost : IGThread
    {
        public override void DoThread()
        {

            PLAutoHelper.ProxyHelper.TinSoftServer(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "tinsoft_tb"), true);

            GrabRecentLikers();
        }


        private void GrabRecentLikers()
        {
            //var hwnd = PLAutoHelper.RPAFirefox.GetFFHandleByPath(@"E:\Google Drive\MerchData\represent\IGAPI - Ramtinak\bin\Debug\SwitchPortableFF\switch-01\App\firefox64\firefox.exe");
            var hwnd = PLAutoHelper.RPAFirefox.GetFFHandleByPath(@"E:\Google Drive\MerchData\represent\IGAPI - Ramtinak\bin\Debug\FirefoxPortable\App\firefox64\firefox.exe");

            var ig_account = new
            {
                username = "duyen.nguyen939",
                password = "123456789kdL",
                id = "6",
                already_hasprofile = "yes"
            };

            var igffpro = new IGClass.IGFFPro(ig_account, "RESIDENT");

            //get post_url

            var post_url = "https://www.instagram.com/p/CInIa1ylggd/";

            //Open post
            PLAutoHelper.RPAFirefox.OpenUrlInCurrentTab(hwnd, post_url);

            //Click liker and wait popup

            PLAutoHelper.RPA.ClickandWait(BMPSource.Likers_likes_stats, BMPSource.Likers_Likes_Title, hwnd, true);


            List<string> users = new List<string>();

            for (int i = 0; i < 10; i++)
            {
            GenerateLogName:
                var log_name = "log_" + LocalHelper.RandomGUID(5) + ".txt";

                var log_path = Path.GetFullPath(Settings.firefox_download_locate) + "\\" + log_name;

                if (File.Exists(log_name))
                    goto GenerateLogName;

                PLAutoHelper.RPAFirefox.OpenNewTabUrl(hwnd, "file:///F:/DOWNLOAD/ui.visionma.html?direct=1&macro=IG-Liker-1&savelog=" + log_name);

                Stopwatch watch_scrape_likers = new Stopwatch();
                watch_scrape_likers.Start();

                while (true)
                {
                    if (File.Exists(log_path))
                        break;

                    if (watch_scrape_likers.Elapsed.TotalSeconds > 15)
                    {
                        throw new System.ArgumentException("Uivision Scrape Likers Error");
                    }

                    Thread.Sleep(1000);
                }

                var log = File.ReadAllText(log_path);

                var start_pattern = "[echo] minad_start\n[info] Executing:  | echo | ${likers_element} |  | \n[echo] ";

                var start = log.IndexOf(start_pattern);


                var stop = log.IndexOf("\n[info] Executing:  | echo | minad_end |  | ");

                var html = log.Substring(start + start_pattern.Length, stop - (start + start_pattern.Length));

                //extract user from html
                var htmlDoc_load = new HtmlAgilityPack.HtmlDocument();
                htmlDoc_load.LoadHtml(html);

                var user_nodes = htmlDoc_load.DocumentNode.SelectNodes("//a[@class='FPmhX notranslate MBL3Z']");

                foreach (var item in user_nodes)
                {
                    users.Add(item.InnerText);
                }

                users = users.Distinct().ToList();

                ScrollUpLikerBox(hwnd);

                Thread.Sleep(200);
            }

            //add to db

            foreach(var item in users)
            {
                API.DBHelpers.AddScrapeUsersToDB_ByUsername(item, "", "");
            }

            igffpro.Dispose();
        }

        private void ScrollUpLikerBox(IntPtr hwnd)
        {
            var points = PLAutoHelper.RPA.FindOutPoints(BMPSource.Likers_FollowBtn, hwnd);

            var choice_point = points[(new Random(Guid.NewGuid().GetHashCode())).Next(points.Count)];

            KAutoHelper.AutoControl.SendClickOnPosition(hwnd, choice_point.X-80, choice_point.Y);

            PLAutoHelper.RPA.SendMouseScrollOnBackground(hwnd, "up", 3, points[0],0.5,"page");
        }
    }
}
