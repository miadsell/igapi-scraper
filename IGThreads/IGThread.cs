﻿using InstagramApiSharp.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IGAPI___Ramtinak.IGThreads
{
    abstract class IGThread
    {

        public abstract void DoThread();

        protected List<Thread> threads = new List<Thread>();
        public void ManageThread()
        {
            if (Program.form.isstop)
            {
              
            }
            else
            {
                int count_running_thread = 0;
                for (int i = 0; i < threads.Count; i++)
                {
                    if (threads[i].ThreadState.ToString() == "Running" || threads[i].ThreadState.ToString() == "WaitSleepJoin")
                    {
                        count_running_thread++;
                    }
                }

                if (count_running_thread == 0)
                {
                    Program.form.UpdateStatusLabel("Stopped");


                    timer_ManageThread.Stop();
                    timer_ManageThread.Dispose();
                }
            }

            GC.Collect();

            Application.DoEvents();
        }

        #region --Ticking Timer--

        private System.Windows.Forms.Timer timer_ManageThread;


        public void InitTimer_ManageThread()
        {
            timer_ManageThread = new System.Windows.Forms.Timer();
            timer_ManageThread.Tick += new EventHandler(timer_ManageThread_Tick);
            timer_ManageThread.Interval = 10000; // in miliseconds
            timer_ManageThread.Start();

        }

        private void timer_ManageThread_Tick(object sender, EventArgs e)
        {
            //System.Threading.Thread thr1 = new System.Threading.Thread(ManageQuoraThread);

            //Task task = Task.Factory.StartNew(() => ManageThread());
            //Task.WaitAll(task);

            Thread worker = new Thread(new ThreadStart(ManageThread));
            worker.Name = "Manage Engage Thread";
            worker.IsBackground = true;
            worker.Start();

        }

        #endregion

        #region --Ticker Update Proxy Status--
        //Update proxy status

        protected System.Windows.Forms.Timer timer_UpdateProxyStatus;

        protected void InitTimer_UpdateProxyStatus()
        {
            timer_UpdateProxyStatus = new System.Windows.Forms.Timer();
            timer_UpdateProxyStatus.Tick += new EventHandler(timer_UpdateProxyStatus_Tick);
            timer_UpdateProxyStatus.Interval = 10000; // in miliseconds
            timer_UpdateProxyStatus.Start();

        }

        private void timer_UpdateProxyStatus_Tick(object sender, EventArgs e)
        {
            Thread worker = new Thread(() => API.ProxyHelpers.ProxyServerOthers());
            worker.Name = "Update Proxy Status Thread";
            worker.IsBackground = true;
            worker.Start();
        }

        #endregion

        #region --Ticker Update Proxy Status--
        //Update proxy status

        protected System.Windows.Forms.Timer timer_UpdateProxyTinSoft;

        protected void InitTimer_UpdateProxyTinSoft()
        {
            timer_UpdateProxyTinSoft = new System.Windows.Forms.Timer();
            timer_UpdateProxyTinSoft.Tick += new EventHandler(timer_UpdateProxyTinSoft_Tick);
            timer_UpdateProxyTinSoft.Interval = 10000; // in miliseconds
            timer_UpdateProxyTinSoft.Start();

        }

        private void timer_UpdateProxyTinSoft_Tick(object sender, EventArgs e)
        {
            Thread worker = new Thread(() => PLAutoHelper.ProxyHelper.TinSoftServer(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "tinsoft_tb")));
            worker.Name = "Update Proxy Status Thread";
            worker.IsBackground = true;
            worker.Start();
        }

        #endregion

        protected object GetObjectValue(object ob, string name)
        {
            return ob.GetType().GetProperty(name).GetValue(ob, null);
        }
    }
}
