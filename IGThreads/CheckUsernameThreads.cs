﻿using InstagramApiSharp.API.Builder;
using InstagramApiSharp.Logger;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.IGThreads
{
    class CheckUsernameThreads : IGThread
    {
        public override void DoThread()
        {
            threads.Clear();

            for (int t = 0; t < 1; t++) //just use 1, not handle get username to check in multi threads
            {
                var pos = t;
                Thread thread = new Thread(() => CheckUserName());
                thread.Name = "scrape" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }

            //API.ProxyHelpers.ProxyServerOthers(true);

            API.TinSoftHelpers.TinSoftServer(true);

            foreach (var t in threads)
            {
                t.Start();
            }


            InitTimer_ManageThread();
            //InitTimer_UpdateProxyStatus();
            InitTimer_UpdateProxyTinSoft();
        }


        private void CheckUserName()
        {
            while (true)
            {
                if (Program.form.isstop)
                    break;

                ////get account
                //var dyn = API.IGHelpers.GetAccount();

                //InstagramApiSharp.API.IInstaApi _instaApi = dyn._instaApi;

                InstagramApiSharp.API.IInstaApi _instaApi = null;

                var proxy_str = API.TinSoftHelpers.GetProxy();

                var proxyinfo = new
                {
                    proxystr = proxy_str
                };

                if (proxyinfo != null)
                {
                    var proxy = new WebProxy()
                    {
                        Address = new Uri("http://" + proxyinfo.proxystr), //i.e: http://1.2.3.4.5:8080
                        BypassProxyOnLocal = false,
                        UseDefaultCredentials = false,

                        //// *** These creds are given to the proxy server, not the web server ***
                        //Credentials = new NetworkCredential(
                        //userName: proxyinfo.user,
                        //password: proxyinfo.pass)
                    };

                    // Now create a client handler which uses that proxy
                    var httpClientHandler = new HttpClientHandler()
                    {
                        Proxy = proxy,
                    };

                    var httpClient = new HttpClient(handler: httpClientHandler, disposeHandler: true)
                    {
                        // if you want to use httpClient, you should set this
                        BaseAddress = new Uri("https://i.instagram.com/")
                    };

                    // create new InstaApi instance using Builder
                    _instaApi = InstaApiBuilder.CreateBuilder()
                        .UseLogger(new DebugLogger(LogLevel.All)) // use logger for requests and debug messages
                        .UseHttpClient(httpClient)
                        .UseHttpClientHandler(httpClientHandler)
                        .Build();
                }


                if (_instaApi == null)
                    throw new Exception();

                try
                {


                    var time_session = 10;

                    if (_instaApi == null)
                        return;


                    //API.DBHelpers.AddAPICallLog(dyn.account.username, "new_session");


                    Stopwatch watch_scrape = new Stopwatch();
                    watch_scrape.Start();

                    int continue_error = 0; //so lan bi loi lien tuc

                    while (true)
                    {
                        if (continue_error > 10)
                            throw new System.ArgumentException("Maybe Account Error");

                        if (watch_scrape.Elapsed.TotalMinutes > time_session)
                            break;

                        string username = null;

                        try
                        {
                            //Get username to check

                            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                            {
                                conn.Open();

                                using (var command = new SqlCommand("select top 1 * from username_for_reg where [is_available] is null or [is_available]=''", conn))
                                {
                                    using(var dataReader=command.ExecuteReader())
                                    {
                                        if (dataReader.HasRows)
                                        {
                                            dataReader.Read();

                                            username = dataReader["username"].ToString();
                                        }
                                        else
                                            return;
                                    }
                                }
                            }



                            var checked_user = IGAPIHelper.Scraper.IsUsernameNotExisted(_instaApi, username).ToString();


                            //update

                            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                            {
                                conn.Open();

                                using (var command = new SqlCommand("update username_for_reg set [is_available]='" + checked_user.ToString() + "' where [username]='" + username + "'", conn))
                                {
                                    command.ExecuteNonQuery();
                                }
                            }


                            //API.DBHelpers.AddAPICallLog(dyn.account.username, "check_username");

                            continue_error = 0;
                        }
                        catch
                        {
                            continue_error++;
                            //API.DBHelpers.AddAPICallLog(dyn.account.username, "check_username_error");
                        }

                        IGAPIHelper.Scraper.Delay(2, 5);

                    }

                    //Release account and proxy

                    //API.DBHelpers.ReleaseScrapeAccount(dyn);
                }
                catch (Exception ex)
                {
                    if (ex.Message == "Maybe Account Error")
                    {
                        //API.DBHelpers.ReleaseScrapeAccount(dyn, true);


                        //API.DBHelpers.AddAPICallLog(dyn.account.username, "account_maybe_error");
                    }

                }

                //API.TinSoftHelpers.ReleaseProxy(dyn.proxyinfo.proxystr);
            }


            
        }
    }
}
