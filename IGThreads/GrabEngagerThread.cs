﻿using InstagramApiSharp.Classes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static IGAPIHelper.Scraper;

namespace IGAPI___Ramtinak.IGThreads
{
    class GrabEngagerThread : IGThread
    {

        string recent_days;

        public GrabEngagerThread(string recent_days)
        {
            this.recent_days = recent_days;
        }

        public override void DoThread()
        {
            threads.Clear();

            for (int t = 0; t < API.DBHelpers.threads; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => GrabEngager());
                thread.Name = "scrape" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }

            //API.ProxyHelpers.ProxyServerOthers(true);

            API.TinSoftHelpers.TinSoftServer(true);

            foreach (var t in threads)
            {
                t.Start();
            }


            InitTimer_ManageThread();
            //InitTimer_UpdateProxyStatus();
            InitTimer_UpdateProxyTinSoft();
        }

        private void GrabEngager()
        {
            while (true)
            {
                if (Program.form.isstop)
                    break;

                //get account
                string proxy = "";

                lock ("TinSoft_DB_GetProxy")
                {
                    while (proxy == "" || proxy == null)
                    {
                        proxy = PLAutoHelper.ProxyHelper.TinSoft_DB_GetProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "tinsoft_tb"));
                        Thread.Sleep(5000);
                    }
                }

                APIObj dyn = IGAPIHelper.Scraper.GetAPIObj(proxy, API.DBHelpers.db_connection_igtoolkit);

                InstagramApiSharp.API.IInstaApi _instaApi = dyn.instaApi;


                if (_instaApi == null)
                    throw new Exception();

                try
                {

                    var time_session = int.Parse(API.DBHelpers.GetValueFromAccountTb(GetObjectValue(dyn.account, "username"), "time_per_session"));

                    if (_instaApi == null)
                        return;


                    API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "new_session");


                    Stopwatch watch_scrape = new Stopwatch();
                    watch_scrape.Start();

                    int continue_error = 0; //so lan bi loi lien tuc

                    while (true)
                    {
                        if (continue_error > 10)
                            throw new System.ArgumentException("Maybe Account Error");

                        if (watch_scrape.Elapsed.TotalMinutes > time_session)
                            break;

                        try
                        {
                            //Get target posts

                            var post = API.DBHelpers.GetPostToScrape(recent_days);


                            if (post == null)
                                return;


                            var engagers = new List<InstagramApiSharp.Classes.Models.InstaUserShort>() { };

                            switch (post.scrape_type)
                            {
                                case "Likers":
                                    engagers.AddRange(GrabLikers(post.posturl, _instaApi));
                                    break;
                                case "Commenters":
                                    engagers.AddRange(GrabCommenters(post.posturl, _instaApi));
                                    break;
                                case "All":
                                    engagers.AddRange(GrabLikers(post.posturl, _instaApi));
                                    engagers.AddRange(GrabCommenters(post.posturl, _instaApi));

                                    break;
                                default:
                                    throw new System.ArgumentException("Not found this type of scrape_type");
                            }

                            foreach (var item in engagers)
                            {
                                API.DBHelpers.AddScrapeUserToDB(item, "engager", post.from_account);
                            }

                            API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "grab_engager");

                            continue_error = 0;
                        }
                        catch
                        {
                            continue_error++;
                            API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "grab_error_engager");
                        }

                        IGAPIHelper.Scraper.Delay(1, 3);

                    }

                    //Release account and proxy


                    IGAPIHelper.Scraper.ReleaseScrapeAccount(GetObjectValue(dyn.account, "username"), API.DBHelpers.db_connection_igtoolkit);
                }
                catch (Exception ex)
                {
                    if (ex.Message == "Maybe Account Error")
                    {

                        IGAPIHelper.Scraper.ReleaseScrapeAccount(GetObjectValue(dyn.account, "username"), API.DBHelpers.db_connection_igtoolkit, true);


                        API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "account_maybe_error");
                    }

                }

                PLAutoHelper.ProxyHelper.TinSoft_DB_ReleaseProxy(proxy, new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "tinsoft_tb"));
            }
        }

        private List<InstagramApiSharp.Classes.Models.InstaUserShort> GrabLikers(string posturl, InstagramApiSharp.API.IInstaApi _instaApi)
        {
            var likers = IGAPIHelper.Scraper.ScrapeLikers(posturl, _instaApi);

            return likers;
        }

        private List<InstagramApiSharp.Classes.Models.InstaUserShort> GrabCommenters(string posturl, InstagramApiSharp.API.IInstaApi _instaApi)
        {
            var comments = IGAPIHelper.Scraper.ScrapeCommenters(posturl, _instaApi);

            var commenters = comments.Value.Comments.Select(c => c.User).ToList<InstagramApiSharp.Classes.Models.InstaUserShort>();

            return commenters;
        }
    }
}
