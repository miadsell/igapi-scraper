﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.IGThreads
{
    class LoginToNewAccount_Thread : IGThread
    {
        public override void DoThread()
        {
            threads.Clear();

            for (int t = 0; t < 1; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => Do_LoginNewAccount());
                thread.Name = "login_" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }

            //API.ProxyHelpers.ProxyServerOthers(true);

            PLAutoHelper.ProxyHelper.XProxyServer_Ticker(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), Settings.proxy_server_ips);

            foreach (var t in threads)
            {
                t.Start();
            }


            InitTimer_ManageThread();
        }


        private void Do_LoginNewAccount()
        {
            while (true)
            {
                //take account
                dynamic account = null;

                account = TakeAccountToLogin();

                if (account == null)
                    break;

                //do login

                string proxy = "";


                while (proxy == "" || proxy == null)
                {
                    //proxy = PLAutoHelper.ProxyHelper.TMProxy_DB_GetProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                    proxy = PLAutoHelper.ProxyHelper.XProxy_DB_GetProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"));
                    Thread.Sleep(5000);
                }


                InstagramApiSharp.API.IInstaApi _instaApi = null;

                try
                {
                    _instaApi = IGAPIHelper.Scraper.Login(account, proxy, API.DBHelpers.db_connection_igtoolkit);
                }
                catch (Exception ex)
                {
                    if (ex.Message == "Challenge is required")
                    {
                        //update state of scrape account
                        IGAPIHelper.Scraper.UpdateScrapeAccount(account.username, "state", "CHALLENGE", API.DBHelpers.db_connection_igtoolkit);

                        goto NextAccount;
                    }
                }


                if (_instaApi == null)
                {
                    goto NextAccount;
                }


                var currentUser = Task.Run(_instaApi.GetCurrentUserAsync).GetAwaiter().GetResult();

                if (currentUser.Info.Message == "challenge_required")
                {
                    //update state of scrape account
                    IGAPIHelper.Scraper.UpdateScrapeAccount(account.username, "state", "CHALLENGE", API.DBHelpers.db_connection_igtoolkit);

                    goto NextAccount;
                }

                NextAccount:

                PLAutoHelper.ProxyHelper.XProxy_DB_ReleaseProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), proxy);
                Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish APITask - Release Proxy");
            }
        }

        private dynamic TakeAccountToLogin()
        {

            string choiced_id = null;

            dynamic account = null;

            lock ("igtoolkit_getaccount")
            {
                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    var command_query =
                        @"select top 1 *
                        from accounts
                        where
	                        [session] is null and
	                        [state] is null and
	                        [status]=''
                        order by id desc";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                dataReader.Read();

                                choiced_id = dataReader["id"].ToString();
                            }
                        }
                    }

                    if (choiced_id != null)
                    {

                        command_query =
                            @"update accounts set [status]='Claimed', [last_use]=GETDATE()
		                        where [id]=@id and ([status]!='Claimed' or [status] is NULL)";


                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = choiced_id;

                            var affect_row = command.ExecuteNonQuery();

                        }
                    }
                    else
                        return null;

                    string select_query =
                            @"select *
                            from accounts
                            where
	                            [id]=@id";

                    using (var command = new SqlCommand(select_query, conn))
                    {
                        command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = choiced_id;
                        using (var dataReader = command.ExecuteReader())
                        {
                            dataReader.Read();

                            var username = dataReader["username"].ToString();
                            var password = dataReader["password"].ToString();
                            var id = dataReader["id"].ToString();
                            var session = dataReader["session"] == null ? "" : dataReader["session"].ToString();
                            var profile = dataReader["profile"].ToString();
                            var already_hasprofile = dataReader["already_hasprofile"] == null ? "" : dataReader["already_hasprofile"].ToString();
                            var time_per_session = int.Parse(dataReader["time_per_session"].ToString());

                            account = new
                            {
                                username,
                                password,
                                id,
                                session,
                                profile,
                                time_per_session,
                                already_hasprofile
                            };
                        }
                    }
                }
            }

            return account;
        }
    }
}
