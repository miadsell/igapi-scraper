﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.IGThreads
{
    class GrabRelatedCommentAccountThread : IGThread
    {
        public override void DoThread()
        {
            threads.Clear();

            for (int t = 0; t < 1; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => GrabAccounts());
                thread.Name = "scrape" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }
            //Load chrome to tab

            //IGHelpers.Helpers.LaunchDriver();

            foreach (var t in threads)
            {
                t.Start();
            }

            InitTimer_ManageThread();
        }

        private void GrabAccounts()
        {
            var pid = PLAutoHelper.RPAFirefox.LaunchFFPortable("FirefoxPortable");
            var hwnd = Process.GetProcessById(pid).MainWindowHandle;

            while (true)
            {

                if (Program.form.isstop)
                    break;
                string profile_url = "";

                //get account that was check
                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();
                    var sql = "update top (1) comment_accounts set scrape_related='Used' output inserted.id where (scrape_related is null or scrape_related != 'Used') and [ok]='OK'";

                    string id = null;

                    using (var command = new SqlCommand(sql, conn))
                    {
                        var value = command.ExecuteScalar();
                        if (value != null)
                            id = value.ToString();
                    }

                    if (id == null)
                        break;

                    sql = "select [ig_url] from comment_accounts where [id]=" + id;


                    using (var command = new SqlCommand(sql, conn))
                    {
                        profile_url = command.ExecuteScalar().ToString();
                    }
                }

                //get related account by simulate chrome

                var related_accounts = (new IGTasks.RelatedAccountTask(hwnd, profile_url)).ReturnTask();

                API.DBHelpers.AddCommentAccountToDB(related_accounts);

                Thread.Sleep((new Random()).Next(3000, 15000));
            }
        }
    }
}
