﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.IGThreads
{
    class GrabSourceUsersThread : IGThread
    {
        public override void DoThread()
        {

            threads.Clear();

            for (int t = 0; t < 1; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => GrabAccounts());
                thread.Name = "scrape" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }
            //Load chrome to tab

            //IGHelpers.Helpers.LaunchDriver();

            foreach (var t in threads)
            {
                t.Start();
            }

            InitTimer_ManageThread();
        }


        private void GrabAccounts()
        {
            var pid = PLAutoHelper.RPAFirefox.LaunchFFPortable("FirefoxPortable");
            var hwnd = Process.GetProcessById(pid).MainWindowHandle;

            while (true)
            {

                if (Program.form.isstop)
                    break;
                string profile_url = "";

                //get account that was check
                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();
                    var sql = "update top (1) source_users set scrape_related='Used' output inserted.id where (scrape_related is null or scrape_related != 'Used') and [ok]='OK'";

                    string id = null;

                    using (var command = new SqlCommand(sql, conn))
                    {
                        var value = command.ExecuteScalar();
                        if (value != null)
                            id = value.ToString();
                    }

                    if (id == null)
                        break;

                    sql = "select [ig_url] from source_users where [id]=" + id;


                    using (var command = new SqlCommand(sql, conn))
                    {
                        profile_url = command.ExecuteScalar().ToString();
                    }
                }

                //get related account by simulate chrome

                var related_accounts = (new IGTasks.RelatedAccountTask(hwnd, profile_url)).ReturnTask();

                API.DBHelpers.AddSourceUsersToDB(related_accounts);

                Thread.Sleep((new Random()).Next(3000, 15000));
            }

        }

        private ChromeDriver LaunchChrome(string profile)
        {
            ChromeDriver driver = null;

            ChromeOptions options = new ChromeOptions();

            options.AddArgument(@"--user-data-dir=" + profile);
            options.AddArgument("--mute-audio");
            options.AddArgument("--start-minimized");

            //var proxy_str = API.DBHelpers.GetProxy();

            //var proxy = new Proxy();
            //proxy.Kind = ProxyKind.Manual;
            //proxy.IsAutoDetect = false;
            //proxy.HttpProxy =
            //proxy.SslProxy = proxy_str;
            //options.Proxy = proxy;
            //options.AddArgument("ignore-certificate-errors");

            ChromeDriverService chromeDriverService = ChromeDriverService.CreateDefaultService("driver\\" + profile.Replace("Profile", ""));
            chromeDriverService.HideCommandPromptWindow = true;

            driver = new ChromeDriver(chromeDriverService, options);


            return driver;
        }

        private List<string> RelatedAccount(ChromeDriver driver, string profile_url)
        {

            List<string> hrefs = new List<string>();
            driver.Navigate().GoToUrl(profile_url);

            try
            {
                WaitElement(driver, "//*[@class='OfoBO']");
            }
            catch
            {
                return hrefs;
            }

            ////Get followers count

            //var span_followers = driver.FindElementByXPath("//a[contains(@class,'-nal3')]/span");
            //var followers = int.Parse(span_followers.GetAttribute("title").Replace(",", ""));

            //if (followers < 10000)
            //    return hrefs;

            //Click button related

            driver.FindElementByClassName("OfoBO").Click();

            //Class ekfSF //a notranslate

            WaitElement(driver, "//a[contains(@class,'notranslate')]");

            var suggest_element = driver.FindElementsByXPath("//a[contains(@class,'notranslate')]");


            foreach (var item in suggest_element)
            {
                hrefs.Add(item.GetAttribute("href"));
            }


            return hrefs;
        }

        private void WaitElement(ChromeDriver driver, string xpath)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementIsVisible(By.XPath(xpath)));
        }

    }
}
