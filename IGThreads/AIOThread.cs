﻿using InstagramApiSharp.Classes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using static IGAPIHelper.Scraper;

namespace IGAPI___Ramtinak.IGThreads
{
    class AIOThread : IGThread
    {
        public override void DoThread()
        {

            threads.Clear();

            for (int t = 0; t < Settings.aio_threads_quantity; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => AIOFunc());
                thread.Name = "scrape" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }

            //API.ProxyHelpers.ProxyServerOthers(true);

            PLAutoHelper.ProxyHelper.XProxyServer_Ticker(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), Settings.proxy_server_ips);

            foreach (var t in threads)
            {
                t.Start();
            }


            InitTimer_ManageThread();
        }

        private void AIOFunc()
        {

            while (true)
            {
                bool found_task = false;

                if (Program.form.isstop)
                {
                    Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Stopped");
                    break;
                }

                if (ThreadHelper.IsAvailableAPITask())
                {
                    found_task = true;
                    APITask();
                }

                if (!found_task)
                {
                    Thread.Sleep(TimeSpan.FromMinutes(5));
                }
            }
        }

        #region --APITask--

        private void APITask()
        {
            Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask");
            //get account

            var continous_error = 0;
        GetAPIOBJ:

            string proxy = "";


            while (proxy == "" || proxy == null)
            {
                //proxy = PLAutoHelper.ProxyHelper.TMProxy_DB_GetProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                proxy = PLAutoHelper.ProxyHelper.XProxy_DB_GetProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"));
                Thread.Sleep(5000);
            }

            APIObj dyn = IGAPIHelper.Scraper.GetAPIObj(proxy, API.DBHelpers.db_connection_igtoolkit);

            if (dyn == null)
            {
                //PLAutoHelper.ProxyHelper.TMProxy_DB_ReleaseProxy(proxy, new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                PLAutoHelper.ProxyHelper.XProxy_DB_ReleaseProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), proxy);
                goto GetAPIOBJ;
            }

            InstagramApiSharp.API.IInstaApi _instaApi = dyn.instaApi;

            Stopwatch watch_scrape = new Stopwatch();
            watch_scrape.Start();

            try
            {

                var time_session = int.Parse(API.DBHelpers.GetValueFromAccountTb(GetObjectValue(dyn.account, "username"), "time_per_session"));

                API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "new_session");

                bool no_grabpost = false, no_grab_commentpost = false, no_grablikers = false, no_grabfollowers = false, no_grab_followback = false, no_grab_follower_formother = false, no_GrabPost_ForComment_FromScrapeUsers = false, no_GrabComment_ForSlave = false;


                while (true)
                {

                    if (watch_scrape.Elapsed.TotalMinutes > time_session)
                        break;

                    if (continous_error > 5)
                    {
                        ThreadHelper.HandleException("Error Over 5 Times", "");
                        break;
                    }

                    string status = null;

                    status = null;

                    //GrabPost For Comment
                    if (Program.form.is_APITask_GrabPost_ForComment && !no_grab_commentpost)
                    {
                        try
                        {
                            status = APITask_GrabPost_ForComment(_instaApi);

                            if (status == "Success")
                            {
                                API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "grab_commentposts");
                                Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish grab_commentposts");
                                continous_error = 0;
                                goto NextRound;
                            }
                            if (status == "NODATA")
                                no_grab_commentpost = true;
                        }
                        catch (Exception ex)
                        {
                            continous_error++;
                            throw new System.ArgumentException("GrabCommentPost: " + ex.Message + "Stacktrace:" + ex.StackTrace);
                        }
                    }

                    //GrabPost For ScrapeUsers
                    if (Program.form.is_APITask_GrabPost_ForComment_FromScrapeUsers && !no_GrabPost_ForComment_FromScrapeUsers)
                    {
                        try
                        {
                            status = APITask_GrabPost_ForComment_FromScrapeUsers(_instaApi);

                            if (status == "Success")
                            {
                                API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "grab_commentposts_for_scrapeusers");
                                Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish grab_commentposts_for_scrapeusers");
                                continous_error = 0;
                                goto NextRound;
                            }
                            if (status == "NODATA")
                                no_GrabPost_ForComment_FromScrapeUsers = true;
                        }
                        catch (Exception ex)
                        {
                            continous_error++;
                            throw new System.ArgumentException("GrabCommentPost: " + ex.Message + "Stacktrace:" + ex.StackTrace);
                        }
                    }

                    //GrabComment_ForSlave
                    if (Program.form.is_APITask_GrabComment_ForSlave && !no_GrabComment_ForSlave)
                    {
                        try
                        {
                            status = APITask_GrabComment_ForSlave(_instaApi, GetObjectValue(dyn.account, "username"));

                            if (status == "Success")
                            {
                                API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "GrabComment_ForSlave");
                                Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish GrabComment_ForSlave");
                                continous_error = 0;
                                goto NextRound;
                            }
                            if (status == "NODATA")
                                no_GrabComment_ForSlave = true;
                        }
                        catch (Exception ex)
                        {
                            continous_error++;
                            throw new System.ArgumentException("GrabComment_ForSlave: " + ex.Message + "Stacktrace:" + ex.StackTrace);
                        }
                    }


                    //GrabPost

                    status = null;

                    if (Program.form.is_APITask_GrabPost && !no_grabpost)
                    {
                        try
                        {
                            status = APITask_GrabPost(_instaApi);

                            if (status == "Success")
                            {
                                API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "grab_posts");
                                Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish grab_posts");
                                continous_error = 0;
                                goto NextRound;
                            }
                            if (status == "NODATA")
                                no_grabpost = true;
                        }
                        catch (Exception ex)
                        {
                            continous_error++;
                            throw new System.ArgumentException("GrabPost: " + ex.Message + "Stacktrace:" + ex.StackTrace);
                        }
                    }

                    //Greb follower for mother
                    status = null;

                    if (Program.form.is_APITask_GrabFollower_ForMother && !no_grab_follower_formother)
                    {
                        try
                        {
                            status = APITask_GrabFollower_ForMotherAccount(_instaApi);

                            if (status == "Success")
                            {
                                API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "grab_follower_formother");
                                Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish grab_follower_formother");
                                continous_error = 0;
                                goto NextRound;
                            }
                            if (status == "NODATA")
                                no_grab_follower_formother = true;
                        }
                        catch (Exception ex)
                        {
                            continous_error++;
                            throw new System.ArgumentException("Grab Follower for Mother: " + ex.Message + "Stacktrace:" + ex.StackTrace);
                        }
                    }

                    //Grab FollowBack

                    status = null;

                    if (Program.form.is_APITask_GrabFollowerBack && !no_grab_followback)
                    {
                        try
                        {
                            status = APITask_GrabFollowerBack(_instaApi);

                            if (status == "Success")
                            {
                                API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "grab_followback");
                                Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish grab_followback");
                                continous_error = 0;
                                goto NextRound;
                            }
                            if (status == "NODATA")
                                no_grab_followback = true;
                        }
                        catch (Exception ex)
                        {
                            continous_error++;
                            throw new System.ArgumentException("Grab FollowBack: " + ex.Message + "Stacktrace:" + ex.StackTrace);
                        }
                    }

                    //Grab Commenters

                    //tam thoi InstagramAPISharp loi phan grab comment, se enable khi mua InstagraAPISharp private

                    //status = null;

                    //if (Program.form.is_APITask_GrabCommenters)
                    //{
                    //    try
                    //    {
                    //        status = APITask_GrabCommenters(_instaApi);

                    //        if (status == "Success")
                    //        {
                    //            API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "grab_commenters");
                    //            Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish grab_commenters");
                    //            continous_error = 0;
                    //            goto NextRound;
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        continous_error++;
                    //        throw new System.ArgumentException("GrabCommenters: " + ex.Message + "Stacktrace:" + ex.StackTrace);
                    //    }
                    //}

                    //Grab Likers
                    status = null;

                    if (Program.form.is_APITask_GrabLikers && !no_grablikers)
                    {
                        //try
                        //{
                        status = APITask_GrabLikers(_instaApi);

                        if (status == "Success")
                        {
                            API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "grab_likers");
                            Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish grab_likers");
                            continous_error = 0;
                            goto NextRound;
                        }
                        if (status == "NODATA")
                            no_grablikers = true;
                        //}
                        //catch (Exception ex)
                        //{
                        //    continous_error++;
                        //    throw new System.ArgumentException("GrabLikers: " + ex.Message + "Stacktrace:" + ex.StackTrace);
                        //}
                    }
                    //Grab Followers

                    status = null;


                    if (Program.form.is_APITask_GrabFollower && !no_grabfollowers)
                    {
                        try
                        {
                            status = APITask_GrabFollower(_instaApi);

                            if (status == "Success")
                            {
                                API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "grab_follower");
                                Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish grab_follower");
                                continous_error = 0;
                                goto NextRound;
                            }
                            if (status == "NODATA")
                                no_grabfollowers = true;
                        }
                        catch (Exception ex)
                        {
                            continous_error++;
                            throw new System.ArgumentException("GrabFollower: " + ex.Message + "Stacktrace:" + ex.StackTrace);
                        }
                    }

                    if (Program.form.is_APITask_GrabUserFullInfo_RESCRAPE)
                    {
                        try
                        {
                            status = APITask_GrabUserFullInfo_ReScrape(_instaApi);

                            if (status == "Success")
                            {
                                API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "scrape_full_info");
                                Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish scrape_full_info RESCRAPE");
                                continous_error = 0;
                                goto NextRound;
                            }
                        }
                        catch (Exception ex)
                        {
                            continous_error++;
                            if (ex.Message == "Timeout")
                            {
                                ThreadHelper.HandleException("GrabUserFullInfo: " + ex.Message, ex.StackTrace);
                                Thread.Sleep(2000);
                                continue;
                            }
                            throw new System.ArgumentException("GrabUserFullInfo: " + ex.Message + "Stacktrace:" + ex.StackTrace);
                        }
                    }

                    //GrabUserFullInfo

                    if (Program.form.is_APITask_GrabUserFullInfo)
                    {
                        try
                        {
                            status = APITask_GrabUserFullInfo(_instaApi);

                            if (status == "Success")
                            {
                                API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "scrape_full_info");
                                Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish scrape_full_info");
                                continous_error = 0;
                                goto NextRound;
                            }
                        }
                        catch (Exception ex)
                        {
                            continous_error++;
                            if (ex.Message == "Timeout")
                            {
                                ThreadHelper.HandleException("GrabUserFullInfo: " + ex.Message, ex.StackTrace);
                                Thread.Sleep(2000);
                                continue;
                            }
                            throw new System.ArgumentException("GrabUserFullInfo: " + ex.Message + "Stacktrace:" + ex.StackTrace);
                        }
                    }




                NextRound:

                    IGAPIHelper.Scraper.Delay(2500, 5000);
                }

                //Release account and proxy


            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("challenge_required"))
                {
                    IGAPIHelper.Scraper.UpdateScrapeAccount(LocalHelper.GetDynamicProperty(dyn.account, "username"), "state", "CHALLENGE", API.DBHelpers.db_connection_igtoolkit);
                    ThreadHelper.HandleException("challenge_required|" + LocalHelper.GetDynamicProperty(dyn.account, "username"), ex.StackTrace);
                    throw new System.ArgumentException("CHALLENGE|Tested FAILED");
                }
                else
                {
                    ThreadHelper.HandleException(ex.Message, ex.StackTrace);
                }
            }
            finally
            {
                IGAPIHelper.Scraper.ReleaseScrapeAccount(GetObjectValue(dyn.account, "username"), API.DBHelpers.db_connection_igtoolkit);
                Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish APITask - Release Scrape Account");

                //PLAutoHelper.ProxyHelper.TMProxy_DB_ReleaseProxy(proxy, new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                PLAutoHelper.ProxyHelper.XProxy_DB_ReleaseProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), proxy);
                Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish APITask - Release Proxy");

                //Write session time
                Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Finished round in " + watch_scrape.Elapsed.TotalMinutes.ToString());
            }
        }


        private string APITask_GrabUserFullInfo(InstagramApiSharp.API.IInstaApi _instaApi)
        {
            //Get user to scrape
            Stopwatch watch_take_data = new Stopwatch();
            watch_take_data.Start();

            var account = AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabUserFullInfo();

            Console.WriteLine("Take data to grab user full info: " + watch_take_data.Elapsed.TotalSeconds.ToString() + " seconds");

            watch_take_data.Restart();

            if (account == null)
                return "NODATA";

            string pk = account.pk;

            string status = null;

            dynamic user_info = null;

            if (pk != "")
                user_info = IGAPIHelper.Scraper.ScrapeFullUserInfo(pk, _instaApi);
            else
                user_info = IGAPIHelper.Scraper.ScrapeFullUserInfo_ByUsername(account.username, _instaApi);

            Console.WriteLine("Time to scrape full user info by API: " + watch_take_data.Elapsed.TotalSeconds.ToString() + " seconds");


            if (GetObjectValue(user_info, "status") == "Success")
            {
                status = "";
                pk = LocalHelper.GetDynamicProperty(GetObjectValue(user_info, "user"), "Pk");
            }
            else
            {
                status = GetObjectValue(user_info, "status");
                if (status == "NotFoundUser")
                {
                    status = GetObjectValue(user_info, "status");
                    goto UpdateStatus;
                }
                if (status.Contains("Oops, an error occurred"))
                {
                    goto UpdateStatus;
                }
                throw new System.ArgumentException(GetObjectValue(user_info, "status"));
            }

            var user_details = GetObjectValue(user_info, "user");

            API.DBHelpers.UpdateUserDetails(account.username, user_details);

        //Update update_at and send to instagramdb

        UpdateStatus:
            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();


                var command_query =
                @"update scrapeusers set [update_at]=getdate(),[state]='READY', [status]='" + status + "' where [UserName]='" + account.username + "' ";



                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = account.username;
                    command.ExecuteNonQuery();
                }

            }




            //API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "scrape_error_full_info");

            return "Success";
        }

        /// <summary>
        /// Just like APITask_GrabUserFullInfo but rescrape old data to do re-engage action like customer care
        /// </summary>
        /// <returns></returns>
        private string APITask_GrabUserFullInfo_ReScrape(InstagramApiSharp.API.IInstaApi _instaApi)
        {
            //Get user to scrape
            Stopwatch watch_take_data = new Stopwatch();
            watch_take_data.Start();

            var account = AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabUserFullInfo_ReScrape();

            Console.WriteLine("Take data to grab user full info: RESCRAPE " + watch_take_data.Elapsed.TotalSeconds.ToString() + " seconds");

            watch_take_data.Restart();

            if (account == null)
                return "NODATA";

            string pk = account.pk;

            string status = null;

            dynamic user_info = null;

            if (pk != "")
                user_info = IGAPIHelper.Scraper.ScrapeFullUserInfo(pk, _instaApi);
            else
                user_info = IGAPIHelper.Scraper.ScrapeFullUserInfo_ByUsername(account.username, _instaApi);

            Console.WriteLine("Time to scrape full user info by API: RESCRAPE " + watch_take_data.Elapsed.TotalSeconds.ToString() + " seconds");


            if (GetObjectValue(user_info, "status") == "Success")
            {
                status = "";
                pk = LocalHelper.GetDynamicProperty(GetObjectValue(user_info, "user"), "Pk");
            }
            else
            {
                status = GetObjectValue(user_info, "status");
                if (status == "NotFoundUser")
                {
                    status = GetObjectValue(user_info, "status");
                    goto UpdateStatus;
                }
                if (status.Contains("Oops, an error occurred"))
                {
                    goto UpdateStatus;
                }
                throw new System.ArgumentException(GetObjectValue(user_info, "status"));
            }

            var user_details = GetObjectValue(user_info, "user");

            API.DBHelpers.UpdateUserDetails(account.username, user_details);

        //Update update_at and send to instagramdb

        UpdateStatus:
            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                PLAutoHelper.SQLHelper.Execute(() =>
                {
                    using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                    {

                        var command_query =
                        @"update scrapeusers set [update_at]=getdate(), [status]='" + status + "' where [UserName]='" + account.username + "' ";



                        using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                        {
                            command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = account.username;
                            command.ExecuteNonQuery();
                        }

                        sqlTransaction.Commit();

                    }
                }/*, "scrapeusers"*/);
            }




            //API.DBHelpers.AddAPICallLog(GetObjectValue(dyn.account, "username"), "scrape_error_full_info");

            return "Success";
        }

        private string APITask_GrabPost(InstagramApiSharp.API.IInstaApi _instaApi)
        {
            //Get target account
            var account = AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabPost();

            if (account != null)
            {
                var dyn_medias = GrabFromAccount(_instaApi, GetObjectValue(account, "username"));

                if (dyn_medias.Count == 0)
                {
                    return "NOPOST";
                }

                //Add to db without manual checking
                API.DBHelpers.AddPostToDB(dyn_medias, GetObjectValue(account, "scrape_type"), GetObjectValue(account, "username"), true);

                return "Success";
            }
            else
                return "NODATA";
            //else
            //{
            //    //If target account is null, get hashtag

            //    var hashtag = API.DBHelpers.GetHashtags();

            //    if (hashtag == null)
            //        return;

            //    var dyn_medias = GrabFromHashtag(_instaApi, hashtag);

            //    //Add to db with manual checking required
            //    API.DBHelpers.AddPostToDB(dyn_medias, null, null, false, true);
            //}

        }


        private List<dynamic> GrabFromAccount(InstagramApiSharp.API.IInstaApi _instaApi, string account_user)
        {
            var medias_result = IGAPIHelper.Scraper.ScrapeMediasFromUser(_instaApi, account_user, 1);

            List<dynamic> dyn_medias = new List<dynamic>();

            if (medias_result.Succeeded == false)
                return dyn_medias;

            var medias = medias_result.Value;

            foreach (var item in medias)
            {
                dyn_medias.Add(new
                {
                    url = "https://www.instagram.com/p/" + item.Code + "/",
                    date = item.TakenAt.ToString()
                });
            }

            return dyn_medias;
        }


        private string APITask_GrabCommenters(InstagramApiSharp.API.IInstaApi _instaApi)
        {
            //Get target posts

            var post = AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabCommenters(Settings.settings_commenters_x_days);

            if (post == null)
                return "NODATA";


            var engagers = new List<InstagramApiSharp.Classes.Models.InstaUserShort>() { };

            //get nextMinId if available

            string pre_nextMinId = ThreadHelper.GrabCommenters_PreNextMinId(post.posturl);

            IResult<InstagramApiSharp.Classes.Models.InstaCommentList> comments_result = IGAPIHelper.Scraper.ScrapeCommenters(post.posturl, _instaApi, Settings.settings_commenters_max_page_loads, pre_nextMinId);

            var commenters = comments_result.Value.Comments.Select(c => c.User).ToList<InstagramApiSharp.Classes.Models.InstaUserShort>();

            List<string> remove_duplicate_commenters_user = new List<string>();

            var remove_dup_commenters = new List<InstagramApiSharp.Classes.Models.InstaUserShort>();


            foreach (var item in commenters)
            {
                if (!remove_duplicate_commenters_user.Contains(item.UserName))
                {
                    remove_dup_commenters.Add(item);
                    remove_duplicate_commenters_user.Add(item.UserName);
                }
            }

            var pre_commenters = IGThreads.ThreadHelper.GrabCommenters_PreCommentersOfPost(post.posturl);

            remove_dup_commenters.RemoveAll(item => pre_commenters.Contains(item.UserName));

            engagers.AddRange(remove_dup_commenters);


            //foreach (var item in engagers)
            //{
            //    API.DBHelpers.AddScrapeUserToDB(item, "commenter", post.from_account);
            //}

            API.DBHelpers.AddScrapeUserToDB_ByList(engagers, "commenter", post.from_account);

            //add log commenters_str

            string commenters_str = "";

            foreach (var item in remove_dup_commenters)
            {
                commenters_str += item.UserName + "\r\n";
            }

            var command_query = "insert into actionlog (log_name,log_account,[value]) values ('scrape_commenter_pre',@post_url,@value)";

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@post_url", System.Data.SqlDbType.VarChar).Value = post.posturl;
                    command.Parameters.Add("@value", System.Data.SqlDbType.VarChar).Value = commenters_str;

                    command.ExecuteNonQuery();
                }
            }

            //add log nextminId
            string nextMinId = "";
            if (comments_result.Value.NextMinId != null)
            {
                nextMinId = comments_result.Value.NextMinId;
            }

            ThreadHelper.GrabCommenters_AddNextMinId(post.posturl, nextMinId);

            return "Success";
        }

        private string APITask_GrabFollower(InstagramApiSharp.API.IInstaApi _instaApi)
        {
            string username = AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabFollower();

            var ig_url = "https://www.instagram.com/" + username + "/";

            if (username == null)
            {
                //Thread.Sleep(TimeSpan.FromMinutes(5));
                return "NODATA";
            }

            var scrape_user_info = IGAPIHelper.Scraper.ScrapeFullUserInfo_ByUsername(username, _instaApi);

            if (LocalHelper.GetDynamicProperty(scrape_user_info, "status").ToString() == "NotFoundUser")
            {
                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    string command_query =
                        @"update target_accounts
                        set
	                        [state]='DISABLE'
                        where
	                        [ig_url] like '%/" + username + "/%'";
                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                return "Success";
            }

            //get current followers count
            var user_info = LocalHelper.GetDynamicProperty(scrape_user_info, "user");

            var current_followers = int.Parse(LocalHelper.GetDynamicProperty(user_info, "FollowerCount").ToString());

            var pre_followers_count = ThreadHelper.GrabFollower_PreFollowerCountOfUser(ig_url);

            var max_scrape_followers = Settings.settings_grab_followers_max_scrape; //max scrape per time

            if (current_followers > 0)
            {
                max_scrape_followers = (current_followers - pre_followers_count >= max_scrape_followers) ? max_scrape_followers : (current_followers - pre_followers_count);
            }

            var load_pages = max_scrape_followers / 100;

            if (load_pages == 0)
                return "Success";

            //get follower
            var followers = IGAPIHelper.Scraper.ScrapeFollowerByUsername(username, _instaApi, load_pages);

            //get pre_followers

            var pre_followers = ThreadHelper.GrabFollower_PreFollowersOfUser(ig_url);

            //except old followers
            followers.RemoveAll(f => pre_followers.Contains(f.UserName));

            //add account to db
            //foreach (var item in followers)
            //{
            //    API.DBHelpers.AddScrapeUserToDB(item, "follower", username);
            //}
            API.DBHelpers.AddScrapeUserToDB_ByList(followers, "follower", username);

            //Add follower_str log
            string followers_str = "";

            foreach (var item in followers)
            {
                followers_str += item.UserName + "\r\n";
            }


            //add current_followers count log 
            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();


                var command_query = "insert into actionlog (log_name,log_account,[value]) values ('scrape_follower_pre',@ig_url,@value)";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@ig_url", System.Data.SqlDbType.VarChar).Value = ig_url;
                    command.Parameters.Add("@value", System.Data.SqlDbType.VarChar).Value = followers_str;
                    command.ExecuteNonQuery();
                }

                command_query = "insert into actionlog (log_name,log_account,[value]) values ('followers_count',@ig_url,@follower_count)";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@ig_url", System.Data.SqlDbType.VarChar).Value = ig_url;
                    command.Parameters.Add("@follower_count", System.Data.SqlDbType.Int).Value = current_followers;
                    command.ExecuteNonQuery();
                }
            }
            return "Success";
        }

        private string APITask_GrabLikers(InstagramApiSharp.API.IInstaApi _instaApi)
        {
            ///FIX GET CURRENT LIKERS AND PRE LIKERS COUNT TO CALCULATE TO SCRAPE HOW MANY TIMES - skip vi max mỗi lan scrap chi duoc 1k items va k the thay doi so luong

            var post = AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabLiker(Settings.settings_likers_x_days);

            if (post == null)
            {
                return "NODATA";
            }

            var post_url = post.posturl;

            //get likers
            List<InstagramApiSharp.Classes.Models.InstaUserShort> likers_instausershort = IGAPIHelper.Scraper.ScrapeLikers(post_url, _instaApi);

            //get pre likers of this post in database

            var pre_likers = IGThreads.ThreadHelper.GrabLiker_PreLikersOfPost(post_url);

            //current_likers.RemoveAll(item => pre_likers.Contains(item));


            likers_instausershort.RemoveAll(item => pre_likers.Contains(item.UserName));

            //foreach (var item in likers_instausershort)
            //{
            //    API.DBHelpers.AddScrapeUserToDB(item, "liker", post.from_account);
            //}
            API.DBHelpers.AddScrapeUserToDB_ByList(likers_instausershort, "liker", post.from_account);

            //add log

            string likers_str = "";

            foreach (var item in likers_instausershort)
            {
                likers_str += item.UserName + "\r\n";
            }

            var command_query = "insert into actionlog (log_name,log_account,[value]) values ('scrape_liker_pre',@post_url,@value)";

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@post_url", System.Data.SqlDbType.VarChar).Value = post_url;
                    command.Parameters.Add("@value", System.Data.SqlDbType.VarChar).Value = likers_str;

                    command.ExecuteNonQuery();
                }
            }

            return "Success";
        }

        private string APITask_GrabPost_ForComment(InstagramApiSharp.API.IInstaApi _instaApi)
        {
            //Get target account
            var account = AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabPost_ForComment();

            if (account != null)
            {
                var dyn_medias = GrabFromAccount(_instaApi, GetObjectValue(account, "username"));

                if (dyn_medias.Count == 0)
                {
                    throw new System.ArgumentException("NoPostError");
                }

                //Add to db without manual checking
                API.DBHelpers.AddCommentPostToDB(dyn_medias);

                return "Success";
            }
            else
                return "NODATA";
        }

        /// <summary>
        /// grab to check who is following back
        /// </summary>
        private string APITask_GrabFollowerBack(InstagramApiSharp.API.IInstaApi _instaApi)
        {
            //take account need check follower back
            dynamic account = AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabFollowerBack();

            if (account != null)
            {
                string username = account.user;

                List<string> followback_account = new List<string>();

                //grab follower
                var load_pages = Settings.settings_grab_followback_loadspage;

                //get follower
                var followers = IGAPIHelper.Scraper.ScrapeFollowerByUsername(username, _instaApi, load_pages);

                if (followers == null)
                    return "Success";

                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_instagram))
                {
                    conn.Open();

                    //get list target_account following_task by accountid

                    string command_query =
                        @"select [target_account]
                            from actionlog
                            where
	                            [accountid]=@accountid and
	                            [action]='following_task'";

                    List<string> target_accounts_following_task = new List<string>();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@accountid", System.Data.SqlDbType.Int).Value = account.id;

                        using (var dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                target_accounts_following_task.Add(dataReader["target_account"].ToString());
                            }
                        }
                    }


                    foreach (var item in followers)
                    {
                        string target_account = item.UserName;

                        ////check every follower to see if following by account in database
                        //command_query =
                        //    @"select count(*)
                        //    from actionlog
                        //    where
                        //     [accountid]=@accountid and
                        //     [action]='following_task' and
                        //     [target_account]=@target_account";


                        //using (var command = new SqlCommand(command_query, conn))
                        //{
                        //    command.Parameters.Add("@accountid", System.Data.SqlDbType.Int).Value = account.id;
                        //    command.Parameters.Add("@target_account", System.Data.SqlDbType.VarChar).Value = target_account;
                        //    var count = int.Parse(command.ExecuteScalar().ToString());

                        //    if (count > 0)
                        //    {
                        //        followback_account.Add(target_account);
                        //    }
                        //}
                        if (target_accounts_following_task.Contains(target_account))
                        {
                            followback_account.Add(target_account);
                        }
                    }
                }

                //update FollowBack property
                if (followback_account.Count > 0)
                {
                    using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                    {
                        conn.Open();

                        foreach (var item in followback_account)
                        {
                            string command_query =
                                @"update scrapeusers
                                set [followback]='yes'
                                where
	                                [UserName]=@UserName";

                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = item;
                                command.ExecuteNonQuery();
                            }
                        }
                    }

                    //add to actionlog of instagramdb

                    using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_instagram))
                    {
                        conn.Open();

                        //get list followback from actionlog

                        List<string> already_add_followback = new List<string>();

                        string command_query =
                            @"select [target_account]
                              from actionlog
                              where
	                            [action]='followback' and [accountid]=@accountId";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@accountId", System.Data.SqlDbType.VarChar).Value = account.id;

                            using (var dataReader = command.ExecuteReader())
                            {
                                while(dataReader.Read())
                                {
                                    already_add_followback.Add(dataReader["target_account"].ToString());
                                }
                            }
                        }

                        foreach (var item in followback_account)
                        {
                            //check if exists in actionlog, if not add
                            //string command_query =
                            //    @"select count(*)
                            //    from actionlog
                            //    where
                            //     [action]='followback' and [target_account]=@target_account and [accountid]=@accountId";

                            //using (var command = new SqlCommand(command_query, conn))
                            //{
                            //    command.Parameters.Add("@accountId", System.Data.SqlDbType.VarChar).Value = account.id;
                            //    command.Parameters.Add("@target_account", System.Data.SqlDbType.VarChar).Value = item;

                            //    int count = int.Parse(command.ExecuteScalar().ToString());

                            //    if (count > 0)
                            //        continue;
                            //}

                            if (!already_add_followback.Contains(item))
                            {

                                command_query =
                                    @"insert into actionlog (accountid,[action],[target_account]) values (@accountid,'followback',@target_account)";

                                using (var command = new SqlCommand(command_query, conn))
                                {
                                    command.Parameters.Add("@accountid", System.Data.SqlDbType.VarChar).Value = account.id;
                                    command.Parameters.Add("@target_account", System.Data.SqlDbType.VarChar).Value = item;
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                }

                return "Success";
            }
            else
                return "NODATA";
        }

        private string APITask_GrabFollower_ForMotherAccount(InstagramApiSharp.API.IInstaApi _instaApi)
        {
            //take mother account need to check followback 
            string account = AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabFollower_ForMother();


            //check and add to actionlog with action='followback_mother'
            if (account != null)
            {
                string username = account;

                List<string> followback_account = new List<string>();

                //grab follower
                var load_pages = Settings.settings_grab_followback_loadspage;

                //get follower
                var followers = IGAPIHelper.Scraper.ScrapeFollowerByUsername(username, _instaApi, load_pages);
                
                //update FollowBack property
                if (followers.Count > 0)
                {
                    //add to actionlog of instagramdb

                    using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_instagram))
                    {
                        conn.Open();


                        //get list follow_mother from actionlog

                        List<string> already_add_follow_mother = new List<string>();

                        string command_query =
                            @"select [target_account]
                                from actionlog
                                where
	                                [action]='follow_mother' and [value]=@value";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@value", System.Data.SqlDbType.VarChar).Value = account;

                            using (var dataReader = command.ExecuteReader())
                            {
                                while (dataReader.Read())
                                {
                                    already_add_follow_mother.Add(dataReader["target_account"].ToString());
                                }
                            }
                        }

                        foreach (var item in followers)
                        {
                            ////check if exists in actionlog, if not add
                            //command_query =
                            //    @"select count(*)
                            //    from actionlog
                            //    where
                            //     [action]='follow_mother' and [target_account]=@target_account and [value]=@value";

                            //using (var command = new SqlCommand(command_query, conn))
                            //{
                            //    command.Parameters.Add("@target_account", System.Data.SqlDbType.VarChar).Value = item.UserName;
                            //    command.Parameters.Add("@value", System.Data.SqlDbType.VarChar).Value = account;

                            //    int count = int.Parse(command.ExecuteScalar().ToString());

                            //    if (count > 0)
                            //        continue;
                            //}

                            if (!already_add_follow_mother.Contains(item.UserName))
                            {

                                command_query =
                                    @"insert into actionlog (accountid,[action],[target_account], [value]) values ('155','follow_mother',@target_account, @value)";

                                using (var command = new SqlCommand(command_query, conn))
                                {
                                    command.Parameters.Add("@value", System.Data.SqlDbType.VarChar).Value = account;
                                    command.Parameters.Add("@target_account", System.Data.SqlDbType.VarChar).Value = item.UserName;
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                }

                return "Success";
            }
            else
                return "NODATA";

            return null;
        }



        private string APITask_GrabPost_ForComment_FromScrapeUsers(InstagramApiSharp.API.IInstaApi _instaApi)
        {
            //neu available data in table scrapeuser_comment_posts <100 => else return "NODATA"
            using (SqlConnection conn_igtoolkit = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn_igtoolkit.Open();

                string command_query =
                    @"select count(*)
                    from scrapeuser_comment_posts
                    where
	                    ([status] is null or [status]='')";

                using(var command=new SqlCommand(command_query,conn_igtoolkit))
                {
                    int count = int.Parse(command.ExecuteScalar().ToString());

                    if (count > 100)
                        return "NODATA";
                }
            }

            //take suitable account to grab latest post
            string account = AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabPost_ForComment_FromScrapeUsers(true);

            //grab latest post
            if (account != null)
            {

                InstagramApiSharp.Classes.Models.InstaMediaList medias = new InstagramApiSharp.Classes.Models.InstaMediaList();

                int retry = 0;
            ScrapeMediasFromUser:
                IResult<InstagramApiSharp.Classes.Models.InstaMediaList> medias_result = IGAPIHelper.Scraper.ScrapeMediasFromUser(_instaApi, account);


                if (medias_result.Succeeded == false)
                {
                    //check if user is set to private

                    if (medias_result.Info.Message == "Not authorized to view user")
                    {
                        //set private => update to scrapeusers igtoolkit
                        using (SqlConnection conn_igtoolkit = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                        {
                            conn_igtoolkit.Open();

                            PLAutoHelper.SQLHelper.Execute(() =>
                            {
                                using (SqlTransaction sqlTransaction = conn_igtoolkit.BeginTransaction())
                                {
                                    using (var command = new SqlCommand("update scrapeusers set [IsPrivate]='True', [status]='' where [UserName]=@UserName", conn_igtoolkit, sqlTransaction))
                                    {
                                        command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = account;
                                        command.ExecuteNonQuery();
                                    }

                                    sqlTransaction.Commit();
                                }
                            }/*, "scrapeusers"*/);
                        }


                        goto ReturnSuccess;
                    }

                    //check if user is remove
                    var userinfo = IGAPIHelper.Scraper.ScrapeFullUserInfo_ByUsername(account, _instaApi);

                    var userinfo_status = PLAutoHelper.NormalFuncHelper.GetDynamicProperty(userinfo, "status");

                    if (userinfo_status == "NotFoundUser")
                    {
                        //update state ERROR
                        using (SqlConnection conn_igtoolkit = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                        {
                            conn_igtoolkit.Open();

                            PLAutoHelper.SQLHelper.Execute(() =>
                            {
                                using (SqlTransaction sqlTransaction = conn_igtoolkit.BeginTransaction())
                                {
                                    using (var command = new SqlCommand("update scrapeusers set [state]='ERROR', [status]='' where [UserName]=@UserName", conn_igtoolkit, sqlTransaction))
                                    {
                                        command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = account;
                                        command.ExecuteNonQuery();
                                    }

                                    sqlTransaction.Commit();
                                }
                            }/*, "scrapeusers"*/);
                        }

                        goto ReturnSuccess;
                    }
                    else
                    {
                        retry++;
                        if (retry < 3)
                        {
                            Thread.Sleep(2000);
                            goto ScrapeMediasFromUser;
                        }
                        else
                        {
                            throw new System.ArgumentException("UnknownError|" + userinfo_status);
                        }
                    }
                }
                else
                {
                    medias = medias_result.Value;

                    if (medias.Count == 0)
                    {
                        //throw new System.ArgumentException("UnknownWhyMediasIsEmpty");

                        //update state ERROR
                        using (SqlConnection conn_igtoolkit = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                        {
                            conn_igtoolkit.Open();

                            PLAutoHelper.SQLHelper.Execute(() =>
                            {
                                using (SqlTransaction sqlTransaction = conn_igtoolkit.BeginTransaction())
                                {
                                    using (var command = new SqlCommand("update scrapeusers set MediaCount=0 where [UserName]=@UserName", conn_igtoolkit, sqlTransaction))
                                    {
                                        command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = account;
                                        command.ExecuteNonQuery();
                                    }

                                    sqlTransaction.Commit();
                                }
                            }/*, "scrapeusers"*/);
                        }

                        goto ReturnSuccess;
                    }

                    //get latest post link
                    var posturl = "https://www.instagram.com/p/" + medias[0].Code + "/";

                    using (SqlConnection conn_igtoolkit = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                    {
                        conn_igtoolkit.Open();

                        PLAutoHelper.SQLHelper.Execute(() =>
                        {
                            using (SqlTransaction sqlTransaction = conn_igtoolkit.BeginTransaction())
                            {
                                using (var command = new SqlCommand("insert into scrapeuser_comment_posts (posturl,creation_date,from_account) values (@posturl,@creation_date,@from_account)", conn_igtoolkit, sqlTransaction))
                                {
                                    command.Parameters.Add("@posturl", System.Data.SqlDbType.VarChar).Value = posturl;

                                    command.Parameters.Add("@creation_date", System.Data.SqlDbType.DateTime).Value = medias[0].TakenAt.ToString();

                                    command.Parameters.Add("@from_account", System.Data.SqlDbType.VarChar).Value = account;
                                    command.ExecuteNonQuery();
                                }

                                sqlTransaction.Commit();
                            }
                        }/*, "scrapeusers"*/);
                    }

                    //data.posturl = posturl;
                    //data.username = f;

                    //add to comment_posts
                }
            ReturnSuccess:

                using (SqlConnection conn_igtoolkit = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn_igtoolkit.Open();

                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (SqlTransaction sqlTransaction = conn_igtoolkit.BeginTransaction())
                        {
                            using (var command = new SqlCommand("update scrapeusers set [status]='' where [UserName]=@UserName", conn_igtoolkit, sqlTransaction))
                            {
                                command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = account;
                                command.ExecuteNonQuery();
                            }

                            sqlTransaction.Commit();
                        }
                    }/*, "scrapeusers"*/);
                }

                return "Success";
            }
            else
                return "NODATA";
        }

        private string APITask_GrabComment_ForSlave(InstagramApiSharp.API.IInstaApi _instaApi, string scraper_user)
        {
            //take slave account need to grab comment
            var choiced_account = AIOHelper.DBLogic.TakeDATAToGrab.DATA_APITask_GrabComment_ForSlave(true);

            if(choiced_account==null)
            {
                return "NODATA";
            }

            //grab comment of latest 12 posts

            //get posts
            int retry = 0;
        GetMedias:
            var medias = IGAPIHelper.Scraper.ScrapeMediasFromUser(_instaApi, choiced_account).Value;

            if (medias == null)
            {
                retry++;
                if (retry > 3)
                    throw new System.ArgumentException("UnknownError");
                Thread.Sleep(2000);
                goto GetMedias;
            }

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_instagram))
            {
                conn.Open();

                string command_query =
                    @"select [id]
                    from ig_account
                    where
	                    [user]=@user";

                string accountId = null;

                using(var command=new SqlCommand(command_query,conn))
                {
                    command.Parameters.Add("@user", System.Data.SqlDbType.VarChar).Value = choiced_account;
                    accountId = command.ExecuteScalar().ToString();

                }

                foreach (var item in medias)
                {
                    var comments = IGAPIHelper.Scraper.ScrapeCommenters("https://www.instagram.com/p/" + item.Code + "/", _instaApi).Value;

                    API.DBHelpers.AddAPICallLog(scraper_user, "grab_commentposts");

                    if (comments != null)
                    {
                        foreach (var comment in comments.Comments)
                        {
                            var user = comment.User.UserName;
                            if (user == choiced_account)
                                continue;
                            var post_url = "https://www.instagram.com/p/" + item.Code + "/";
                            var comment_pk = comment.Pk;
                            var comment_text = comment.Text;
                            var comment_date = comment.CreatedAt;
                            var scraped_at = DateTime.Now;

                            try
                            {
                                using (var command = new SqlCommand("insert into slaves_comment (accountId,slave_user,post_url,comment_pk,comment_user,comment_text,comment_at) values (" + accountId + ",'" + choiced_account + "','" + post_url + "','" + comment_pk + "',@comment_user,@comment_text,'" + comment_date.ToString() + "')", conn))
                                {
                                    command.Parameters.Add("@comment_text", System.Data.SqlDbType.NVarChar).Value = comment_text;
                                    command.Parameters.Add("@comment_user", System.Data.SqlDbType.VarChar).Value = user;
                                    command.ExecuteNonQuery();
                                }
                            }
                            catch { }

                        }
                    }


                    IGAPIHelper.Scraper.Delay(1500, 3000);
                }
            }

            return "Success";

        }

        private void APITask_GrabMSDetails(InstagramApiSharp.API.IInstaApi _instaApi, string user)
        {
            //get account need take details

            var account_detail = IGAPIHelper.Scraper.ScrapeFullUserInfo_ByUsername(user, _instaApi);

            //add information to actionlog

            var command_query = "insert into actionlog (log_name,log_account,[value]) values ('scrape_details',@user,@value)";

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@post_url", System.Data.SqlDbType.VarChar).Value = user;
                    command.Parameters.Add("@value", System.Data.SqlDbType.VarChar).Value = Newtonsoft.Json.JsonConvert.SerializeObject(account_detail);

                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region --BrowserTask--

        private void BrowserTask()
        {
            Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do BrowserTask");
            //get account
            dynamic account = IGAPIHelper.Scraper.GetAccountToScrape_Browser(API.DBHelpers.db_connection_igtoolkit);

            //launch igffpro
            var igffpro = new IGClass.IGFFPro(account, "RESIDENT");

            var hwnd = igffpro.firefox_hwnd;

            InstagramApiSharp.API.IInstaApi _instaApi = Login(account, igffpro.fFProxy.proxy_str, API.DBHelpers.db_connection_igtoolkit);

            var time_session = int.Parse(API.DBHelpers.GetValueFromAccountTb(GetObjectValue(account, "username"), "time_per_session"));

            API.DBHelpers.AddAPICallLog(GetObjectValue(account, "username"), "new_session");

            Stopwatch watch_scrape = new Stopwatch();
            watch_scrape.Start();

            while (true)
            {
                if (Program.form.isstop)
                    break;

                if (watch_scrape.Elapsed.TotalMinutes > time_session)
                    break;

                //get post_url

                var post = ThreadHelper.DATA_GrabLiker(10);

                var post_url = post.posturl;


                Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do BrowserTask - Grab like for " + post_url);

                //get current likes quantity
                var current_likes = ThreadHelper.GrabLiker_CurrentLikeOfPost(_instaApi, post_url);

                //get previous likes quantity (số likes ở lần scrape gần nhất) - đã save vào database
                var pre_likes = ThreadHelper.GrabLiker_PreLikeOfPost(post_url);

                ///max likers to scrape in this run
                var max_likers = 200;

                if (pre_likes > 0 && (current_likes - pre_likes < max_likers))
                    max_likers = current_likes - pre_likes;

                //Open post
                PLAutoHelper.RPAFirefox.OpenUrlInCurrentTab(hwnd, post_url); Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do BrowserTask - Open post in browser");

                //Click liker and wait popup

                PLAutoHelper.RPA.ClickandWait(BMPSource.Likers_likes_stats, BMPSource.Likers_Likes_Title, hwnd, true); Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do BrowserTask - Hit likers");


                List<string> users = new List<string>();

                int pre_scrape_count = 0;//so account grab duoc o vong lap truoc

                while (true)
                {
                GenerateLogName:
                    var log_name = "log_" + LocalHelper.RandomGUID(5) + ".txt"; Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do BrowserTask - Grabbing and save to " + log_name);

                    var log_path = Path.GetFullPath(Settings.firefox_download_locate) + "\\" + log_name;

                    if (File.Exists(log_name))
                        goto GenerateLogName;

                    PLAutoHelper.RPAFirefox.OpenNewTabUrl(hwnd, "file:///F:/DOWNLOAD/ui.visionma.html?direct=1&macro=IG-Liker-1&savelog=" + log_name);

                    Stopwatch watch_scrape_likers = new Stopwatch();
                    watch_scrape_likers.Start();

                    while (true)
                    {
                        if (File.Exists(log_path))
                            break;

                        if (watch_scrape_likers.Elapsed.TotalSeconds > 15)
                        {
                            throw new System.ArgumentException("Uivision Scrape Likers Error");
                        }

                        Thread.Sleep(1000);
                    }

                    string log = "";

                    while (log == "")
                    {
                        try
                        {
                            log = File.ReadAllText(log_path);
                        }
                        catch { }
                        Thread.Sleep(500);
                    }

                    var start_pattern = "[echo] minad_start\n[info] Executing:  | echo | ${likers_element} |  | \n[echo] ";

                    var start = log.IndexOf(start_pattern);


                    var stop = log.IndexOf("\n[info] Executing:  | echo | minad_end |  | ");

                    var html = log.Substring(start + start_pattern.Length, stop - (start + start_pattern.Length));

                    //extract user from html
                    var htmlDoc_load = new HtmlAgilityPack.HtmlDocument();
                    htmlDoc_load.LoadHtml(html);

                    var user_nodes = htmlDoc_load.DocumentNode.SelectNodes("//a[@class='FPmhX notranslate MBL3Z']");

                    foreach (var item in user_nodes)
                    {
                        users.Add(item.InnerText);
                    }

                    users = users.Distinct().ToList();

                    if (users.Count >= max_likers)
                        break;

                    if (users.Count == pre_scrape_count)
                        break;

                    pre_scrape_count = users.Count;

                    ScrollUpLikerBox(hwnd);

                    Thread.Sleep(200);
                }

                //add to db

                foreach (var item in users)
                {
                    API.DBHelpers.AddScrapeUsersToDB_ByUsername(item, "liker", post.from_account);
                }

                //add current_likes to db

                var command_query = "insert into actionlog (log_name,log_account,[value]) values ('likers_count',@post_url,@value)";

                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@post_url", System.Data.SqlDbType.VarChar).Value = post_url;
                        command.Parameters.Add("@value", System.Data.SqlDbType.VarChar).Value = current_likes.ToString();

                        command.ExecuteNonQuery();
                    }
                }

                Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do BrowserTask - Finish grab likers for " + post_url);
            }

            Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do BrowserTask - Finish session - Dispoing IGFFPro");

            igffpro.Dispose();
        }

        private void ScrollUpLikerBox(IntPtr hwnd)
        {
            var points = PLAutoHelper.RPA.FindOutPoints(BMPSource.Likers_FollowBtn, hwnd);

            var choice_point = points[(new Random(Guid.NewGuid().GetHashCode())).Next(points.Count)];

            KAutoHelper.AutoControl.SendClickOnPosition(hwnd, choice_point.X - 80, choice_point.Y);

            PLAutoHelper.RPA.SendMouseScrollOnBackground(hwnd, "up", 3, points[0], 0.5, "page");
        }


        #endregion
    }
}
