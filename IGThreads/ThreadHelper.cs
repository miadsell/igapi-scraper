﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.IGThreads
{
    static class ThreadHelper
    {
        #region --DATABASE--

        static public void ClearScrapeUserStatus()
        {
            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                while (true)
                {

                    using (var sqlTrans = conn.BeginTransaction())
                    {
                        try
                        {

                            using (var command = new SqlCommand("update scrapeusers set [status]='' where [status]='Claimed'", conn, sqlTrans))
                            {
                                command.ExecuteNonQuery();
                            }
                            sqlTrans.Commit();
                            break;
                        }
                        catch (Exception ex)
                        { }
                    }

                }

            }
        }

        static public bool IsAvailableAPITask()
        {
            if (Program.form.is_APITask_GrabUserFullInfo_RESCRAPE)
            {
                if (AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabUserFullInfo_ReScrape(false) != null)
                    return true;
            }

            if (Program.form.is_APITask_GrabUserFullInfo)
                if (AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabUserFullInfo(false) != null)
                    return true;

            if (Program.form.is_APITask_GrabPost)
                if (AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabPost(false) != null)
                    return true;

            //if (DATA_GrabCommenters(1, false) != null && Program.form.is_APITask_GrabCommenters)
            //    return true;

            if (Program.form.is_APITask_GrabLikers)
                if (AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabLiker(1, false) != null)
                    return true;

            if (Program.form.is_APITask_GrabFollower)
                if (AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabFollower(false) != null)
                    return true;

            if (Program.form.is_APITask_GrabFollower_ForMother)
                if (AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabFollower_ForMother(false) != null)
                    return true;

            if (Program.form.is_APITask_GrabFollowerBack)
                if (AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabFollowerBack(false) != null)
                    return true;

            if (Program.form.is_APITask_GrabPost_ForComment)
                if (AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabPost_ForComment(false) != null)
                    return true;

            if (Program.form.is_APITask_GrabPost_ForComment_FromScrapeUsers)
                if (AIOHelper.DBLogic.TakeDATAToGrab.DATA_GrabPost_ForComment_FromScrapeUsers(false) != null)
                    return true;

            if (Program.form.is_APITask_GrabComment_ForSlave)
                if (AIOHelper.DBLogic.TakeDATAToGrab.DATA_APITask_GrabComment_ForSlave(false) != null)
                    return true;

            return false;
        }

        static public bool IsAvailableBrowserTask()
        {
            var command_grab_likers = @"select count(*) from posts
                        where
                            [checked] = 'Checked' and
                              [ok] = 'OK' and
                                ([scrape_type] = 'All' or[scrape_type] = 'Likers') and
                                   [posturl] not in (select[log_account]

                                            from actionlog

                                            where

                                                [log_name] = 'scrape_liker' and

                                                DATEDIFF(minute, do_at, getdate()) < 10) and
                                (@recent_days = 0  or DATEDIFF(day, [creation_date], getdate()) < @recent_days";

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand(command_grab_likers, conn))
                {

                    command.Parameters.Add("@recent_days", System.Data.SqlDbType.Int).Value = 5; //recently 5 day

                    var count = int.Parse(command.ExecuteScalar().ToString());
                    if (count > 0)
                        return true;
                }
            }

            return false;
        }

        #region --GrabUserFullInfo--

        /// <summary>
        /// get data need scrape full info
        /// </summary>
        static public dynamic DATA_GrabUserFullInfo(bool is_take_data = true)
        {
            if (Settings.aio_settings.GrabUserFullInfo.data.ToString() == "")
                return DATA_GrabUserFullInfo_Default(is_take_data);
            else
                return ThreadHelperDirectCenter.DATA_GrabUserFullInfo.RunByCenter(is_take_data);
        }

        static private dynamic DATA_GrabUserFullInfo_Default(bool is_take_data)
        {
            dynamic account = null;


            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                while (true)
                {
                    var command_query = @"SELECT top 1 *
                                    FROM scrapeusers
                                    WHERE ([update_at] is null or [update_at] = '')
                                      AND ([status] is null or [status] = '')
                                      AND 'https://www.instagram.com/' +[from_account]+'/' NOT IN
                                        (SELECT [ig_url]
                                         FROM target_accounts
                                         WHERE [state]='PAUSED')
                                    ORDER BY [update_at] DESC";

                    using (var command = new SqlCommand(command_query, conn))
                    {

                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                dataReader.Read();
                                var pk = dataReader["pk"].ToString();
                                var username = dataReader["UserName"].ToString();

                                account = new
                                {
                                    pk,
                                    username
                                };
                            }
                        }
                    }

                    if (account != null && is_take_data)
                    {
                        command_query = "update scrapeusers set [status]='Claimed' where [UserName] = @user and ([status] is null or [status] = '')";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@user", System.Data.SqlDbType.VarChar).Value = account.username;

                            var affected_row = command.ExecuteNonQuery();

                            if (affected_row == 1)
                                break;
                            else
                                account = null;
                        }
                    }
                }
            }


            return account;
        }


        #endregion

        #region --GrabPost--

        /// <summary>
        /// get data to grab post
        /// </summary>
        static public dynamic DATA_GrabPost(bool is_take_data = true)
        {
            dynamic dyn_account = null;
            string id = null;

            lock ("DATA_GrabPost")
            {
                var command_query = @"SELECT top 1 [id],[ig_url],[scrape_type]
                                    FROM target_accounts
                                    WHERE CHECKED= 'Checked'
                                      AND [ok] = 'OK'
                                      AND (DATEDIFF(DAY,[last_scrape], getdate()) > 1
                                           OR last_scrape IS NULL)
                                      AND ([state] is null or [state]='')";

                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    using (var command = new SqlCommand(command_query, conn))
                    {

                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                dataReader.Read();
                                id = dataReader["id"].ToString();
                                var ig_url = dataReader["ig_url"].ToString();
                                var username = ig_url.Replace("https://www.instagram.com/", "").Replace("/", "");
                                var scrape_type = dataReader["scrape_type"].ToString();

                                dyn_account = new { username, scrape_type };
                            }
                        }
                    }

                    if (id != null && is_take_data)
                    {
                        command_query = "update target_accounts set last_scrape=GETDATE() where[id] = @id";
                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }

            return dyn_account;
        }

        #endregion

        #region --GrabCommenters

        static public dynamic DATA_GrabCommenters(int recent_days = 1, bool is_take_data = true)
        {
            dynamic post = null;
            string choiced_postId = null;

            lock ("DATA_GrabCommenters")
            {

                var command_query = @"
                                    SELECT top 1 [posturl],[id],[scrape_type],[from_account]
                                    FROM posts p OUTER apply
                                      (SELECT top 1 *
                                       FROM actionlog
                                       WHERE [log_account]=p.posturl  and [log_name]='scrape_commenter' --get lan scrape gan nhat doi voi url nay

                                       ORDER BY id DESC) AS al
                                    WHERE [checked] = 'Checked'
                                      AND [ok] = 'OK'
                                      AND ([scrape_type] = 'All' or[scrape_type] = 'Commenters')
                                      AND [posturl] not in (select [log_account]
                                      FROM actionlog WHERE [log_name] = 'scrape_commenter'
                                      AND DATEDIFF(MINUTE, do_at, getdate()) < 5) --dieu kien de tranh 2 thread lay cung 1 data

                                      AND (@recent_days = 0
                                           OR DATEDIFF(DAY, [creation_date], getdate()) < @recent_days)
                                      AND 'https://www.instagram.com/' +[from_account]+'/' NOT IN --skip data tu target_account da paused

                                        (SELECT [ig_url]
                                         FROM target_accounts
                                         WHERE [state]='PAUSED')
                                    ORDER BY do_at ASC";

                List<dynamic> temp_posturls = new List<dynamic>();

                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@recent_days", System.Data.SqlDbType.Int).Value = recent_days;

                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                dataReader.Read();

                                var posturl = dataReader["posturl"].ToString();
                                var postId = dataReader["id"].ToString();
                                var scrape_type = dataReader["scrape_type"].ToString();
                                var from_account = dataReader["from_account"].ToString();

                                temp_posturls.Add(new
                                {
                                    postId,
                                    posturl,
                                    scrape_type,
                                    from_account
                                });
                            }
                        }
                    }


                    foreach (var item in temp_posturls)
                    {
                        //check if posturl has nextMinID="" or not

                        //check if has scrape_commenter_nextMinID log, if not this is first time scrape commenters for this url

                        var command_query_count_nextMinId =
                                @"select count(*)
                            from actionlog
                            where

                                [log_name] = 'scrape_commenter_nextMinID' and
                                  [log_account] = @postUrl";

                        using (var command = new SqlCommand(command_query_count_nextMinId, conn))
                        {
                            command.Parameters.Add("@postUrl", System.Data.SqlDbType.VarChar).Value = item.posturl;

                            var count = int.Parse(command.ExecuteScalar().ToString());
                            if (count == 0)
                            {
                                choiced_postId = item.postId;
                                post = item;
                                break;
                            }
                        }

                        //check xem co ton tai nextMinId="" khong

                        command_query_count_nextMinId =
                                    @"select count(*)
                                    from actionlog
                                    where

                                        [log_name] = 'scrape_commenter_nextMinID' and
                                          [log_account] = @postUrl and
                                          [value]=''";

                        using (var command = new SqlCommand(command_query_count_nextMinId, conn))
                        {
                            command.Parameters.Add("@postUrl", System.Data.SqlDbType.VarChar).Value = item.posturl;

                            var count = int.Parse(command.ExecuteScalar().ToString());
                            if (count >= 1)
                            {
                                continue;
                            }
                            else
                            {
                                choiced_postId = item.postId;
                                post = item;
                                break;
                            }
                        }
                    }

                    if (choiced_postId != null && is_take_data)
                    {
                        command_query = "insert into actionlog (log_name,log_account) values ('scrape_commenter',@ig_url)";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@ig_url", System.Data.SqlDbType.VarChar).Value = post.posturl;
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }

            return post;
        }

        static public List<string> GrabCommenters_PreCommentersOfPost(string post_url)
        {
            string pre_commenters_str = "";

            var command_query =
                @"select *
                from actionlog
                where
                    [log_name] = 'scrape_commenter_pre' and
                      [log_account] = @post_url";

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {

                    command.Parameters.Add("@post_url", System.Data.SqlDbType.VarChar).Value = post_url;

                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                pre_commenters_str += dataReader["value"].ToString();
                            }
                        }
                    }
                }
            }

            List<string> pre_commenters = pre_commenters_str.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            return pre_commenters;
        }

        static public void GrabCommenters_AddNextMinId(string postUrl, string nextMinID)
        {
            var command_query = "insert into actionlog (log_name,log_account,[value]) values ('scrape_commenter_nextMinID',@post_url,@nextMinID)";

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {

                    command.Parameters.Add("@post_url", System.Data.SqlDbType.VarChar).Value = postUrl;
                    command.Parameters.Add("@nextMinID", System.Data.SqlDbType.VarChar).Value = nextMinID;

                    command.ExecuteNonQuery();
                }
            }
        }

        static public string GrabCommenters_PreNextMinId(string postUrl)
        {
            string nextMinId = "";
            var command_query =
                @"SELECT top 1 *
                FROM actionlog
                WHERE[log_name] = 'scrape_commenter_nextMinID'
                  AND[log_account] = @postUrl
                ORDER BY id DESC";

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@postUrl", System.Data.SqlDbType.VarChar).Value = postUrl;
                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();
                            nextMinId = dataReader["value"].ToString();
                        }
                    }
                }
            }

            return nextMinId;
        }

        #endregion

        #region --GrabFollower--
        static public string DATA_GrabFollower(bool is_take_data = true)
        {
            string username = null;

            lock ("DATA_GrabFollower")
            {
                var command_query = @"SELECT top 1 [ig_url]
                                    FROM target_accounts
                                    WHERE [scrape_follower] = 'yes'
                                      AND 
                                        (SELECT count([log_account])
                                         FROM actionlog
                                         WHERE [log_name] = 'scrape_follower'
                                           AND DATEDIFF(MINUTE, do_at, getdate()) < @mins
										   AND [log_account]=target_accounts.ig_url)=0
                                      AND ([state] is null or [state]='')";

                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@mins", System.Data.SqlDbType.Int).Value = Settings.settings_grab_followers_check_x_mins;
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                dataReader.Read();
                                username = dataReader["ig_url"].ToString();
                            }
                        }
                    }

                    if (username != null && is_take_data)
                    {
                        command_query = "insert into actionlog (log_name,log_account) values ('scrape_follower',@ig_url)";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@ig_url", System.Data.SqlDbType.VarChar).Value = username;

                            command.ExecuteNonQuery();
                        }

                        username = username.Replace("https://www.instagram.com/", "").Replace("/", "");
                    }

                }
            }

            return username;
        }

        static public List<string> GrabFollower_PreFollowersOfUser(string ig_url)
        {
            string pre_followers_str = "";

            var command_query =
                @"select *
                from actionlog
                where
                    [log_name] = 'scrape_follower_pre' and
                      [log_account] = @ig_url";

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {

                    command.Parameters.Add("@ig_url", System.Data.SqlDbType.VarChar).Value = ig_url;

                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                pre_followers_str += dataReader["value"].ToString();
                            }
                        }
                    }
                }
            }

            List<string> pre_followers = pre_followers_str.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            return pre_followers;
        }

        /// <summary>
        /// get latest follower count from database
        /// </summary>
        /// <param name="_instaApi"></param>
        /// <param name="post_url"></param>
        /// <returns></returns>
        static public int GrabFollower_PreFollowerCountOfUser(string ig_url)
        {
            int pre_followers_count = 0;

            var command_query = @"select top 1 *
                                from actionlog
                                where
                                    [log_name] = 'followers_count' and
                                      [log_account] = @ig_url order by do_at desc";

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@ig_url", System.Data.SqlDbType.VarChar).Value = ig_url;

                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();

                            pre_followers_count = int.Parse(dataReader["value"].ToString());
                        }
                    }
                }
            }

            return pre_followers_count;
        }

        #endregion

        #region --Grab LIKER--

        static public dynamic DATA_GrabLiker(int recent_days = 1, bool is_take_data = true)
        {
            dynamic post = null;
            string postId = null;

            lock ("DATA_GrabLiker")
            {

                //var command_query = @"SELECT top 1 p.[id],[posturl],[scrape_type],[from_account]
                //                    FROM posts p OUTER apply
                //                      (SELECT top 1 *
                //                       FROM actionlog
                //                       WHERE [log_account]=p.posturl and [log_name]='scrape_liker'
                //                       ORDER BY id DESC) AS al
                //                    WHERE [checked] = 'Checked'
                //                      AND [ok] = 'OK'
                //                      AND ([scrape_type] = 'All' or[scrape_type] = 'Likers')
                //                      AND [posturl] not in (select [log_account]
                //                      FROM actionlog WHERE [log_name] = 'scrape_liker'
                //                      AND DATEDIFF(MINUTE, do_at, getdate()) < 10)
                //                      AND (@recent_days = 0
                //                           OR DATEDIFF(DAY, [creation_date], getdate()) < @recent_days)
                //                      AND 'https://www.instagram.com/' +[from_account]+'/' NOT IN
                //                        (SELECT [ig_url]
                //                         FROM target_accounts
                //                         WHERE [state]='PAUSED')
                //                    ORDER BY do_at ASC";

                var command_query =
                    @" SELECT  p.[id],[posturl],[scrape_type],[from_account]--, [do_at]
                                    FROM posts p OUTER apply
                                      (SELECT top 1 *
                                       FROM actionlog
                                       WHERE [log_account]=p.posturl and [log_name]='scrape_liker'
                                       ORDER BY id DESC) AS al
                                    WHERE [checked] = 'Checked'
                                      AND [ok] = 'OK'
                                      AND ([scrape_type] = 'All' or[scrape_type] = 'Likers')
                                      --AND [posturl] not in (select [log_account]
                                      --FROM actionlog WHERE [log_name] = 'scrape_liker'
                                      --AND DATEDIFF(MINUTE, do_at, getdate()) < 10)
                                      AND (@recent_days = 0
                                           OR DATEDIFF(DAY, [creation_date], getdate()) < @recent_days)
                                      AND 'https://www.instagram.com/' +[from_account]+'/' IN
                                        (SELECT [ig_url]
                                         FROM target_accounts
                                         WHERE [state]='')"; //get list account that can be scrape liker

                List<dynamic> post_lists = new List<dynamic>();

                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@recent_days", System.Data.SqlDbType.Int).Value = recent_days;
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {

                                    var temp_postId = dataReader["id"].ToString();
                                    var posturl = dataReader["posturl"].ToString();
                                    var scrape_type = dataReader["scrape_type"].ToString();
                                    var from_account = dataReader["from_account"].ToString();

                                    post_lists.Add(new
                                    {
                                        postId=temp_postId,
                                        posturl,
                                        scrape_type,
                                        from_account
                                    });
                                }
                            }
                        }
                    }

                    foreach (var p in post_lists)
                    {
                        //check each post

                        //check if this post from from_account that was paused
                        command_query =
                            @"select [state]
                            from target_accounts
                            where
	                            [ig_url] like '%/" + p.from_account + "/'";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            var state_obj = command.ExecuteScalar();

                            if (state_obj.ToString() != "")
                            {
                                continue;

                            }
                        }

                        //check to verify not do action in last 10 minutes
                        command_query =
                            @"select count([do_at])
                            from actionlog
                            where
	                            [log_name] = 'scrape_liker' and
	                            [log_account]=@ig_url and
	                            DATEDIFF(MINUTE, do_at, getdate()) < 10";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@ig_url", System.Data.SqlDbType.VarChar).Value = p.posturl;

                            if (int.Parse(command.ExecuteScalar().ToString()) > 0)
                                continue;
                        }

                        //toi buoc nay tuc data thoa dieu kien


                        postId = p.postId;
                        post = p;
                        break;

                    }

                    if (postId != null && is_take_data)
                    {
                        //add log

                        command_query = "insert into actionlog (log_name,log_account) values ('scrape_liker',@ig_url)";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.Parameters.Add("@ig_url", System.Data.SqlDbType.VarChar).Value = post.posturl;

                            command.ExecuteNonQuery();
                        }
                    }
                }
            }

            return post;
        }

        static public List<string> GrabLiker_PreLikersOfPost(string post_url)
        {
            string pre_likers_str = "";

            var command_query =
                @"select *
                from actionlog
                where
                    [log_name] = 'scrape_liker_pre' and
                      [log_account] = @post_url";

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {

                    command.Parameters.Add("@post_url", System.Data.SqlDbType.VarChar).Value = post_url;

                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                pre_likers_str += dataReader["value"].ToString();
                            }
                        }
                    }
                }
            }

            List<string> pre_likers = pre_likers_str.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            return pre_likers;
        }

        static public int GrabLiker_CurrentLikeOfPost(InstagramApiSharp.API.IInstaApi _instaApi, string post_url)
        {
            var task_media_id = Task.Run(() =>
            {
                return _instaApi.MediaProcessor.GetMediaIdFromUrlAsync(new Uri(post_url));
            });

            var media_id = task_media_id.GetAwaiter().GetResult().Value.ToString();

            var task_media = Task.Run(() =>
            {
                return _instaApi.MediaProcessor.GetMediaByIdAsync(media_id);
            });
            var media = task_media.GetAwaiter().GetResult();

            var likes_count = media.Value.LikesCount;

            return likes_count;
        }


        /// <summary>
        /// get latest likes count of post in database
        /// </summary>
        /// <param name="post_url"></param>
        static public int GrabLiker_PreLikeOfPost(string post_url)
        {
            int pre_like = 0;

            var command_query = @"select top 1 *
                from actionlog
                where

                    [log_name] = 'likers_count' and
                      [log_account] = @post_url order by do_at desc";

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@post_url", System.Data.SqlDbType.VarChar).Value = post_url;

                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            dataReader.Read();

                            pre_like = int.Parse(dataReader["value"].ToString());
                        }
                    }
                }
            }

            return pre_like;
        }

        #endregion

        #endregion

        #region --DetectAIO--

        static public List<dynamic> DetectGenderCountry_PLAuto_GetTask_Multi(int quantity = 100)
        {
            string project_name = Settings.aio_settings["Project"].ToString();

            switch (project_name)
            {
                case "weightloss":
                    return DetectGenderCountry_PLAuto_GetTask_Multi_weightloss(quantity);
                default:
                    return DetectGenderCountry_PLAuto_GetTask_Multi_Default(quantity);
            }
        }

        static private List<dynamic> DetectGenderCountry_PLAuto_GetTask_Multi_Default(int quantity)
        {
            List<dynamic> choiced_profiles = new List<dynamic>();

            //try
            //{
            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                var command_query =
                @"select top " + quantity + @" [pk],[UserName],[Op_FullName]
                        from scrapeusers
                        where
					        ([Op_FullName] is not null and [Op_FullName]!='') and
	                        ([status] is null or [status]='') and
					        --([state] is null or [state]!='Ready') and
					        (select count([log_name]) --chua detect gender
								        from actionlog
								        where [log_name]='detect_location' --dung location de cover luon cac data dung phuong phap cu da detect_gender nhung chua detect_location
										        and log_account=[UserName])=0 and
					        [UserName] in (select a1.log_account --da detect name_image
								        from actionlog a1
								        where a1.[log_name]='DetectName_Image') AND 
								        'https://www.instagram.com/' +[from_account]+'/' IN
																	        (SELECT [ig_url]
																	         FROM target_accounts
																	         WHERE [state]='' or [state] is null)
							order by first_added desc";

                List<dynamic> profiles = new List<dynamic>();

                PLAutoHelper.SQLHelper.Execute(() =>
                {
                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    var id = dataReader["pk"].ToString();
                                    var UserName = dataReader["UserName"].ToString();
                                    var name = dataReader["Op_FullName"].ToString();

                                    profiles.Add(new
                                    {
                                        id,
                                        UserName,
                                        name
                                    });
                                }
                            }
                        }
                    }
                }/*, "scrapeusers"*/);

                if (profiles.Count > 0)
                {

                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (var sqlTrans = conn.BeginTransaction())
                        {
                            foreach (var p in profiles)
                            {
                                command_query = "update scrapeusers set [status]='Claimed' where [UserName]=@UserName and ([status] is null or [status]='')";

                                using (var command = new SqlCommand(command_query, conn, sqlTrans))
                                {
                                    command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = p.UserName;
                                    var count = command.ExecuteNonQuery();


                                    if (count == 1)
                                    {
                                        choiced_profiles.Add(p);
                                    }
                                }

                            }

                            sqlTrans.Commit();

                        }
                    }/*, "scrapeusers"*/);

                }

            }
            //}
            //catch { }


            return choiced_profiles;
        }

        static private List<dynamic> DetectGenderCountry_PLAuto_GetTask_Multi_weightloss(int quantity)
        {
            List<dynamic> choiced_profiles = new List<dynamic>();

            //try
            //{
            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                //         var command_query =
                //         @"select top " + quantity + @" [pk],[UserName],[Op_FullName]
                //                 from scrapeusers
                //                 where
                //      ([Op_FullName] is not null and [Op_FullName]!='') and
                //                  ([status] is null or [status]='') and
                //      --([state] is null or [state]!='Ready') and
                //      (select count([log_name]) --chua detect gender
                //	        from actionlog
                //	        where [log_name]='detect_location' --dung location de cover luon cac data dung phuong phap cu da detect_gender nhung chua detect_location
                //			        and log_account=[UserName])=0 and
                //      [UserName] in (select a1.log_account --da detect name_image
                //	        from actionlog a1
                //	        where a1.[log_name]='DetectName_Image') AND 
                //	        'https://www.instagram.com/' +[from_account]+'/' IN
                //										        (SELECT [ig_url]
                //										         FROM target_accounts
                //										         WHERE [state]='' or [state] is null)
                //order by first_added desc";

                var command_query =
                    @"select top " + quantity + @" [pk],[UserName],[Op_FullName]
                        from scrapeusers
                        where
					        ([Op_FullName] is not null and [Op_FullName]!='') and --da detect name_image
	                        ([status] is null or [status]='') and
					        --([state] is null or [state]!='Ready') and
					        detect_gender is null and --chua detect gender
							'https://www.instagram.com/' +[from_account]+'/' IN --source van dang chay
																	        (SELECT [ig_url]
																	         FROM target_accounts
																	         WHERE [state]='' or [state] is null)
							order by first_added desc";

                List <dynamic> profiles = new List<dynamic>();

                PLAutoHelper.SQLHelper.Execute(() =>
                {
                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    var id = dataReader["pk"].ToString();
                                    var UserName = dataReader["UserName"].ToString();
                                    var name = dataReader["Op_FullName"].ToString();

                                    profiles.Add(new
                                    {
                                        id,
                                        UserName,
                                        name
                                    });
                                }
                            }
                        }
                    }
                }/*, "scrapeusers"*/);

                if (profiles.Count > 0)
                {

                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (var sqlTrans = conn.BeginTransaction())
                        {
                            foreach (var p in profiles)
                            {
                                command_query = "update scrapeusers set [status]='Claimed' where [UserName]=@UserName and ([status] is null or [status]='')";

                                using (var command = new SqlCommand(command_query, conn, sqlTrans))
                                {
                                    command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = p.UserName;
                                    var count = command.ExecuteNonQuery();


                                    if (count == 1)
                                    {
                                        choiced_profiles.Add(p);
                                    }
                                }

                            }

                            sqlTrans.Commit();

                        }
                    }/*, "scrapeusers"*/);

                }

            }
            //}
            //catch { }


            return choiced_profiles;
        }

        static public string Namesor_ApiKey()
        {
            string apiKey = null;

            var command_query =
                @"select top 1 *
                from namsor_api 
                where 
	                ([state] is null or [state]='') and
	                ([status] is null or [status]='')
					order by [used_at] asc";

            lock ("Namesor_ApiKey")
            {

                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                dataReader.Read();
                                apiKey = dataReader["apiKey"].ToString();
                            }
                        }
                    }

                    if (apiKey != null)
                    {
                        command_query = "update namsor_api set [status]='Claimed',[used_at]=getdate() where [apiKey]='" + apiKey + "'";

                        using (var command = new SqlCommand(command_query, conn))
                        {
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
            return apiKey;
        }

        static public void Namesor_API_UpdateProperty(string apiKey, string property, string value)
        {
            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                var command_query = "update namsor_api set [" + property + "]='" + value + "' where [apiKey]='" + apiKey + "'";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        static public void Namesor_ClearApiKey()
        {
            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                var command_query = "update namsor_api set [status]=''";

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        static public List<dynamic> DetectName_Image_GetTask(bool is_take_data = true)
        {
            List<dynamic> choiced_profiles = new List<dynamic>();
            //dynamic profile = null;



            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                var command_query =
                    @"select top 200 [pk],[UserName],[FullName], [Op_FullName], [ProfilePicture]
                            from scrapeusers
                            where
	                            ([status] is null or [status]='') and
					            --([state] is null or [state]='') and
                                ([pk] is not null and [pk]!='') and
					            [Op_FullName] is null and
								'https://www.instagram.com/' +[from_account]+'/' NOT IN
																	(SELECT [ig_url]
																	 FROM target_accounts
																	 WHERE [state]='PAUSED')
							order by first_added desc";


                List<dynamic> profiles = new List<dynamic>();

                PLAutoHelper.SQLHelper.Execute(() =>
                {
                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    var pk = dataReader["pk"].ToString();
                                    var UserName = dataReader["UserName"].ToString();
                                    var FullName = dataReader["FullName"].ToString();
                                    var ProfilePicture = dataReader["ProfilePicture"].ToString();

                                    var profile = new
                                    {
                                        pk,
                                        UserName,
                                        FullName,
                                        ProfilePicture
                                    };

                                    profiles.Add(profile);
                                }
                            }
                        }
                    }
                }/*, "scrapeusers"*/);

                if (profiles.Count > 0)
                {
                    if (is_take_data)
                    {
                        try
                        {
                            PLAutoHelper.SQLHelper.Execute(() =>
                            {
                                using (SqlTransaction sqlTrans = conn.BeginTransaction())
                                {

                                    for (int i = 0; i < profiles.Count; i++)
                                    {

                                        command_query = "update scrapeusers set [status]='Claimed' where [UserName]=@UserName and ([status] is null or [status]='')";


                                        using (var command = new SqlCommand(command_query, conn, sqlTrans))
                                        {
                                            command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = profiles[i].UserName;
                                            var count = command.ExecuteNonQuery();
                                            if (count == 1)
                                            {
                                                choiced_profiles.Add(profiles[i]);
                                            }
                                        }
                                    }


                                    sqlTrans.Commit();
                                }
                            });
                        }
                        catch(Exception ex)
                        {
                            ThreadHelper.HandleException(ex.Message, ex.StackTrace);
                        }

                    }
                    else
                    {
                        return choiced_profiles;
                    }
                }


            }
            return choiced_profiles;
        }

        static public dynamic DetectLanguage_GetTask(bool is_take_data = true)
        {
            dynamic profile = null;

            

            lock ("DetectLanguage")
            {
                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    while (true)
                    {
                        var command_query =
                @"select top 1 *
                from scrapeusers
                where
	                ([detect_language] is null or [detect_language]='') and
	                ([update_at] is not null and update_at!='') and
	                ([update_at] is not null or update_at='') and
	                ([status] is null or [status]='') and
	                ([following] is null or [following]='') and
	                ([likeuserpost] is null or [likeuserpost]='')";
                        using (var command = new SqlCommand(command_query, conn))
                        {
                            using (var dataReader = command.ExecuteReader())
                            {
                                if (dataReader.HasRows)
                                {
                                    dataReader.Read();
                                    var pk = dataReader["pk"].ToString();
                                    var UserName = dataReader["UserName"].ToString();
                                    var bio = dataReader["Biography"].ToString();

                                    profile = new
                                    {
                                        pk,
                                        UserName,
                                        bio
                                    };
                                }
                            }
                        }

                        if (profile != null && is_take_data)
                        {
                            command_query = "update scrapeusers set [status]='Claimed' where [pk]=@pk and ([status] is null or [status]='')";

                            using (var command = new SqlCommand(command_query, conn))
                            {
                                command.Parameters.Add("@pk", System.Data.SqlDbType.Float).Value = profile.pk;
                                var count = command.ExecuteNonQuery();
                                if (count == 1)
                                {
                                    break;
                                }
                                else
                                {
                                    profile = null;
                                }
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }

            return profile;
        }



        static public dynamic DetectGender_GetTask(bool is_take_data = true)
        {
            dynamic profile = null;

            

            lock ("DetectGender")
            {
                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    while (true)
                    {
                        using (var sqlTrans = conn.BeginTransaction())
                        {
                            try
                            {
                                var command_query =
                                @"select top 1 *
                        from scrapeusers
                        where
					        ([Op_FullName] is not null and [Op_FullName]!='') and
	                        ([status] is null or [status]='') and
					        ([state] is null or [state]!='Ready') and
					        (select count([log_name]) --chua detect gender
								        from actionlog
								        where [log_name]='detect_gender'
										        and log_account=[UserName])=0 and
					        [UserName] in (select a1.log_account --da detect name_image
								        from actionlog a1
								        where a1.[log_name]='DetectName_Image') AND 
								        'https://www.instagram.com/' +[from_account]+'/' NOT IN
																	        (SELECT [ig_url]
																	         FROM target_accounts
																	         WHERE [state]='PAUSED')";

                                using (var command = new SqlCommand(command_query, conn, sqlTrans))
                                {
                                    using (var dataReader = command.ExecuteReader())
                                    {
                                        if (dataReader.HasRows)
                                        {
                                            dataReader.Read();
                                            var pk = dataReader["pk"].ToString();
                                            var UserName = dataReader["UserName"].ToString();
                                            var fullname = dataReader["Op_FullName"].ToString();

                                            profile = new
                                            {
                                                pk,
                                                UserName,
                                                fullname
                                            };
                                        }
                                    }
                                }

                                if (profile != null && is_take_data)
                                {
                                    command_query = "update scrapeusers set [status]='Claimed' where [pk]=@pk and ([status] is null or [status]='')";

                                    using (var command = new SqlCommand(command_query, conn, sqlTrans))
                                    {
                                        command.Parameters.Add("@pk", System.Data.SqlDbType.Float).Value = profile.pk;
                                        var count = command.ExecuteNonQuery();
                                        if (count == 1)
                                        {
                                            sqlTrans.Commit();
                                            break;
                                        }
                                        else
                                        {
                                            profile = null;
                                        }
                                    }
                                }
                                else
                                {
                                    break;
                                }
                            }
                            catch(Exception ex)
                            {
                                HandleException(ex.Message, ex.StackTrace);
                            }
                        }
                    }
                }
            }

            return profile;
        }

        static public List<dynamic> DetectGender_GetTask_Multi(int quantity = 100)
        {

            List<dynamic> choiced_profiles = new List<dynamic>();

            //try
            //{
            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();



                var command_query =
                @"select top " + quantity + @" [pk],[UserName],[Op_FullName]
                        from scrapeusers
                        where
					        ([Op_FullName] is not null and [Op_FullName]!='') and
	                        ([status] is null or [status]='') and
					        ([state] is null or [state]!='Ready') and
					        (select count([log_name]) --chua detect gender
								        from actionlog
								        where [log_name]='detect_gender'
										        and log_account=[UserName])=0 and
					        [UserName] in (select a1.log_account --da detect name_image
								        from actionlog a1
								        where a1.[log_name]='DetectName_Image') AND 
								        'https://www.instagram.com/' +[from_account]+'/' NOT IN
																	        (SELECT [ig_url]
																	         FROM target_accounts
																	         WHERE [state]='PAUSED')
							order by first_added asc";

                List<dynamic> profiles = new List<dynamic>();

                PLAutoHelper.SQLHelper.Execute(() =>
                {
                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    var id = dataReader["pk"].ToString();
                                    var UserName = dataReader["UserName"].ToString();
                                    var name = dataReader["Op_FullName"].ToString();

                                    profiles.Add(new
                                    {
                                        id,
                                        UserName,
                                        name
                                    });
                                }
                            }
                        }
                    }
                }/*, "scrapeusers"*/);

                if (profiles.Count > 0)
                {

                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (var sqlTrans = conn.BeginTransaction())
                        {
                            foreach (var p in profiles)
                            {
                                command_query = "update scrapeusers set [status]='Claimed' where [UserName]=@UserName and ([status] is null or [status]='')";

                                using (var command = new SqlCommand(command_query, conn, sqlTrans))
                                {
                                    command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = p.UserName;
                                    var count = command.ExecuteNonQuery();


                                    if (count == 1)
                                    {
                                        choiced_profiles.Add(p);
                                    }
                                }

                            }

                            sqlTrans.Commit();

                        }
                    }/*, "scrapeusers"*/);

                }

            }
            //}
            //catch { }


            return choiced_profiles;
        }

        static public dynamic DetectCountry_GetTask(bool is_take_data = true)
        {

            dynamic profile = null;



            lock ("DetectCountry")
            {
                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    while (true)
                    {
                        using (var sqlTransaction = conn.BeginTransaction())
                        {

                            try
                            {
                                var command_query =
                           @"select top 1 *
                from scrapeusers
                where
					([Op_FullName] is not null and [Op_FullName]!='') and
	                ([status] is null or [status]='') and
					([state] is null or [state]!='Ready') and
					detect_gender='male' and
					(select count([log_name]) --chua detect location
								from actionlog
								where [log_name]='detect_location'
										and log_account=[UserName])=0 AND 
					'https://www.instagram.com/' +[from_account]+'/' NOT IN
																	(SELECT [ig_url]
																	 FROM target_accounts
																	 WHERE [state]='PAUSED')";

                                using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                {
                                    using (var dataReader = command.ExecuteReader())
                                    {
                                        if (dataReader.HasRows)
                                        {
                                            dataReader.Read();
                                            var pk = dataReader["pk"].ToString();
                                            var UserName = dataReader["UserName"].ToString();
                                            var fullname = dataReader["Op_FullName"].ToString();

                                            profile = new
                                            {
                                                pk,
                                                UserName,
                                                fullname
                                            };
                                        }
                                    }
                                }

                                if (profile != null && is_take_data)
                                {
                                    command_query = "update scrapeusers set [status]='Claimed' where [pk]=@pk and ([status] is null or [status]='')";

                                    using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                    {
                                        command.Parameters.Add("@pk", System.Data.SqlDbType.Float).Value = profile.pk;
                                        var count = command.ExecuteNonQuery();
                                        if (count == 1)
                                        {
                                            sqlTransaction.Commit();
                                            break;
                                        }
                                        else
                                        {
                                            profile = null;
                                        }
                                    }
                                }
                                else
                                {
                                    break;
                                }
                            }
                            catch(Exception ex)
                            {
                                HandleException(ex.Message, ex.StackTrace);
                            }
                        }
                    }

                }
            }

            return profile;
        }

        static public List<dynamic> DetectCountry_GetTask_Multi(int quantity = 50)
        {
            List<dynamic> choiced_profiles = new List<dynamic>();

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();


                //try
                //{
                var command_query =
                @"select top " + quantity.ToString() + @"  [pk],[UserName],[Op_FullName]
                        from scrapeusers
                        where
					        ([Op_FullName] is not null and [Op_FullName]!='') and
	                        ([status] is null or [status]='') and
					        ([state] is null or [state]!='Ready') and
					        detect_gender='male' and
					        (select count([log_name]) --chua detect location
								        from actionlog
								        where [log_name]='detect_location'
										        and log_account=[UserName])=0 AND 
					        'https://www.instagram.com/' +[from_account]+'/' NOT IN
																	        (SELECT [ig_url]
																	         FROM target_accounts
																	         WHERE [state]='PAUSED')
							order by first_added asc";

                List<dynamic> profiles = new List<dynamic>();

                PLAutoHelper.SQLHelper.Execute(() =>
                {
                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    var id = dataReader["pk"].ToString();
                                    var UserName = dataReader["UserName"].ToString();
                                    var name = dataReader["Op_FullName"].ToString();

                                    profiles.Add(new
                                    {
                                        id,
                                        UserName,
                                        name
                                    });
                                }
                            }
                        }
                    }
                }/*, "scrapeusers"*/);

                if (profiles.Count > 0)
                {

                    PLAutoHelper.SQLHelper.Execute(() =>
                    {
                        using (var sqlTrans = conn.BeginTransaction())
                        {
                            foreach (var p in profiles)
                            {
                                {
                                    command_query = "update scrapeusers set [status]='Claimed' where [UserName]=@UserName and ([status] is null or [status]='')";

                                    using (var command = new SqlCommand(command_query, conn, sqlTrans))
                                    {
                                        command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = p.UserName;
                                        var count = command.ExecuteNonQuery();


                                        if (count == 1)
                                        {
                                            choiced_profiles.Add(p);
                                        }
                                    }


                                }
                            }/*, "scrapeusers"*/

                            sqlTrans.Commit();
                        }

                        //}
                        //catch (Exception ex)
                        //{
                        //    HandleException(ex.Message, ex.StackTrace);
                        //}
                    });

                }
            }


            return choiced_profiles;
        }

        static public void DetectAIO_UpdateProperty(string pk, string property, string value)
        {
            var command_query =
                @"update scrapeusers set [" + property + "]=@value where [pk]=@pk";

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                while (true)
                {

                    SqlTransaction sqlTransaction = conn.BeginTransaction();

                    try
                    {
                        using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                        {
                            if (property == "Op_FullName")
                                command.Parameters.Add("@value", System.Data.SqlDbType.NVarChar).Value = value;
                            else
                                command.Parameters.Add("@value", System.Data.SqlDbType.VarChar).Value = value;
                            command.Parameters.Add("@pk", System.Data.SqlDbType.Float).Value = pk;
                            command.ExecuteNonQuery();
                        }

                        sqlTransaction.Commit();
                        break;
                    }
                    catch
                    {
                        sqlTransaction.Rollback();
                    }
                }
            }
        }

        #endregion

        #region --ActionLog--

        static public void AddActionLog(string log_name, string log_account, string value)
        {
            var command_query = "insert into actionlog ([log_name],[log_account],[value]) values (@log_name,@log_account,@value)";

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@log_name", System.Data.SqlDbType.VarChar).Value = log_name;
                    command.Parameters.Add("@log_account", System.Data.SqlDbType.VarChar).Value = log_account;
                    command.Parameters.Add("@value", System.Data.SqlDbType.VarChar).Value = value;

                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region --Handle Log Exception--


        static public void HandleException(string message, string stack_trace)
        {
            var command_query = "insert into log_exception ([message],[stack_trace]) values (@message,@stack_trace)";

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                using (var command = new SqlCommand(command_query, conn))
                {
                    command.Parameters.Add("@message", System.Data.SqlDbType.NText).Value = message;
                    command.Parameters.Add("@stack_trace", System.Data.SqlDbType.NText).Value = stack_trace;

                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region --CopyPosts--

        static public void UploadContentsToOwnServer()
        {

            Program.form.UpdateCopyContentStatus("Uploading images to server....");

            int finished = 0;
            int total_tasks = 0;

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                var command_query =
                    @"select count(*)
                    from copy_posts
                    where
	                    [uploaded_contents] is null or CONVERT(NVARCHAR(MAX), [uploaded_contents])=''";

                using (var command = new SqlCommand(command_query, conn))
                {
                    total_tasks = int.Parse(command.ExecuteScalar().ToString());
                }

            }

            while (true)
            {
                string command_query = null;

                dynamic data = null;

                //read data from database where uploaded_contents is null or=''
                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    command_query =
                        @"select *
                    from copy_posts
                    where
	                    [uploaded_contents] is null or CONVERT(NVARCHAR(MAX), [uploaded_contents])=''";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                dataReader.Read();

                                data = new
                                {
                                    id = dataReader["id"].ToString(),
                                    contents = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(dataReader["contents"].ToString())
                                };
                            }
                        }
                    }

                }

                if (data == null)
                    break;

                List<string> uploaded_contents = new List<string>();

                //upload to imodstyle
                foreach (var item in data.contents)
                {
                    //download to local
                    var local_path = PLAutoHelper.NormalFuncHelper.OptimizePathEndBackSlash(Path.GetFullPath("Temp")) + PLAutoHelper.NormalFuncHelper.RandomGUID(4) + ".jpg";

                    DownloadImage(item.ToString(), local_path);


                    //reupload to own server
                    var uploaded_url = UploadFileToImodstyleServer(local_path);

                    uploaded_contents.Add(uploaded_url);
                }

                //update to database
                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    command_query =
                        @"update copy_posts set [uploaded_contents]=@uploaded_contents where [id]=@id";

                    using(var command=new SqlCommand(command_query,conn))
                    {
                        command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = data.id;
                        command.Parameters.Add("@uploaded_contents", System.Data.SqlDbType.VarChar).Value = Newtonsoft.Json.JsonConvert.SerializeObject(uploaded_contents);
                        command.ExecuteNonQuery();
                    }
                }

                finished++;


                Program.form.UpdateCopyContentStatus("Finished " + finished.ToString() + "/" + total_tasks.ToString());
            }


            Program.form.UpdateCopyContentStatus("Completed");
        }

        static public string UploadFileToImodstyleServer(string filename)
        {

            string online_url = null;

            using (var client = new ScpClient("149.28.85.43", 2222, "root", ")W3grpLF.gS6G2+?"))
            {
                client.RemotePathTransformation = RemotePathTransformation.ShellQuote;
                client.Connect();

                //using (MemoryStream ms = new MemoryStream())
                //using (FileStream file = new FileStream(@"F:\DOWNLOAD\gmail_new.txt", FileMode.Open, FileAccess.Read))
                //{
                //    file.CopyTo(ms);

                //    client.BufferSize = 4 * 1024; // bypass Payload error large files
                //    client.Upload(ms, "/home/imodstyle.com/public_html/igldplayer/gmail_new.txt");
                //}

                using (var fileStream = new FileStream(filename, FileMode.Open))
                {
                    var extension = Path.GetExtension(filename);

                    var generate_filename = Path.GetFileNameWithoutExtension(filename).Replace(" ", "") + "_" + PLAutoHelper.NormalFuncHelper.RandomGUID(5) + extension;

                    online_url = "https://imodstyle.com/igldplayer/" + generate_filename;

                    client.Upload(fileStream, "/home/imodstyle.com/public_html/igldplayer/" + generate_filename);
                }

                //using (var fileStream = new FileStream(@"F:\DOWNLOAD\gmail_new.txt", FileMode.Open))
                //{
                //    client.Upload(fileStream, "/home/imodstyle.com/public_html/igldplayer/gmail_new_1.txt");
                //}

                //using (var stream = ToStream(Image.FromFile(@"F:\DOWNLOAD\Screenshot_125.png"), ImageFormat.Png))
                //{

                //    client.BufferSize = 4 * 1024; // bypass Payload error large files
                //    client.Upload(stream, "/home/imodstyle.com/public_html/igldplayer/test3.png");
                //}
            }

            return online_url;
        }

        static public void DownloadImage(string imageurl, string savepath)
        {

            using (var m = new MemoryStream(new WebClient().DownloadData(imageurl)))
            {
                using (var img = Bitmap.FromStream(m))
                {

                    img.Save(savepath, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
            }
        }

        static public void UploadImagesToGdrive()
        {
            Program.form.UpdateCopyContentStatus("Uploading images to server....");

            int finished = 0;
            int total_tasks = 0;

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                var command_query =
                    @"select count(*)
                    from copy_posts
                    where
	                    [uploaded_contents] is null or CONVERT(NVARCHAR(MAX), [uploaded_contents])=''";

                using (var command = new SqlCommand(command_query, conn))
                {
                    total_tasks = int.Parse(command.ExecuteScalar().ToString());
                }

            }

            while (true)
            {
                string command_query = null;

                dynamic data = null;

                //read data from database where uploaded_contents is null or=''
                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    command_query =
                        @"select *
                    from copy_posts
                    where
	                    [uploaded_contents] is null or CONVERT(NVARCHAR(MAX), [uploaded_contents])=''";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        using (var dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                dataReader.Read();

                                data = new
                                {
                                    id = dataReader["id"].ToString(),
                                    contents = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(dataReader["contents"].ToString())
                                };
                            }
                        }
                    }

                }

                if (data == null)
                    break;

                List<string> uploaded_contents = new List<string>();

                //upload to imodstyle
                foreach (var item in data.contents)
                {
                    //download to local
                    var local_path = PLAutoHelper.NormalFuncHelper.OptimizePathEndBackSlash(Path.GetFullPath("Temp")) + PLAutoHelper.NormalFuncHelper.RandomGUID(4) + ".jpg";

                    DownloadImage(item.ToString(), local_path);


                    //reupload to own server
                    //var uploaded_url = UploadFileToImodstyleServer(local_path);

                    var uploaded_url = PLAutoHelper.GDriveAPI.GDrive_UploadSingleFile(local_path, "1xjTgI1UQnROuNMBgXXzYnyzjbH2woLvZ");

                    uploaded_contents.Add(uploaded_url);
                }

                //update to database
                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();

                    command_query =
                        @"update copy_posts set [uploaded_contents]=@uploaded_contents where [id]=@id";

                    using (var command = new SqlCommand(command_query, conn))
                    {
                        command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = data.id;
                        command.Parameters.Add("@uploaded_contents", System.Data.SqlDbType.VarChar).Value = Newtonsoft.Json.JsonConvert.SerializeObject(uploaded_contents);
                        command.ExecuteNonQuery();
                    }
                }

                finished++;


                Program.form.UpdateCopyContentStatus("Finished " + finished.ToString() + "/" + total_tasks.ToString());
            }


            Program.form.UpdateCopyContentStatus("Completed");
        }

        #endregion

    }
}
