﻿using InstagramApiSharp.Classes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static IGAPIHelper.Scraper;

namespace IGAPI___Ramtinak.IGThreads
{
    class CopyPostFromSourceThread : IGThread
    {
        public override void DoThread()
        {
            threads.Clear();

            for (int t = 0; t < 1; t++)
            {
                var pos = t;
                Thread thread = new Thread(() => CopyPostFromSource());
                thread.Name = "scrape" + pos.ToString();
                thread.SetApartmentState(ApartmentState.STA);
                threads.Add(thread);
            }

            //API.ProxyHelpers.ProxyServerOthers(true);

            PLAutoHelper.ProxyHelper.XProxyServer_Ticker(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), Settings.proxy_server_ips);

            foreach (var t in threads)
            {
                t.Start();
            }


            InitTimer_ManageThread();
        }

        List<string> source_accounts;
        string niche_content;
        int max_pages = 1;

        public CopyPostFromSourceThread(string niche_content, List<string> source_profile_urls, int max_pages)
        {
            this.niche_content = niche_content;

            this.source_accounts = new List<string>();

            foreach(var s in source_profile_urls)
            {
                source_accounts.Add(s.Replace("https://www.instagram.com/", "").Replace("/", ""));
            }
            
            this.max_pages = max_pages;
        }

        private void CopyPostFromSource()
        {
            Program.form.UpdateCopyContentStatus("Doing copy post from source...");

            int copy_count = 0;

            while (source_accounts.Count > 0)
            {

            GetAPIOBJ:

                string proxy = "";


                while (proxy == "" || proxy == null)
                {
                    //proxy = PLAutoHelper.ProxyHelper.TMProxy_DB_GetProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                    proxy = PLAutoHelper.ProxyHelper.XProxy_DB_GetProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"));
                    Thread.Sleep(5000);
                }

                APIObj dyn = IGAPIHelper.Scraper.GetAPIObj(proxy, API.DBHelpers.db_connection_igtoolkit);

                if (dyn == null)
                {
                    //PLAutoHelper.ProxyHelper.TMProxy_DB_ReleaseProxy(proxy, new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                    PLAutoHelper.ProxyHelper.XProxy_DB_ReleaseProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), proxy);
                    goto GetAPIOBJ;
                }

                InstagramApiSharp.API.IInstaApi _instaApi = dyn.instaApi;


                //InstagramApiSharp.Classes.Models.InstaMediaList medias = new InstagramApiSharp.Classes.Models.InstaMediaList();
                Stopwatch watch_scrape = new Stopwatch();
                watch_scrape.Start();


                try
                {

                    var time_session = int.Parse(API.DBHelpers.GetValueFromAccountTb(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(dyn.account, "username"), "time_per_session"));

                    API.DBHelpers.AddAPICallLog(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(dyn.account, "username"), "new_session");

                    while (source_accounts.Count>0)
                    {

                        string user = source_accounts[0];

                        if (watch_scrape.Elapsed.TotalMinutes > time_session)
                            break;
                        ScrapeMediasFromUser:

                        IResult<InstagramApiSharp.Classes.Models.InstaMediaList> medias_result = IGAPIHelper.Scraper.ScrapeMediasFromUser(dyn.instaApi, user, max_pages);

                        using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                        {
                            conn.Open();

                            foreach (var item in medias_result.Value)
                            {
                                bool hasaudio = item.HasAudio;

                                string producttype = item.ProductType;

                                string post_type = "image";

                                if (hasaudio || producttype == "clips")
                                {
                                    post_type = "video";//SKIP POST_TYPE VIDEO
                                    continue;
                                };

                                //--
                                var ig_code = item.Code;

                                //---

                                var taken_at = item.TakenAt.ToString();

                                //--

                                List<string> images = new List<string>();

                                if (item.Carousel != null)
                                {
                                    foreach (var c in item.Carousel)
                                    {
                                        if (c.MediaType.ToString() == "Image")
                                        {
                                            images.Add(c.Images[0].Uri);
                                        }
                                    }

                                    //foreach (var img in item.Carousel[0].Images)
                                    //    images.Add(img.Uri);
                                }
                                else
                                {
                                    images.Add(item.Images[0].Uri);
                                }

                                string caption = "";

                                if (item.Caption != null)
                                {
                                    caption = item.Caption.Text;
                                }

                                //check if exists in database (niche_content + code)

                                string command_query =
                                    @"select count(*)
                        from copy_posts
                        where
	                        [niche_content]=@niche_content and [ig_code]=@ig_code";

                                using (var command = new SqlCommand(command_query, conn))
                                {

                                    command.Parameters.Add("@niche_content", System.Data.SqlDbType.VarChar).Value = niche_content;
                                    command.Parameters.Add("@ig_code", System.Data.SqlDbType.VarChar).Value = ig_code;
                                    var count = int.Parse(command.ExecuteScalar().ToString());
                                    if (count > 0)
                                        continue;
                                }

                                //insert data to database
                                command_query = @"insert into copy_posts (niche_content,ig_code,posted_date,caption,contents,post_type, source_account) values (@niche_content,@ig_code,@posted_date,@caption,@contents,@post_type,@source_account)";

                                using (var command = new SqlCommand(command_query, conn))
                                {
                                    command.Parameters.Add("@niche_content", System.Data.SqlDbType.VarChar).Value = niche_content;
                                    command.Parameters.Add("@ig_code", System.Data.SqlDbType.VarChar).Value = ig_code;
                                    command.Parameters.Add("@posted_date", System.Data.SqlDbType.DateTime).Value = taken_at;
                                    command.Parameters.Add("@caption", System.Data.SqlDbType.NText).Value = caption;
                                    command.Parameters.Add("@post_type", System.Data.SqlDbType.VarChar).Value = post_type;
                                    command.Parameters.Add("@contents", System.Data.SqlDbType.VarChar).Value = Newtonsoft.Json.JsonConvert.SerializeObject(images);
                                    command.Parameters.Add("@source_account", System.Data.SqlDbType.VarChar).Value = user;

                                    command.ExecuteNonQuery();
                                }

                                copy_count++;

                                Program.form.UpdateCopyContentStatus("Finished copy " + copy_count.ToString() + " posts");
                            }
                        }

                        PLAutoHelper.RPA.XDelay(5, 10);

                        source_accounts.Remove(user);
                    }
                }
                finally
                {
                    IGAPIHelper.Scraper.ReleaseScrapeAccount(PLAutoHelper.NormalFuncHelper.GetDynamicProperty(dyn.account, "username"), API.DBHelpers.db_connection_igtoolkit);
                    Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish APITask - Release Scrape Account");

                    //PLAutoHelper.ProxyHelper.TMProxy_DB_ReleaseProxy(proxy, new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                    PLAutoHelper.ProxyHelper.XProxy_DB_ReleaseProxy(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection_igtoolkit, "proxytb"), proxy);
                    Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Do APITask | Finish APITask - Release Proxy");

                    //Write session time
                    Program.form.WriteLogToForm_AIOGrab(Thread.CurrentThread.Name + ": " + "Finished round in " + watch_scrape.Elapsed.TotalMinutes.ToString());
                }
            }


            Program.form.UpdateCopyContentStatus("Completed");
        }
    }
}
