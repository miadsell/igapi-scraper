﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IGAPI___Ramtinak.IGThreads
{
    class DetectAIOThread : IGThread
    {
        string proxy_apikey;

        public DetectAIOThread(string proxy_apikey)
        {
            this.proxy_apikey = proxy_apikey;
        }

        public override void DoThread()
        {

            ThreadHelper.Namesor_ClearApiKey();

            threads.Clear();


            //no internet thread - thread not use internet

            Thread thread_detectname_image = new Thread(() => DetectName_Image());
            thread_detectname_image.Name = "detectname_image";
            thread_detectname_image.SetApartmentState(ApartmentState.STA);
            threads.Add(thread_detectname_image);

            //if (Program.form.is_DetectGender)
            //{
            //    for (int t = 0; t < 1; t++) //set thread dua tren proxy, 1 proxy set tu 3-5 threads
            //    {
            //        var pos = t;
            //        Thread thread = new Thread(() => DetectGender_Country_ByPLAUTO());
            //        thread.Name = "detectgender_" + pos.ToString();
            //        thread.SetApartmentState(ApartmentState.STA);
            //        threads.Add(thread);
            //    }
            //}

            //if (Program.form.is_DetectCountry)
            //{
            //    for (int t = 0; t < 1; t++) //set thread dua tren proxy, 1 proxy set tu 3-5 threads
            //    {
            //        var pos = t;
            //        Thread thread = new Thread(() => DetectCountry_Patch());
            //        thread.Name = "detectlocation_" + pos.ToString();
            //        thread.SetApartmentState(ApartmentState.STA);
            //        threads.Add(thread);
            //    }
            //}

            if (Program.form.is_DetectGender && Program.form.is_DetectCountry)
            {
                for (int t = 0; t < 1; t++) //set thread dua tren proxy, 1 proxy set tu 3-5 threads
                {
                    var pos = t;
                    Thread thread = new Thread(() => DetectGender_Country_ByPLAUTO());
                    thread.Name = "detectgender_country_" + pos.ToString();
                    thread.SetApartmentState(ApartmentState.STA);
                    threads.Add(thread);
                }
            }

            foreach (var t in threads)
            {
                t.Start();
            }

            if (Program.form.is_Detect_run_proxy_server)
            {
                //PLAutoHelper.ProxyHelper.TMProxyServer_Ticker(new PLAutoHelper.ProxyHelper.ProxyDB(API.DBHelpers.db_connection, "tmproxy_tb"));
                throw new System.ArgumentException("not implement proxy server");
            }

            InitTimer_ManageThread();
        }

        //private void DetectAIO()
        //{
        //    string proxy = "";

        //    while(true)
        //    {
        //        while (true)
        //        {
        //            proxy = Tinsoft_GetProxy(proxy);

        //            if (proxy != null)
        //                break;
        //            Thread.Sleep(TimeSpan.FromMinutes(1));
        //        }


        //        if(ThreadHelper.DetectLanguage_GetTask(false)!=null && Program.form.is_DetectLanguage)
        //        {
        //            DetectLanguage();
        //            continue;
        //        }

        //        if (ThreadHelper.DetectGender_GetTask(false) != null && Program.form.is_DetectGender)
        //        {
        //            //luon check gender truoc neu co nhu cau biet gioi tinh vi gender cost 1 unit
        //            DetectGender(proxy);
        //            continue;
        //        }

        //        if (ThreadHelper.DetectCountry_GetTask(false) != null && Program.form.is_DetectCountry)
        //        {
        //            //cost 10 units
        //            DetectCountry(proxy);
        //            continue;
        //        }

        //        Thread.Sleep(TimeSpan.FromMinutes(5));
        //    }
        //}

        /// <summary>
        /// check if has profile image and optimize Fullname
        /// </summary>
        private void DetectName_Image()
        {

            if (!Program.form.is_DetectNameImage)
                return;
            while (true)
            {
                if (Program.form.isstop)
                {
                    Program.form.WriteLogToForm_DetectPersonality("DetectName_Image: stopped");
                    break;
                }

                //get profile to update
                List<dynamic> profiles = ThreadHelper.DetectName_Image_GetTask();

                if (profiles.Count == 0)
                {
                    Thread.Sleep(TimeSpan.FromMinutes(10));
                    continue;
                }
                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();


                    foreach (var p in profiles)
                    {
                        PLAutoHelper.SQLHelper.Execute(() =>
                        {
                            using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                            {

                                var optimize_name = PLAutoHelper.DetectPersonality.DetectNameFromIG_Fullname(p.FullName);

                                string hasprofilepic = "no";

                                if (((string)p.ProfilePicture).Contains("instagram.fsgn5"))
                                    hasprofilepic = "yes";

                                //update Op_FullName, HasProfilePic, status=''
                                var command_query = @"update scrapeusers set [Op_FullName]=@optimize_name, [HasProfilePic]=@hasprofilepic, [status]='' where [UserName]=@UserName" +
                                @" insert into actionlog ([log_name],[log_account],[value]) values ('DetectName_Image',@UserName,'')";

                                using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                {
                                    command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = p.UserName;
                                    command.Parameters.Add("@optimize_name", System.Data.SqlDbType.NVarChar).Value = optimize_name;
                                    command.Parameters.Add("@hasprofilepic", System.Data.SqlDbType.VarChar).Value = hasprofilepic;
                                    command.ExecuteNonQuery();
                                }


                                ////add log
                                ////add log to action log
                                //command_query = "insert into actionlog ([log_name],[log_account],[value]) values ('DetectName_Image','" + p.UserName + "','')";

                                //using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                //{
                                //    command.ExecuteNonQuery();
                                //}



                                sqlTransaction.Commit();

                            }
                        }/*, "scrapeusers"*/);
                    }



                    Program.form.WriteLogToForm_DetectPersonality("DetectName_Image: finished " + profiles.Count + " profiles");
                }

            }
        }


        private void DetectLanguage()
        {
            while (true)
            {
                var profile = ThreadHelper.DetectLanguage_GetTask();

                if (profile == null)
                    break;

                var json = PLAutoHelper.DetectPersonality.DetectLanguageAPI_DetectText(profile.bio);

                var json_dynamic = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(json);

                string lang = json_dynamic.data.detections[0].language;

                //update language to dbs
                ThreadHelper.DetectAIO_UpdateProperty(profile.pk, "detect_language", lang);


                //unclaimed status
                API.DBHelpers.UpdateScrapeUserData(long.Parse(profile.pk.ToString()), "status", "");
            }
        }

        #region --Detect By PLAUTO library--

        private void DetectGender_Country_ByPLAUTO()
        {
            while (true)
            {
                if (Program.form.isstop)
                {
                    Program.form.WriteLogToForm_DetectPersonality("DetectGender_Country: stopped");
                    break;
                }

                while (true)
                {

                    if (Program.form.isstop)
                        break;

                    var max_data = 100;

                    var profiles = new List<dynamic>();

                    try
                    {
                        profiles = ThreadHelper.DetectGenderCountry_PLAuto_GetTask_Multi(max_data);
                    }
                    catch (Exception ex)
                    {
                        ThreadHelper.HandleException(ex.Message, ex.StackTrace);
                        Program.form.WriteLogToForm_DetectPersonality("Detect Gender: " + ex.Message);
                    }


                    if (profiles.Count == 0)
                    {
                        Thread.Sleep(TimeSpan.FromMinutes(1));
                        continue;
                    }

                    //List<dynamic> result_profiles = new List<dynamic>();

                    //check if proxy working

                    //List<Task<dynamic>> task_detects = new List<Task<dynamic>>();

                    foreach (var p in profiles)
                    {

                        //var task = Task.Run(() =>
                        //{
                        dynamic result = null;

                        var data = PLAutoHelper.DetectPersonality.DetectGenderCountry_ByFullName_PLAuto(p.name);
                        if (data != null)
                        {
                            dynamic data_dynamic = (Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(data));

                            string UserName = p.UserName;
                            string gender = data_dynamic.gender.ToString();
                            string country = "";

                            for (int i = 0; i < 3 && i < (data_dynamic.country.Count); i++)
                            {
                                if (country != "")
                                    country += ",";

                                country += data_dynamic.country[i].country.ToString();
                            }

                            result = new
                            {
                                UserName = p.UserName,
                                gender = (Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(data)).gender.ToString(),
                                country
                            };
                        }
                        else
                        {
                            result = new
                            {
                                UserName = p.UserName,
                                gender = "",
                                country = ""
                            };
                        }

                        //return result;
                        //});

                        //task_detects.Add(task);


                        //foreach(var t in task_detects)
                        //{
                        //    var result = t.GetAwaiter().GetResult();

                        //    result_profiles.Add(result);
                        //}

                        using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                        {
                            conn.Open();


                            //foreach (var r in result_profiles)
                            //{
                                PLAutoHelper.SQLHelper.Execute(() =>
                                {
                                    using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                                    {

                                    //var pk = LocalHelper.GetDynamicProperty(r, "id").ToString();

                                    //string UserName = profiles.Where(p => p.id == pk).First().UserName;

                                    //string gender = LocalHelper.GetDynamicProperty(r, "gender").ToString();


                                    //update gender to dbs
                                    string UserName = result.UserName.ToString();
                                        string gender = result.gender.ToString();
                                        string country = result.country.ToString();

                                    //update detect_gender
                                    var command_query =
                                        @"update scrapeusers set [detect_gender]='" + gender + "', [detect_location]='" + country + "', [status]='' where [UserName]=@UserName" +
                                        @" insert into actionlog ([log_name],[log_account],[value]) values ('detect_gender',@UserName,'')" +
                                        @" insert into actionlog ([log_name],[log_account],[value]) values ('detect_location',@UserName,'')";

                                        using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                        {
                                            command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = UserName;
                                            command.ExecuteNonQuery();
                                        }


                                        sqlTransaction.Commit();
                                    }
                                }/*, "scrapeusers"*/);
                            //}

                        }

                        //Program.form.WriteLogToForm_DetectPersonality(Thread.CurrentThread.Name + ": DetectGender: Finished Detect Gender for 1 profiles");
                    }

                    Program.form.WriteLogToForm_DetectPersonality(Thread.CurrentThread.Name + ": DetectGender: Finished Detect Gender for " + profiles.Count.ToString() + " profiles");
                }
            }
        }

        #endregion


        #region --Detect By Namsor--

        private void DetectGender_Patch()
        {
            string proxy = null;

            while (true)
            {

                if (Program.form.isstop)
                    break;
                string apiKey = ThreadHelper.Namesor_ApiKey();

                if (apiKey == null)
                    return;

                //proxy = TMProxy_GetProxy(proxy);
                proxy = XProxy_GetProxy();

                while (true)
                {

                    if (Program.form.isstop)
                        break;

                    var max_data = 0;

                    GetMaxData:
                    try
                    {
                        max_data = MaxNamSorPatch(apiKey, 100, 1, proxy);

                        if (max_data == 0)
                            break;
                    }
                    catch (Exception ex)
                    {
                        if (!PLAutoHelper.ProxyHelper.IsProxyWorking(proxy))
                        {
                            //proxy = TMProxy_GetProxy(proxy);
                            proxy = XProxy_GetProxy();
                            goto GetMaxData;
                        }
                        else
                        {
                            ThreadHelper.HandleException(ex.Message, ex.StackTrace);
                            Program.form.WriteLogToForm_DetectPersonality("DetectGender: " + ex.Message);
                            break;
                        }
                    }

                    var profiles = new List<dynamic>();

                    try
                    {
                        profiles = ThreadHelper.DetectGender_GetTask_Multi(max_data);
                    }
                    catch (Exception ex)
                    {
                        ThreadHelper.HandleException(ex.Message, ex.StackTrace);
                        Program.form.WriteLogToForm_DetectPersonality("Detect Gender: " + ex.Message);
                    }


                    if (profiles.Count == 0)
                    {
                        Thread.Sleep(TimeSpan.FromMinutes(5));
                    }

                    List<dynamic> result_profiles = new List<dynamic>();

                    //check if proxy working

                    CheckProxy:
                    if (!PLAutoHelper.ProxyHelper.IsProxyWorking(proxy))
                    {
                        //proxy = TMProxy_GetProxy(proxy);
                        proxy = XProxy_GetProxy();
                    }

                    try
                    {
                        var submit_profiles = new List<dynamic>() { };

                        foreach (var p in profiles)
                        {
                            submit_profiles.Add(new
                            {
                                id = p.id,
                                name = p.name
                            });
                        }
                        result_profiles = PLAutoHelper.DetectPersonality.DetectGenderPatch_Namsor(submit_profiles, apiKey, proxy);
                    }
                    catch (Exception ex)
                    {
                        //check if ip working
                        if (!PLAutoHelper.ProxyHelper.IsProxyWorking(proxy))
                        {
                            ////unclaimed profiles
                            //using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection))
                            //{
                            //    conn.Open();

                            //    using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                            //    {
                            //        foreach (var p in profiles)
                            //        {
                            //            var pk = p.pk.ToString();

                            //            //unclaimed status
                            //            var command_query = @"update scrapeusers set [status]='' where [pk]=" + pk;

                            //            using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                            //            {
                            //                command.ExecuteNonQuery();
                            //            }
                            //        }

                            //        sqlTransaction.Commit();
                            //    }
                            //}
                            //break;
                            goto CheckProxy;
                        }
                        ThreadHelper.HandleException(ex.Message, ex.StackTrace);
                        Program.form.WriteLogToForm_DetectPersonality("DetectGender: " + ex.Message);
                        break;
                    }
                    using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                    {
                        conn.Open();


                        foreach (var r in result_profiles)
                        {
                            PLAutoHelper.SQLHelper.Execute(() =>
                            {
                                using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                                {

                                    var pk = LocalHelper.GetDynamicProperty(r, "id").ToString();

                                    string UserName = profiles.Where(p => p.id == pk).First().UserName;

                                    string gender = LocalHelper.GetDynamicProperty(r, "gender").ToString();


                                    //lock scrapeusers
                                    //var command_query =
                                    //    @"select * from scrapeusers with (tablockx)
                                    //where [status]='Claimed'";

                                    //using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                    //{
                                    //    command.ExecuteNonQuery();
                                    //}

                                    //update gender to dbs

                                    if (gender != "male" && gender != "female")
                                        throw new System.ArgumentException("Not recognize, dev handle this type");

                                    //update detect_gender
                                    var command_query =
                                    @"update scrapeusers set [detect_gender]='" + gender + "', [status]='' where [UserName]=@UserName" +
                                    @" insert into actionlog ([log_name],[log_account],[value]) values ('detect_gender',@UserName,'')";

                                    using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                    {
                                        command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = UserName;
                                        command.ExecuteNonQuery();
                                    }



                                    ////unclaimed status
                                    //command_query = @"update scrapeusers set [status]='' where [pk]=" + pk;

                                    //using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                    //{
                                    //    command.ExecuteNonQuery();
                                    //}


                                    //command_query = "select [UserName] from scrapeusers where [pk]=" + pk;

                                    //string username = null;

                                    //using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                    //{
                                    //    username = command.ExecuteScalar().ToString();
                                    //}

                                    //add log to action log
                                    //command_query = "insert into actionlog ([log_name],[log_account],[value]) values ('detect_gender','" + UserName + "','')";

                                    //using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                    //{
                                    //    command.ExecuteNonQuery();
                                    //}


                                    sqlTransaction.Commit();
                                }
                            }/*, "scrapeusers"*/);
                        }

                    }




                    Program.form.WriteLogToForm_DetectPersonality(Thread.CurrentThread.Name + ": DetectGender: Finished Detect Gender for " + profiles.Count.ToString() + " profiles");
                }

                //Unclaimed API
                ThreadHelper.Namesor_API_UpdateProperty(apiKey, "status", "");
            }


            Program.form.WriteLogToForm_DetectPersonality(Thread.CurrentThread.Name + ": DetectGender: Finished A Round");
        }
    
        private void DetectGender()
        {
            string proxy = "";

            while (true)
            {

                if (Program.form.isstop)
                    break;
                string apiKey = ThreadHelper.Namesor_ApiKey();

                if (apiKey == null)
                    return;

                //proxy = TMProxy_GetProxy(proxy);
                proxy = XProxy_GetProxy();

                while (true)
                {

                    if (Program.form.isstop)
                        break;
                    var profile = ThreadHelper.DetectGender_GetTask();

                    if (profile == null)
                    {
                        Thread.Sleep(TimeSpan.FromMinutes(5));
                    }

                    string json = null;

                    try
                    {
                        json = PLAutoHelper.DetectPersonality.DetectGender_Namsor(profile.fullname, apiKey, proxy);
                    }
                    catch (Exception ex)
                    {
                        //check if ip working
                        if (!PLAutoHelper.ProxyHelper.IsProxyWorking(proxy))
                            break;
                        ThreadHelper.HandleException(ex.Message, ex.StackTrace);
                        Program.form.WriteLogToForm_DetectPersonality("DetectGender: " + ex.Message);
                        continue;
                    }

                    var json_dynamic = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(json);

                    string gender = json_dynamic.likelyGender;

                    //update gender to dbs

                    if (gender != "male" && gender != "female")
                        throw new System.ArgumentException("Not recognize, dev handle this type");

                    ThreadHelper.DetectAIO_UpdateProperty(profile.pk, "detect_gender", gender);

                    //add log to action log
                    ThreadHelper.AddActionLog("detect_gender", profile.UserName, "");

                    //unclaimed status
                    ThreadHelper.DetectAIO_UpdateProperty(profile.pk, "status", "");


                    Program.form.WriteLogToForm_DetectPersonality(Thread.CurrentThread.Name + ": DetectGender: Finished Detect Gender for " + profile.UserName);
                }


                Program.form.WriteLogToForm_DetectPersonality(Thread.CurrentThread.Name + ": DetectGender: Finished A Round");
            }
        }

        private void DetectCountry_Patch()
        {
            string proxy = null;

            while (true)
            {

                if (Program.form.isstop)
                    break;
                string apiKey = ThreadHelper.Namesor_ApiKey();

                if (apiKey == null)
                    return;

                //proxy = TMProxy_GetProxy(proxy);
                proxy = XProxy_GetProxy();

                while (true)
                {

                    if (Program.form.isstop)
                        break;

                    var max_data = 0;

                    GetMaxData:
                    try
                    {
                        max_data = MaxNamSorPatch(apiKey, 50, 10, proxy);

                        if (max_data == 0)
                            break;
                    }
                    catch (Exception ex)
                    {
                        if (!PLAutoHelper.ProxyHelper.IsProxyWorking(proxy))
                        {
                            //proxy = TMProxy_GetProxy(proxy);
                            proxy = XProxy_GetProxy();
                            goto GetMaxData;
                        }
                        else
                        {
                            ThreadHelper.HandleException(ex.Message, ex.StackTrace);
                            Program.form.WriteLogToForm_DetectPersonality("DetectLocation: " + ex.Message);
                            break;
                        }
                    }

                    var profiles = new List<dynamic>();

                    try
                    {
                        profiles = ThreadHelper.DetectCountry_GetTask_Multi(max_data);
                    }
                    catch (Exception ex)
                    {
                        ThreadHelper.HandleException(ex.Message, ex.StackTrace);
                        Program.form.WriteLogToForm_DetectPersonality("DetectLocation: " + ex.Message);
                    }

                    if (profiles.Count == 0)
                    {
                        Thread.Sleep(TimeSpan.FromMinutes(5));
                    }

                    List<dynamic> result_profiles = new List<dynamic>();

                    //check if proxy working

                    CheckProxy:
                    if (!PLAutoHelper.ProxyHelper.IsProxyWorking(proxy))
                    {
                        //proxy = TMProxy_GetProxy(proxy);
                        proxy = XProxy_GetProxy();
                    }

                    try
                    {
                        var submit_profiles = new List<dynamic>() { };

                        foreach(var p in profiles)
                        {
                            submit_profiles.Add(new
                            {
                                id = p.id,
                                name = p.name
                            });
                        }

                        result_profiles = PLAutoHelper.DetectPersonality.DetectCountryPatch_Namsor(submit_profiles, apiKey, proxy);
                    }
                    catch (Exception ex)
                    {
                        //check if ip working
                        if (!PLAutoHelper.ProxyHelper.IsProxyWorking(proxy))
                        {
                            ////unclaimed profiles
                            //using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection))
                            //{
                            //    conn.Open();

                            //    using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                            //    {
                            //        foreach (var p in profiles)
                            //        {
                            //            var pk = p.pk.ToString();

                            //            //unclaimed status
                            //            var command_query = @"update scrapeusers set [status]='' where [pk]=" + pk;

                            //            using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                            //            {
                            //                command.ExecuteNonQuery();
                            //            }
                            //        }

                            //        sqlTransaction.Commit();
                            //    }
                            //}
                            //break;
                            goto CheckProxy;
                        }
                        ThreadHelper.HandleException(ex.Message, ex.StackTrace);
                        Program.form.WriteLogToForm_DetectPersonality("DetectLocation: " + ex.Message);
                        break;
                    }
                    using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                    {
                        conn.Open();

                        //lock actionlog

                        foreach (var r in result_profiles)
                        {
                            PLAutoHelper.SQLHelper.Execute(() =>
                            {
                                using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                                {



                                    //    //lock scrapeusers
                                    //    var command_query =
                                    //        @"select * from scrapeusers with (tablockx)
                                    //where [status]='Claimed'";

                                    //    using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                    //    {
                                    //        command.ExecuteNonQuery();
                                    //    }
                                    var pk = LocalHelper.GetDynamicProperty(r, "id").ToString();

                                    string UserName = profiles.Where(p => p.id == pk).First().UserName;

                                    string country = LocalHelper.GetDynamicProperty(r, "country").ToString();


                                    //update detect_location
                                    var command_query = @"update scrapeusers set [detect_location]='" + country + "', [status]='' where [UserName]=@UserName" +
                                    @" insert into actionlog ([log_name],[log_account],[value]) values ('detect_location',@UserName,'')";

                                    using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                    {
                                        command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar).Value = UserName;
                                        command.ExecuteNonQuery();
                                    }


                                    ////unclaimed status
                                    //command_query = @"update scrapeusers set [status]='' where [pk]=" + pk;

                                    //using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                    //{
                                    //    command.ExecuteNonQuery();
                                    //}

                                    ////var pk = LocalHelper.GetDynamicProperty(r, "id").ToString();

                                    ////get username by pk
                                    //command_query = "select [UserName] from scrapeusers where [pk]=" + pk;

                                    //string username = null;

                                    //using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                    //{
                                    //    username = command.ExecuteScalar().ToString();

                                    //}
                                    //add log to action log
                                    //command_query = "insert into actionlog ([log_name],[log_account],[value]) values ('detect_location','" + UserName + "','')";

                                    //using (var command = new SqlCommand(command_query, conn, sqlTransaction))
                                    //{
                                    //    command.ExecuteNonQuery();
                                    //}


                                    sqlTransaction.Commit();
                                }

                            }/*, "scrapeusers"*/);

                        }


                        Program.form.WriteLogToForm_DetectPersonality(Thread.CurrentThread.Name + ": DetectLocation: Finished Detect Location for " + profiles.Count.ToString() + " profiles");
                    }
                }


                //Unclaimed API
                ThreadHelper.Namesor_API_UpdateProperty(apiKey, "status", "");


                Program.form.WriteLogToForm_DetectPersonality(Thread.CurrentThread.Name + ": DetectLocation: Finished A Round");
            }
        }

        private void DetectCountry()
        {
            string proxy = "";

            while (true)
            {

                if (Program.form.isstop)
                    break;

                string apiKey = ThreadHelper.Namesor_ApiKey();

                if (apiKey == null)
                    return;

                //proxy = TMProxy_GetProxy(proxy);
                proxy = XProxy_GetProxy();

                while (true)
                {

                    if (Program.form.isstop)
                        break;

                    var profile = ThreadHelper.DetectCountry_GetTask();


                    if (profile == null)
                    {
                        Thread.Sleep(TimeSpan.FromMinutes(5));
                    }

                    string json = null;


                    try
                    {
                        json = PLAutoHelper.DetectPersonality.DetectCountry_Namsor(profile.fullname, apiKey, proxy);
                    }
                    catch (Exception ex)
                    {
                        //check if ip working
                        if (!PLAutoHelper.ProxyHelper.IsProxyWorking(proxy))
                            break;

                        ThreadHelper.HandleException(ex.Message, ex.StackTrace);
                        Program.form.WriteLogToForm_DetectPersonality("DetectCountry: " + ex.Message);
                        continue;
                    }



                    var json_dynamic = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(json);

                    string country = json_dynamic.country;

                    //update gender to dbs
                    ThreadHelper.DetectAIO_UpdateProperty(profile.pk, "detect_location", country);

                    //add log to action log
                    ThreadHelper.AddActionLog("detect_location", profile.UserName, "");

                    //unclaimed status
                    ThreadHelper.DetectAIO_UpdateProperty(profile.pk, "status", "");

                    Program.form.WriteLogToForm_DetectPersonality(Thread.CurrentThread.Name + ": DetectLocation: Finished Detect Gender for " + profile.UserName);
                }

                Program.form.WriteLogToForm_DetectPersonality(Thread.CurrentThread.Name + ": DetectLocation: Finished A Round");
            }
        }

        #endregion

        #region --Namsor Helpers


        /// <summary>
        /// lấy số lương task toi dat cho lan POST nay dua tren apiusage
        /// </summary>
        /// <param name="apikey"></param>
        /// <param name="cost_per_unit"></param>
        /// <returns></returns>
        private int MaxNamSorPatch(string apikey, int expected_units, int cost_per_unit = 10, string proxy=null)
        {
            var current_apiusage = PLAutoHelper.DetectPersonality.Namsor_APIUsage(apikey, proxy);

            var max_data_based_on_apiusage = (5000 - current_apiusage) / cost_per_unit;

            if (max_data_based_on_apiusage <= 0)
            {
                //update apikey state
                using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
                {
                    conn.Open();
                    using (var command = new SqlCommand("update namsor_api set [state]='LIMIT' where [apiKey]='" + apikey + "'", conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }

                return 0;
            }

            var max_patch = expected_units;

            if (expected_units > max_data_based_on_apiusage)
                max_patch = max_data_based_on_apiusage;

            return max_patch;
        }

        #endregion

        #region --simple tinsoft proxy helpers--


        /// <summary>
        /// 
        /// </summary>
        /// <param name="pre_proxy">check if current proxy is same with pre_proxy, if yes =>wait</param>
        private string Tinsoft_GetProxy(string pre_proxy)
        {
            string current_proxy;

            while (true)
            {
                current_proxy = PLAutoHelper.ProxyHelper.TinSoft_GetCurrentProxyByApiKey(proxy_apikey);

                if (current_proxy == pre_proxy || current_proxy == null)
                    Thread.Sleep(TimeSpan.FromSeconds(20));
                else
                {
                    pre_proxy = current_proxy;
                    //check if proxy is working
                    if (PLAutoHelper.ProxyHelper.IsProxyWorking(current_proxy))
                        break;
                }
            }

            return current_proxy;
        }

        #endregion

        #region --simple tmproxy proxy helpers--

        private string TMProxy_GetProxy(string pre_proxy)
        {
            string current_proxy;

            while (true)
            {
                current_proxy = PLAutoHelper.ProxyHelper.TMProxy_CurrentProxy(proxy_apikey);

                if (current_proxy == pre_proxy || current_proxy == null)
                    Thread.Sleep(TimeSpan.FromSeconds(20));
                else
                {
                    pre_proxy = current_proxy;
                    //check if proxy is working
                    if (PLAutoHelper.ProxyHelper.IsProxyWorking(current_proxy))
                        break;
                }
            }

            return current_proxy;
        }

        #endregion

        #region --simple XProxy helper--

        private string XProxy_GetProxy()
        {
            string proxy = null;

            using (SqlConnection conn = new SqlConnection(API.DBHelpers.db_connection_igtoolkit))
            {
                conn.Open();

                var command_query =
                    @"select top 1 [proxy_str]
                    from proxytb
                    order by NEWID()";

                using(var command=new SqlCommand(command_query,conn))
                {
                    proxy = command.ExecuteScalar().ToString();
                }

            }

            return proxy;
        }

        #endregion
    }
}
